//
//  DateAndTimePickerViewController.m
//  
//
//  Created by LiminLin on 2015/10/10.
//
//

#import "DateAndTimePickerViewController.h"

@interface DateAndTimePickerViewController ()

@end

@implementation DateAndTimePickerViewController

- (IBAction)datePickerViewValueChanged:(UIDatePicker *)sender {
    [self.iDelegate dateDidSelect: sender.date];
}

@end
