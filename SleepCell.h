//
//  SleepCell.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import <UIKit/UIKit.h>

@interface SleepCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelSleepTime;
@property (weak, nonatomic) IBOutlet UILabel *labelWakeTime;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalTime;

@end
