//
//  DateAndTimePickerViewController.h
//  
//
//  Created by LiminLin on 2015/10/10.
//
//

#import <UIKit/UIKit.h>

@protocol IDateAndTimePickerViewControllerController <NSObject>

@required
- (void) dateDidSelect: (NSDate *) date;

@end

@interface DateAndTimePickerViewController : UIViewController

@property (weak, nonatomic) id <IDateAndTimePickerViewControllerController> iDelegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;


@end
