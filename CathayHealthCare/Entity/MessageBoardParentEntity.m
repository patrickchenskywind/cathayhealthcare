//
//  MessageBoardParentEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "MessageBoardParentEntity.h"

@implementation MessageBoardParentEntity

+ (RKObjectMapping *)responseMapping {
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    NSMutableDictionary *mutableDictionary = [[[[MessageBoardParentEntity alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayMsg"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayMsg" mapping: [MessageBoardEntity responseMapping]];
    
    return objectMapping;
}

@end
