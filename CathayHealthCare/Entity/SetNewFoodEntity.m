//
//  SetNewFoodEntity.m
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/9/30.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "SetNewFoodEntity.h"


@implementation SetNewFoodEntity

+ (RKObjectMapping *)responseMapping {
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}




@end
