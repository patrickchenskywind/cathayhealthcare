//
//  PhyConditionParentEntity.h
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/7.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface PhyConditionParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayCondition;

@end
