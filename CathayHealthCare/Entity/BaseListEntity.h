//
//  Entity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <RestKit/RestKit.h>
#import <NSObject-ObjectMap/NSObject+ObjectMap.h>

@interface BaseListEntity : NSObject

+ (RKObjectMapping *)responseMapping;
+ (id)currentEntity;

@end

