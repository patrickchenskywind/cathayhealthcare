//
//  BaseEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseEntity.h"
#import "DefindConfig.h"

@interface BaseEntity ()<UIAlertViewDelegate>

@end

@implementation BaseEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

- (void) setMsg:(NSString *)msg {
    
    _msg = msg;
    
    if ( ! [msg isEqualToString: @"00"]) {
        if ([msg isEqualToString: @"04"]) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey: ACCESSTOKEN];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        NSString *errorMsg = [NSString stringWithFormat: @"MSG %@",_msg];
        [self performSelectorOnMainThread: @selector(showErrorMsgAlert:)
                               withObject: NSLocalizedString(errorMsg, nil)
                            waitUntilDone: NO];

    }
}

- (void) showErrorMsgAlert: (NSString *) msg {
        [[[UIAlertView alloc] initWithTitle: msg  message:nil delegate: nil cancelButtonTitle: @"確定" otherButtonTitles: nil] show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
}

@end
