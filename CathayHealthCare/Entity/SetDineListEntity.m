//
//  SetDineListEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetDineListEntity.h"

@implementation SetDineListEntity

-(NSString *)fid{
    if (!_fid) {
        return @"";
    }
    return _fid;
}

-(NSString *)quan{
    if (!_quan) {
        return @"";
    }
    return _quan;
}

-(NSString *)foodGraph{
    if (!_foodGraph) {
        return @"";
    }
    return _foodGraph;
}


@end
