//
//  PhyWaistlineEntity.m
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "PhyWaistlineEntity.h"

@implementation PhyWaistlineEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

@end
