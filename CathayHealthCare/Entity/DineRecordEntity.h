//
//  DineRecordEntity.h
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/5.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseListEntity.h"

@interface DineRecordEntity : BaseListEntity

@property (strong , nonatomic) NSString *did;
@property (strong , nonatomic) NSString *dineType;
@property (strong , nonatomic) NSString *fid;
@property (strong , nonatomic) NSString *name;
@property (strong , nonatomic) NSString *quan;
@property (strong , nonatomic) NSString *cal;
@property (strong , nonatomic) NSString *nid;
@property (strong , nonatomic) NSString *frequency;
@property (strong , nonatomic) NSString *time;

@end
