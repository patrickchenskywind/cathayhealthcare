//
//  LoginEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LoginEntity.h"
#import <NSObject-ObjectMap/NSObject+ObjectMap.h>


@implementation LoginEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

+ (LoginEntity *) get {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"LoginEntity"];
    NSArray *entities = [context executeFetchRequest:request error:nil];
    
    if (entities.count >0) {
        
        LoginEntity *loginEntity = [[LoginEntity alloc] init];
        NSMutableDictionary * mutableDictionary = [[loginEntity propertyDictionary] mutableCopy];
        [mutableDictionary removeObjectForKey: @"api"];
        [mutableDictionary removeObjectForKey: @"msg"];
        
        [mutableDictionary removeObjectForKey: @"debugDescription"];
        [mutableDictionary removeObjectForKey: @"description"];
        [mutableDictionary removeObjectForKey: @"hash"];
        [mutableDictionary removeObjectForKey: @"superclass"];
        
        [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [loginEntity setValue:[entities[0] valueForKey:key] forKey: key];
        }];
        return loginEntity;
    }
    else {
        return nil;
    }
}


- (void)saveToLocal {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"LoginEntity"];
    NSArray *loginList = [context executeFetchRequest:request error:nil];
    NSManagedObject *loginData = nil;
    if (loginList.count > 0) {
        loginData = loginList[0];
    }
    else {
        loginData = [NSEntityDescription insertNewObjectForEntityForName: @"LoginEntity" inManagedObjectContext: context];
    }
    
    NSMutableDictionary * mutableDictionary = [[[[LoginEntity alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"api"];
    [mutableDictionary removeObjectForKey: @"msg"];
    
    [mutableDictionary removeObjectForKey: @"debugDescription"];
    [mutableDictionary removeObjectForKey: @"description"];
    [mutableDictionary removeObjectForKey: @"hash"];
    [mutableDictionary removeObjectForKey: @"superclass"];
    
    [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([self valueForKey:key] && (![[self valueForKey:key] respondsToSelector:@selector(length)] || [[self valueForKey:key] length] > 0)) {
                [loginData setValue:[self valueForKey:key] forKey:key];
        }
    }];
    [context save: nil];
}

@end
