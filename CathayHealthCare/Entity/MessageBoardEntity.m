//
//  MessageBoardEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "MessageBoardEntity.h"


@implementation MessageBoardEntity

+ (RKObjectMapping *)responseMapping {
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

@end
