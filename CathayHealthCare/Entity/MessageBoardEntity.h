//
//  MessageBoardEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseListEntity.h"

@interface MessageBoardEntity : BaseListEntity

@property (strong, nonatomic) NSString * date;
@property (strong, nonatomic) NSString * message;
@property (strong, nonatomic) NSString * author;
@property (strong, nonatomic) NSString * uid;
@property (strong, nonatomic) NSString * photo;

@end
