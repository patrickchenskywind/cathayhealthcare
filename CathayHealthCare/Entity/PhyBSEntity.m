//
//  PhyBSEntity.m
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "PhyBSEntity.h"

@implementation PhyBSEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

@end
