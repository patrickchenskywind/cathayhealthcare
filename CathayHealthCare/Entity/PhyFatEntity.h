//
//  PhyFatEntity.h
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "BaseListEntity.h"

@interface PhyFatEntity : BaseListEntity

@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *fid;
@property (strong, nonatomic) NSString *fat;

@end
