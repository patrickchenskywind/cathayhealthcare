//
//  DineRecordEntity.m
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/5.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "DineRecordEntity.h"

@implementation DineRecordEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

@end
