//
//  PhyHDLEntity.m
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "PhyHDLEntity.h"

@implementation PhyHDLEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

@end
