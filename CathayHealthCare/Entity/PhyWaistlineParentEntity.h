//
//  PhyWaistlineParentEntity.h
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "BaseEntity.h"
#import "PhyWaistlineEntity.h"

@interface PhyWaistlineParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayWaistline;

@end
