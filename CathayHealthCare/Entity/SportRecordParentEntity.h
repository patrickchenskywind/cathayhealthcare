//
//  SportRecordParentEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseEntity.h"
#import "SportRecordEntity.h"

@interface SportRecordParentEntity : BaseEntity

@property (strong, nonatomic) NSArray *arraySport;

@end
