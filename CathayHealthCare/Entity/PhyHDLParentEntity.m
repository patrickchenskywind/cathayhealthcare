//
//  PhyHDLParentEntity.m
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "PhyHDLParentEntity.h"

@implementation PhyHDLParentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayHdl"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayHdl" mapping: [PhyHDLEntity responseMapping]];
    return objectMapping;
}

@end
