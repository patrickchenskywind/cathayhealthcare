//
//  PhyWaistlineEntity.h
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "BaseListEntity.h"

@interface PhyWaistlineEntity : BaseListEntity

@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *wid;
@property (strong, nonatomic) NSString *waistline;

@end
