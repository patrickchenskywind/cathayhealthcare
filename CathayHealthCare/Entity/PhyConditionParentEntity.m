//
//  PhyConditionParentEntity.m
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/7.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "PhyConditionParentEntity.h"
#import "PhyConditionEntity.h"

@implementation PhyConditionParentEntity
+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayCondition"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayCondition" mapping: [PhyConditionEntity responseMapping]];
    return objectMapping;
}
@end
