//
//  PhyWeightEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/13.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseListEntity.h"

@interface PhyWeightEntity : BaseListEntity

@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *wid;
@property (strong, nonatomic) NSString *weight;

+ (PhyWeightEntity *) seachWithDate: (NSString *) date;
+ (NSArray *) getAll;
+ (PhyWeightEntity *) getFirst;
+ (NSArray *) getLastFive;
+ (void) removeAll;
- (void) saveToLocal;
- (void) remove;

@end
