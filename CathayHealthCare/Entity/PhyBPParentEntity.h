//
//  PhyBPParentEntity.h
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "BaseEntity.h"
#import "PhyBPEntity.h"

@interface PhyBPParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayBp;

@end
