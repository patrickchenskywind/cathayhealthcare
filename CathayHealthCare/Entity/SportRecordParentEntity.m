//
//  SportRecordParentEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SportRecordParentEntity.h"

@implementation SportRecordParentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arraySport"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arraySport" mapping: [SportRecordEntity responseMapping]];
    return objectMapping;
}

@end
