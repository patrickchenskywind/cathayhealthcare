//
//  NourishmentEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseListEntity.h"

@interface NourishmentListEntity : BaseListEntity

@property (strong, nonatomic) NSString * nid;
@property (strong, nonatomic) NSString * providerType;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * brand;
@property (strong, nonatomic) NSString * dose;

- (void) saveToLocal;
+ (void) removeAll;
+ (NSArray *) seachWithString: (NSString *) string;
+ (NSArray *) seachWithNidArray: (NSArray *) nidArray;

@end
