//
//  PhyConditionEntity.h
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/7.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseListEntity.h"

@interface PhyConditionEntity : BaseListEntity

@property (nonatomic,strong) NSString *cid;
@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSString *notePhoto;
@property (nonatomic,strong) NSString *symptom;
@property (nonatomic,strong) NSString *treatment;

+ (PhyConditionEntity *) seachWithDate: (NSString *) date;
+ (NSArray *) getAll;
+ (PhyConditionEntity *) getFirst;
+ (NSArray *) getLastFive;
+ (void) removeAll;
- (void) saveToLocal;
- (void) remove;

@end
