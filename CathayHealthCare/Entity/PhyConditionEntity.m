//
//  PhyConditionEntity.m
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/7.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "PhyConditionEntity.h"

@implementation PhyConditionEntity
+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

+ (PhyConditionEntity *) seachWithDate: (NSString *) date {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyConditionEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"date == %@", date];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        
        PhyConditionEntity *phyConditionEntity = [[PhyConditionEntity alloc] init];
        NSMutableDictionary * mutableDictionary = [[PhyConditionEntity propertyDictionary] mutableCopy];
        [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [phyConditionEntity setValue:[entities[0] valueForKey:key] forKey: key];
        }];
        return phyConditionEntity;
    }
    else {
        return nil;
    }
}

+ (NSArray *) getAll {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyConditionEntity"];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    if (entities.count >0) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary * mutableDictionary = [[PhyConditionEntity propertyDictionary] mutableCopy];
        [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            PhyConditionEntity *phyConditionEntity = [[PhyConditionEntity alloc] init];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [phyConditionEntity setValue: [entities[idx] valueForKey:key] forKey: key];
            }];
            [array insertObject: phyConditionEntity atIndex: idx];
        }];
        
        return [array copy];
    }
    else {
        return nil;
    }
}

+ (PhyConditionEntity *) getFirst {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyConditionEntity"];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        
        PhyConditionEntity *phyConditionEntity = [[PhyConditionEntity alloc] init];
        NSMutableDictionary * mutableDictionary = [[PhyConditionEntity propertyDictionary] mutableCopy];
        [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [phyConditionEntity setValue:[entities[0] valueForKey:key] forKey: key];
        }];
        return phyConditionEntity;
    }
    else {
        return nil;
    }
}

+ (NSArray *) getLastFive {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyConditionEntity"];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        entities  = [entities sortedArrayUsingDescriptors: [NSArray arrayWithObject: [NSSortDescriptor sortDescriptorWithKey: @"date" ascending: NO]]];
        NSInteger tempInteger = 0;
        NSMutableArray *array = [NSMutableArray array];
        if (entities.count > 5 ) {
            tempInteger = entities.count - 6;
        }
        
        for (int i = (int)tempInteger; i < entities.count; i++) {
            PhyConditionEntity *phyConditionEntity = [[PhyConditionEntity alloc] init];
            NSMutableDictionary * mutableDictionary = [[PhyConditionEntity propertyDictionary] mutableCopy];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [phyConditionEntity setValue: [entities[i] valueForKey:key] forKey: key];
            }];
            [array addObject: phyConditionEntity];
        }
        
        return [array copy];
    }
    else {
        return nil;
    }
}


- (void)saveToLocal {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyConditionEntity"];
    NSArray *phyConditionEntityList = [context executeFetchRequest: request error: nil];
    NSManagedObject *phyConditionEntity = nil;
    if (phyConditionEntityList.count > 0) {
        BOOL hasSame = NO;
        for (int i = 0;  i < phyConditionEntityList.count; i++) {
            if ([self.date isEqualToString: ((PhyConditionEntity *)phyConditionEntityList[i]).date]) {
                phyConditionEntity = phyConditionEntityList[i];
                hasSame = YES;
            }
        }
        if ( ! hasSame) {
            phyConditionEntity = [NSEntityDescription insertNewObjectForEntityForName: @"PhyConditionEntity" inManagedObjectContext: context];
        }
    }
    else {
        phyConditionEntity = [NSEntityDescription insertNewObjectForEntityForName: @"PhyConditionEntity" inManagedObjectContext: context];
    }
    
    NSMutableDictionary * mutableDictionary = [[[[PhyConditionEntity alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([self valueForKey: key] && (![[self valueForKey: key] respondsToSelector: @selector(length)] || [[self valueForKey: key] length] > 0)) {
            [phyConditionEntity setValue: [self valueForKey: key] forKey: key];
        }
    }];
    [context save: nil];
}

- (void)remove {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyConditionEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"date == %@", self.date];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}

+ (void)removeAll {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSArray *entities = [context executeFetchRequest: [[NSFetchRequest alloc] initWithEntityName: @"PhyConditionEntity"] error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}

@end
