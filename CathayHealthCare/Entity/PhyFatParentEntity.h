//
//  SetPhyFatParentEntity.h
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "BaseEntity.h"
#import "PhyFatEntity.h"

@interface PhyFatParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayFat;

@end
