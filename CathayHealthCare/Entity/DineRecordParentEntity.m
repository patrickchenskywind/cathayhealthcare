//
//  DineRecordParentEntity.m
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/5.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "DineRecordParentEntity.h"

@implementation DineRecordParentEntity

+ (RKObjectMapping *)responseMapping {
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[DineRecordParentEntity alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayDine"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayDine" mapping: [DineRecordEntity responseMapping]];
    
    return objectMapping;
}


@end
