//
//  PhyBSParentEntity.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "BaseEntity.h"
#import "PhyBSEntity.h"

@interface PhyBSParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayBs;

@end
