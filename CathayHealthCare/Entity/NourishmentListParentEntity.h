//
//  NourishmentParentEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseEntity.h"
#import "NourishmentListEntity.h"

@interface NourishmentListParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayNourishmentList;

@end
