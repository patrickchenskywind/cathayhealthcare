//
//  PhySleepParentEntity.m
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "PhySleepParentEntity.h"

@implementation PhySleepParentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arraySleep"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arraySleep" mapping: [PhySleepEntity responseMapping]];
    return objectMapping;
}

@end
