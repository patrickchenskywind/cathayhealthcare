//
//  DineDetailEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseEntity.h"

@interface DineDetailEntity : BaseEntity

@property (nonatomic, strong) NSString * name; //食物名稱
@property (nonatomic, strong) NSString * brand; //食物品牌
@property (nonatomic, strong) NSString * cal; //熱量
@property (nonatomic, strong) NSString * unit; //分量+計量單位 ex.200克
@property (nonatomic, strong) NSString * starchSixType; //全穀根莖
@property (nonatomic, strong) NSString * proteinSixType; //肉蛋豆魚
@property (nonatomic, strong) NSString * vegetableSixType; //蔬菜
@property (nonatomic, strong) NSString * fruitSixType; //水果
@property (nonatomic, strong) NSString * dairySixType; //低脂乳品
@property (nonatomic, strong) NSString * oilsSixType; //油脂及堅果
@property (nonatomic, strong) NSString * protein; //蛋白質
@property (nonatomic, strong) NSString * fat; //脂肪
@property (nonatomic, strong) NSString * saturation; //飽和
@property (nonatomic, strong) NSString * polyunsaturated; //多元不飽和
@property (nonatomic, strong) NSString * monounsaturated; //單元不飽和
@property (nonatomic, strong) NSString * trans; //反式
@property (nonatomic, strong) NSString * cholesterol; //膽固醇
@property (nonatomic, strong) NSString * carbohydrates; //碳水化合物
@property (nonatomic, strong) NSString * sugar; //糖
@property (nonatomic, strong) NSString * fiber; //纖維
@property (nonatomic, strong) NSString * sodium; //鈉
@property (nonatomic, strong) NSString * potassium; //鉀
@property (nonatomic, strong) NSString * calcium; //鈣
@property (nonatomic, strong) NSString * iron; //鐵

@end
