//
//  PhyBPEntity.h
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "BaseListEntity.h"

@interface PhyBPEntity : BaseListEntity

@property (nonatomic, strong) NSString *date; //日期
@property (nonatomic, strong) NSString *bpid; //唯一值
@property (nonatomic, strong) NSString *sbp; //收縮壓
@property (nonatomic, strong) NSString *dbp; //舒張壓
@property (nonatomic, strong) NSString *heartBeat; //安靜心跳

@end
