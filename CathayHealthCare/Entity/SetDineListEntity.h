//
//  SetDineListEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseListEntity.h"

@interface SetDineListEntity : BaseListEntity

@property (strong, nonatomic) NSString *fid;
@property (strong, nonatomic) NSString *quan;
@property (strong, nonatomic) NSString *foodGraph;

@end
