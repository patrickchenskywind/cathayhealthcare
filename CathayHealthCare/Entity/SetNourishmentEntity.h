//
//  SetNourishmentEntity.h
//  CathayHealthcare
//
//  Created by Patrick on 2015/10/7.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"

@interface SetNourishmentEntity : BaseEntity

@property (nonatomic,strong) NSString *nid;

@end
