//
//  PhyTGParentEntity.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "BaseEntity.h"
#import "PhyTGEntity.h"

@interface PhyTGParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayTg;

@end
