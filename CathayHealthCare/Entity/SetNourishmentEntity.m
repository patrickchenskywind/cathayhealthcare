//
//  SetNourishmentEntity.m
//  CathayHealthcare
//
//  Created by Patrick on 2015/10/7.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "SetNourishmentEntity.h"

@implementation SetNourishmentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

@end
