//
//  DineRecordParentEntity.h
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/5.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEntity.h"
#import "DineRecordEntity.h"

@interface DineRecordParentEntity : BaseEntity

@property (nonatomic,strong) NSArray *arrayDine;

@end
