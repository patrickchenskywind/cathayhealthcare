//
//  FoodParentEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "FoodListParentEntity.h"

@implementation FoodListParentEntity

+ (RKObjectMapping *)responseMapping {
    
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[FoodListParentEntity alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayFood"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayFood" mapping: [FoodListEntity responseMapping]];
    
    return objectMapping;
}

@end
