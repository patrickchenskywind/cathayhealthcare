//
//  MessageBoardParentEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseEntity.h"
#import "MessageBoardEntity.h"

@interface MessageBoardParentEntity : BaseEntity

@property (strong, nonatomic) NSArray *arrayMsg;

@end
