//
//  LoginEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseEntity.h"

@interface LoginEntity : BaseEntity

@property (strong, nonatomic) NSString * accessToken;
@property (strong, nonatomic) NSString * uid;
@property (strong, nonatomic) NSString * premission;
@property (strong, nonatomic) NSString * workType;
@property (strong, nonatomic) NSString * gender;
@property (strong, nonatomic) NSString * birthday;
@property (strong, nonatomic) NSString * height;
@property (strong, nonatomic) NSString * weight;
@property (strong, nonatomic) NSString * weightGoal;
@property (strong, nonatomic) NSString * weightTarget;
@property (strong, nonatomic) NSString * heartBeat;

+ (LoginEntity *) get;
- (void) saveToLocal;

@end
