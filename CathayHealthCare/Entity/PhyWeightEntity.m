//
//  PhyWeightEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/13.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "PhyWeightEntity.h"

@implementation PhyWeightEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}


+ (PhyWeightEntity *) seachWithDate: (NSString *) date {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyWeightEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"date == %@", date];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        
        PhyWeightEntity *phyWeightEntity = [[PhyWeightEntity alloc] init];
        NSMutableDictionary * mutableDictionary = [[PhyWeightEntity propertyDictionary] mutableCopy];
        [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [phyWeightEntity setValue:[entities[0] valueForKey:key] forKey: key];
        }];
        return phyWeightEntity;
    }
    else {
        return nil;
    }
}

+ (NSArray *) getAll {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyWeightEntity"];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    if (entities.count >0) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary * mutableDictionary = [[PhyWeightEntity propertyDictionary] mutableCopy];
        [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            PhyWeightEntity *phyWeightEntity = [[PhyWeightEntity alloc] init];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [phyWeightEntity setValue: [entities[idx] valueForKey:key] forKey: key];
            }];
            [array insertObject: phyWeightEntity atIndex: idx];
        }];

        return [array copy];
    }
    else {
        return nil;
    }
}

+ (PhyWeightEntity *) getFirst {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyWeightEntity"];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        
        PhyWeightEntity *phyWeightEntity = [[PhyWeightEntity alloc] init];
        NSMutableDictionary * mutableDictionary = [[PhyWeightEntity propertyDictionary] mutableCopy];
        [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [phyWeightEntity setValue:[entities[0] valueForKey:key] forKey: key];
        }];
        return phyWeightEntity;
    }
    else {
        return nil;
    }
}

+ (NSArray *) getLastFive {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyWeightEntity"];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        entities  = [entities sortedArrayUsingDescriptors: [NSArray arrayWithObject: [NSSortDescriptor sortDescriptorWithKey: @"date" ascending: NO]]];
        NSInteger tempInteger = 0;
        NSMutableArray *array = [NSMutableArray array];
        if (entities.count > 5 ) {
            tempInteger = entities.count - 6;
        }
        
        for (int i = tempInteger; i < entities.count; i++) {
            PhyWeightEntity *phyWeightEntity = [[PhyWeightEntity alloc] init];
            NSMutableDictionary * mutableDictionary = [[PhyWeightEntity propertyDictionary] mutableCopy];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [phyWeightEntity setValue: [entities[i] valueForKey:key] forKey: key];
            }];
            [array addObject: phyWeightEntity];
        }

        return [array copy];
    }
    else {
        return nil;
    }
}


- (void)saveToLocal {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyWeightEntity"];
    NSArray *phyWeightEntityList = [context executeFetchRequest: request error: nil];
    NSManagedObject *phyWeightEntity = nil;
    if (phyWeightEntityList.count > 0) {
        BOOL hasSame = NO;
        for (int i = 0;  i < phyWeightEntityList.count; i++) {
            if ([self.date isEqualToString: ((PhyWeightEntity *)phyWeightEntityList[i]).date]) {
                phyWeightEntity = phyWeightEntityList[i];
                hasSame = YES;
            }
        }
        if ( ! hasSame) {
            phyWeightEntity = [NSEntityDescription insertNewObjectForEntityForName: @"PhyWeightEntity" inManagedObjectContext: context];
        }
    }
    else {
        phyWeightEntity = [NSEntityDescription insertNewObjectForEntityForName: @"PhyWeightEntity" inManagedObjectContext: context];
    }
    
    NSMutableDictionary * mutableDictionary = [[[[PhyWeightEntity alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([self valueForKey: key] && (![[self valueForKey: key] respondsToSelector: @selector(length)] || [[self valueForKey: key] length] > 0)) {
            [phyWeightEntity setValue: [self valueForKey: key] forKey: key];
        }
    }];
    [context save: nil];
}

- (void)remove {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyWeightEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"date == %@", self.date];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}

+ (void)removeAll {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSArray *entities = [context executeFetchRequest: [[NSFetchRequest alloc] initWithEntityName: @"PhyWeightEntity"] error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}

@end
