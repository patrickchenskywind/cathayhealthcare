//
//  FoodListEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseListEntity.h"

@interface FoodListEntity : BaseListEntity

@property (strong, nonatomic) NSString * fid;
@property (strong, nonatomic) NSString * providerType;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * brand;
@property (strong, nonatomic) NSString * unit;
@property (strong, nonatomic) NSString * cal;

- (void) saveToLocal;
+ (void) removeAll;
+ (NSArray *) seachWithString: (NSString *) string;
+ (NSArray *) seachWithFidArray: (NSArray *) fidArray;
+ (FoodListEntity *) seachWithFid: (NSString *) fid;

@end
