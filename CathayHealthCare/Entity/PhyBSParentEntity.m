//
//  PhyBSParentEntity.m
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "PhyBSParentEntity.h"

@implementation PhyBSParentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayBs"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayBs" mapping: [PhyBSEntity responseMapping]];
    return objectMapping;
}

@end
