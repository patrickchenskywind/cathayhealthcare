//
//  PhyWeightParentEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseEntity.h"
#import "PhyWeightEntity.h"

@interface PhyWeightParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayWeight;

@end
