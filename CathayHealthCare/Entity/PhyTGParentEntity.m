//
//  PhyTGParentEntity.m
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "PhyTGParentEntity.h"

@implementation PhyTGParentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayTg"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayTg" mapping: [PhyTGEntity responseMapping]];
    return objectMapping;
}

@end
