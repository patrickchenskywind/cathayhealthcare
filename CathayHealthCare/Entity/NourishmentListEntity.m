//
//  NourishmentEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "NourishmentListEntity.h"

@implementation NourishmentListEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

- (void)saveToLocal {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSManagedObject *nourishmentdData = nil;
    nourishmentdData = [NSEntityDescription insertNewObjectForEntityForName: @"NourishmentListEntity" inManagedObjectContext: context];
    NSMutableDictionary * mutableDictionary = [[[[NourishmentListEntity alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([self valueForKey: key] && (![[self valueForKey: key] respondsToSelector: @selector(length)] || [[self valueForKey: key] length] > 0)) {
            [nourishmentdData setValue:[self valueForKey: key] forKey: key];
        }
    }];
    [context save: nil];
}

+ (void)removeAll {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSArray *entities = [context executeFetchRequest: [[NSFetchRequest alloc] initWithEntityName: @"NourishmentListEntity"] error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}

+ (NSArray *) seachWithString: (NSString *) string {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"NourishmentListEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"name CONTAINS[cd] %@", string];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    if (entities.count >0) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary * mutableDictionary = [[NourishmentListEntity propertyDictionary] mutableCopy];
        [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NourishmentListEntity *nourishmentListEntity = [[NourishmentListEntity alloc] init];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [nourishmentListEntity setValue: [entities[idx] valueForKey:key] forKey: key];
            }];
            [array addObject: nourishmentListEntity];
        }];
        return [array copy];
    }
    else {
        return nil;
    }
}

+ (NSArray *) seachWithNidArray: (NSArray *) nidArray {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"NourishmentListEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(fid IN %@)" argumentArray: nidArray];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    if (entities.count >0) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary * mutableDictionary = [[NourishmentListEntity propertyDictionary] mutableCopy];
        [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NourishmentListEntity *nourishmentListEntity = [[NourishmentListEntity alloc] init];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [nourishmentListEntity setValue: [entities[idx] valueForKey:key] forKey: key];
            }];
            [array addObject: nourishmentListEntity];
        }];
        return [array copy];
    }
    else {
        return nil;
    }
}

@end
