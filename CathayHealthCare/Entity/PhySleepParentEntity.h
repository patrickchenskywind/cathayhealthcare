//
//  PhySleepParentEntity.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "BaseEntity.h"
#import "PhySleepEntity.h"

@interface PhySleepParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arraySleep;

@end
