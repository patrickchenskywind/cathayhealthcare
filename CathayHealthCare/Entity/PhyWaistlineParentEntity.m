//
//  PhyWaistlineParentEntity.m
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "PhyWaistlineParentEntity.h"

@implementation PhyWaistlineParentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayWaistline"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayWaistline" mapping: [PhyWaistlineEntity responseMapping]];
    return objectMapping;
}

@end
