//
//  FoodListEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "FoodListEntity.h"

@implementation FoodListEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

- (void)saveToLocal {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSManagedObject *foodData = nil;
    foodData = [NSEntityDescription insertNewObjectForEntityForName: @"FoodListEntity" inManagedObjectContext: context];
    NSMutableDictionary * mutableDictionary = [[[[FoodListEntity alloc] init] propertyDictionary] mutableCopy];
    
    [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([self valueForKey: key] &&
            (![[self valueForKey:key] respondsToSelector:@selector(length)] || [[self valueForKey:key] length] > 0)) {
            [foodData setValue:[self valueForKey:key] forKey:key];
        }
    }];
    [context save:nil];
}

+ (void)removeAll {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSArray *entities = [context executeFetchRequest: [[NSFetchRequest alloc] initWithEntityName: @"FoodListEntity"] error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}

+ (NSArray *) seachWithString: (NSString *) string {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"FoodListEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"name CONTAINS[cd] %@", string];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    if (entities.count >0) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary * mutableDictionary = [[FoodListEntity propertyDictionary] mutableCopy];
        [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            FoodListEntity *foodListEntity = [[FoodListEntity alloc] init];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [foodListEntity setValue: [entities[idx] valueForKey:key] forKey: key];
            }];
            [array addObject: foodListEntity];
        }];
        return [array copy];
    }
    else {
        return nil;
    }
}

+ (FoodListEntity *) seachWithFid: (NSString *) fid {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"FoodListEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"fid == %@", fid];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    if (entities.count >0) {
        
        FoodListEntity *foodListEntity = [[FoodListEntity alloc] init];
        NSMutableDictionary * mutableDictionary = [[foodListEntity propertyDictionary] mutableCopy];
        [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [foodListEntity setValue:[entities[0] valueForKey:key] forKey: key];
        }];
        return foodListEntity;
    }
    else {
        return nil;
    }
}

+ (NSArray *) seachWithFidArray: (NSArray *) fidArray {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"FoodListEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(fid IN %@)", fidArray];
    [request setPredicate: predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    if (entities.count > 0) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary * mutableDictionary = [[FoodListEntity propertyDictionary] mutableCopy];
        [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            FoodListEntity *foodListEntity = [[FoodListEntity alloc] init];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [foodListEntity setValue: [entities[idx] valueForKey:key] forKey: key];
            }];
            [array addObject: foodListEntity];
        }];
        return [array copy];
    }
    else {
        return nil;
    }
}

@end
