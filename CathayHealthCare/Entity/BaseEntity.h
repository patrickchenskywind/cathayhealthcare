//
//  BaseEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseListEntity.h"

@interface BaseEntity : BaseListEntity

@property(nonatomic, strong) NSString *api;
@property(nonatomic, strong) NSString *msg;

@end
