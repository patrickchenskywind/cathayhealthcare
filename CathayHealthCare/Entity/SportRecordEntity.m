//
//  SportRecordEntity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SportRecordEntity.h"
#import "Toos.h"

@implementation SportRecordEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

+ (SportRecordEntity *) seachWithDate: (NSString *) date {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"SportRecordEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"date == %@", date];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        SportRecordEntity *sportRecordEntity = [[SportRecordEntity alloc] init];
        NSMutableDictionary * mutableDictionary = [[SportRecordEntity propertyDictionary] mutableCopy];
        [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [SportRecordEntity setValue: [entities[0] valueForKey: key] forKey: key];
        }];
        return sportRecordEntity;
    }
    else {
        return nil;
    }
}

+ (SportRecordEntity *) seachWithSid: (NSString *) sid {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"SportRecordEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"sid == %@", sid];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        SportRecordEntity *sportRecordEntity = [[SportRecordEntity alloc] init];
        NSMutableDictionary * mutableDictionary = [[SportRecordEntity propertyDictionary] mutableCopy];
        [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [sportRecordEntity setValue:[entities[0] valueForKey:key] forKey: key];
        }];
        return sportRecordEntity;
    }
    else {
        return nil;
    }
}

+ (NSArray *) getAll {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"PhyWeightEntity"];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    
    if (entities.count >0) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary * mutableDictionary = [[SportRecordEntity propertyDictionary] mutableCopy];
        [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            SportRecordEntity *sportRecordEntity = [[SportRecordEntity alloc] init];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [sportRecordEntity setValue: [entities[idx] valueForKey:key] forKey: key];
            }];
            [array insertObject: sportRecordEntity atIndex: idx];
        }];
        
        return [array copy];
    }
    else {
        return nil;
    }
}

+ (NSArray *) getRecordByNSDate: (NSDate*) date {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"SportRecordEntity"];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    if (entities.count  > 0 ) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary * mutableDictionary = [[SportRecordEntity propertyDictionary] mutableCopy];
        [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            SportRecordEntity *sportRecordEntity = [[SportRecordEntity alloc] init];
            [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [sportRecordEntity setValue: [entities[idx] valueForKey:key] forKey: key];
            }];
            
            NSCalendar *cal = [NSCalendar currentCalendar];
            NSDateComponents *components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate: date];
            NSDate *target = [cal dateFromComponents: components];
            components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate: [Toos getNSDateFromFormServerTimeInterval: sportRecordEntity.date]];
            NSDate *otherDate = [cal dateFromComponents: components];
            if([target isEqualToDate: otherDate]){
                [array addObject: sportRecordEntity];
            }
        }];
        return [array copy];
    }
    else {
        return nil;
    }
}

- (void) saveToLocal {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSManagedObject *sportRecordEntity = nil;
    if (self.sid && self.sid.length > 0) {
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"SportRecordEntity"];
        NSArray *sportRecordEntityList = [context executeFetchRequest: request error: nil];
        
        if (sportRecordEntityList.count > 0) {
            BOOL hasSame = NO;
            for (int i = 0;  i < sportRecordEntityList.count; i++) {
                if ([self.sid isEqualToString: ((SportRecordEntity *)sportRecordEntityList[i]).sid]) {
                    sportRecordEntity = sportRecordEntityList[i];
                    hasSame = YES;
                }
            }
            if ( ! hasSame) {
                sportRecordEntity = [NSEntityDescription insertNewObjectForEntityForName: @"SportRecordEntity" inManagedObjectContext: context];
            }
        }
        else {
            sportRecordEntity = [NSEntityDescription insertNewObjectForEntityForName: @"SportRecordEntity" inManagedObjectContext: context];
        }
    }
    else {
        sportRecordEntity = [NSEntityDescription insertNewObjectForEntityForName: @"SportRecordEntity" inManagedObjectContext: context];
    }
    
    NSMutableDictionary * mutableDictionary = [[[[SportRecordEntity alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if ([self valueForKey: key] && (![[self valueForKey: key] respondsToSelector: @selector(length)] || [[self valueForKey: key] length] > 0)) {
            [sportRecordEntity setValue: [self valueForKey: key] forKey: key];
        }
    }];
    [context save: nil];
}

- (void)removeByDate {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"SportRecordEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"date == %@", self.date];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}

- (void)removeBySid {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName: @"SportRecordEntity"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"sid == %@", self.sid];
    [request setPredicate:predicate];
    NSArray *entities = [context executeFetchRequest: request error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}


+ (void)removeAll {
    NSManagedObjectContext *context = [[AppDelegate sharedAppDelegate] managedObjectContext];
    NSArray *entities = [context executeFetchRequest: [[NSFetchRequest alloc] initWithEntityName: @"SportRecordEntity"] error: nil];
    NSError *error = nil;
    __block NSError *weakError = error;
    [entities enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [context deleteObject: obj];
        [context save: &weakError];
    }];
}

@end
