//
//  Entity.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseListEntity.h"

@implementation BaseListEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    return objectMapping;
}

+ (id)currentEntity {
    id _currentEntity = [[self alloc] init];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([self class])];
    NSArray *entities = [[[AppDelegate sharedAppDelegate] managedObjectContext] executeFetchRequest:request error:nil];
    if (entities.count > 0) {
        NSManagedObject *object = entities[0];
        NSEntityDescription *entity = [object entity];
        NSDictionary *attributes = [entity attributesByName];
        [attributes enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
            const char * type = property_getAttributes(class_getProperty([self class], key.UTF8String));
            NSString * typeString = [NSString stringWithUTF8String:type];
            NSArray * attributes = [typeString componentsSeparatedByString:@","];
            NSString * typeAttribute = [attributes objectAtIndex:0];
            NSString * propertyType = [typeAttribute substringFromIndex:1];
            const char * rawPropertyType = [propertyType UTF8String];
            
            if (strcmp(rawPropertyType, @encode(NSArray)) == 0) {
                [_currentEntity setValue:[NSKeyedUnarchiver unarchiveObjectWithData:[object valueForKey:key]] forKey:key];
            }
            else {
                [_currentEntity setValue:[object valueForKey:key] forKey:key];
            }
        }];
        
    }
    else {
        [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:[[AppDelegate sharedAppDelegate] managedObjectContext]];
        [[[AppDelegate sharedAppDelegate] managedObjectContext] save:nil];
    }
    
    return _currentEntity;
}

@end
