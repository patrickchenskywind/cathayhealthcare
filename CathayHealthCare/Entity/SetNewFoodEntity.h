//
//  SetNewFoodEntity.h
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/9/30.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "BaseListEntity.h"

@interface SetNewFoodEntity : BaseListEntity

@property (strong, nonatomic) NSString *fid;
@property (strong, nonatomic) NSString *msg;
@property (strong, nonatomic) NSString *api;




@end
