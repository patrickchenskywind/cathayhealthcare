//
//  PhyHDLParentEntity.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "BaseEntity.h"
#import "PhyHDLEntity.h"

@interface PhyHDLParentEntity : BaseEntity

@property (nonatomic, strong) NSArray *arrayHdl;

@end
