//
//  PhyTGEntity.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "BaseListEntity.h"

@interface PhyTGEntity : BaseListEntity
/*
 讀取:只傳遞accessToken
 新增:tgid為空值、date & tg有值
 編輯:tgid & date & tg有值
 刪除:tgid有值、date & tg為空值
 */

@property (nonatomic, strong) NSString * tgid; //唯一值
@property (nonatomic, strong) NSString * date; //日期
@property (nonatomic, strong) NSString * tg; //tg

@end
