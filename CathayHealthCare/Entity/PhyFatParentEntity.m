//
//  SetPhyFatParentEntity.m
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "PhyFatParentEntity.h"

@implementation PhyFatParentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayFat"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayFat" mapping: [PhyFatEntity responseMapping]];
    return objectMapping;
}

@end
