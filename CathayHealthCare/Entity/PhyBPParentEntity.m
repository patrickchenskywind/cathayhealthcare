//
//  PhyBPParentEntity.m
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "PhyBPParentEntity.h"

@implementation PhyBPParentEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    NSMutableDictionary *mutableDictionary = [[[[self alloc] init] propertyDictionary] mutableCopy];
    [mutableDictionary removeObjectForKey: @"arrayBp"];
    [objectMapping addAttributeMappingsFromDictionary: mutableDictionary];
    [objectMapping addRelationshipMappingWithSourceKeyPath: @"arrayBp" mapping: [PhyBPEntity responseMapping]];
    return objectMapping;
}

@end
