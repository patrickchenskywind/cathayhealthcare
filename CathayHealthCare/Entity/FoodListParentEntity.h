//
//  FoodParentEntity.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseEntity.h"
#import "FoodListEntity.h"

@interface FoodListParentEntity : BaseEntity

@property (strong , nonatomic) NSArray *arrayFood;

@end
