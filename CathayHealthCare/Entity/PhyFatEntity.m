//
//  PhyFatEntity.m
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "PhyFatEntity.h"

@implementation PhyFatEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

@end
