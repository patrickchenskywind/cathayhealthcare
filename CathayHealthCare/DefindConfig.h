//
//  DefindConfig.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#ifndef CathayHealthcare_DefindConfig_h
#define CathayHealthcare_DefindConfig_h

#define SUCCESSCODE @"00"

#define ACCESSTOKEN @"ACCESSTOKEN"
#define DEVICE_TOKEN @"DEVICE_TOKEN"

#define kNSNotificationShowMBProgressHUD @"kNSNotificationShowMBProgressHUD"
#define kNSNotificationCloseMBProgressHUD @"kNSNotificationCloseMBProgressHUD"

#define SCREEN_BOUND [UIScreen mainScreen].bounds

// user default
#define HAS_BEEN_LAUNCH @"HAS_BEEN_LAUNCH"
#define HEALTH_RECORD_BUTTON_SWITCH @"HEALTH_RECORD_BUTTON_SWITCH"
#define HEALTH_RECORD_BUTTON_TITLE  @"HEALTH_RECORD_BUTTON_TITLE"

#define FOODLIST_DOWNLOAD_DATE @"FOODLIST_DOWNLOAD_DATE"
#define NOURISHMENTLIST_DOWNLOAD_DATE @"NOURISHMENTLIST_DOWNLOAD_DATE"

#define FOOD_FAVORITES_LIST @"FOOD_FAVORITES_LIST"
#define NOURISHMENT_FAVORITES_LIST @"NOURISHMENT_FAVORITES_LIST"
#define SUGGEST_CAL @"SUGGEST_CAL"

// HealthListStatus
#define DICTIONARY_HEALTHLISTSTATUS  @"dictionaryHealthListStatus"
#define DICTIONARY_HEALTHLISTSTATUS_KEY_FAT  @"fat"
#define DICTIONARY_HEALTHLISTSTATUS_KEY_WAISTLINE  @"waistLine"
#define DICTIONARY_HEALTHLISTSTATUS_KEY_HEARTBEAT  @"heartBeat"
#define DICTIONARY_HEALTHLISTSTATUS_KEY_BLOODSUGAR  @"bloodSugar"
#define DICTIONARY_HEALTHLISTSTATUS_KEY_TG  @"tg"
#define DICTIONARY_HEALTHLISTSTATUS_KEY_HDL @"hdl"
#define DICTIONARY_HEALTHLISTSTATUS_KEY_SLEEP  @"sleep"
#define DICTIONARY_HEALTHLISTSTATUS_KEY_STATUS  @"status"

#endif
