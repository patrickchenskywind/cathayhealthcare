//
//  ApiPath.h
//  
//
//  Created by LiminLin on 2015/9/1.
//
//

#ifndef _ApiPath_h
#define _ApiPath_h

#define API_NAME_REGISTER @"register"
#define API_NAME_LOGIN @"login"
#define API_NAME_SETBASESETTING @"setBaseSetting"
#define API_NAME_GETBASESETTING @"getBaseSetting"
#define API_NAME_SETBODYMANAGE @"setBodyManage"
#define API_NAME_GETBODYMANAGE @"getBodyManage"
#define API_NAME_SETFOODMANGE @"setFoodManage"
#define API_NAME_GETFOODMANAGE @"getFoodManage"
#define API_NAME_SETSPORTMANAGE @"setSportManage"
#define API_NAME_GETSPORTMANAGE @"getSportManage"
#define API_NAME_SETPHYWEIGHT @"setPhyWeight"
#define API_NAME_SETPHYFAT @"setPhyFat"
#define API_NAME_SETPHYBP @"setPhyBP"
#define API_NAME_SETPHYBS @"setPhyBS"
#define API_NAME_SETPHYSLEEP @"setPhySleep"
#define API_NAME_SETPHYWAISTLINE @"setPhyWaistline"
#define API_NAME_SETPHYCONDITION @"setPhyCondition"
#define API_NAME_SETPHYTG @"setPhyTG"
#define API_NAME_SETPHYHDL @"setPhyHDL"
#define API_NAME_GETSUGGEST @"getSuggest"
#define API_NAME_GETDAYFOOD @"getDayFood"
#define API_NAME_GETDINERECORD @"getDineRecord"
#define API_NAME_GETDINEDETAIL @"getDineDetail"
#define API_NAME_GETFOODLIST @"getFoodList"
#define API_NAME_SETNEWFOOD @"setNewFood"
#define API_NAME_GETNOURISHMENTLIST @"getNourishmentList"
#define API_NAME_SETNORISHMENT @"setNourishment"
#define API_NAME_SETNOURISSHMENTRECORD @"setNourishmentRecord"
#define API_NAME_SETDINE @"setDine"
#define API_NAME_GETSPORTRECORD @"getSportRecord"
#define API_NAME_SETWORKOUT @"setWorkOut"
#define API_NAME_GETHEALDIARY @"getHealDiary"
#define API_NAME_GETRANKLIST @"getRankList"
#define API_NAME_SETMESSAGEBOARD @"setMessageBoard"
#define API_NAME_GETGROUPMESSAGELIST @"getGroupMessageList"
#define API_NAME_SETGROUPMESSAGE @"setGroupMessage"
#define API_NAME_GETHEALCALENDER @"getHealCalendar"
#define API_NAME_SETHELCALENDER @"setHealCalendar"
#define API_NAME_SETFORA @"setFora"
#define API_NAME_SETGARMIN @"setGarmin"
#define API_NAME_GETUNREADMESSAGENUM @"getUnReadMessageNum"
#define API_NAME_FORGOTPASS @"forgotPass"

#endif
