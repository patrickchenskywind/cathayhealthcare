//
//  HttpRequest.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "HttpResponse.h"

@interface HttpRequest : NSObject

@property (retain, nonatomic) NSString *host;
@property (retain, nonatomic) NSString *relativePath;
@property (retain, nonatomic) NSDictionary *parameter;
@property (assign, nonatomic) RKRequestMethod httpMethodMode;
@property (retain, nonatomic) NSString *contentType;

@property (retain, nonatomic) RKMapping *requestMapping;
@property (retain, nonatomic) RKRequestDescriptor *requestDescriptor;

@property (retain, nonatomic) RKMapping *responseMapping;
@property (retain, nonatomic) RKResponseDescriptor *responseDescriptor;


- (void) sendRequest: (void(^)(HttpResponse *response)) responseCallBack;

@end
