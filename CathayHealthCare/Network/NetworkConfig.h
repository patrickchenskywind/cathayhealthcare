//
//  NetworkConfig.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkConfig : NSObject

#ifdef __IPHONE_9_0
//#error iOS9預設無法建立http連線，需使用https by Daniel
#endif
+ (NSString *)getRemoteServerProtocol;
+ (NSString *)getRemoteServerHOST;
+ (NSString *)getRemoteServerPort;
+ (NSString *)getRemoteServerAPIPath;
+ (NSString *)getRemoteServerStoreAPIPath;
+ (NSString *)urlInApi:(NSString *)apiName;

@end
