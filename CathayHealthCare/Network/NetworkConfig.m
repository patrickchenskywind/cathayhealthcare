//
//  NetworkConfig.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "NetworkConfig.h"

@implementation NetworkConfig

+ (NSString *)getRemoteServerProtocol {
    return @"http";
}

+ (NSString *)getRemoteServerHOST {
    return @"211.76.139.84";
}

+ (NSString *)getRemoteServerPort {
    return @"8080";
}

+ (NSString *)getRemoteServerAPIPath {
    return @"Health";
}

+ (NSString *)getRemoteServerStoreAPIPath {
    return @"mobile";
}

+ (NSString *)urlInApi:(NSString *)apiName {
    NSLog(@"%@",[NSString stringWithFormat:@"%@://%@:%@/%@/%@/%@",
                 [self getRemoteServerProtocol],
                 [self getRemoteServerHOST],
                 [self getRemoteServerPort],
                 [self getRemoteServerAPIPath],
                 [self getRemoteServerStoreAPIPath],
                 apiName]);
    
    return [NSString stringWithFormat:@"%@://%@:%@/%@/%@/%@", [self getRemoteServerProtocol], [self getRemoteServerHOST], [self getRemoteServerPort], [self getRemoteServerAPIPath], [self getRemoteServerStoreAPIPath], apiName];
}

@end
