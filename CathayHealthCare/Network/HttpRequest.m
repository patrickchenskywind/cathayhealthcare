//
//  HttpRequest.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HttpRequest.h"
#import "DefindConfig.h"

@implementation HttpRequest

- (NSString *) generateHost {
    return self.host;
}

- (NSString *) generateRelativePath {
    return self.relativePath;
}

- (NSDictionary *) generateParameter {
    return self.parameter;
}

- (RKRequestMethod) generateHttpMethodMode {
    return self.httpMethodMode;
}

- (NSString *) generateContentType {
    return (self.contentType) ? self.contentType : RKMIMETypeJSON;
}

- (RKMapping *) generateRequestMapping {
    return self.requestMapping;
}

- (RKRequestDescriptor *) generateRequestDescriptor {
    return self.requestDescriptor;
}

- (RKMapping *) generateResponseMapping {
    return self.responseMapping;
}

- (RKResponseDescriptor *) generateResponseDescriptor {
    if(self.responseDescriptor) {
        return self.responseDescriptor;
    }
    else {
        RKMapping *responseMapping = [self generateResponseMapping];
        
        if (responseMapping) {
            NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx
            return  [RKResponseDescriptor responseDescriptorWithMapping: responseMapping
                                                                 method: [self generateHttpMethodMode]
                                                            pathPattern: nil
                                                                keyPath: nil
                                                            statusCodes: statusCodes];
        }
        return nil;
    }
}

- (void) sendRequest: (void(^)(HttpResponse *response)) responseCallBack {
    
    NSURL *host = [NSURL URLWithString: [self generateHost]];
    NSString *relativePath = [self generateRelativePath];
    NSDictionary *parameter = [self generateParameter];
    
    RKRequestDescriptor *requestDescriptor = [self generateRequestDescriptor];
    RKResponseDescriptor *responseDescriptor = [self generateResponseDescriptor];
    
    RKObjectManager *manager = [RKObjectManager managerWithBaseURL: host];
    [manager setRequestSerializationMIMEType: [self generateContentType]];
    [RKMIMETypeSerialization registerClass: [RKNSJSONSerialization class] forMIMEType: @"text/html"];
    
    if (requestDescriptor) {
        [manager addRequestDescriptor: requestDescriptor];
    }
    if (responseDescriptor) {
        [manager addResponseDescriptor: responseDescriptor];
    }
    
    void (^successCallBack)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) = ^void(RKObjectRequestOperation *operation, RKMappingResult *mappingResult){
        if ( responseCallBack ) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            HttpResponse *response = [[HttpResponse alloc] init];
            [response setIsSuccessed: ( operation.HTTPRequestOperation.response.statusCode == 200 )];
            [response setStatusCode: operation.HTTPRequestOperation.response.statusCode];
            [response setResponseBody: operation.HTTPRequestOperation.responseString];
            [response setOperation: operation];
            [response setResult: mappingResult];
            [response setErrorDescription: nil];
            
            responseCallBack(response);
            
        }
    };
    
    void (^failCallBack)(RKObjectRequestOperation *operation, NSError *error) = ^void(RKObjectRequestOperation *operation, NSError *error){
        if ( responseCallBack ) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            HttpResponse *response = [[HttpResponse alloc] init];
            [response setIsSuccessed: ( operation.HTTPRequestOperation.response.statusCode == 200 )];
            [response setStatusCode: operation.HTTPRequestOperation.response.statusCode];
            [response setResponseBody: operation.HTTPRequestOperation.responseString];
            [response setOperation: operation];
            [response setResult: nil];
            [response setErrorDescription: error.description];
            responseCallBack(response);
        }
    };
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled: YES];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationShowMBProgressHUD object: nil];
    switch ([self generateHttpMethodMode]) {
        case RKRequestMethodGET:
            [manager getObject: nil path: relativePath parameters: parameter success: successCallBack failure: failCallBack];
            break;
        case RKRequestMethodPOST:
            [manager postObject: nil path: relativePath parameters: parameter success: successCallBack failure: failCallBack];
            break;
        case RKRequestMethodPUT:
            [manager putObject: nil path: relativePath parameters: parameter success: successCallBack failure: failCallBack];
            break;
        case RKRequestMethodDELETE:
            [manager deleteObject: nil path: relativePath parameters: parameter success: successCallBack failure: failCallBack];
            break;
        case RKRequestMethodPATCH:
            [manager patchObject: nil path: relativePath parameters: parameter success: successCallBack failure: failCallBack];
            break;
        case RKRequestMethodHEAD:
        case RKRequestMethodOPTIONS:
        case RKRequestMethodAny:
            break;
    }
}

@end
