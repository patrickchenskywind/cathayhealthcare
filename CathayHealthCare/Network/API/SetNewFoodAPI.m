//
//  SetNewFoodAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetNewFoodAPI.h"

@implementation SetNewFoodAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETNEWFOOD];
}

- (NSDictionary *)generateParameter {
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"]       = self.accessToken;
    parameter[@"name"]              = self.name ;
    parameter[@"brand"]             = self.brand;
    parameter[@"cal"]               = self.cal;
    parameter[@"unit"]              = self.unit;
    parameter[@"foodGraph"]         = self.foodGraph ;
    parameter[@"starchSixType"]     = self.starchSixType;
    parameter[@"proteinSixType"]    = self.proteinSixType;
    parameter[@"vegetableSixType"]  = self.vegetableSixType;
    parameter[@"fruitSixType"]      = self.fruitSixType ;
    parameter[@"dairySixType"]      = self.dairySixType;
    parameter[@"oilsSixType"]       = self.oilsSixType;
    parameter[@"protein"]           = self.protein;
    parameter[@"fat"]               = self.fat ;
    parameter[@"saturation"]        = self.saturation;
    parameter[@"polyunsaturated"]   = self.polyunsaturated;
    parameter[@"monounsaturated"]   = self.monounsaturated;
    parameter[@"trans"]             = self.trans ;
    parameter[@"cholesterol"]       = self.cholesterol;
    parameter[@"carbohydrates"]     = self.carbohydrates;
    parameter[@"sugar"]             = self.sugar;
    parameter[@"fiber"]             = self.fiber;
    parameter[@"sodium"]            = self.sodium;
    parameter[@"potassium"]         = self.potassium;
    parameter[@"calcium"]           = self.calcium ;
    parameter[@"iron"]              = self.iron;

    
    return parameter;
}

-(RKObjectMapping *)responseMapping{
    return [SetNewFoodEntity responseMapping];
    
}

-(NSString *)name{
    if (!_name)
        return @"";
    return _name;
}

-(NSString *)brand{
    if (!_brand)
        return @"";
    return _brand;
}
-(NSString *)cal{
    if (!_cal)
        return @"";
    return _cal;
}

-(NSString *)unit{
    if (!_unit)
        return @"";
    return _unit;
}
-(NSString *)foodGraph{
    if (!_foodGraph)
        return @"";
    return _foodGraph;
}

-(NSString *)starchSixType{
    if (!_starchSixType)
        return @"";
    return _starchSixType;
}
-(NSString *)proteinSixType{
    if (!_proteinSixType)
        return @"";
    return _proteinSixType;
}

-(NSString *)vegetableSixType{
    if (!_vegetableSixType)
        return @"";
    return _vegetableSixType;
}
-(NSString *)fruitSixType{
    if (!_fruitSixType)
        return @"";
    return _fruitSixType;
}

-(NSString *)dairySixType{
    if (!_dairySixType)
        return @"";
    return _dairySixType;
}
-(NSString *)oilsSixType{
    if (!_oilsSixType)
        return @"";
    return _oilsSixType;
}

-(NSString *)protein{
    if (!_protein)
        return @"";
    return _protein;
}
-(NSString *)fat{
    if (!_fat)
        return @"";
    return _fat;
}

-(NSString *)saturation{
    if (!_saturation)
        return @"";
    return _saturation;
}
-(NSString *)polyunsaturated{
    if (!_polyunsaturated)
        return @"";
    return _polyunsaturated;
}

-(NSString *)monounsaturated{
    if (!_monounsaturated)
        return @"";
    return _monounsaturated;
}
-(NSString *)trans{
    if (!_trans)
        return @"";
    return _trans;
}

-(NSString *)cholesterol{
    if (!_cholesterol)
        return @"";
    return _cholesterol;
}

-(NSString *)carbohydrates{
    if (!_carbohydrates)
        return @"";
    return _carbohydrates;
}

-(NSString *)sugar{
    if (!_sugar)
        return @"";
    return _sugar;
}

-(NSString *)fiber{
    if (!_fiber)
        return @"";
    return _fiber;
}
-(NSString *)sodium{
    if (!_sodium)
        return @"";
    return _sodium;
}

-(NSString *)potassium{
    if (!_potassium)
        return @"";
    return _potassium;
}
-(NSString *)calcium{
    if (!_calcium)
        return @"";
    return _calcium;
}

-(NSString *)iron{
    if (!_iron)
        return @"";
    return _iron;
}


@end
