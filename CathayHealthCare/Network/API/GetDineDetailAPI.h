//
//  GetDineDetailAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "DineDetailEntity.h"

@interface GetDineDetailAPI : BaseAPI

//fid或是barcode二擇一
@property (nonatomic, strong) NSString * fid; //食物唯一編號
@property (nonatomic, strong) NSString * barcode; //食物條碼序號


@end
