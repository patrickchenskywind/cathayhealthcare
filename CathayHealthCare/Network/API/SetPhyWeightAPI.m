//
//  SetPhyWeightAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhyWeightAPI.h"

@implementation SetPhyWeightAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYWEIGHT];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"wid"] = self.wid;
    parameter[@"date"] = self.date ;
    parameter[@"weight"] = self.weight;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhyWeightParentEntity responseMapping];
}


- (NSString *) wid {
    if (! _wid)
        return @"";
    return _wid;
}

- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) weight {
    if (! _weight)
        return @"";
    return _weight;
}


@end
