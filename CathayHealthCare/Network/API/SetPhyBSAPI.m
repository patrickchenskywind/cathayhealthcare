//
//  SetPhyBSAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhyBSAPI.h"

@implementation SetPhyBSAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYBS];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"date"] = self.date ;
    parameter[@"bid"] = self.bid;
    parameter[@"beforeDine"] = self.beforeDine;
    parameter[@"aftreDine"] = self.aftreDine;
    parameter[@"HbA1c"] = self.HbA1c;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhyBSParentEntity responseMapping];
}

- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) bid {
    if (! _bid)
        return @"";
    return _bid;
}

- (NSString *) beforeDine {
    if (! _beforeDine)
        return @"";
    return _beforeDine;
}

- (NSString *) aftreDine {
    if (! _aftreDine)
        return @"";
    return _aftreDine;
}

- (NSString *) HbA1c {
    if (! _HbA1c)
        return @"";
    return _HbA1c;
}

@end
