//
//  SetPhyTGAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhyTGAPI.h"

@implementation SetPhyTGAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYTG];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"date"] = self.date ;
    parameter[@"tgid"] = self.tgid;
    parameter[@"tg"] = self.tg;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhyTGParentEntity responseMapping];
}

- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) tgid {
    if (! _tgid)
        return @"";
    return _tgid;
}

- (NSString *) tg {
    if (! _tg)
        return @"";
    return _tg;
}

@end
