//
//  SetPhyFatAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhyFatAPI.h"

@implementation SetPhyFatAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYFAT];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"fid"] = self.fid;
    parameter[@"date"] = self.date ;
    parameter[@"fat"] = self.fat;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhyFatParentEntity responseMapping];
}


- (NSString *) fid {
    if (! _fid)
        return @"";
    return _fid;
}

- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) fat {
    if (! _fat)
        return @"";
    return _fat;
}

@end
