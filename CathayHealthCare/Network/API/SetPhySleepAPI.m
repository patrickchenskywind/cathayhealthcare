
//
//  SetPhySleepAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhySleepAPI.h"

@implementation SetPhySleepAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYSLEEP];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"date"] = self.date ;
    parameter[@"sid"] = self.sid;
    parameter[@"sleepTime"] = self.sleepTime;
    parameter[@"wakeTime"] = self.wakeTime;
    parameter[@"sleepGraph"] = self.sleepGraph;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhySleepParentEntity responseMapping];
}

- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) sid {
    if (! _sid)
        return @"";
    return _sid;
}

- (NSString *) sleepTime {
    if (! _sleepTime)
        return @"";
    return _sleepTime;
}

- (NSString *) wakeTime {
    if (! _wakeTime)
        return @"";
    return _wakeTime;
}

- (NSString *) sleepGraph {
    if (! _sleepGraph)
        return @"";
    return _sleepGraph;
}

@end
