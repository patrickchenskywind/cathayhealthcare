//
//  RegisterAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "BaseEntity.h"

@interface RegisterAPI : BaseAPI

@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * password;
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * birthday;      //生日yyyy/MM/dd轉為千秒
@property (nonatomic, strong) NSString * bloodType;     // bloodType    “0”:A   “1”:AB      “2”:B  “3”:O  “4”:Other
@property (nonatomic, strong) NSString * gender;        //gender        “0”:男   “1”:女
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * tel;           //手機 , 格式為0912345678 or +886912345678
@property (nonatomic, strong) NSString * nationality;   // nationality  “0”:本國  “1”:外國
@property (nonatomic, strong) NSString * id;

@end
