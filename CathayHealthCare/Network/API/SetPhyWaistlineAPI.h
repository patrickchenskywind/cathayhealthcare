//
//  SetPhyWaistlineAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "PhyWaistlineParentEntity.h"

@interface SetPhyWaistlineAPI : BaseAPI

/*
 讀取:只傳遞accessToken
 新增:wid為空值、date & waistline有值
 編輯:wid & date & waistline有值
 刪除:wid有值、date & waistline為空值
 */

@property (nonatomic, strong) NSString * wid; //唯一值
@property (nonatomic, strong) NSString * date; //日期
@property (nonatomic, strong) NSString * waistline; //腰圍

@end
