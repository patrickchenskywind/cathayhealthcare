//
//  SetPhySleepAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "PhySleepParentEntity.h"

@interface SetPhySleepAPI : BaseAPI

/*
 讀取:只傳遞accessToken
 新增:sid為空值、date & sleepTime & wakeTime & sleepGraph有值
 編輯:sid & date & sleepTime & wakeTime & sleepGraph有值
 刪除:sid有值、date & sleepTime & wakeTime & sleepGraph為空值
 */

@property (nonatomic, strong) NSString * date; //日期
@property (nonatomic, strong) NSString * sid; //唯一值
@property (nonatomic, strong) NSString * sleepTime; //就寢時間
@property (nonatomic, strong) NSString * wakeTime; //起床時間
@property (nonatomic, strong) NSString * sleepGraph; //照片轉為base64字串

@end
