//
//  ForgotPassAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "BaseEntity.h"

@interface ForgotPassAPI : BaseAPI

@property (nonatomic, strong) NSString * mail;

@end
