//
//  GetFoodListAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GetFoodListAPI.h"

@implementation GetFoodListAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_GETFOODLIST];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [FoodListParentEntity responseMapping];
}

@end
