//
//  ForgotPassAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ForgotPassAPI.h"

@implementation ForgotPassAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_LOGIN];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"mail"] = self.mail ;
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [BaseEntity responseMapping];
}

- (NSString *) mail{
    if (! _mail)
        return @"";
    return _mail;
}

@end
