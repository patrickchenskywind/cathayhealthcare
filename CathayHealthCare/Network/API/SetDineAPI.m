//
//  SetDineAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetDineAPI.h"

@implementation SetDineAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETDINE];
}

- (RKObjectMapping *)responseMapping {
    return [BaseEntity responseMapping];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    parameter[@"did"] = self.did ;
    parameter[@"date"] = self.date;
    parameter[@"dineType"] = self.dineType;
    
    NSString *dineList = @"[";
    for (int i = 0; i < self.arrayDineList.count ; i++) {
        
        NSDictionary   *dic   = self.arrayDineList[i];
        NSMutableArray *datas = [[NSMutableArray alloc] init];
        
        for (NSString *key in dic) {
            NSString *data = [NSString stringWithFormat:@"\"%@\":\"%@\"",key,[dic objectForKey:key]];
            [datas addObject:data];
        }
        
        dineList = [dineList stringByAppendingFormat:@"{%@}",[datas componentsJoinedByString:@","]];
        
        if (i < self.arrayDineList.count -1) {
            dineList = [dineList stringByAppendingString:@","];
        }
        
    }
    dineList = [dineList stringByAppendingString:@"]"];
    parameter[@"arrayDineList"] = dineList;
    
    return parameter;
}

- (NSString *) did{
    if (! _did)
        return @"";
    return _did;
}

- (NSString *) date {
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) dineType {
    if (! _dineType)
        return @"";
    return _dineType;
}

- (NSArray *) arrayDineList {
    if (_arrayDineList.count > 0)
        return _arrayDineList;
    return nil;
}

@end
