//
//  SetPhyWeightAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "PhyWeightParentEntity.h"

@interface SetPhyWeightAPI : BaseAPI

/*
 /setPhyWeight
 讀取:只傳遞accessToken
 新增:wid為空值、date & weight 有值
 編輯:wid & date & weight 有值
 刪除:wid有值、date & weight 為空值
 */

@property (nonatomic, strong) NSString * wid; //資料編號
@property (nonatomic, strong) NSString * date; //日期
@property (nonatomic, strong) NSString * weight; //體重(同步更新本機&線上的會員體重記錄)

@end
