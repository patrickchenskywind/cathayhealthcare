//
//  SetNourishmentAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetNourishmentAPI.h"
#import "SetNourishmentEntity.h"

@implementation SetNourishmentAPI
- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETNORISHMENT];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    
    parameter[@"accessToken"] = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN];
    parameter[@"name"]  = self.name;
    parameter[@"brand"] = self.brand;
    parameter[@"dose"]  = self.dose;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [SetNourishmentEntity responseMapping];
}
@end
