//
//  SetMessageBoardAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetMessageBoardAPI.h"

@implementation SetMessageBoardAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETMESSAGEBOARD];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    parameter[@"date"] = self.date ;
    parameter[@"message"] = self.message;
    parameter[@"photo"] = self.photo;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [MessageBoardParentEntity responseMapping];
}

- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) message {
    if (! _message)
        return @"";
    return _message;
}

- (NSString *) photo {
    if (! _photo)
        return @"";
    return _photo;
}

@end
