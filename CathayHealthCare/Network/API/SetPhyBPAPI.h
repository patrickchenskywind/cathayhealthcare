//
//  SetPhyBPAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "PhyBPParentEntity.h"

@interface SetPhyBPAPI : BaseAPI

/*
 讀取:只傳遞accessToken
 新增:bpid為空值、date & sbp & dbp & heartBeat有值
 編輯:bpid & date & sbp & dbp & heartBeat有值
 刪除:bpid有值、date & sbp & dbp & heartBeat為空值
 */

@property (nonatomic, strong) NSString *date; //日期
@property (nonatomic, strong) NSString *bpid; //唯一值
@property (nonatomic, strong) NSString *sbp; //收縮壓
@property (nonatomic, strong) NSString *dbp; //舒張壓
@property (nonatomic, strong) NSString *heartBeat; //安靜心跳

@end
