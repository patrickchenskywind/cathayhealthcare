//
//  SetPhyHDLAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "PhyHDLParentEntity.h"

@interface SetPhyHDLAPI : BaseAPI

/*
 讀取:只傳遞accessToken
 新增:hdlid為空值、date & hdl有值
 編輯:hdlid & date & hdl有值
 刪除:hdlid有值、date & hdl為空值
 */

@property (nonatomic, strong) NSString * hdlid; //唯一值
@property (nonatomic, strong) NSString * date; //日期
@property (nonatomic, strong) NSString * hdl; //hdl

@end
