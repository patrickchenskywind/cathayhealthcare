//
//  SetPhyHDLAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhyHDLAPI.h"

@implementation SetPhyHDLAPI
- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYHDL];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"hdlid"] = self.hdlid ;
    parameter[@"date"] = self.date;
    parameter[@"hdl"] = self.hdl;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhyHDLParentEntity responseMapping];
}
- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) hdlid {
    if (! _hdlid)
        return @"";
    return _hdlid;
}

- (NSString *) hdl {
    if (! _hdl)
        return @"";
    return _hdl;
}

@end
