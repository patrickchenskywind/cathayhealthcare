//
//  SetPhyBPAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhyBPAPI.h"

@implementation SetPhyBPAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYBP];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"bpid"] = self.bpid;
    parameter[@"date"] = self.date ;
    parameter[@"sbp"] = self.sbp;
    parameter[@"dbp"] = self.dbp;
    parameter[@"heartBeat"] = self.heartBeat;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhyBPParentEntity responseMapping];
}


- (NSString *) bpid {
    if (! _bpid)
        return @"";
    return _bpid;
}

- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) sbp {
    if (! _sbp)
        return @"";
    return _sbp;
}
- (NSString *) dbp {
    if (! _dbp)
        return @"";
    return _dbp;
}
- (NSString *) heartBeat {
    if (! _heartBeat)
        return @"";
    return _heartBeat;
}

@end
