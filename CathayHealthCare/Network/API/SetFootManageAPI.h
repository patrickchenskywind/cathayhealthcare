//
//  SetFootManage.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface SetFootManageAPI : BaseAPI

@property (nonatomic, strong) NSString * bmr; //基礎代謝率
@property (nonatomic, strong) NSString * dayCal; //每日建議卡路里
@property (nonatomic, strong) NSString * mealCal; //每餐攝取卡路里
@property (nonatomic, strong) NSString * starchSixType; //全穀根莖
@property (nonatomic, strong) NSString * proteinSixType; //肉蛋豆魚
@property (nonatomic, strong) NSString * vegetableSixType;//蔬菜
@property (nonatomic, strong) NSString * fruitSixType;//水果
@property (nonatomic, strong) NSString * dairySixType;//低脂乳品
@property (nonatomic, strong) NSString * oilsSixType;//低脂乳品
@property (nonatomic, strong) NSString * restTime;//剩餘目標天數
@property (nonatomic, strong) NSString * protein;//蛋白質
@property (nonatomic, strong) NSString * fat;//脂肪
@property (nonatomic, strong) NSString * carbohydrates;//碳水化合物

@end
