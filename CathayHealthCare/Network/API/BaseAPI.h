//
//  BaseAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "HttpRequest.h"
#import <NSObject-ObjectMap/NSObject+ObjectMap.h>
#import "ApiPath.h"
#import "NetworkConfig.h"
#import "DefindConfig.h"
#import "BaseEntity.h"

#define LIST_SIZE 20

@interface BaseAPI : NSObject

@property(nonatomic, strong) NSString *accessToken;

- (NSString *)generateHost;
- (NSString *)generateRelativePath;
- (NSDictionary *)generateParameter;
- (NSString *)generateContentType;
- (RKObjectMapping *)responseMapping;

- (void)get:(void(^)(HttpResponse *response))responseCallBack;
- (void)post:(void(^)(HttpResponse *response))responseCallBack;

@end
