//
//  SetNourishmentRecordAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface SetNourishmentRecordAPI : BaseAPI

/*
 did無值:新增一筆用餐記錄
 did有值:移除did這筆記錄
 */

@property (nonatomic, strong) NSString * did; //用餐記錄唯一編號
@property (nonatomic, strong) NSString * date; //用藥日期
@property (nonatomic, strong) NSMutableArray * arrayNourishmentList; //詳細吃了什麼

/*
 arrayNourishmentList 內容物件
 nid	Str	營養品唯一編碼
 frequency	Str	頻率
 time	Str	服用時間
 */

@end
