//
//  SetNourishmentAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface SetNourishmentAPI : BaseAPI

@property (nonatomic, strong) NSString * name; //名稱
@property (nonatomic, strong) NSString * brand; //品牌
@property (nonatomic, strong) NSString * dose; //劑量

@end
