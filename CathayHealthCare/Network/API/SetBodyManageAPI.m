//
//  SetBodyManageAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetBodyManageAPI.h"

@implementation SetBodyManageAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETBODYMANAGE];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    parameter[@"weightGoal"] = self.weightGoal ;
    parameter[@"weightTarget"] = self.weightTarget;
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [BaseEntity responseMapping];
}

- (NSString *) weightGoal{
    if (! _weightGoal)
        return @"";
    return _weightGoal;
}

- (NSString *) weightTarget {
    if (! _weightTarget)
        return @"";
    return _weightTarget;
}

@end
