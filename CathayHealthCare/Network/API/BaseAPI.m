//
//  BaseAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@implementation BaseAPI


- (NSString *)generateHost {
    return [NSString stringWithFormat:@"%@://%@:%@", [NetworkConfig getRemoteServerProtocol], [NetworkConfig getRemoteServerHOST], [NetworkConfig getRemoteServerPort]];
}

- (NSString *)generateRelativePath {
    return nil;
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    NSUserDefaults *userDefaults  = [NSUserDefaults standardUserDefaults];
    if ([userDefaults stringForKey: ACCESSTOKEN] &&[userDefaults stringForKey: ACCESSTOKEN].length > 0)
        parameter[@"accessToken"] = [userDefaults stringForKey: ACCESSTOKEN];
    
    return parameter;
}

- (NSString *)generateContentType {
    return RKMIMETypeFormURLEncoded;
}

- (RKObjectMapping *)responseMapping {
    NSMutableDictionary *entityDictionary = [NSMutableDictionary dictionaryWithDictionary:[[[BaseEntity alloc] init] propertyDictionary]];
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [BaseEntity class]];
    [objectMapping addAttributeMappingsFromDictionary: entityDictionary];
    
    return objectMapping;
}

- (void)sendRequest:(NSInteger)httpMethod withCallBack:(void(^)(HttpResponse *response))responseCallBack {
    HttpRequest *request = [[HttpRequest alloc] init];
    [request setHost:[self generateHost]];
    [request setRelativePath:[self generateRelativePath]];
    [request setParameter:[self generateParameter]];
    [request setContentType:[self generateContentType]];
    [request setHttpMethodMode:httpMethod];
    [request setRequestMapping:nil];
    [request setRequestDescriptor:nil];
    [request setResponseMapping:[self responseMapping]];
    [request setResponseDescriptor:nil];
    [request sendRequest:responseCallBack];
}

- (void)get:(void(^)(HttpResponse *response))responseCallBack {
    [self sendRequest:RKRequestMethodGET withCallBack:responseCallBack];
}

- (void)post:(void(^)(HttpResponse *response))responseCallBack {
    [self sendRequest:RKRequestMethodPOST withCallBack:responseCallBack];
}

@end
