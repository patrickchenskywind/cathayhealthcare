//
//  SetBaseSetting.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetBaseSettingAPI.h"


@implementation SetBaseSettingAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETBASESETTING];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    parameter[@"workType"] = self.workType ;
    parameter[@"gender"] = self.gender;
    parameter[@"height"] = self.height;
    parameter[@"weight"] = self.weight;
    parameter[@"birthday"] = self.birthday;
    parameter[@"heartbeat"] = self.heartbeat;
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [BaseEntity responseMapping];
}

- (NSString *) workType{
    if (! _workType)
        return @"";
    return _workType;
}

- (NSString *) gender {
    if (! _gender)
        return @"";
    return _gender;
}

- (NSString *) height {
    if (! _height)
        return @"";
    return _height;
}

- (NSString *) weight {
    if (! _weight)
        return @"";
    return _weight;
}

- (NSString *) birthday {
    if (! _birthday)
        return @"";
    return _birthday;
}

- (NSString *) heartbeat {
    if (! _heartbeat)
        return @"";
    return _heartbeat;
}

@end
