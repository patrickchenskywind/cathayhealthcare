//
//  GetHealCalendarAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface GetHealCalendarAPI : BaseAPI

@property (nonatomic, strong) NSString * date; //日期

@end
