//
//  SetForaAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface SetForaAPI : BaseAPI

@property (nonatomic, strong) NSString * foraDeviceId; //fora 設備編號

@end
