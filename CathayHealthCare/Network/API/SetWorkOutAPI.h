//
//  SetWorkOutAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "BaseEntity.h"

@interface SetWorkOutAPI : BaseAPI

/*
 新增:sid為空值
 修改:sid有值
 刪除:sid有值 , date & sportType & heartBeat & sportTime & cal & burnTime & sportStep為空值
 */

@property (nonatomic, strong) NSString * sid; //運動資料唯一值
@property (nonatomic, strong) NSString * date; //運動日期 , 千秒
@property (nonatomic, strong) NSString * sportType; //“0”:快走 “1”:跑步 “2”:跑步(快) (以此類推往下新增 ,參照運動項目對照表)
@property (nonatomic, strong) NSString * heartBeat; //運動平均心跳
@property (nonatomic, strong) NSString * sportTime; //運動持續時間 , 千秒
@property (nonatomic, strong) NSString * cal; //燃燒熱量
@property (nonatomic, strong) NSString * burnTime; //有效燃脂區間 , 千秒
@property (nonatomic, strong) NSString * sportStep; //步數值

@end
