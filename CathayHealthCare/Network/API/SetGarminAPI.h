//
//  SetGarminAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface SetGarminAPI : BaseAPI

@property (nonatomic, strong) NSString * garminAccount; //Garmin帳號
@property (nonatomic, strong) NSString * garminPassword; //Garmin密碼

@end
