//
//  SetPhyConditionAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhyConditionAPI.h"

@implementation SetPhyConditionAPI
- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYCONDITION];
}

- (NSDictionary *)generateParameter {
    
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"wid"] = self.cid;
    parameter[@"date"] = self.date ;
    parameter[@"weight"] = self.symptom;
    parameter[@"treatment"] = self.treatment;
    parameter[@"notePhoto"] = self.notePhoto;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhyConditionParentEntity responseMapping];
}

-(NSString *)cid{
    if (!_cid) {
        return @"";
    }
    return _cid;
}

-(NSString *)date{
    if (!_date) {
        return @"";
    }
    return _date;
}
-(NSString *)symptom{
    if (!_symptom) {
        return @"";
    }
    return _symptom;
}
-(NSString *)treatment{
    if (!_treatment) {
        return @"";
    }
    return _treatment;
}
-(NSString *)notePhoto{
    if (!_notePhoto) {
        return @"";
    }
    return _notePhoto;
}

@end
