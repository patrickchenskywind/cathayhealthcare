//
//  SetHealCalendarAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface SetHealCalendarAPI : BaseAPI

@property (nonatomic, strong) NSString * rid; //提醒事件唯一值
@property (nonatomic, strong) NSString * startTime; //開始時間,千秒
@property (nonatomic, strong) NSString * endTime; //結束時間,千秒
@property (nonatomic, strong) NSString * remindTime; //提醒時間,千秒
@property (nonatomic, strong) NSString * title; //主題
@property (nonatomic, strong) NSString * content; //內容

@end
