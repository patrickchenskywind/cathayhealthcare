//
//  SetWorkOutAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetWorkOutAPI.h"

@implementation SetWorkOutAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_LOGIN];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    parameter[@"sid"] = self.sid ;
    parameter[@"date"] = self.date;
    parameter[@"sportType"] = self.sportType;
    parameter[@"heartBeat"] = self.heartBeat;
    parameter[@"sportTime"] = self.sportTime;
    parameter[@"cal"] = self.cal ;
    parameter[@"burnTime"] = self.burnTime;
    parameter[@"sportStep"] = self.sportStep;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [BaseEntity responseMapping];
}

- (NSString *) sid{
    if (! _sid)
        return @"";
    return _sid;
}

- (NSString *) date {
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) sportType {
    if (! _sportType)
        return @"";
    return _sportType;
}

- (NSString *) heartBeat {
    if (! _heartBeat)
        return @"";
    return _heartBeat;
}

- (NSString *) sportTime {
    if (! _sportTime)
        return @"";
    return _sportTime;
}

- (NSString *) cal{
    if (! _cal)
        return @"";
    return _cal;
}

- (NSString *) burnTime {
    if (! _burnTime)
        return @"";
    return _burnTime;
}

- (NSString *) sportStep {
    if (! _sportStep)
        return @"";
    return _sportStep;
}

@end
