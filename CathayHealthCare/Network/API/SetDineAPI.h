//
//  SetDineAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "BaseEntity.h"

/*
 did無值:新增一筆用餐記錄
 did有值:移除did這筆記錄
 */

@interface SetDineAPI : BaseAPI

@property (nonatomic, strong) NSString * did; //用餐記錄唯一編號
@property (nonatomic, strong) NSString * date; //用餐日期
@property (nonatomic, strong) NSString * dineType; // 餐別 “0”:早餐 “1”:午餐 “2”:晚餐 “3”:點心 “4”:水
@property (nonatomic, strong) NSArray * arrayDineList; //詳細吃了什麼

/*
 arrayDineList 內容物件
 fid	Str	食物唯一編碼
 quan	Str	分量
 foodGraph	Str	照片轉為base64字串
 */

@end
