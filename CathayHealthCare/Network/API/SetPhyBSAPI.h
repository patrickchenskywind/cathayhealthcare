//
//  SetPhyBSAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "PhyBSParentEntity.h"

@interface SetPhyBSAPI : BaseAPI

/*
 讀取:只傳遞accessToken
 新增:bid為空值、date & beforeDine & aftreDine & HbA1c有值
 編輯:bid & date & beforeDine & aftreDine & HbA1c有值
 刪除:bid有值、date & beforeDine & aftreDine & HbA1c為空值
 */

@property (nonatomic, strong) NSString *date ; //日期
@property (nonatomic, strong) NSString *bid ; //唯一值
@property (nonatomic, strong) NSString *beforeDine ; //飯前血糖
@property (nonatomic, strong) NSString *aftreDine ; //飯後血糖
@property (nonatomic, strong) NSString *HbA1c ; //糖化血色素

@end
