//
//  GetNourishmentListAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GetNourishmentListAPI.h"

@implementation GetNourishmentListAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_GETNOURISHMENTLIST];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [NourishmentListParentEntity responseMapping];
}

@end
