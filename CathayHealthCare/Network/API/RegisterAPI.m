//
//  RegisterAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RegisterAPI.h"

@implementation RegisterAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_REGISTER];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    
    parameter[@"email"] = self.email ;
    parameter[@"password"] = self.password;
    parameter[@"name"] = self.name;
    parameter[@"birthday"] = self.birthday;
    parameter[@"bloodType"] = self.bloodType;
    parameter[@"gender"] = self.gender;
    parameter[@"address"] = self.address;
    parameter[@"tel"] = self.tel;
    parameter[@"nationality"] = self.nationality;
    parameter[@"id"] = self.id;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [BaseEntity responseMapping];
}

- (NSString *) email{
    if (! _email)
        return @"";
    return _email;
}

- (NSString *) password {
    if (! _password)
        return @"";
    return _password;
}

- (NSString *) name {
    if (! _name)
        return @"";
    return _name;
}

- (NSString *) birthday {
    if (! _birthday)
        return @"";
    return _birthday;
}

- (NSString *) bloodType {
    if (! _bloodType)
        return @"";
    return _bloodType;
}

- (NSString *) gender {
    if (! _gender)
        return @"";
    return _gender;
}

- (NSString *) address {
    if (! _address)
        return @"";
    return _address;
}

- (NSString *) tel {
    if (! _tel)
        return @"";
    return _tel;
}

- (NSString *) nationality {
    if (! _nationality)
        return @"";
    return _nationality;
}

- (NSString *) id {
    if (! _id)
        return @"";
    return _id;
}
@end
