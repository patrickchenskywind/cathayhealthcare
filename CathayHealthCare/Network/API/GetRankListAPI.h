//
//  GetRankListAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface GetRankListAPI : BaseAPI

@property (nonatomic, strong) NSString * type; //排行榜類別 “0”:減重 “1”:運動

@end
