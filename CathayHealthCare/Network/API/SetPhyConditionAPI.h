//
//  SetPhyConditionAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "PhyConditionParentEntity.h"

@interface SetPhyConditionAPI : BaseAPI

@property (nonatomic, strong) NSString * cid; //notePhoto
@property (nonatomic, strong) NSString * date; //日期
@property (nonatomic, strong) NSString * symptom; //症狀
@property (nonatomic, strong) NSString * treatment; //處理方式
@property (nonatomic, strong) NSString * notePhoto; //備註照片 , base64字串

@end
