//
//  LoginAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LoginAPI.h"


@implementation LoginAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_LOGIN];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"account"] = self.account ;
    parameter[@"password"] = self.password;
    parameter[@"deviceType"] = self.deviceType;
    parameter[@"pushToken"] = self.pushToken;
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [LoginEntity responseMapping];
}

- (NSString *) account{
    if (! _account)
        return @"";
    return _account;
}

- (NSString *) password {
    if (! _password)
        return @"";
    return _password;
}

- (NSString *) deviceType {
    if (! _deviceType)
        return @"";
    return _deviceType;
}

- (NSString *) pushToken {
    if (! _pushToken)
        return @"";
    return _pushToken;
}

@end
