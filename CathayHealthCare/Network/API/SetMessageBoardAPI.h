//
//  SetMessageBoardAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "MessageBoardParentEntity.h"

@interface SetMessageBoardAPI : BaseAPI

@property (nonatomic, strong) NSString * date; //留言日期
@property (nonatomic, strong) NSString * message; //留言訊息(空值代表不新增只取回留言)
@property (nonatomic, strong) NSString * photo; //Base64字串

@end
