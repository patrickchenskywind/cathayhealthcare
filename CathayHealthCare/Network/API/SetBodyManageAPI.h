//
//  SetBodyManageAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface SetBodyManageAPI : BaseAPI

@property (nonatomic, strong) NSString * weightGoal; //目標體重
@property (nonatomic, strong) NSString * weightTarget; //“0”:維持 “正數”:增重 “負數”:減重

@end
