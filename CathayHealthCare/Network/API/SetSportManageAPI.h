//
//  SetSportManageAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"

@interface SetSportManageAPI : BaseAPI

@property (nonatomic, strong) NSString * water; //水分
@property (nonatomic, strong) NSString * workoutTime; //每週運動量(分鐘)
@property (nonatomic, strong) NSString * dayTarget; //達成目標體重天數
@property (nonatomic, strong) NSString * heartBeatMin; //有氧運動心跳範圍最小值
@property (nonatomic, strong) NSString * heartBeatMax; //有氧運動心跳範圍最大值

@end
