//
//  SetPhyFatAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "PhyFatParentEntity.h"

@interface SetPhyFatAPI : BaseAPI

/*
 讀取:只傳遞accessToken
 新增:fid為空值、date & fat 有值
 編輯:fid & date & fat 有值
 刪除:fid有值、date & fat 為空值
 */

@property (nonatomic, strong) NSString * fid; //唯一值
@property (nonatomic, strong) NSString * date; //日期
@property (nonatomic, strong) NSString * fat; //體脂

@end
