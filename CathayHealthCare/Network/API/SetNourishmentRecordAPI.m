
//
//  SetNourishmentRecordAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetNourishmentRecordAPI.h"

@implementation SetNourishmentRecordAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETNOURISSHMENTRECORD];
}

- (RKObjectMapping *)responseMapping {
    return [BaseEntity responseMapping];
}


- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    parameter[@"did"] = self.did ;
    parameter[@"date"] = self.date;
    
    NSString *dineList = @"[";
    for (int i = 0; i < self.arrayNourishmentList.count ; i++) {
        
        NSDictionary   *dic   = self.arrayNourishmentList[i];
        NSMutableArray *datas = [[NSMutableArray alloc] init];
        
        for (NSString *key in dic) {
            NSString *data = [NSString stringWithFormat:@"\"%@\":\"%@\"",key,[dic objectForKey:key]];
            [datas addObject:data];
        }
        
        dineList = [dineList stringByAppendingFormat:@"{%@}",[datas componentsJoinedByString:@","]];
        
        if (i < self.arrayNourishmentList.count -1) {
            dineList = [dineList stringByAppendingString:@","];
        }
        
    }
    dineList = [dineList stringByAppendingString:@"]"];
    parameter[@"arrayNourishmentList"] = dineList;
    
    return parameter;
}
@end
