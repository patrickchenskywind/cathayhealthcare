//
//  LoginAPI.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "LoginEntity.h"

@interface LoginAPI : BaseAPI

@property (nonatomic, strong) NSString * account;       //電子郵件信箱or身分證字號or護照號
@property (nonatomic, strong) NSString * password;      //密碼，需大於六碼
@property (nonatomic, strong) NSString * deviceType;    //“0”:iOS “1”:android
@property (nonatomic, strong) NSString * pushToken;     //android裝置傳遞gcm regId  iOS裝置置傳device token

@end
