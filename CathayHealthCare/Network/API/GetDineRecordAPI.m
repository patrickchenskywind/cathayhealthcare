//
//  GetDineRecordAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GetDineRecordAPI.h"
#import "DineRecordParentEntity.h"

@implementation GetDineRecordAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_GETDINERECORD];
}

-(NSDictionary *)generateParameter{
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"accessToken"] = self.accessToken;
    parameter[@"date"]        = self.date;
    
    return parameter;
}


-(NSString *)date
{
    if (!_date) {
        return @"";
    }
    return _date;
}

- (RKObjectMapping *)responseMapping {
    return [DineRecordParentEntity responseMapping];
}

@end
