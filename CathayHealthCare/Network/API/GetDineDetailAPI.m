//
//  GetDineDetailAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GetDineDetailAPI.h"

@implementation GetDineDetailAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_GETDINEDETAIL];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
    parameter[@"fid"] = self.fid ;
    parameter[@"barcode"] = self.barcode;
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [DineDetailEntity responseMapping];
}

- (NSString *) fid{
    if (! _fid)
        return @"";
    return _fid;
}

- (NSString *) barcode {
    if (! _barcode)
        return @"";
    return _barcode;
}

@end
