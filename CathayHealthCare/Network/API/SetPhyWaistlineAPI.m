//
//  SetPhyWaistlineAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SetPhyWaistlineAPI.h"

@implementation SetPhyWaistlineAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_SETPHYWAISTLINE];
}

- (NSDictionary *)generateParameter {
    NSMutableDictionary *parameter = [[super generateParameter] mutableCopy];
    
    parameter[@"wid"] = self.wid;
    parameter[@"date"] = self.date ;
    parameter[@"waistline"] = self.waistline;
    
    return parameter;
}

- (RKObjectMapping *)responseMapping {
    return [PhyWaistlineParentEntity responseMapping];
}


- (NSString *) wid {
    if (! _wid)
        return @"";
    return _wid;
}

- (NSString *) date{
    if (! _date)
        return @"";
    return _date;
}

- (NSString *) waistline {
    if (! _waistline)
        return @"";
    return _waistline;
}

@end
