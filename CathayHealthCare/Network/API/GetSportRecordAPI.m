//
//  GetSportRecordAPI.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GetSportRecordAPI.h"

@implementation GetSportRecordAPI

- (NSString *)generateRelativePath {
    return [NetworkConfig urlInApi: API_NAME_GETSPORTRECORD];
}

- (NSDictionary *)generateParameter {
    return  [super generateParameter];
}

- (RKObjectMapping *)responseMapping {
    return [SportRecordParentEntity responseMapping];
}

@end
