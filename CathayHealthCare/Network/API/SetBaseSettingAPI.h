//
//  SetBaseSetting.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BaseAPI.h"
#import "BaseEntity.h"

@interface SetBaseSettingAPI : BaseAPI

@property (nonatomic, strong) NSString * workType; //“0”:低度工作 “1”:中度工作 “2”:重度工作
@property (nonatomic, strong) NSString * gender; //“0”:男 “1”:女
@property (nonatomic, strong) NSString * height;
@property (nonatomic, strong) NSString * weight;
@property (nonatomic, strong) NSString * birthday;  //生日yyyy/MM/dd轉為千秒
@property (nonatomic, strong) NSString * heartbeat;

@end
