//
//  HttpResponse.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface HttpResponse : NSObject

@property (assign, nonatomic) BOOL isSuccessed;
@property (assign, nonatomic) NSInteger statusCode;
@property (retain, nonatomic) NSString *responseBody;

@property (retain, nonatomic) RKObjectRequestOperation *operation;
@property (retain, nonatomic) RKMappingResult *result;
@property (retain, nonatomic) NSString *errorDescription;

@end
