//
//  Toos.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Toos : NSObject

+ (BOOL) checkMsgCodeWihtMsg: (NSString *) msg;
+ (NSString *) getYMDFormDate: (NSDate *) date;
+ (NSString *) getYMDHMFormDate:(NSDate *) date;
+ (NSDate *) getDateFormString: (NSString *) string;
+ (NSDate *) getNSDateFromFormServerTimeInterval: (NSString *)timeInterval;
+ (NSDate *) subOneDay:(NSDate *) date;
+ (NSDate *) addOneDay:(NSDate *) date;
+ (NSString *) getThousandSecondStringFromDate: (NSDate *) date;
+ (NSString *) getYMDHMStringFormServerTimeInterval: (NSString *) timeInterval;
+ (NSString *) getYMDStringFormServerTimeInterval: (NSString *) timeInterval;
+ (NSString *) getTimeThroughStringFormServerTimeInterval: (NSString *)timeInterval;
+ (NSInteger) getUserAgeWithTimeInterVal: (NSString *) timeInterval;
+ (BOOL)isCurrentDay:(NSDate *)aDate;
+ (NSString *) getBase64StringFromImage:(UIImage *)image;
+ (UIImage *) getImageFromBase64String: (NSString *) string;

@end
