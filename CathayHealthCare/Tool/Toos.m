//
//  Toos.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "Toos.h"
#import "DefindConfig.h"

@implementation Toos
#pragma mark - Open

+ (BOOL) checkMsgCodeWihtMsg: (NSString *) msg {
    return [msg isEqualToString: SUCCESSCODE];
}

+ (NSString *) getYMDFormDate: (NSDate *) date {
    return [self formatDate: @"yyyy/MM/dd" forDate: date];
}

+ (NSString *) getYMDHMFormDate:(NSDate *) date {
    return [self formatDate: @"yyyy/MM/dd hh:mm" forDate: date];
}

+ (NSDate *) getDateFormString: (NSString *) string {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSDate *date = [dateFormatter dateFromString: string];
    return date;
}

+ (NSDate *) getNSDateFromFormServerTimeInterval: (NSString *)timeInterval {
    double integerTimeInterval = [timeInterval longLongValue] / 1000;
    return [NSDate dateWithTimeIntervalSince1970: (NSUInteger)integerTimeInterval];
}

+ (NSString *) getThousandSecondStringFromDate: (NSDate *) date {
    double milliseconds = [date timeIntervalSince1970] * 1000;
    return  [NSString stringWithFormat:@"%.00f",milliseconds];
}

+ (NSString *) getYMDHMStringFormServerTimeInterval: (NSString *) timeInterval {
    double integerTimeInterval = [timeInterval longLongValue] / 1000;
    return [self getYMDHMFormDate: [NSDate dateWithTimeIntervalSince1970: (NSUInteger)integerTimeInterval]];
}

+ (NSString *) getYMDStringFormServerTimeInterval: (NSString *) timeInterval {
    double integerTimeInterval = [timeInterval longLongValue] / 1000;
    return [self getYMDFormDate: [NSDate dateWithTimeIntervalSince1970: (NSUInteger)integerTimeInterval]];
}

+ (NSString *) getTimeThroughStringFormServerTimeInterval: (NSString *)timeInterval {
    NSInteger integerTimeInterval = [timeInterval longLongValue] / 1000 / 60;
    NSString *timeString = @"";
    NSInteger day = 0.0, hour = 0.0, minute = 0.0;
    
    if ( integerTimeInterval / 1440 >= 1 ) {
        day = integerTimeInterval / 1440;
        timeString = [timeString stringByAppendingFormat: @" %ld天", day];
    }
    if ( (integerTimeInterval - (day * 1440)) / 60 >= 1 ) {
        hour = (integerTimeInterval - (day * 1440)) / 60;
        timeString = [timeString stringByAppendingFormat: @" %ld小時", hour];
    }
    if ( integerTimeInterval - ((day * 1440) + (hour * 60)) >= 1) {
        minute = integerTimeInterval - ((day * 1440) + (hour * 60));
        timeString =[timeString stringByAppendingFormat: @" %ld分鐘", minute];
    }
    return timeString;
}

+ (NSInteger) getUserAgeWithTimeInterVal: (NSString *) timeInterval {
    [timeInterval doubleValue];
    NSDate* birthday = [NSDate dateWithTimeIntervalSince1970: [timeInterval longLongValue] / 1000];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components: NSCalendarUnitYear
                                       fromDate: birthday
                                       toDate: [NSDate date]
                                       options:0];
    NSInteger age = [ageComponents year];
    
    return age;
}

+ (BOOL)isCurrentDay:(NSDate *)aDate {
    if (aDate == nil) return NO;
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate: [NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:aDate];
    NSDate *otherDate = [cal dateFromComponents:components];
    if([today isEqualToDate:otherDate])
        return YES;
    
    return NO;
}

+ (NSDate *) subOneDay:(NSDate *) date {
    double milliseconds = [date timeIntervalSince1970];
    milliseconds = milliseconds - 86400;
    return [NSDate dateWithTimeIntervalSince1970: milliseconds];
}

+ (NSDate *) addOneDay:(NSDate *) date {
    double milliseconds = [date timeIntervalSince1970];
    milliseconds = milliseconds + 86400;
    return [NSDate dateWithTimeIntervalSince1970: milliseconds];
}

+ (NSString *) getBase64StringFromImage:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions: NSDataBase64Encoding64CharacterLineLength];
}

+ (UIImage *) getImageFromBase64String: (NSString *) string {
    NSData *data = [[NSData alloc]initWithBase64EncodedString: string options: NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

#pragma mark - Internal
+ (NSString *) formatDate: (NSString *) dateFormate forDate: (NSDate *) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: dateFormate];
    
    return [dateFormatter stringFromDate: date];
}

@end
