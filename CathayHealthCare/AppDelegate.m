//
//  AppDelegate.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/12.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AppDelegate.h"
#import "DefindConfig.h"
#import "GetFoodListAPI.h"
#import "GetNourishmentListAPI.h"
#import "Toos.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

+ (AppDelegate *)sharedAppDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSLog(@"bundlePath = %@",[[NSBundle mainBundle]bundlePath]);
    
    UIViewController *rootViewControoler =  [[LaunchViewController alloc] initWithNibName:@"LaunchViewController" bundle:nil];
    [self.window setRootViewController: rootViewControoler];
    
    if ( [application respondsToSelector: @selector(registerUserNotificationSettings:)] ) {
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes: UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings: notificationSettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
    else {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
#pragma GCC diagnostic pop
    }
    
    [[UINavigationBar appearance] setTintColor: [UIColor whiteColor]];
    
    [self checkListData];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self checkListData];
}

- (void) checkListData {
    if ( ! [[NSUserDefaults standardUserDefaults] objectForKey: FOODLIST_DOWNLOAD_DATE] || ! [Toos isCurrentDay: [[NSUserDefaults standardUserDefaults] objectForKey: FOODLIST_DOWNLOAD_DATE]]  ) {
        [self executGetFoodListAPI];
    }
    if ( ! [[NSUserDefaults standardUserDefaults] objectForKey: NOURISHMENTLIST_DOWNLOAD_DATE] || ! [Toos isCurrentDay: [[NSUserDefaults standardUserDefaults] objectForKey: NOURISHMENTLIST_DOWNLOAD_DATE]]  ) {
        [self executGetNourishmentListAPI];
    }
}

- (void) executGetFoodListAPI {
    GetFoodListAPI *api = [[GetFoodListAPI alloc] init];
    [api post:^(HttpResponse *response) {
        FoodListParentEntity *foodListParentEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg: foodListParentEntity.msg]) {
            if (foodListParentEntity.arrayFood.count > 0) {
                [FoodListEntity removeAll];
                [foodListParentEntity.arrayFood enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    FoodListEntity *foodListEntity = obj;
                    [foodListEntity saveToLocal];
                }];
                [[NSUserDefaults standardUserDefaults] setObject: [NSDate date] forKey: FOODLIST_DOWNLOAD_DATE];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }];
}

- (void) executGetNourishmentListAPI {
    GetNourishmentListAPI *api = [[GetNourishmentListAPI alloc] init];
    [api post:^(HttpResponse *response) {
        NourishmentListParentEntity *nourishmentParentEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg: nourishmentParentEntity.msg]) {
            if (nourishmentParentEntity.arrayNourishmentList.count > 0) {
                [NourishmentListEntity removeAll];
                [nourishmentParentEntity.arrayNourishmentList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    NourishmentListEntity *nourishmentEntity = obj;
                    [nourishmentEntity saveToLocal];
                }];
                [[NSUserDefaults standardUserDefaults] setObject: [NSDate date] forKey: NOURISHMENTLIST_DOWNLOAD_DATE];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - APNS Notification
#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

#endif
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *deviceTokenString = [deviceToken description];
    
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString: @" " withString: @""];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString: @"<" withString: @""];
    deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString: @">" withString: @""];
    
    [[NSUserDefaults standardUserDefaults] setObject: deviceToken forKey: DEVICE_TOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"DeviceToken = %@", deviceTokenString);
}



#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.gred" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreData" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CoreData.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support
- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
