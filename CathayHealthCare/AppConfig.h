//
//  AppConfig.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/25.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//


/*
 _dic_buttonSwitch = @{@"weight":@YES,
 @"waistline":@YES,
 @"fat":@YES,
 @"heartBeat":@YES,
 @"bloodSugar":@YES,
 @"TG":@YES,
 @"HDL":@YES,
 @"sleep":@YES,
 @"healthStatus":@YES};
 */
#import <Foundation/Foundation.h>

enum HEALTH_RECORD_BUTTON_NUM
{
    NUM_BUTTON_WEIGHT           =1,
    NUM_BUTTON_FAT              =2,
    NUM_BUTTON_WAISTLINE        =3,
    NUM_BUTTON_HEARTBEAT        =4,
    NUM_BUTTON_BLOODSUGAR       =5,
    NUM_BUTTON_TG               =6,
    NUM_BUTTON_HDL              =7,
    NUM_BUTTON_SLEEP            =8,
    NUM_BUTTON_STATUS           =9,
    
};

static NSString * const BUTTON_IDENTIFY_WEIGHT            = @"weight";
static NSString * const BUTTON_IDENTIFY_WAISTLINE         = @"waistline";
static NSString * const BUTTON_IDENTIFY_FAT               = @"fat";
static NSString * const BUTTON_IDENTIFY_HEARTBEAT         = @"heartBeat";
static NSString * const BUTTON_IDENTIFY_BLOODSUGAR        = @"bloodSugar";
static NSString * const BUTTON_IDENTIFY_TG                = @"TG";
static NSString * const BUTTON_IDENTIFY_HDL               = @"HDL";
static NSString * const BUTTON_IDENTIFY_SLEEP             = @"sleep";
static NSString * const BUTTON_IDENTIFY_HEALTHSTATUS      = @"healthStatus";

//#define SCREEN_BOUND [UIScreen mainScreen].bounds
//
//// user default
//#define HAS_BEEN_LAUNCH @"HAS_BEEN_LAUNCH"
//#define HEALTH_RECORD_BUTTON_SWITCH @"HEALTH_RECORD_BUTTON_SWITCH"
//#define HEALTH_RECORD_BUTTON_TITLE  @"HEALTH_RECORD_BUTTON_TITLE"



