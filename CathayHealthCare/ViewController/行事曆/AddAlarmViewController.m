//
//  AddAlarmViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddAlarmViewController.h"
#import "UIColor+Hex.h"

@interface AddAlarmViewController ()

@end

@implementation AddAlarmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _button_ok = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_button_ok setTitle:@"確定" forState:UIControlStateNormal];
    [_button_ok setBackgroundColor:[UIColor colorWithHex:0x00A29A]];
    [_button_ok setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.buttonView addSubview:_button_ok];
    
    if (_isAdd) {
        [self setTitle:@"新增備忘"];
        [_button_ok setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, CGRectGetHeight(self.buttonView.frame))];
        
    }
    else{
        [self setTitle:@"編輯備忘"];
        _button_delete = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_button_delete setTitle:@"刪除" forState:UIControlStateNormal];
        [_button_delete setBackgroundColor:[UIColor colorWithHex:0x5AB7AE]];
        [_button_delete setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.buttonView addSubview:_button_delete];
        [_button_delete setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width/2, CGRectGetHeight(self.buttonView.frame))];
        [_button_ok setFrame:CGRectMake(CGRectGetMaxX(_button_delete.frame), 0, [UIScreen mainScreen].bounds.size.width/2, CGRectGetHeight(self.buttonView.frame))];
        
    }

    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [_text_alarmTime initialWithpullDownContent:@[@"五分鐘",@"10分鐘",@"半小時",@"1小時"] borderColor:[UIColor colorWithHex:0x727271]];
    [_text_alarmTime.text setText:@"五分鐘"];
    [self setStyle];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setStyle
{

    [_text_alarmTime.text setFont:[UIFont systemFontOfSize:12.0]];
    
    [_text_title.layer setBorderWidth:1];
    [_text_title.layer setCornerRadius:5.0];
    [_text_title.layer setBorderColor:[UIColor colorWithHex:0x727271].CGColor];
    
    [_text_startD.layer setBorderWidth:1];
    [_text_startD.layer setCornerRadius:5.0];
    [_text_startD.layer setBorderColor:[UIColor colorWithHex:0x727271].CGColor];
    
    [_text_startT.layer setBorderWidth:1];
    [_text_startT.layer setCornerRadius:5.0];
    [_text_startT.layer setBorderColor:[UIColor colorWithHex:0x727271].CGColor];
    
    [_text_endD.layer setBorderWidth:1];
    [_text_endD.layer setCornerRadius:5.0];
    [_text_endD.layer setBorderColor:[UIColor colorWithHex:0x727271].CGColor];
    
    [_text_endT.layer setBorderWidth:1];
    [_text_endT.layer setCornerRadius:5.0];
    [_text_endT.layer setBorderColor:[UIColor colorWithHex:0x727271].CGColor];
    
    [_text_content.layer setBorderWidth:1];
    [_text_content.layer setCornerRadius:5.0];
    [_text_content.layer setBorderColor:[UIColor colorWithHex:0x727271].CGColor];
}

@end
