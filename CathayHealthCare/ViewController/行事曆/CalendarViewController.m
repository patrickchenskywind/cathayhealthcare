//
//  CalendarViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "CalendarViewController.h"
#import "AddAlarmViewController.h"


@interface CalendarViewController ()

@end

@implementation CalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"健康行事曆"];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self setAddButton];
}

-(void)setAddButton
{
    _button_add = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button_add setFrame:CGRectMake(0,
                                         0,
                                         self.navigationController.navigationBar.frame.size.height,
                                         self.navigationController.navigationBar.frame.size.height)];
    [_button_add setImage:[UIImage imageNamed:@"btn_add"] forState:UIControlStateNormal];
    [_button_add addTarget:self action:@selector(addButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingItem = [[UIBarButtonItem alloc] initWithCustomView:_button_add];
    [self.navigationItem setRightBarButtonItem:settingItem];
}

-(void)addButtonPress:(UIButton *)button
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    AddAlarmViewController *vc_addAlarm = [[AddAlarmViewController alloc] initWithNibName:@"AddAlarmViewController" bundle:nil];
    [vc_addAlarm setIsAdd:YES];
    [self.navigationController pushViewController:vc_addAlarm animated:NO];
}
-(void)editAlarm
{
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    AddAlarmViewController *vc_addAlarm = [[AddAlarmViewController alloc] initWithNibName:@"AddAlarmViewController" bundle:nil];
    [vc_addAlarm setIsAdd:NO];
    [self.navigationController pushViewController:vc_addAlarm animated:NO];
}


@end
