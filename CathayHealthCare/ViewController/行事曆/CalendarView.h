//
//  CalendarView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VRGCalendarView.h"


@protocol CalendarViewDelegate <NSObject>

-(void)editAlarm;

@end

@interface CalendarView : UIView<UITableViewDataSource,UITableViewDelegate,VRGCalendarViewDelegate>


@property (nonatomic,strong)IBOutlet UITableView *table_event;
@property (nonatomic,strong)IBOutlet UIView      *view_main;
@property (nonatomic,strong)IBOutlet UIView  *view_point;

@property (nonatomic,strong) VRGCalendarView *calendar;
@property (nonatomic,strong) NSArray *array_event;
@property (nonatomic,strong) NSArray *array_isMark;
@property (nonatomic,strong) NSArray *array_isAlarm;
@property (nonatomic,weak  )IBOutlet id<CalendarViewDelegate> delegate;


@end
