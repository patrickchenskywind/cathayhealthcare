//
//  CalendarView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "CalendarView.h"
#import "AlarmCell.h"

#define VIEWBORDERWIDTH 0.3f

@implementation CalendarView

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
 
        _array_event = @[@"09:00起床",@"12:00吃藥",@"14:00看醫生"];
        _array_isAlarm = @[@NO,@YES,@NO];
        _array_isMark = @[@NO,@NO,@NO];
        
    }
    return self;
}


-(void)layoutSubviews
{
    

    
    if (!_calendar) {
        _calendar = [[VRGCalendarView alloc] init];
        [_calendar setDelegate:self];
        [_view_main addSubview:_calendar];
    }
    
    [_calendar setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, _view_main.frame.size.height)];
    [_view_point.layer setCornerRadius:CGRectGetWidth(_view_point.frame)/2];

}


#pragma mark CalendarDelegate
-(void)calendarView:(VRGCalendarView *)calendarView switchedToMonth:(int)month targetHeight:(float)targetHeight animated:(BOOL)animated {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    if (month==[components month]) {
        NSArray *dates = [NSArray arrayWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:5], nil];
        [calendarView markDates:dates];
    }
}

-(void)calendarView:(VRGCalendarView *)calendarView dateSelected:(NSDate *)date {
    NSLog(@"Selected date = %@",date);
}


#pragma mark UITableViewDelegate & Datasource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _array_event.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlarmCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AlarmCell"];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AlarmCell" owner:self options:nil] objectAtIndex:0];
        [cell setEventDesc:[_array_event objectAtIndex:indexPath.row]
                    isMark:[[_array_isMark objectAtIndex:indexPath.row] boolValue]
                   isAlarm:[[_array_isAlarm objectAtIndex:indexPath.row] boolValue]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_delegate editAlarm];
}

@end
