//
//  CalendarViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarView.h"

@interface CalendarViewController : UIViewController<CalendarViewDelegate>

@property (nonatomic,strong) UIButton *button_add;

@end
