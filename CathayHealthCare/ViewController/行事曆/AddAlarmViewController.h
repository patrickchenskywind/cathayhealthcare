//
//  AddAlarmViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullDownTextField.h"

@interface AddAlarmViewController : UIViewController
@property (nonatomic,strong) IBOutlet PullDownTextField *text_alarmTime;
@property (nonatomic,strong) IBOutlet UITextField *text_startD;
@property (nonatomic,strong) IBOutlet UITextField *text_startT;
@property (nonatomic,strong) IBOutlet UITextField *text_endD;
@property (nonatomic,strong) IBOutlet UITextField *text_endT;

@property (nonatomic,strong) IBOutlet UITextView  *text_content;
@property (nonatomic,strong) IBOutlet UITextField *text_title;


@property (nonatomic,strong) IBOutlet UIView *buttonView;
@property (nonatomic,strong) UIButton *button_ok;
@property (nonatomic,strong) UIButton *button_delete;

@property (nonatomic,assign) BOOL isAdd;

@end
