//
//  HealthRecordView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HealthRecordView.h"
#import "DefindConfig.h"
#import "DeleteRecordView.h"
#import "DeleteBPRecordView.h"
#import "DeleteBSRecordView.h"
#import "Toos.h"
#import "PhyBPEntity.h"
#import "PhyBSEntity.h"

@interface HealthRecordView ()<RecordWeightViewDelegate, RecordFatViewDelegate, RecordWaistLineViewDelegate, RecordHeartBeatViewDelegate, RecordBloodSugarViewDelegate, RecordTGViewDelegate, RecordHDLViewDelegate, RecordSleepViewDelegate, DeleteRecordViewDelegate, DeleteBPRecordViewDelegate, DeleteBSRecordViewDelegate>

@property (nonatomic, strong) DeleteRecordView *deleteRecordView;
@property (nonatomic, strong) DeleteBPRecordView *deleteBPRecordView;
@property (nonatomic, strong) DeleteBSRecordView *deleteBSRecordView;

@end

@implementation HealthRecordView

-(void)layoutSubviews {
    [super layoutSubviews];
    [self newObject];
    [self setLayout];
}

-(void)newObject {
    if ( ! self.view_fat) {
        [self setView_fat: [[[NSBundle mainBundle] loadNibNamed: @"RecordFatView" owner: self options: nil] objectAtIndex: 0]];
        [self.view_fat setIDelegate: self];
        [self.mainView addSubview: self.view_fat];
    }
    if ( ! self.view_waistLine) {
        [self setView_waistLine: [[[NSBundle mainBundle] loadNibNamed: @"RecordWaistLineView" owner: self options: nil] objectAtIndex: 0]];
        [self.view_waistLine setIDelegate: self];
        [self.mainView addSubview: self.view_waistLine];
    }
    if ( ! self.view_heartBeat) {
        [self setView_heartBeat: [[[NSBundle mainBundle] loadNibNamed: @"RecordHeartBeatView" owner: self options: nil] objectAtIndex: 0]];
        [self.view_heartBeat setIDelegate: self];
        [self.mainView addSubview: self.view_heartBeat];
    }
    if ( ! self.view_bloodSugar) {
        [self setView_bloodSugar: [[[NSBundle mainBundle] loadNibNamed: @"RecordBloodSugarView" owner: self options: nil] objectAtIndex:0]];
        [self.view_bloodSugar setIDelegate: self];
        [self.mainView addSubview: self.view_bloodSugar];
    }
    if ( ! self.view_TG) {
        [self setView_TG: [[[NSBundle mainBundle] loadNibNamed: @"RecordTGView" owner: self options: nil] objectAtIndex: 0]];
        [self.view_TG setIDelegate: self];
        [self.mainView addSubview: self.view_TG];
    }
    if ( ! self.view_HDL) {
        [self setView_HDL: [[[NSBundle mainBundle] loadNibNamed: @"RecordHDLView" owner: self options: nil] objectAtIndex: 0]];
        [self.view_HDL setIDelegate: self];
        [self.mainView addSubview: self.view_HDL];
    }
    if (!self.view_sleep) {
        [self setView_sleep: [[[NSBundle mainBundle] loadNibNamed: @"RecordSleepView" owner: self options: nil] objectAtIndex: 0]];
        [self.view_sleep setIDelegate: self];
        [self.mainView addSubview: self.view_sleep];
    }
    if (!_view_healthStatus) {
        [self setView_healthStatus: [[[NSBundle mainBundle] loadNibNamed: @"RecordHealthStatusView" owner: self options: nil] objectAtIndex: 0]];
        [self.mainView addSubview: self.view_healthStatus];
    }
    if (!_view_weight){
        [self setView_weight: [[[NSBundle mainBundle] loadNibNamed: @"RecordWeightView" owner: self options: nil] objectAtIndex: 0]];
        [self.view_weight setDelegate: self];
        [self.mainView addSubview: self.view_weight];
    }
}

-(void)setLayout{
    [self.view_weight setFrame: self.mainView.bounds];
    [self.view_fat setFrame: self.mainView.bounds];
    [self.view_waistLine setFrame: self.mainView.bounds];
    [self.view_bloodSugar setFrame: self.mainView.bounds];
    [self.view_heartBeat setFrame: self.mainView.bounds];
    [self.view_TG setFrame: self.mainView.bounds];
    [self.view_HDL setFrame: self.mainView.bounds];
    [self.view_sleep setFrame: self.mainView.bounds];
    [self.view_healthStatus setFrame: self.mainView.bounds];
}

#pragma mark - RecordWeightViewDelegate
-(void)deleteRecordViewShowWithView: (RecordWeightView *)view date: (NSString *)date weight: (NSString *)weight {
    [self setupDeleteRecordViewWithView: view date: date info: weight];
}

#pragma mark - RecordFatViewDelegate
-(void)deleteRecordViewShowWithView: (RecordFatView *)view date: (NSString *)date fat: (NSString *)fat {
    [self setupDeleteRecordViewWithView: view date: date info: fat];
}

#pragma mark - RecordWaistLineViewDelegate
-(void)deleteRecordViewShowWithView: (RecordWaistLineView *)view date: (NSString *)date waistLine: (NSString *)waistLine {
    [self setupDeleteRecordViewWithView: view date: date info: waistLine];
}

#pragma mark - RecordHeartBeatViewDelegate
-(void)deleteRecordViewShowWithView: (RecordHeartBeatView *)view phyBPEntity: (PhyBPEntity *)phyBPEntity {
    if ( ! self.deleteBPRecordView) {
        [self  setDeleteBPRecordView: [[[NSBundle mainBundle] loadNibNamed: @"DeleteBPRecordView" owner: self options: nil] objectAtIndex: 0]];
        [self.deleteBPRecordView setFrame: CGRectMake(0, 0, 300, 300)];
        [self.deleteBPRecordView setCenter: CGPointMake(CGRectGetWidth(self.bounds) / 2,
                                                      CGRectGetHeight(self.bounds) / 2)];
        [self.deleteBPRecordView.labelDate setText: [Toos getYMDStringFormServerTimeInterval: phyBPEntity.date]];
        [self.deleteBPRecordView.labelSbp setText: [NSString stringWithFormat: @"%@mmHg",phyBPEntity.sbp]];
        [self.deleteBPRecordView.labelDbp setText: [NSString stringWithFormat: @"%@mmHg",phyBPEntity.dbp]];
        [self.deleteBPRecordView.labelHeartbeat setText: [NSString stringWithFormat: @"%@次/分",phyBPEntity.heartBeat]];
    
        [self.deleteBPRecordView setDelegate: self];
        [self.deleteBPRecordView setView: view];
        [self addSubview: self.deleteBPRecordView];
    }
}

#pragma mark - RecordBloodSugarViewDelegate
-(void)deleteRecordViewShowWithView: (RecordBloodSugarView *)view phyBSEntity: (PhyBSEntity *)phyBSEntity {
    if ( ! self.deleteBSRecordView) {
        [self  setDeleteBSRecordView: [[[NSBundle mainBundle] loadNibNamed: @"DeleteBSRecordView" owner: self options: nil] objectAtIndex: 0]];
        [self.deleteBSRecordView setFrame: CGRectMake(0, 0, 300, 300)];
        [self.deleteBSRecordView setCenter: CGPointMake(CGRectGetWidth(self.bounds) / 2,
                                                        CGRectGetHeight(self.bounds) / 2)];
        [self.deleteBSRecordView.labelDate setText: [Toos getYMDStringFormServerTimeInterval: phyBSEntity.date]];
        [self.deleteBSRecordView.labelBefore setText: [NSString stringWithFormat: @"%@mg/dL",phyBSEntity.beforeDine]];
        [self.deleteBSRecordView.labelAfter setText: [NSString stringWithFormat: @"%@mg/dL",phyBSEntity.aftreDine]];
        [self.deleteBSRecordView.labelHemoglobin setText: [NSString stringWithFormat: @"%@%@",phyBSEntity.HbA1c,@"%"]];
        
        [self.deleteBSRecordView setDelegate: self];
        [self.deleteBSRecordView setView: view];
        [self addSubview: self.deleteBSRecordView];
    }
}

#pragma mark - RecordTGViewDelegate
-(void)deleteRecordViewShowWithView: (RecordTGView *)view date: (NSString *)date tg: (NSString *)tg; {
    [self setupDeleteRecordViewWithView: view date: date info: tg];
}

#pragma mark - RecordHDLViewDelegate
-(void)deleteRecordViewShowWithView: (RecordHDLView *)view date: (NSString *)date hdl: (NSString *)hdl {
    [self setupDeleteRecordViewWithView: view date: date info: hdl];
}

#pragma mark - RecordSleepViewDelegate
- (void) deleteRecordViewShowWithView: (RecordSleepView *)view phySleepEntity: (PhySleepEntity *)phySleepEntity {
    NSLog(@"deleteRecordViewShowWithView");
}
- (void) buttonPhotoPress {
    [self.iDelegate buttonPhotoPress];
}

#pragma mark - DeleteRecordViewDelegate
- (void) deleteDateButtonPress:(id) view  {
    if ([view isKindOfClass: [RecordWeightView class]]) {
        [self.view_weight deleteData];
    }
    else if ([view isKindOfClass: [RecordFatView class]]) {
        [self.view_fat deleteData];
    }
    else if ([view isKindOfClass: [RecordWaistLineView class]]) {
        [self.view_waistLine deleteData];
    }
    else if ([view isKindOfClass: [RecordTGView class]]) {
        [self.view_TG deleteData];
    }
    else if ( [view isKindOfClass: [RecordHDLView class]] ) {
        [self.view_HDL deleteData];
    }
    
    [self dismissDeleteRecordView];
}

- (void) dismissDeleteRecordView {
    [self.deleteRecordView.maskView removeFromSuperview];
    [self.deleteRecordView setMaskView: nil];
    [self.deleteRecordView removeFromSuperview];
    [self setDeleteRecordView: nil];
}

#pragma mark - DeleteBPRecordViewDelegate
- (void) deleteBPDateButtonPress:(id) view {
    if ([view isKindOfClass: [RecordHeartBeatView class]]) {
        [self.view_heartBeat deleteData];
    }
    [self dismissDeleteBPRecordView];
}

- (void) dismissDeleteBPRecordView {
    [self.deleteBPRecordView.maskView removeFromSuperview];
    [self.deleteBPRecordView setMaskView: nil];
    [self.deleteBPRecordView removeFromSuperview];
    [self setDeleteBPRecordView: nil];
}

#pragma mark - DeleteBSRecordViewDelegate
- (void) deleteBSDateButtonPress:(id) view{
    if ([view isKindOfClass: [RecordBloodSugarView class]]) {
        [self.view_bloodSugar deleteData];
    }
    [self dismissDeleteBSRecordView];
}

- (void) dismissDeleteBSRecordView {
    [self.deleteBSRecordView.maskView removeFromSuperview];
    [self.deleteBSRecordView setMaskView: nil];
    [self.deleteBSRecordView removeFromSuperview];
    [self setDeleteBSRecordView: nil];
}


#pragma mark -------------------!!
- (void) setupDeleteRecordViewWithView: (id)view date: (NSString *)date info: (NSString *)info {
    if ( ! self.deleteRecordView) {
        [self setDeleteRecordView: [[[NSBundle mainBundle] loadNibNamed: @"DeleteRecordView" owner: self options: nil] objectAtIndex: 0]];
        [self.deleteRecordView setFrame: CGRectMake(0, 0, 300, 200)];
        [self.deleteRecordView setCenter: CGPointMake(CGRectGetWidth(self.bounds) / 2,
                                                      CGRectGetHeight(self.bounds) / 2)];
        [self.deleteRecordView setDelegate: self];
        [self.deleteRecordView.labelLeft setText: date];
        [self.deleteRecordView.labelRight setText: info];
        [self.deleteRecordView setView: view];
        [self addSubview: self.deleteRecordView];
    }
}

@end
