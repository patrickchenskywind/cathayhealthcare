//
//  HealthRecordViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "HealthRecordView.h"

@class HealthRecordButton;
@interface HealthRecordViewController : ViewController

@property (retain, nonatomic)HealthRecordView *view;

@end
