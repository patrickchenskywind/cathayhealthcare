//
//  RecordHealthStatusView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordHealthStatusView.h"
#import "UIColor+Hex.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "Toos.h"
#import "PhyConditionEntity.h"
#import "PhyConditionParentEntity.h"
#import "SetPhyConditionAPI.h"


@interface RecordHealthStatusView()<IDatePickerViewController,UITextViewDelegate,UITextFieldDelegate>{
    UITextView *currentTextView;
}

@property (strong, nonatomic) WYPopoverController      *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;

@end

@implementation RecordHealthStatusView



-(void)awakeFromNib{
    
    
    [self.text_date setText: [Toos getYMDFormDate: [NSDate date]]];
    self.array_healthRecord     = [NSMutableArray array];
    self.text_date.enabled      = NO;
    self.text_Symptom.delegate  = self;
    self.text_handle.delegate   = self;
    [self addDoneToolBarToKeyboard:self.text_handle];
}

-(void)layoutSubviews
{
    [_text_date.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_date.layer setBorderWidth:1];
    [_text_date.layer setCornerRadius:5];
    
    [_text_handle.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_handle.layer setBorderWidth:1];
    [_text_handle.layer setCornerRadius:5];
    
    [_text_Symptom.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_Symptom.layer setBorderWidth:1];
    [_text_Symptom.layer setCornerRadius:5];
    

    
    [_button_send.layer setCornerRadius:5];
    
    
}



#pragma mark Button Event 

- (IBAction)buttonDatePress:(UIButton *)sender {
    
    [self closeKeyboard];
    
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}


- (IBAction)submitRecord:(UIButton *)sender {
 
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSString *date    = self.text_date.text;
    NSString *symptom = self.text_Symptom.text;
    NSString *handle  = self.text_handle.text;
    NSString *imageString;
    
    NSData *data = UIImagePNGRepresentation(_image_camera.image) ? UIImagePNGRepresentation(_image_camera.image) : nil;
    
    if (!data) {
        imageString = @"";
    }
    else{
        imageString = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    }
    
    SetPhyConditionAPI *api = [[SetPhyConditionAPI alloc] init];
    
    [api setCid:@""];
    [api setDate:[Toos getThousandSecondStringFromDate:[dateFormatter dateFromString:date]]];
    [api setSymptom:symptom];
    [api setTreatment:handle];
    [api setNotePhoto:imageString];
    [api post:^(HttpResponse *response) {
        
        PhyConditionParentEntity *entity = response.result.firstObject;
        
        if ([Toos checkMsgCodeWihtMsg:entity.msg]) {
            _array_healthRecord = [NSMutableArray arrayWithArray:entity.arrayCondition];
            [_table_list reloadData];
            NSLog(@"SetPhyConditionAPI success");
        }
        else{
            NSLog(@"SetPhyConditionAPI fails");
        }
    }];
}


#pragma mark IDatePickerViewController Delegate

- (void) dateDidSelect:(NSDate *) date{
    [self setDate: date];
    [self.text_date setText: [Toos getYMDFormDate: date]];
}


#pragma mark UITableViewDelegate & Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _array_healthRecord.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"weightCell"];
    
    PhyConditionEntity *entity = _array_healthRecord[indexPath.row];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"weightCell"];
        cell.textLabel.text = [Toos getYMDStringFormServerTimeInterval:entity.date];
        cell.detailTextLabel.text = entity.symptom;
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

#pragma mark UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark Add Button To TextView

-(void)addDoneToolBarToKeyboard:(UITextView *)textView {
    UIToolbar* doneToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    doneToolbar.barStyle = UIBarStyleBlackTranslucent;
    doneToolbar.items = [NSArray arrayWithObjects:
                         [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonClickedDismissKeyboard)],
                         nil];
    [doneToolbar sizeToFit];
    textView.inputAccessoryView = doneToolbar;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    currentTextView = textView;
}

-(void)doneButtonClickedDismissKeyboard
{
    [currentTextView resignFirstResponder];
}

@end
