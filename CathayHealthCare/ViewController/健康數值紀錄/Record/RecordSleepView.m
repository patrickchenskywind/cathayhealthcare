//
//  RecordSleepView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordSleepView.h"
#import "UIColor+Hex.h"
#import "Toos.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DateAndTimePickerViewController.h"
#import "SetPhySleepAPI.h"
#import "SleepCell.h"

@interface RecordSleepView ()<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, IDateAndTimePickerViewControllerController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DateAndTimePickerViewController *sleepDatePickerViewController;
@property (strong, nonatomic) DateAndTimePickerViewController *wakeDatePickerViewController;
@property (strong, nonatomic) NSDate *wakeDate;
@property (strong, nonatomic) NSDate *sleepDate;
@property (strong, nonatomic) NSDate *didUsedDate;
@property (strong, nonatomic) NSDate *didUsedSleepTime;
@property (strong, nonatomic) NSDate *didUsedWakeTime;
@property (strong, nonatomic) NSArray *sleepData;
@property (strong, nonatomic) NSIndexPath *indexPathDidSelect;
@property (strong, nonatomic) UIButton *buttonDidPress;
@property (strong, nonatomic) NSString *base64Image;

@end

@implementation RecordSleepView
- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_text_sleepF.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_sleepF.layer setBorderWidth:1];
    [_text_sleepF.layer setCornerRadius:5];

    [_text_sleepD.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_sleepD.layer setBorderWidth:1];
    [_text_sleepD.layer setCornerRadius:5];
    
    [_text_wakeD.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_wakeD.layer setBorderWidth:1];
    [_text_wakeD.layer setCornerRadius:5];
    
    [_text_wakeF.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_wakeF.layer setBorderWidth:1];
    [_text_wakeF.layer setCornerRadius:5];
    
    [_button_send.layer setCornerRadius:5];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(closeKeyboard)];
    [tapGestureRecognizer setDelegate: self];
    [self.viewContainer addGestureRecognizer: tapGestureRecognizer];
}

#pragma mark - IBAction
- (IBAction)buttonSleepDatePress:(UIButton *)sender {
    [self closeKeyboard];
    [self setButtonDidPress: sender];
    if ( ! self.sleepDatePickerViewController) {
        [self setSleepDatePickerViewController: [[DateAndTimePickerViewController alloc] initWithNibName: @"DateAndTimePickerViewController" bundle: nil]];
        [self.sleepDatePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.sleepDatePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width + 60, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (IBAction)buttonWakeDatePress:(UIButton *)sender {
    [self closeKeyboard];
    [self setButtonDidPress: sender];
    if ( ! self.wakeDatePickerViewController) {
        [self setWakeDatePickerViewController: [[DateAndTimePickerViewController alloc] initWithNibName: @"DateAndTimePickerViewController" bundle: nil]];
        [self.wakeDatePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.wakeDatePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width + 60, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    
    if ([self.buttonDidPress isEqual: self.buttonSleep]) {
        [self setSleepDate: date];
        [self.text_sleepF setText: [Toos getYMDFormDate: date]];
        [self.text_sleepD setText: [[Toos getYMDHMFormDate: date] substringFromIndex: 11]];
    }
    else if ( [self.buttonDidPress isEqual: self.buttonWake]) {
        [self setWakeDate: date];
        [self.text_wakeF setText: [Toos getYMDFormDate: date]];
        [self.text_wakeD setText: [[Toos getYMDHMFormDate: date] substringFromIndex: 11]];
    }
    
    if (self.sleepDate && self.wakeDate) {
        NSString *throughString = [NSString stringWithFormat: @"%f",([[Toos getThousandSecondStringFromDate: self.wakeDate] doubleValue] - [[Toos getThousandSecondStringFromDate: self.sleepDate] doubleValue])] ;
        [self.labelThrough setText: [Toos getTimeThroughStringFormServerTimeInterval: throughString]];
    }
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
//    if (self.text_date.text.length > 0 && self.text_hdl.text.length > 0) {
//        if ( ! [self.date isEqualToDate: self.didUsedDate] && ! [self.text_hdl.text isEqualToString: self.didUsedHDL]) {
//            SetPhyHDLAPI *api = [[SetPhyHDLAPI alloc] init];
//            [api setDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
//            [api setHdl: self.text_hdl.text];
//            [api post:^(HttpResponse *response) {
//                PhyHDLParentEntity *phyHDLParentEntity = response.result.firstObject;
//                if ( [Toos checkMsgCodeWihtMsg: phyHDLParentEntity.msg] ) {
//                    [self setupDataWihtArray: phyHDLParentEntity.arrayHdl];
//                }
//            }];
//        }
    //    }
}
- (IBAction)buttonPhotoPress:(id)sender {
    [self.iDelegate buttonPhotoPress];
}


#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sleepData.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SleepCell *cell = [tableView dequeueReusableCellWithIdentifier: @"SleepCell"];
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"SleepCell" owner: self options: nil] objectAtIndex: 0];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    PhySleepEntity *phySleepEntity = self.sleepData[indexPath.row];
    [cell.labelSleepTime setText: [Toos getYMDHMFormDate: [Toos getNSDateFromFormServerTimeInterval: phySleepEntity.sleepTime]]];
    [cell.labelWakeTime setText: [Toos getYMDHMFormDate: [Toos getNSDateFromFormServerTimeInterval: phySleepEntity.wakeTime]]];
    if (phySleepEntity.sleepGraph && phySleepEntity.sleepGraph.length > 0) {
        [cell.labelTotalTime setText: [Toos getTimeThroughStringFormServerTimeInterval: phySleepEntity.sleepGraph]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setIndexPathDidSelect: indexPath];
    PhySleepEntity *phySleepEntity = self.sleepData[indexPath.row];
    [self.iDelegate deleteRecordViewShowWithView: self phySleepEntity: phySleepEntity];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 96;
}

- (void) setupData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetPhySleepAPI *api = [[SetPhySleepAPI alloc] init];
        [api post:^(HttpResponse *response) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            PhySleepParentEntity *phySleepParentEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: phySleepParentEntity.msg]) {
                if (phySleepParentEntity.arraySleep.count > 0) {
                    [self setupDataWihtArray: phySleepParentEntity.arraySleep];
                }
            }
        }];
    }
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if( ! [touch.view isEqual: self.viewContainer]) {
        return NO;
    }
    return YES;
}

- (void) setupDataWihtArray:(NSArray*)array {
    [self setSleepData: array];
    [self.table_record reloadData];
}

- (void) deleteData {
    SetPhySleepAPI *api = [[SetPhySleepAPI alloc] init];
    [api setSid: ((PhySleepEntity *)self.sleepData[self.indexPathDidSelect.row]).sid];
    [api post:^(HttpResponse *response) {
        PhySleepParentEntity *phySleepParentEntity = response.result.firstObject;
        if ( [Toos checkMsgCodeWihtMsg: phySleepParentEntity.msg] ) {
            [self setupDataWihtArray: phySleepParentEntity.arraySleep];
        }
    }];
}

- (void) setupImageViewWithImage: (UIImage *) image base64String: (NSString *) base64String {
    [self.imageViewPhoto setImage: image];
    [self setBase64Image: base64String];
}

@end
