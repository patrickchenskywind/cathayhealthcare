//
//  RecordBloodSugarView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class RecordBloodSugarView, PhyBSEntity;
@protocol RecordBloodSugarViewDelegate <NSObject>

    -(void)deleteRecordViewShowWithView: (RecordBloodSugarView *)view phyBSEntity: (PhyBSEntity *)phyBSEntity;

@end

@interface RecordBloodSugarView : View
@property (nonatomic,strong) IBOutlet UITextField *text_date;
@property (nonatomic,strong) IBOutlet UITextField *text_before;
@property (nonatomic,strong) IBOutlet UITextField *text_after;
@property (nonatomic,strong) IBOutlet UITextField *text_Hemoglobin;

@property (nonatomic,strong) IBOutlet UIButton *button_send;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (nonatomic, weak)id<RecordBloodSugarViewDelegate> iDelegate;

- (void) deleteData;
- (void) setupData;


@end
