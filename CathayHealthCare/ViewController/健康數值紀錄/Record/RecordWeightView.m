//
//  RecordWeightView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordWeightView.h"
#import "UIColor+Hex.h"
#import "PhyWeightEntity.h"
#import "Toos.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "DefindConfig.h"
#import "SetPhyWeightAPI.h"
#import "RecordGeneralCell.h"

@interface RecordWeightView () <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, IDatePickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *didUsedDate;
@property (strong, nonatomic) NSString *didUsedWeight;
@property (strong, nonatomic) NSArray *weightData;
@property (strong, nonatomic) NSIndexPath *indexPathDidSelect;

@end

#warning 會員身份功能未實作

@implementation RecordWeightView

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_text_date.layer setBorderColor: [[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_date.layer setBorderWidth: 1];
    [_text_date.layer setCornerRadius: 5];
    [_text_weight.layer setBorderColor: [[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_weight.layer setBorderWidth: 1];
    [_text_weight.layer setCornerRadius: 5];
    [_button_send.layer setCornerRadius: 5];
    [_table_record setScrollEnabled: NO];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(closeKeyboard)];
    [tapGestureRecognizer setDelegate: self];
    [self.viewContainer addGestureRecognizer: tapGestureRecognizer];
}

- (IBAction)submitRecord:(UIButton *)sender {
    if (self.date && ! [self.date isEqual: self.didUsedDate] && self.text_weight.text.length > 0  && ! [self.didUsedWeight isEqualToString: self.text_weight.text]) {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
            SetPhyWeightAPI *api = [[SetPhyWeightAPI alloc] init];
            PhyWeightEntity *phyWeightEntity  = [PhyWeightEntity seachWithDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
            if (phyWeightEntity && phyWeightEntity.wid && phyWeightEntity.wid.length > 0)
                [api setWid: phyWeightEntity.wid];
            [api setWeight: self.text_weight.text];
            [api setDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
            
            [api post:^(HttpResponse *response) {
                if ([Toos checkMsgCodeWihtMsg: ((PhyWeightParentEntity *)response.result.firstObject).msg]) {
#warning 未完成 需儲存至本地端資料 檢查是否正確
                    [self savePhyWeightToLocalWithDate: api.date weight: api.weight wid: api.wid];
                }
            }];
        }
        else{
            [self savePhyWeightToLocalWithDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]] weight: self.text_weight.text wid: nil];
        }
    }
}

-(void) savePhyWeightToLocalWithDate: (NSString *)date weight: (NSString *)weight wid:(NSString *)wid {
    [self setDidUsedDate: self.date];
    [self setDidUsedWeight: weight];
    if ([PhyWeightEntity seachWithDate: date]) {
        PhyWeightEntity *phyWeightEntity = [PhyWeightEntity seachWithDate: date];
        [phyWeightEntity remove];
    }
    PhyWeightEntity *phyWeightEntity = [[PhyWeightEntity alloc] init];
    [phyWeightEntity setWeight: weight];
    [phyWeightEntity setDate: date];
    [phyWeightEntity setWid: wid];
    [phyWeightEntity saveToLocal];
    [self reloadData];
}

- (IBAction)buttonDatePress:(UIButton *)sender {
    [self closeKeyboard];
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    [self setDate: date];
    [self.text_date setText: [Toos getYMDFormDate: date]];
    [self.text_date adjustsFontSizeToFitWidth];
}


#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.weightData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RecordGeneralCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RecordGeneralCell"];
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"RecordGeneralCell" owner: self options: nil] objectAtIndex: 0];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    
    PhyWeightEntity *phyWeightEntity = self.weightData[indexPath.row];
    [cell.leableLeft setText: [Toos getYMDStringFormServerTimeInterval: phyWeightEntity.date]];
    [cell.leableRight setText: [NSString stringWithFormat: @"%@公斤",phyWeightEntity.weight]];
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGRectZero.size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setIndexPathDidSelect: indexPath];
    PhyWeightEntity *phyWeightEntity = self.weightData[indexPath.row];
    [_delegate deleteRecordViewShowWithView: self date: [Toos getYMDStringFormServerTimeInterval: phyWeightEntity.date]  weight: [NSString stringWithFormat:@"%@ 公斤",phyWeightEntity.weight]];
}

- (void) deleteData {
    PhyWeightEntity *phyWeightEntity = self.weightData[self.indexPathDidSelect.row];
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        if (phyWeightEntity.wid && phyWeightEntity.wid.length > 0) {
#warning 需刪除伺服器資料
            SetPhyWeightAPI *api = [[SetPhyWeightAPI alloc] init];
            [api setWid: phyWeightEntity.wid];
            
            [api post:^(HttpResponse *response) {
                if ([Toos checkMsgCodeWihtMsg: ((PhyWeightParentEntity *)response.result.firstObject).msg]) {
#warning 未完成 需檢查是否正確
                    
                    [self removePhyWeightData: phyWeightEntity];
                }
            }];
        }
        else{
            [self removePhyWeightData: phyWeightEntity];
        }
    }
    else{
        [self removePhyWeightData: phyWeightEntity];
    }
}

- (void) removePhyWeightData: (PhyWeightEntity *) phyWeightEntity {
    [phyWeightEntity remove];
    [self reloadData];
}

- (void) reloadData {
    [self setupData];
    [self.table_record reloadData];
}

- (void) setupData {
    [self setWeightData: [PhyWeightEntity getLastFive]];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if( ! [touch.view isEqual: self.viewContainer]) {
        return NO;
    }
    return YES;
}

@end
