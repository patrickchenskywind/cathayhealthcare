//
//  RecordHeartBeatView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordHeartBeatView.h"
#import "UIColor+Hex.h"
#import "Toos.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "SetPhyBPAPI.h"
#import "BPCell.h"

@interface RecordHeartBeatView ()<UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, IDatePickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *didUsedDate;
@property (strong, nonatomic) NSString *didUsedBeat;
@property (strong, nonatomic) NSString *didUsedDBP;
@property (strong, nonatomic) NSString *didUsedSBP;
@property (strong, nonatomic) NSArray *bpData;
@property (strong, nonatomic) NSIndexPath *indexPathDidSelect;

@end

@implementation RecordHeartBeatView
- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_text_date.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_date.layer setBorderWidth:1];
    [_text_date.layer setCornerRadius:5];
    
    [_text_beat.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_beat.layer setBorderWidth:1];
    [_text_beat.layer setCornerRadius:5];
    
    [_text_DBP.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_DBP.layer setBorderWidth:1];
    [_text_DBP.layer setCornerRadius:5];
    
    [_text_SBP.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_SBP.layer setBorderWidth:1];
    [_text_SBP.layer setCornerRadius:5];
    
    [_button_send.layer setCornerRadius:5];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(closeKeyboard)];
    [tapGestureRecognizer setDelegate: self];
    [self.viewContainer addGestureRecognizer: tapGestureRecognizer];
    
}

#pragma mark - IBAction
- (IBAction)buttonDatePress:(UIButton *)sender {
    [self closeKeyboard];
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    [self setDate: date];
    [self.text_date setText: [Toos getYMDFormDate: date]];
    [self.text_date adjustsFontSizeToFitWidth];
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
    if (self.text_date.text.length > 0 && self.text_SBP.text.length > 0 && self.text_DBP.text.length > 0 && self.text_beat.text.length > 0) {
        if ( ! [self.date isEqualToDate: self.didUsedDate] &&
            ! [self.text_SBP.text isEqualToString: self.didUsedSBP] &&
            ! [self.text_DBP.text isEqualToString: self.didUsedDBP] &&
            ! [self.text_beat.text isEqualToString: self.didUsedBeat]
            ) {
            
            SetPhyBPAPI *api = [[SetPhyBPAPI alloc] init];
            [api setDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
            [api setDbp: self.text_DBP.text];
            [api setSbp: self.text_SBP.text];
            [api setHeartBeat: self.text_beat.text];
            [api post:^(HttpResponse *response) {
                [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
                PhyBPParentEntity *phyBPParentEntity = response.result.firstObject;
                if ([Toos checkMsgCodeWihtMsg: phyBPParentEntity.msg]) {
                    [self setDidUsedDate: self.date];
                    [self setDidUsedSBP: self.text_SBP.text];
                    [self setDidUsedDBP: self.text_DBP.text];
                    [self setDidUsedBeat: self.text_beat.text];
                    [self setupDataWihtArray: phyBPParentEntity.arrayBp];
                }
            }];
        }
    }
}

#pragma mark UITableViewDelegate & Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bpData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BPCell *cell = [tableView dequeueReusableCellWithIdentifier: @"BPCell"];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"BPCell" owner: self options: nil] objectAtIndex: 0];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    
    PhyBPEntity *phyBPEntity = self.bpData[indexPath.row];
    
    [cell.labelDate setText: [Toos getYMDStringFormServerTimeInterval: phyBPEntity.date]];
    [cell.labelSbp setText:  [NSString stringWithFormat: @"%@mmHg",phyBPEntity.sbp]];
    [cell.labelDbp setText:  [NSString stringWithFormat: @"%@mmHg",phyBPEntity.dbp]];
    [cell.labelHeartBeat setText:  [NSString stringWithFormat: @"%@次/分",phyBPEntity.heartBeat]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setIndexPathDidSelect: indexPath];
    PhyBPEntity *phyBPEntity = self.bpData[indexPath.row];
    [self.iDelegate deleteRecordViewShowWithView: self phyBPEntity: phyBPEntity];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 125;
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if( ! [touch.view isEqual: self.viewContainer]) {
        return NO;
    }
    return YES;
}

- (void) deleteData {
    SetPhyBPAPI *api = [[SetPhyBPAPI alloc] init];
    [api setBpid: ((PhyBPEntity *)self.bpData[self.indexPathDidSelect.row]).bpid];
    [api post:^(HttpResponse *response) {
        [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
        PhyBPParentEntity *phyBPParentEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg: phyBPParentEntity.msg]) {
            [self setupDataWihtArray: phyBPParentEntity.arrayBp];
        }
    }];
}

- (void) setupData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetPhyBPAPI *api = [[SetPhyBPAPI alloc] init];
        [api post:^(HttpResponse *response) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            PhyBPParentEntity *phyBPParentEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: phyBPParentEntity.msg]) {
                if (phyBPParentEntity.arrayBp.count > 0) {
                    [self setupDataWihtArray: phyBPParentEntity.arrayBp];
                }
            }
        }];
    }
}

- (void) setupDataWihtArray:(NSArray*)array {
    [self setBpData: array];
    [self.table_record reloadData];
}

@end
