//
//  RecordSleepView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class RecordSleepView, PhySleepEntity;
@protocol RecordSleepViewDelegate <NSObject>

    - (void) deleteRecordViewShowWithView: (RecordSleepView *)view phySleepEntity: (PhySleepEntity *)phySleepEntity;
    - (void) buttonPhotoPress;

@end

@interface RecordSleepView : View

@property (nonatomic, strong) IBOutlet UIView *viewContainer;
@property (nonatomic, strong) IBOutlet UITextField *text_sleepF;
@property (nonatomic, strong) IBOutlet UITextField *text_sleepD;
@property (nonatomic, strong) IBOutlet UITextField *text_wakeF;
@property (nonatomic, strong) IBOutlet UITextField *text_wakeD;
@property (nonatomic, strong) IBOutlet UIButton *button_send;
@property (nonatomic, strong) IBOutlet UITableView *table_record;

@property (weak, nonatomic) IBOutlet UIButton *buttonSleep;
@property (weak, nonatomic) IBOutlet UIButton *buttonWake;
@property (weak, nonatomic) IBOutlet UILabel *labelThrough;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhoto;

@property (nonatomic, weak) id<RecordSleepViewDelegate> iDelegate;

- (void) setupImageViewWithImage: (UIImage *) image base64String: (NSString *) base64String;

- (void) deleteData;
- (void) setupData;

@end
