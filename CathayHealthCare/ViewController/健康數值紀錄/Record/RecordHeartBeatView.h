//
//  RecordHeartBeatView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class RecordHeartBeatView, PhyBPEntity;
@protocol RecordHeartBeatViewDelegate <NSObject>

    -(void)deleteRecordViewShowWithView: (RecordHeartBeatView *)view phyBPEntity: (PhyBPEntity *)phyBPEntity;

@end

@interface RecordHeartBeatView : View

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic,strong) IBOutlet UITextField *text_SBP;
@property (nonatomic,strong) IBOutlet UITextField *text_DBP;
@property (nonatomic,strong) IBOutlet UITextField *text_date;
@property (nonatomic,strong) IBOutlet UITextField *text_beat;

@property (nonatomic,strong) IBOutlet UIButton  *button_send;
@property (nonatomic, strong) IBOutlet UITableView *table_record;

@property (nonatomic, weak)id<RecordHeartBeatViewDelegate> iDelegate;

- (void) deleteData;
- (void) setupData;

@end


