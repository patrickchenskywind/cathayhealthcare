//
//  RecordWeightView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class RecordWeightView;
@protocol RecordWeightViewDelegate <NSObject>

    -(void)deleteRecordViewShowWithView: (RecordWeightView *)view date: (NSString *)date weight: (NSString *)weight;

@end

@interface RecordWeightView : View

@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (nonatomic, strong) IBOutlet UITextField *text_date;
@property (nonatomic, strong) IBOutlet UITextField *text_weight;
@property (nonatomic, strong) IBOutlet UIButton    *button_send;
@property (nonatomic, strong) IBOutlet UITableView *table_record;

@property (nonatomic, weak)id<RecordWeightViewDelegate> delegate;

- (void) deleteData;

@end
