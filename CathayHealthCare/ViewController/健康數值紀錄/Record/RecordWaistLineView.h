//
//  RecordWaistLineView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class RecordWaistLineView;
@protocol RecordWaistLineViewDelegate <NSObject>

-(void)deleteRecordViewShowWithView: (RecordWaistLineView *)view date: (NSString *)date waistLine: (NSString *)waistLine;

@end

@interface RecordWaistLineView : View

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic, strong) IBOutlet UITextField *text_date;
@property (nonatomic, strong) IBOutlet UITextField *text_cm;
@property (nonatomic, strong) IBOutlet UIButton *button_send;
@property (nonatomic, strong) IBOutlet UITableView *table_record;

@property (nonatomic, weak)id<RecordWaistLineViewDelegate> iDelegate;

- (void) deleteData;
- (void) setupData;

@end
