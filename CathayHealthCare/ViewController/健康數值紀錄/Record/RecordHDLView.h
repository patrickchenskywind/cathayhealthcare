//
//  RecordHDLView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class RecordHDLView;
@protocol RecordHDLViewDelegate <NSObject>

-(void)deleteRecordViewShowWithView: (RecordHDLView *)view date: (NSString *)date hdl: (NSString *)hdl;

@end

@interface RecordHDLView : View

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic,strong) IBOutlet UITextField *text_date;
@property (nonatomic,strong) IBOutlet UITextField *text_hdl;
@property (nonatomic,strong) IBOutlet UIButton    *button_send;
@property (nonatomic, strong) IBOutlet UITableView *table_record;
@property (nonatomic, weak) id<RecordHDLViewDelegate> iDelegate;

- (void) deleteData;
- (void) setupData;

@end
