//
//  RecordTGView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class RecordTGView;
@protocol RecordTGViewDelegate <NSObject>

-(void)deleteRecordViewShowWithView: (RecordTGView *)view date: (NSString *)date tg: (NSString *)tg;

@end

@interface RecordTGView : View

@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (nonatomic,strong) IBOutlet UITextField *text_date;
@property (nonatomic,strong) IBOutlet UITextField *text_tg;
@property (nonatomic,strong) IBOutlet UIButton    *button_send;
@property (nonatomic, strong) IBOutlet UITableView *table_record;

@property (nonatomic, weak)id<RecordTGViewDelegate> iDelegate;

- (void) deleteData;
- (void) setupData;

@end






