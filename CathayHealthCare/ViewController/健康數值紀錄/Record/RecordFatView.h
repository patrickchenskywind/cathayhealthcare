//
//  RecordFatView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
@class RecordFatView;
@protocol RecordFatViewDelegate <NSObject>

    -(void)deleteRecordViewShowWithView: (RecordFatView *)view date: (NSString *)date fat: (NSString *)fat;

@end

@interface RecordFatView : View

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic, strong) IBOutlet UITextField *text_date;
@property (nonatomic, strong) IBOutlet UITextField *text_percent;
@property (nonatomic, strong) IBOutlet UIButton *button_send;
@property (nonatomic, strong) IBOutlet UITableView *table_record;

@property (nonatomic, weak)id<RecordFatViewDelegate> iDelegate;

- (void) deleteData;
- (void) setupData;

@end
