//
//  RecordWaistLineView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordWaistLineView.h"
#import "UIColor+Hex.h"
#import "Toos.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "SetPhyWaistlineAPI.h"
#import "RecordGeneralCell.h"

@interface RecordWaistLineView ()<UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, IDatePickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *didUsedDate;
@property (strong, nonatomic) NSString *didUsedWaistLine;
@property (strong, nonatomic) NSArray *waistLineData;
@property (strong, nonatomic) NSIndexPath *indexPathDidSelect;

@end

@implementation RecordWaistLineView

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_text_date.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_date.layer setBorderWidth:1];
    [_text_date.layer setCornerRadius:5];
    [_text_cm.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_cm.layer setBorderWidth:1];
    [_text_cm.layer setCornerRadius:5];
    
    [_button_send.layer setCornerRadius:5];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(closeKeyboard)];
    [tapGestureRecognizer setDelegate: self];
    [self.viewContainer addGestureRecognizer: tapGestureRecognizer];
}

#pragma mark - IBAction
- (IBAction)buttonDatePress:(UIButton *)sender {
    [self closeKeyboard];
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    [self setDate: date];
    [self.text_date setText: [Toos getYMDFormDate: date]];
    [self.text_date adjustsFontSizeToFitWidth];
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
    if (self.text_date.text.length > 0 && self.text_cm.text.length > 0) {
        if ( ! [self.date isEqualToDate: self.didUsedDate] && ! [self.text_cm.text isEqualToString: self.didUsedWaistLine]) {
            SetPhyWaistlineAPI *api = [[SetPhyWaistlineAPI alloc] init];
            [api setDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
            [api setWaistline: self.text_cm.text];
            [api post:^(HttpResponse *response) {
                PhyWaistlineParentEntity *phyWaistlineParentEntity = response.result.firstObject;
                if ( [Toos checkMsgCodeWihtMsg: phyWaistlineParentEntity.msg] ) {
                    [self setDidUsedDate: self.date];
                    [self setDidUsedWaistLine: self.text_cm.text];
                    [self setupDataWihtArray: phyWaistlineParentEntity.arrayWaistline];
                }
            }];
        }
    }
}

#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.waistLineData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RecordGeneralCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RecordGeneralCell"];
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"RecordGeneralCell" owner: self options: nil] objectAtIndex: 0];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    PhyWaistlineEntity *phyWaistlineEntity = self.waistLineData[indexPath.row];
    [cell.leableLeft setText: [Toos getYMDStringFormServerTimeInterval: phyWaistlineEntity.date]];
    [cell.leableRight setText:  [NSString stringWithFormat: @"%@公分",phyWaistlineEntity.waistline]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setIndexPathDidSelect: indexPath];
    PhyWaistlineEntity *phyWaistlineEntity = self.waistLineData[indexPath.row];
    [self.iDelegate deleteRecordViewShowWithView: self date: [Toos getYMDStringFormServerTimeInterval: phyWaistlineEntity.date] waistLine: [NSString stringWithFormat:@"%@ 公分",phyWaistlineEntity.waistline]];
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if( ! [touch.view isEqual: self.viewContainer]) {
        return NO;
    }
    return YES;
}

- (void) setupData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetPhyWaistlineAPI *api = [[SetPhyWaistlineAPI alloc] init];
        [api post:^(HttpResponse *response) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            PhyWaistlineParentEntity *phyWaistlineParentEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: phyWaistlineParentEntity.msg]) {
                if (phyWaistlineParentEntity.arrayWaistline.count > 0) {
                    [self setupDataWihtArray: phyWaistlineParentEntity.arrayWaistline];
                }
            }
        }];
    }
}

- (void) setupDataWihtArray:(NSArray*)array {
    [self setWaistLineData: array];
    [self.table_record reloadData];
}

- (void) deleteData {
    SetPhyWaistlineAPI *api = [[SetPhyWaistlineAPI alloc] init];
    [api setWid: ((PhyWaistlineEntity *)self.waistLineData[self.indexPathDidSelect.row]).wid];
    [api post:^(HttpResponse *response) {
        PhyWaistlineParentEntity *phyWaistlineParentEntity = response.result.firstObject;
        if ( [Toos checkMsgCodeWihtMsg: phyWaistlineParentEntity.msg] ) {
            [self setupDataWihtArray: phyWaistlineParentEntity.arrayWaistline];
        }
    }];
}

@end
