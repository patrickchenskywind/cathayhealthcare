//
//  RecordTGView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordTGView.h"
#import "UIColor+Hex.h"
#import "Toos.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "SetPhyTGAPI.h"
#import "RecordGeneralCell.h"

@interface RecordTGView ()<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, IDatePickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *didUsedDate;
@property (strong, nonatomic) NSString *didUsedTG;
@property (strong, nonatomic) NSArray *tgData;
@property (strong, nonatomic) NSIndexPath *indexPathDidSelect;

@end

@implementation RecordTGView
- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_text_date.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_date.layer setBorderWidth:1];
    [_text_date.layer setCornerRadius:5];
    [_text_tg.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_tg.layer setBorderWidth:1];
    [_text_tg.layer setCornerRadius:5];
    
    [_button_send.layer setCornerRadius:5];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(closeKeyboard)];
    [tapGestureRecognizer setDelegate: self];
    [self.viewContainer addGestureRecognizer: tapGestureRecognizer];
}

#pragma mark - IBAction
- (IBAction)buttonDatePress:(UIButton *)sender {
    [self closeKeyboard];
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    [self setDate: date];
    [self.text_date setText: [Toos getYMDFormDate: date]];
    [self.text_date adjustsFontSizeToFitWidth];
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
    if (self.text_date.text.length > 0 && self.text_tg.text.length > 0) {
        if ( ! [self.date isEqualToDate: self.didUsedDate] && ! [self.text_tg.text isEqualToString: self.didUsedTG]) {
            SetPhyTGAPI *api = [[SetPhyTGAPI alloc] init];
            [api setDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
            [api setTg: self.text_tg.text];
            [api post:^(HttpResponse *response) {
                PhyTGParentEntity *phyTGParentEntity = response.result.firstObject;
                if ( [Toos checkMsgCodeWihtMsg: phyTGParentEntity.msg] ) {
                    [self setupDataWihtArray: phyTGParentEntity.arrayTg];
                }
            }];
        }
    }
}

#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tgData.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RecordGeneralCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RecordGeneralCell"];
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"RecordGeneralCell" owner: self options: nil] objectAtIndex: 0];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    PhyTGEntity *phyTGEntity = self.tgData[indexPath.row];
    [cell.leableLeft setText: [Toos getYMDStringFormServerTimeInterval: phyTGEntity.date]];
    [cell.leableRight setText:  [NSString stringWithFormat: @"%@mg/dL",phyTGEntity.tg]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setIndexPathDidSelect: indexPath];
    PhyTGEntity *phyTGEntity = self.tgData[indexPath.row];
    [self.iDelegate deleteRecordViewShowWithView: self date: [Toos getYMDStringFormServerTimeInterval: phyTGEntity.date] tg: [NSString stringWithFormat: @"%@mg/dL",phyTGEntity.tg]];
}

- (void) setupData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetPhyTGAPI *api = [[SetPhyTGAPI alloc] init];
        [api post:^(HttpResponse *response) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            PhyTGParentEntity *phyTGParentEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: phyTGParentEntity.msg]) {
                if (phyTGParentEntity.arrayTg.count > 0) {
                    [self setupDataWihtArray: phyTGParentEntity.arrayTg];
                }
            }
        }];
    }
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if( ! [touch.view isEqual: self.viewContainer]) {
        return NO;
    }
    return YES;
}

- (void) setupDataWihtArray:(NSArray*)array {
    [self setTgData: array];
    [self.table_record reloadData];
}

- (void) deleteData {
    SetPhyTGAPI *api = [[SetPhyTGAPI alloc] init];
    [api setTgid: ((PhyTGEntity *)self.tgData[self.indexPathDidSelect.row]).tgid];
    [api post:^(HttpResponse *response) {
        PhyTGParentEntity *phyTGParentEntity = response.result.firstObject;
        if ( [Toos checkMsgCodeWihtMsg: phyTGParentEntity.msg] ) {
            [self setupDataWihtArray: phyTGParentEntity.arrayTg];
        }
    }];
}

@end
