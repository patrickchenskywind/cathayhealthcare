//
//  RecordFatView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordFatView.h"
#import "UIColor+Hex.h"
#import "Toos.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "SetPhyFatAPI.h"
#import "RecordGeneralCell.h"

@interface RecordFatView ()<UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, IDatePickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *didUsedDate;
@property (strong, nonatomic) NSString *didUsedFat;
@property (strong, nonatomic) NSArray *fatData;
@property (strong, nonatomic) NSIndexPath *indexPathDidSelect;

@end

@implementation RecordFatView

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_text_date.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_date.layer setBorderWidth:1];
    [_text_date.layer setCornerRadius:5];
    [_text_percent.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_percent.layer setBorderWidth:1];
    [_text_percent.layer setCornerRadius:5];
    
    [_button_send.layer setCornerRadius:5];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(closeKeyboard)];
    [tapGestureRecognizer setDelegate: self];
    [self.viewContainer addGestureRecognizer: tapGestureRecognizer];
}

#pragma mark - IBAction
- (IBAction)buttonDatePress:(UIButton *)sender {
    [self closeKeyboard];
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    [self setDate: date];
    [self.text_date setText: [Toos getYMDFormDate: date]];
    [self.text_date adjustsFontSizeToFitWidth];
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
    if (self.text_date.text.length > 0 && self.text_percent.text.length > 0) {
        if ( ! [self.date isEqualToDate: self.didUsedDate] && ! [self.text_percent.text isEqualToString: self.didUsedFat]) {
            SetPhyFatAPI *api = [[SetPhyFatAPI alloc] init];
            [api setDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
            [api setFat: self.text_percent.text];
            [api post:^(HttpResponse *response) {
                PhyFatParentEntity *phyFatParentEntity = response.result.firstObject;
                if ( [Toos checkMsgCodeWihtMsg: phyFatParentEntity.msg] ) {
                    
                    [self setupDataWihtArray: phyFatParentEntity.arrayFat];
                }
            }];
        }
    }
}

#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fatData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RecordGeneralCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RecordGeneralCell"];
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"RecordGeneralCell" owner: self options: nil] objectAtIndex: 0];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    PhyFatEntity *phyFatEntity = self.fatData[indexPath.row];
    [cell.leableLeft setText: [Toos getYMDStringFormServerTimeInterval: phyFatEntity.date]];
    [cell.leableRight setText:  [NSString stringWithFormat: @"%@%@",phyFatEntity.fat,@"%"]];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setIndexPathDidSelect: indexPath];
    PhyFatEntity *phyFatEntity = self.fatData[indexPath.row];
    [self.iDelegate deleteRecordViewShowWithView: self date: [Toos getYMDStringFormServerTimeInterval: phyFatEntity.date] fat: [NSString stringWithFormat:@"%@ %@",phyFatEntity.fat,@"%"]];
}

- (void) setupData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetPhyFatAPI *api = [[SetPhyFatAPI alloc] init];
        [api post:^(HttpResponse *response) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            PhyFatParentEntity *phyFatParentEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: phyFatParentEntity.msg]) {
                if (phyFatParentEntity.arrayFat.count > 0) {
                    [self setupDataWihtArray: phyFatParentEntity.arrayFat]; 
                }
            }
        }];
    }
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if( ! [touch.view isEqual: self.viewContainer]) {
        return NO;
    }
    return YES;
}

- (void) setupDataWihtArray:(NSArray*)array {
    [self setFatData: array];
    [self.table_record reloadData];
}

- (void) deleteData {
    SetPhyFatAPI *api = [[SetPhyFatAPI alloc] init];
    [api setFid: ((PhyFatEntity *)self.fatData[self.indexPathDidSelect.row]).fid];
    [api post:^(HttpResponse *response) {
        PhyFatParentEntity *phyFatParentEntity = response.result.firstObject;
        if ( [Toos checkMsgCodeWihtMsg: phyFatParentEntity.msg] ) {
            [self setupDataWihtArray: phyFatParentEntity.arrayFat];
        }
    }];
}

@end
