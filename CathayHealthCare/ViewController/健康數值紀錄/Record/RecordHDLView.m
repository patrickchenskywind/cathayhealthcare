//
//  RecordHDLView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordHDLView.h"
#import "UIColor+Hex.h"
#import "Toos.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "SetPhyHDLAPI.h"
#import "RecordGeneralCell.h"

@interface RecordHDLView ()<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, IDatePickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *didUsedDate;
@property (strong, nonatomic) NSString *didUsedHDL;
@property (strong, nonatomic) NSArray *hdlData;
@property (strong, nonatomic) NSIndexPath *indexPathDidSelect;

@end

@implementation RecordHDLView
- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_text_date.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_date.layer setBorderWidth:1];
    [_text_date.layer setCornerRadius:5];
    [_text_hdl.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_hdl.layer setBorderWidth:1];
    [_text_hdl.layer setCornerRadius:5];
    
    [_button_send.layer setCornerRadius:5];
    
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(closeKeyboard)];
    [tapGestureRecognizer setDelegate: self];
    [self.viewContainer addGestureRecognizer: tapGestureRecognizer];
    
}

#pragma mark - IBAction
- (IBAction)buttonDatePress:(UIButton *)sender {
    [self closeKeyboard];
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    [self setDate: date];
    [self.text_date setText: [Toos getYMDFormDate: date]];
    [self.text_date adjustsFontSizeToFitWidth];
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
    if (self.text_date.text.length > 0 && self.text_hdl.text.length > 0) {
        if ( ! [self.date isEqualToDate: self.didUsedDate] && ! [self.text_hdl.text isEqualToString: self.didUsedHDL]) {
            SetPhyHDLAPI *api = [[SetPhyHDLAPI alloc] init];
            [api setDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
            [api setHdl: self.text_hdl.text];
            [api post:^(HttpResponse *response) {
                PhyHDLParentEntity *phyHDLParentEntity = response.result.firstObject;
                if ( [Toos checkMsgCodeWihtMsg: phyHDLParentEntity.msg] ) {
                    [self setupDataWihtArray: phyHDLParentEntity.arrayHdl];
                }
            }];
        }
    }
}

#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.hdlData.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RecordGeneralCell *cell = [tableView dequeueReusableCellWithIdentifier: @"RecordGeneralCell"];
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"RecordGeneralCell" owner: self options: nil] objectAtIndex: 0];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    PhyHDLEntity *phyHDLEntity = self.hdlData[indexPath.row];
    [cell.leableLeft setText: [Toos getYMDStringFormServerTimeInterval: phyHDLEntity.date]];
    [cell.leableRight setText:  [NSString stringWithFormat: @"%@mg/dL",phyHDLEntity.hdl]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setIndexPathDidSelect: indexPath];
    PhyHDLEntity *phyHDLEntity = self.hdlData[indexPath.row];
    [self.iDelegate deleteRecordViewShowWithView: self date: [Toos getYMDStringFormServerTimeInterval: phyHDLEntity.date] hdl: [NSString stringWithFormat: @"%@mg/dL",phyHDLEntity.hdl]];
}

- (void) setupData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetPhyHDLAPI *api = [[SetPhyHDLAPI alloc] init];
        [api post:^(HttpResponse *response) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            PhyHDLParentEntity *phyHDLParentEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: phyHDLParentEntity.msg]) {
                if (phyHDLParentEntity.arrayHdl.count > 0) {
                    [self setupDataWihtArray: phyHDLParentEntity.arrayHdl];
                }
            }
        }];
    }
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if( ! [touch.view isEqual: self.viewContainer]) {
        return NO;
    }
    return YES;
}

- (void) setupDataWihtArray:(NSArray*)array {
    [self setHdlData: array];
    [self.table_record reloadData];
}

- (void) deleteData {
    SetPhyHDLAPI *api = [[SetPhyHDLAPI alloc] init];
    [api setHdlid: ((PhyHDLEntity *)self.hdlData[self.indexPathDidSelect.row]).hdlid];
    [api post:^(HttpResponse *response) {
        PhyHDLParentEntity *phyHDLParentEntity = response.result.firstObject;
        if ( [Toos checkMsgCodeWihtMsg: phyHDLParentEntity.msg] ) {
            [self setupDataWihtArray: phyHDLParentEntity.arrayHdl];
        }
    }];
}

@end
