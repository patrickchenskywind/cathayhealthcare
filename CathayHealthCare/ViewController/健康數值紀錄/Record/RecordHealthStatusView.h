//
//  RecordHealthStatusView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "View.h"

@interface RecordHealthStatusView : View<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic,strong) IBOutlet UITextField *text_date;
@property (nonatomic,strong) IBOutlet UITextField *text_Symptom;
@property (nonatomic,strong) IBOutlet UITextView  *text_handle;
@property (nonatomic,strong) IBOutlet UITableView *table_list;
@property (nonatomic,strong) IBOutlet UIImageView *image_camera;
@property (nonatomic,strong) IBOutlet UIButton    *button_send;

@property (nonatomic,strong) NSMutableArray *array_healthRecord;

@end
