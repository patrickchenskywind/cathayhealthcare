//
//  RecordBloodSugarView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordBloodSugarView.h"
#import "UIColor+Hex.h"
#import "Toos.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "SetPhyBSAPI.h"
#import "BSCell.h"

@interface RecordBloodSugarView ()<UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, IDatePickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSDate *didUsedDate;
@property (strong, nonatomic) NSString *didUsedBefore;
@property (strong, nonatomic) NSString *didUsedAfter;
@property (strong, nonatomic) NSString *didUsedHemoglobin;
@property (strong, nonatomic) NSArray *bloodSugarData;
@property (strong, nonatomic) NSIndexPath *indexPathDidSelect;

@end

@implementation RecordBloodSugarView

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        [self setupData];
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_text_date.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_date.layer setBorderWidth:1];
    [_text_date.layer setCornerRadius:5];
    
    [_text_after.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_after.layer setBorderWidth:1];
    [_text_after.layer setCornerRadius:5];
    
    [_text_before.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_before.layer setBorderWidth:1];
    [_text_before.layer setCornerRadius:5];
    
    [_text_Hemoglobin.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [_text_Hemoglobin.layer setBorderWidth:1];
    [_text_Hemoglobin.layer setCornerRadius:5];
    
    
    [_button_send.layer setCornerRadius:5];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(closeKeyboard)];
    [tapGestureRecognizer setDelegate: self];
    [self.viewContainer addGestureRecognizer: tapGestureRecognizer];
}

#pragma mark - IBAction
- (IBAction)buttonDatePress:(UIButton *)sender {
    [self closeKeyboard];
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    [self setDate: date];
    [self.text_date setText: [Toos getYMDFormDate: date]];
    [self.text_date adjustsFontSizeToFitWidth];
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
    if (self.text_date.text.length > 0 && self.text_before.text.length > 0 && self.text_after.text.length > 0 && self.text_Hemoglobin.text.length > 0) {
        if ( ! [self.date isEqualToDate: self.didUsedDate] &&
            ! [self.text_before.text isEqualToString: self.didUsedBefore] &&
            ! [self.text_after.text isEqualToString: self.didUsedAfter] &&
            ! [self.text_Hemoglobin.text isEqualToString: self.didUsedHemoglobin]
            ) {
            
            SetPhyBSAPI *api = [[SetPhyBSAPI alloc] init];
            [api setDate: [Toos getThousandSecondStringFromDate: [Toos getDateFormString: self.text_date.text]]];
            [api setBeforeDine: self.text_before.text];
            [api setAftreDine: self.text_after.text];
            [api setHbA1c: self.text_Hemoglobin.text];
            [api post:^(HttpResponse *response) {
                [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
                PhyBSParentEntity *phyBSParentEntity = response.result.firstObject;
                if ([Toos checkMsgCodeWihtMsg: phyBSParentEntity.msg]) {
                    [self setDidUsedDate: self.date];
                    [self setDidUsedBefore: self.text_before.text];
                    [self setDidUsedAfter: self.text_after.text];
                    [self setDidUsedHemoglobin: self.text_Hemoglobin.text];
                    [self setupDataWihtArray: phyBSParentEntity.arrayBs];
                }
            }];
        }
    }
}


#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bloodSugarData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BSCell *cell = [tableView dequeueReusableCellWithIdentifier: @"BSCell"];
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"BSCell" owner: self options: nil] objectAtIndex: 0];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    
    PhyBSEntity *phyBSEntity = self.bloodSugarData[indexPath.row];
    
    [cell.labelDate setText: [Toos getYMDStringFormServerTimeInterval: phyBSEntity.date]];
    [cell.labelBefore setText:  [NSString stringWithFormat: @"%@mmHg",phyBSEntity.beforeDine]];
    [cell.labelAfter setText:  [NSString stringWithFormat: @"%@mmHg",phyBSEntity.aftreDine]];
    [cell.labelHemoglobin setText:  [NSString stringWithFormat: @"%@次/分",phyBSEntity.HbA1c]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self setIndexPathDidSelect: indexPath];
    PhyBSEntity *phyBSEntity = self.bloodSugarData[indexPath.row];
    [self.iDelegate deleteRecordViewShowWithView: self phyBSEntity: phyBSEntity];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 125;
}


#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if( ! [touch.view isEqual: self.viewContainer]) {
        return NO;
    }
    return YES;
}

- (void) deleteData {
    SetPhyBSAPI *api = [[SetPhyBSAPI alloc] init];
    [api setBid: ((PhyBSEntity *)self.bloodSugarData[self.indexPathDidSelect.row]).bid];
    [api post:^(HttpResponse *response) {
        [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
        PhyBSParentEntity *phyBPParentEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg: phyBPParentEntity.msg]) {
            [self setupDataWihtArray: phyBPParentEntity.arrayBs];
        }
    }];
}

- (void) setupData {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetPhyBSAPI *api = [[SetPhyBSAPI alloc] init];
        [api post:^(HttpResponse *response) {
            [[NSNotificationCenter defaultCenter] postNotificationName: kNSNotificationCloseMBProgressHUD object: nil];
            PhyBSParentEntity *phyBSParentEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: phyBSParentEntity.msg]) {
                if (phyBSParentEntity.arrayBs.count > 0) {
                    [self setupDataWihtArray: phyBSParentEntity.arrayBs];
                }
            }
        }];
    }
}

- (void) setupDataWihtArray:(NSArray *)array {
    [self setBloodSugarData: array];
    [self.tableView reloadData];
}

@end
