//
//  HealthRecordModel.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/25.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HealthRecordModel.h"
#import "AppConfig.h"

@implementation HealthRecordModel


+(instancetype)sharedInstance
{
    static HealthRecordModel *sharedInstance ;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        

        
    });
    return sharedInstance;
}

-(id)init{
    
    self = [super init];
    
    if (self) {
        
        /*
         假資料 之後從api抓
         */
        
        _dic_buttonSwitch = @{BUTTON_IDENTIFY_WEIGHT          :@YES,
                              BUTTON_IDENTIFY_FAT             :@YES,
                              BUTTON_IDENTIFY_WAISTLINE       :@YES,
                              BUTTON_IDENTIFY_HEARTBEAT       :@YES,
                              BUTTON_IDENTIFY_BLOODSUGAR      :@YES,
                              BUTTON_IDENTIFY_TG              :@YES,
                              BUTTON_IDENTIFY_HDL             :@YES,
                              BUTTON_IDENTIFY_SLEEP           :@YES,
                              BUTTON_IDENTIFY_HEALTHSTATUS    :@YES};
        /*
         為了照流程圖的順序，改用array紀錄title
         
        _dic_buttonTitle = @{BUTTON_WEIGHT_ID           :@"體重",
                             BUTTON_WAISTLINE_ID        :@"腰圍",
                             BUTTON_FAT_ID              :@"體脂",
                             BUTTON_HEARTBEAT_ID        :@"血壓/脈搏",
                             BUTTON_BLOODSUGAR_ID       :@"血糖",
                             BUTTON_TG_ID               :@"三酸甘油脂",
                             BUTTON_HDL_ID              :@"高密度脂蛋白",
                             BUTTON_SLEEP_ID            :@"睡眠",
                             BUTTON_HEALTHSTATUS_ID     :@"健康狀況"};
         */
        _array_buttonTitle = @[[NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_WEIGHT,@"體重"],
                               [NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_FAT,@"體脂"],
                               [NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_WAISTLINE,@"腰圍"],
                               [NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_HEARTBEAT,@"血壓/脈搏"],
                               [NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_BLOODSUGAR,@"血糖"],
                               [NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_TG,@"三酸甘油脂"],
                               [NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_HDL,@"高密度脂蛋白"],
                               [NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_SLEEP,@"睡眠"],
                               [NSString stringWithFormat:@"%@,%@",BUTTON_IDENTIFY_HEALTHSTATUS,@"健康狀況"]
                               ];
        
        _array_weightHistory = @[@{@"date":@"20150101",@"value":@"123"},
                                 @{@"date":@"20150101",@"value":@"123"},
                                 @{@"date":@"20150101",@"value":@"123"},
                                 @{@"date":@"20150101",@"value":@"123"},
                                 @{@"date":@"20150101",@"value":@"123"}];
        
        _array_waistlineHistory = @[@{@"date":@"20150101",@"value":@"123"},
                                    @{@"date":@"20150101",@"value":@"123"},
                                    @{@"date":@"20150101",@"value":@"123"},
                                    @{@"date":@"20150101",@"value":@"123"},
                                    @{@"date":@"20150101",@"value":@"123"}];
        
        _array_fatHistory = @[@{@"date":@"20150101",@"value":@"123"},
                              @{@"date":@"20150101",@"value":@"123"},
                              @{@"date":@"20150101",@"value":@"123"},
                              @{@"date":@"20150101",@"value":@"123"},
                              @{@"date":@"20150101",@"value":@"123"}];
        
        _array_heartBeatHistory = @[@{@"date":@"20150101",@"value":@"123"},
                                    @{@"date":@"20150101",@"value":@"123"},
                                    @{@"date":@"20150101",@"value":@"123"},
                                    @{@"date":@"20150101",@"value":@"123"},
                                    @{@"date":@"20150101",@"value":@"123"}];
        
        _array_bloodSugarHistory = @[@{@"date":@"20150101",@"value":@"123"},
                                     @{@"date":@"20150101",@"value":@"123"},
                                     @{@"date":@"20150101",@"value":@"123"},
                                     @{@"date":@"20150101",@"value":@"123"},
                                     @{@"date":@"20150101",@"value":@"123"}];
        
        _array_TGHistory = @[@{@"date":@"20150101",@"value":@"123"},
                             @{@"date":@"20150101",@"value":@"123"},
                             @{@"date":@"20150101",@"value":@"123"},
                             @{@"date":@"20150101",@"value":@"123"},
                             @{@"date":@"20150101",@"value":@"123"}];
        
        _array_HDLHistory = @[@{@"date":@"20150101",@"value":@"123"},
                              @{@"date":@"20150101",@"value":@"123"},
                              @{@"date":@"20150101",@"value":@"123"},
                              @{@"date":@"20150101",@"value":@"123"},
                              @{@"date":@"20150101",@"value":@"123"}];
        
        _array_sleepHistory = @[@{@"date":@"20150101",@"value":@"123"},
                                @{@"date":@"20150101",@"value":@"123"},
                                @{@"date":@"20150101",@"value":@"123"},
                                @{@"date":@"20150101",@"value":@"123"},
                                @{@"date":@"20150101",@"value":@"123"}];
        
        _array_healthStatusHistory = @[@{@"date":@"20150101",@"value":@"123"},
                                       @{@"date":@"20150101",@"value":@"123"},
                                       @{@"date":@"20150101",@"value":@"123"},
                                       @{@"date":@"20150101",@"value":@"123"},
                                       @{@"date":@"20150101",@"value":@"123"}];
    }
    return self;
}

@end
