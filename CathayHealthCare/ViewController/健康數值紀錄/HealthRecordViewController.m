//
//  HealthRecordViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HealthRecordViewController.h"
#import "RecordSettingViewController.h"
#import "NewRegistViewController.h"
#import "ForgotPasswordViewController.h"
#import "DefindConfig.h"
#import "LoginView.h"
#import "PhotoFromIPC.h"
#import "UIImageCrop.h"
#import "Toos.h"


@interface HealthRecordViewController () <LoginViewDelegate, IHealthRecordViewDelegate,PhotoFromIPCDelegate, UIImageCropDelegate>
@property (nonatomic,strong) LoginView *view_login;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonWeightHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonFatHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonWaislineHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonHearbeatHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonBloodSugarHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonTGHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonHDLHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonSleepHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonHealthStatusHeight;

@property (strong, nonatomic) PhotoFromIPC *photoFromIPC;

@end

@implementation HealthRecordViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self changeSettingButtonStyle];
    [self setTitle: @"健康數值紀錄"];
    [self.view.buttonWeight setSelected: YES];
    
    [self.view setIDelegate: self];
    
    [self setPhotoFromIPC: [[PhotoFromIPC alloc] initWithDelegate: self  viewController: self]];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed: @"titlebar_lightgreen"] forBarMetrics: UIBarMetricsDefault];
    if ([[NSUserDefaults standardUserDefaults] objectForKey: DICTIONARY_HEALTHLISTSTATUS]) {
        NSDictionary *dictionary = [NSDictionary dictionary];
        dictionary = [[NSUserDefaults standardUserDefaults] objectForKey: DICTIONARY_HEALTHLISTSTATUS];
        [self setupButtonShow: dictionary key: DICTIONARY_HEALTHLISTSTATUS_KEY_FAT constrain: self.constraintButtonFatHeight];
        [self setupButtonShow: dictionary key: DICTIONARY_HEALTHLISTSTATUS_KEY_WAISTLINE constrain: self.constraintButtonWaislineHeight];
        [self setupButtonShow: dictionary key: DICTIONARY_HEALTHLISTSTATUS_KEY_HEARTBEAT constrain: self.constraintButtonHearbeatHeight];
        [self setupButtonShow: dictionary key: DICTIONARY_HEALTHLISTSTATUS_KEY_BLOODSUGAR constrain: self.constraintButtonBloodSugarHeight];
        [self setupButtonShow: dictionary key: DICTIONARY_HEALTHLISTSTATUS_KEY_TG constrain: self.constraintButtonTGHeight];
        [self setupButtonShow: dictionary key: DICTIONARY_HEALTHLISTSTATUS_KEY_HDL constrain: self.constraintButtonHDLHeight];
        [self setupButtonShow: dictionary key: DICTIONARY_HEALTHLISTSTATUS_KEY_SLEEP constrain: self.constraintButtonSleepHeight];
        [self setupButtonShow: dictionary key: DICTIONARY_HEALTHLISTSTATUS_KEY_STATUS constrain: self.constraintButtonHealthStatusHeight];
    }
}

- (void) setupButtonShow: (NSDictionary *) dictionary key: (NSString *)key constrain:(NSLayoutConstraint *)constrain {
    if ( [[dictionary objectForKey: key] isEqualToNumber: [NSNumber numberWithBool: NO]])
        [constrain setConstant: 0];
    else
        [constrain setConstant: 51];
}

#pragma mark - LoginViewDelegate
-(void)showRegistView {
    [self.navigationController pushViewController: [[NewRegistViewController alloc] initWithNibName: @"NewRegistViewController" bundle: nil] animated: YES];
}

- (void) forgetPasswordButtonPress {
    [self.navigationController pushViewController: [[ForgotPasswordViewController alloc] initWithNibName: @"ForgotPasswordViewController" bundle: nil] animated: YES];
}

-(void)dissMissLoginView {
    [self.view_login.view_mask removeFromSuperview];
    [self.view_login setView_mask: nil];
    [self.view_login removeFromSuperview];
    [self setView_login: nil];
}

-(void)changeSettingButtonStyle {
    UIButton *settingButton = [UIButton buttonWithType: UIButtonTypeCustom];
    [settingButton setFrame:CGRectMake(0,
                                       0,
                                       self.navigationController.navigationBar.frame.size.height,
                                       self.navigationController.navigationBar.frame.size.height)];
    [settingButton setImage: [UIImage imageNamed:@"btn_setting"] forState: UIControlStateNormal];
    [settingButton addTarget: self action: @selector(settingButtonPress:) forControlEvents: UIControlEventTouchUpInside];
    [self.navigationItem setRightBarButtonItem: [[UIBarButtonItem alloc] initWithCustomView: settingButton]];
}

-(void)settingButtonPress:(id)button {
    [self.navigationController pushViewController: [[RecordSettingViewController alloc] initWithNibName: @"RecordSettingViewController" bundle:nil] animated: YES];
}

- (IBAction)buttonWeightPress:(UIButton *)sender {
    [self changButtonSelest: sender];
    [self.view.mainView bringSubviewToFront: self.view.view_weight];
}

- (IBAction)buttonBodyFatPress:(UIButton *)sender {
    if ([self checkPermission]) {
        [self changButtonSelest: sender];
        [self.view.mainView bringSubviewToFront: self.view.view_fat];
    }
}

- (IBAction)buttonWaistPress:(UIButton *)sender {
    if ([self checkPermission]) {
        [self changButtonSelest: sender];
        [self.view.mainView bringSubviewToFront: self.view.view_waistLine];
    }
}

- (IBAction)buttonHearbeatPress:(UIButton *)sender {
    if ([self checkPermission]) {
        [self changButtonSelest: sender];
        [self.view.mainView bringSubviewToFront: self.view.view_heartBeat];
    }
}

- (IBAction)buttonSugarPress:(UIButton *)sender {
    if ([self checkPermission]) {
        [self changButtonSelest: sender];
        [self.view.mainView bringSubviewToFront: self.view.view_bloodSugar];
    }
}

- (IBAction)buttonTGPress:(UIButton *)sender {
    if ([self checkPermission]) {
        [self changButtonSelest: sender];
        [self.view.mainView bringSubviewToFront: self.view.view_TG];
    }
}

- (IBAction)buttonHDLPress:(UIButton *)sender {
    if ([self checkPermission]) {
        [self changButtonSelest: sender];
        [self.view.mainView bringSubviewToFront: self.view.view_HDL];
    }
}

- (IBAction)buttonSleepPress:(UIButton *)sender {
    if ([self checkPermission]) {
        [self changButtonSelest: sender];
        [self.view.mainView bringSubviewToFront: self.view.view_sleep];
    }
}

- (IBAction)buttonHealthPress:(UIButton *)sender {
    if ([self checkPermission]) {
        [self changButtonSelest: sender];
        [self.view.mainView bringSubviewToFront: self.view.view_healthStatus];
    }
}

- (void) changButtonSelest: (UIButton *) sender {
    [sender setSelected: YES];
    for (id obj in self.view.view_button.subviews) {
        if ([obj isKindOfClass: [UIButton class]]) {
            if ( ! [obj isEqual: sender]) {
                [obj setSelected: NO];
                [self closeKeyboard];
            }
        }
    }
}

- (BOOL) checkPermission {
    if ( ! [[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        if (!self.view_login) {
            [self setView_login: [[[NSBundle mainBundle] loadNibNamed: @"LoginView" owner: self options: nil] objectAtIndex: 0]];
            [self.view_login setFrame: CGRectMake(0,
                                                  0,
                                                  CGRectGetWidth(self.view.bounds) - CGRectGetWidth(self.view.bounds) / 9,
                                                  CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.view.bounds) / 3)];
            
            [self.view_login setCenter: CGPointMake(CGRectGetWidth(self.view.bounds) / 2,
                                                    CGRectGetHeight(self.view.bounds) / 2)];
            [self.view_login setDelegate: self];
            [self.view addSubview: self.view_login];
        }
        return NO;
    }
    else {
        return  YES;
    }
}

#pragma mark - IHealthRecordViewDelegate
- (void) buttonPhotoPress {
    [self.photoFromIPC show: nil];
}

#pragma mark - PhotoFromIPCDelegate
- (void) getPhoto: (UIImage *)image {
    UIImageCrop *imageCrop = [[UIImageCrop alloc] init];
    [imageCrop setDelegate: self];
    [imageCrop setRatioOfWidthAndHeight: 200 / 200];
    [imageCrop setImage: image];
    [imageCrop showWithAnimation: NO];
}

#pragma mark - UIImageCropDelegate
- (void) cropImage: (UIImage *)cropImage forOriginalImage: (UIImage *) originalImage {
    [self.view.view_sleep setupImageViewWithImage: cropImage base64String: [Toos getBase64StringFromImage: cropImage]];
}

@end
