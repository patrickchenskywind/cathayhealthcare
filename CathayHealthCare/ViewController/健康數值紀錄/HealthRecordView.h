//
//  HealthRecordView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

#import "RecordWeightView.h"
#import "RecordFatView.h"
#import "RecordWaistLineView.h"
#import "RecordHeartBeatView.h"
#import "RecordBloodSugarView.h"
#import "RecordTGView.h"
#import "RecordHDLView.h"
#import "RecordSleepView.h"
#import "RecordHealthStatusView.h"

@protocol  IHealthRecordViewDelegate <NSObject>
@required
    - (void) buttonPhotoPress;

@end

@class HealthRecordButton;
@interface HealthRecordView : View

@property (nonatomic,weak) id<IHealthRecordViewDelegate> iDelegate;

@property (nonatomic,strong) IBOutlet UIView *mainView;
@property (nonatomic,strong) IBOutlet UIView *view_button;
@property (nonatomic,weak) IBOutlet UIButton *buttonWeight;

@property (nonatomic,strong) RecordWeightView       *view_weight;
@property (nonatomic,strong) RecordFatView          *view_fat;
@property (nonatomic,strong) RecordWaistLineView    *view_waistLine;
@property (nonatomic,strong) RecordHeartBeatView    *view_heartBeat;
@property (nonatomic,strong) RecordBloodSugarView   *view_bloodSugar;
@property (nonatomic,strong) RecordTGView           *view_TG;
@property (nonatomic,strong) RecordHDLView          *view_HDL;
@property (nonatomic,strong) RecordSleepView        *view_sleep;
@property (nonatomic,strong) RecordHealthStatusView *view_healthStatus;

@end
