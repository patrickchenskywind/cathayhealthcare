//
//  HealthRecordModel.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/25.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HealthRecordModel : NSObject

@property (nonatomic,strong)NSDictionary      *dic_buttonSwitch;
@property (nonatomic,strong)NSArray           *array_buttonTitle;

@property (nonatomic,strong)NSArray      *array_weightHistory;
@property (nonatomic,strong)NSArray      *array_waistlineHistory;
@property (nonatomic,strong)NSArray      *array_fatHistory;
@property (nonatomic,strong)NSArray      *array_heartBeatHistory;
@property (nonatomic,strong)NSArray      *array_bloodSugarHistory;
@property (nonatomic,strong)NSArray      *array_TGHistory;
@property (nonatomic,strong)NSArray      *array_HDLHistory;
@property (nonatomic,strong)NSArray      *array_sleepHistory;
@property (nonatomic,strong)NSArray      *array_healthStatusHistory;

+(instancetype)sharedInstance;
@end
