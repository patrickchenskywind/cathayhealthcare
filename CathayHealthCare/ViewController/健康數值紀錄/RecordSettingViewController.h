//
//  RecordSettingViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"

@interface RecordSettingViewController : ViewController

@property (nonatomic,strong) IBOutlet UIButton *button_metabolize;
@property (nonatomic,strong) IBOutlet UIButton *button_fat;
@property (nonatomic,strong) IBOutlet UIButton *button_waistLine;
@property (nonatomic,strong) IBOutlet UIButton *button_heartBeat;
@property (nonatomic,strong) IBOutlet UIButton *button_bloodSugar;
@property (nonatomic,strong) IBOutlet UIButton *button_TG;
@property (nonatomic,strong) IBOutlet UIButton *button_HDL;
@property (nonatomic,strong) IBOutlet UIButton *button_sleep;
@property (nonatomic,strong) IBOutlet UIButton *button_status;

@end

