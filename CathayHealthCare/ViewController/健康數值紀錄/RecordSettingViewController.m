//
//  RecordSettingViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordSettingViewController.h"
#import "DefindConfig.h"

@interface RecordSettingViewController ()

@property (strong, nonatomic) NSMutableDictionary *dictionaryHealthListStatus;

@end

@implementation RecordSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"偏好設定"];
    [self setupDictionaryHealthListStatus];
    [self setupUI];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSUserDefaults standardUserDefaults] setObject: self.dictionaryHealthListStatus forKey: DICTIONARY_HEALTHLISTSTATUS];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setupDictionaryHealthListStatus {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: DICTIONARY_HEALTHLISTSTATUS]) {
        [self setDictionaryHealthListStatus: [[[NSUserDefaults standardUserDefaults] objectForKey: DICTIONARY_HEALTHLISTSTATUS] mutableCopy]];
    }
    else {
        [self setDictionaryHealthListStatus: [NSMutableDictionary dictionary] ];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_FAT];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_WAISTLINE];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_HEARTBEAT];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_BLOODSUGAR];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_TG];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_HDL];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_SLEEP];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_STATUS];
    }
}

- (void) setupUI {
    [self.button_fat setSelected: [[self.dictionaryHealthListStatus objectForKey: DICTIONARY_HEALTHLISTSTATUS_KEY_FAT] boolValue]];
    [self.button_waistLine setSelected: [[self.dictionaryHealthListStatus objectForKey: DICTIONARY_HEALTHLISTSTATUS_KEY_WAISTLINE] boolValue]];
    [self.button_heartBeat setSelected: [[self.dictionaryHealthListStatus objectForKey: DICTIONARY_HEALTHLISTSTATUS_KEY_HEARTBEAT] boolValue]];
    [self.button_bloodSugar setSelected: [[self.dictionaryHealthListStatus objectForKey: DICTIONARY_HEALTHLISTSTATUS_KEY_BLOODSUGAR] boolValue]];
    [self.button_TG setSelected: [[self.dictionaryHealthListStatus objectForKey: DICTIONARY_HEALTHLISTSTATUS_KEY_TG] boolValue]];
    [self.button_HDL setSelected: [[self.dictionaryHealthListStatus objectForKey: DICTIONARY_HEALTHLISTSTATUS_KEY_HDL] boolValue]];
    [self.button_sleep setSelected: [[self.dictionaryHealthListStatus objectForKey: DICTIONARY_HEALTHLISTSTATUS_KEY_SLEEP] boolValue]];
    [self.button_status setSelected: [[self.dictionaryHealthListStatus objectForKey: DICTIONARY_HEALTHLISTSTATUS_KEY_STATUS] boolValue]];
    [self checkButtonSelect];
}

- (IBAction)buttonMetabolizePress:(UIButton *)sender {
    [sender setSelected: ! sender.selected];
    if (sender.selected) {
        [self.button_fat setSelected: NO];
        [self.button_waistLine setSelected: YES];
        [self.button_heartBeat setSelected: YES];
        [self.button_bloodSugar setSelected: YES];
        [self.button_TG setSelected: YES];
        [self.button_HDL setSelected: YES];
        [self.button_sleep setSelected: NO];
        [self.button_status setSelected: NO];
        
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: NO] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_FAT];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_WAISTLINE];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_HEARTBEAT];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_BLOODSUGAR];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_TG];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: YES] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_HDL];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: NO] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_SLEEP];
        [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: NO] forKey: DICTIONARY_HEALTHLISTSTATUS_KEY_STATUS];
    }
}

- (IBAction)buttonFatPress:(UIButton *)sender {
    [self setupElementWtih: sender wihtKey: DICTIONARY_HEALTHLISTSTATUS_KEY_FAT];
}
- (IBAction)buttonWaistLinePress:(UIButton *)sender {
    [self setupElementWtih: sender wihtKey: DICTIONARY_HEALTHLISTSTATUS_KEY_WAISTLINE];
}
- (IBAction)buttonHeartBeatPress:(UIButton *)sender {
    [self setupElementWtih: sender wihtKey: DICTIONARY_HEALTHLISTSTATUS_KEY_HEARTBEAT];
}
- (IBAction)buttonBloodSugarPress:(UIButton *)sender {
    [self setupElementWtih: sender wihtKey: DICTIONARY_HEALTHLISTSTATUS_KEY_BLOODSUGAR];
}
- (IBAction)buttonTGPress:(UIButton *)sender {
    [self setupElementWtih: sender wihtKey: DICTIONARY_HEALTHLISTSTATUS_KEY_TG];
}
- (IBAction)buttonHDLPress:(UIButton *)sender {
    [self setupElementWtih: sender wihtKey: DICTIONARY_HEALTHLISTSTATUS_KEY_HDL];
}
- (IBAction)buttonSleepPress:(UIButton *)sender {
    [self setupElementWtih: sender wihtKey: DICTIONARY_HEALTHLISTSTATUS_KEY_SLEEP];
}
- (IBAction)buttonStatusPress:(UIButton *)sender {
    [self setupElementWtih: sender wihtKey: DICTIONARY_HEALTHLISTSTATUS_KEY_STATUS];
}

- (void) setupElementWtih:(UIButton *) sender wihtKey: (NSString *) key {
    [sender setSelected: ! sender.selected];
    [self checkButtonSelect];
    [self.dictionaryHealthListStatus setObject: [NSNumber numberWithBool: sender.selected] forKey: key];
}

- (void) checkButtonSelect {
    if ( ! self.button_fat.selected &&
        self.button_waistLine.selected &&
        self.button_heartBeat.selected &&
        self.button_bloodSugar.selected &&
        self.button_TG.selected &&
        self.button_HDL.selected &&
        ! self.button_sleep.selected &&
        ! self.button_status.selected) {
        [self.button_metabolize setSelected: YES];
    }
    else {
        [self.button_metabolize setSelected: NO];
    }
}

@end
