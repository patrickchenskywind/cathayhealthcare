//
//  GroupView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GroupView.h"
#import "UIColor+Hex.h"

@implementation GroupView

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _array_message = @[@{@"groupName":@"群組名稱",
                             @"date":@"2015/5/5 15:55",
                             @"message":@"留言留言留言留言留言留言"},
                           @{@"groupName":@"群組名稱",
                             @"date":@"2015/5/5 15:55",
                             @"message":@"留言留言留言留言留言留言"},
                           @{@"groupName":@"群組名稱",
                             @"date":@"2015/5/5 15:55",
                             @"message":@"留言留言留言留言留言留言"}
                           ];
    }
    return self;
}


#pragma mark UITableViewDataSource & Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 87;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array_message.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"groupCell"];
    if (!cell) {
        
        NSDictionary *groupInfo = [_array_message objectAtIndex:indexPath.row];
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"GroupMessageCell" owner:self options:nil] objectAtIndex:0];
        
        [cell.label_groupName setText:[groupInfo objectForKey:@"groupName"]];
        [cell.label_time setText:[groupInfo objectForKey:@"date"]];
        [cell.text_message setText:[groupInfo objectForKey:@"message"]];
        [cell setBackgroundColor:[UIColor colorWithHex:0xE5F8F3]];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGRectZero.size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_delegate goGroupDetail];
}

@end
