//
//  ReferrenceView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@interface ReferrenceView : View

@property (nonatomic,strong) IBOutlet UILabel       *label_title;
@property (nonatomic,strong) IBOutlet UITableView   *table_info;
@property (nonatomic,strong) IBOutlet UIButton      *button_goWeb;

@end
