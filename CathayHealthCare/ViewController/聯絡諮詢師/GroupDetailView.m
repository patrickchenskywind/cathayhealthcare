//
//  GroupDetailView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GroupDetailView.h"

@implementation GroupDetailView

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"yyyy/MM/dd hh:mm"];
        _bubbleData = [[NSMutableArray alloc] initWithObjects:
                       [NSBubbleData dataWithText:[NSString stringWithFormat:@"%@\nMarge, there's something that I want to ask you, but I'm afraid, because if you say no, it will destroy me and make me a criminal.",[dateformatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:-300]]]
                                          andDate:[NSDate dateWithTimeIntervalSinceNow:-300]
                                          andType:BubbleTypeMine andAuthor:@""],
                       [NSBubbleData dataWithText:[NSString stringWithFormat:@"%@\nWell, I haven't said no to you yet, have I?",[dateformatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:-280]]]
                        
                                          andDate:[NSDate dateWithTimeIntervalSinceNow:-280]
                                          andType:BubbleTypeSomeoneElse
                                        andAuthor:@"健康諮詢師"],
                       nil];
    }
    return self;
}

-(void)layoutSubviews {
    if (!_table_message.bubbleDataSource) {
        [_table_message setBubbleDataSource:self];
        [_table_message reloadData];
    }
}

-(IBAction)sendButtonPress:(id)sender {
    NSString *sendMessage = _text_sendMsg.text;
    NSString *checkMessage = [sendMessage stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (checkMessage.length > 0) {
        [_bubbleData addObject:[NSBubbleData dataWithText:sendMessage andDate:[NSDate date] andType:BubbleTypeMine andAuthor:@""]];
        [_text_sendMsg setText:@""];
        [_table_message reloadData];
    }
}

#pragma mark - UIBubbleTableViewDataSource implementation
- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView {
    return _bubbleData.count;
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row {
    return _bubbleData[row];
}

#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
