//
//  MessageViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "MessageViewController.h"
#import "SetMessageBoardAPI.h"
#import "Toos.h"
#import "LoginEntity.h"
#import "NSBubbleData.h"

@interface MessageViewController () <UIBubbleTableViewDataSource>

@end

@implementation MessageViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"互動留言"];
    [self.view.table_message setBubbleDataSource: self];
    [self changeRefreshButtonStyle];
    
    [self executSetMessageBoardAPIWithDate: nil message: nil photo: nil isRefresh: YES];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object: nil];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

-(void)changeRefreshButtonStyle {
    _button_refresh = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button_refresh setFrame:CGRectMake(0,
                                         0,
                                         self.navigationController.navigationBar.frame.size.height,
                                         self.navigationController.navigationBar.frame.size.height)];
    [_button_refresh setImage:[UIImage imageNamed:@"btn_refresh"] forState:UIControlStateNormal];
    [_button_refresh addTarget:self action:@selector(refreshButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingItem = [[UIBarButtonItem alloc] initWithCustomView:_button_refresh];
    [self.navigationItem setRightBarButtonItem:settingItem];
}

-(void)refreshButtonPress:(UIButton *)button {
    [self executSetMessageBoardAPIWithDate: [NSDate date] message: nil photo: nil isRefresh: YES];

}

- (IBAction)buttonSubmitPress:(id)sender {
    NSString *checkMessage = [self.view.text_sendMsg.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (checkMessage.length > 0)
        [self executSetMessageBoardAPIWithDate: [NSDate date] message: checkMessage photo: nil isRefresh: NO];
}

- (void) executSetMessageBoardAPIWithDate: (NSDate *)date
                                  message: (NSString *)message
                                    photo: (NSString *)photo
                                isRefresh: (BOOL)isRefresh {
    SetMessageBoardAPI *api = [[SetMessageBoardAPI alloc] init];
    if ( ! isRefresh) {
        [api setDate: [Toos getThousandSecondStringFromDate: date]];
        [api setMessage: message];
        [api setPhoto: photo];
    }
    [api post:^(HttpResponse *response) {
        MessageBoardParentEntity *messageBoardParentEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg: messageBoardParentEntity.msg]) {
            if ( ! isRefresh) {
                [self.view.text_sendMsg setText:@""];
                [self.view.table_message reloadData];
                [self.view.bubbleData addObject:[NSBubbleData dataWithText: [NSString stringWithFormat: @"%@\n%@",[Toos getYMDHMFormDate: date], message]
                                                                   andDate: date
                                                                   andType: BubbleTypeMine
                                                                 andAuthor: @""]];
            }
            else {
                [self.view setBubbleData: [NSMutableArray array]];
                LoginEntity *loginEntity = [LoginEntity get];
                for (int i = 0 ; i < messageBoardParentEntity.arrayMsg.count;  i++) {
                    MessageBoardEntity *messageBoardEntity = messageBoardParentEntity.arrayMsg[i];
                    
                    if ([messageBoardEntity.uid isEqualToString: loginEntity.uid]) {
                        [self.view.bubbleData addObject: [NSBubbleData dataWithText: [NSString stringWithFormat: @"%@\n%@",[Toos getYMDHMStringFormServerTimeInterval: messageBoardEntity.date], messageBoardEntity.message]
                                                                            andDate: [NSDate date]
                                                                            andType: BubbleTypeMine
                                                                          andAuthor: nil]];
                        
                    }
                    else {
                        [self.view.bubbleData addObject: [NSBubbleData dataWithText: [NSString stringWithFormat: @"%@\n%@",[Toos getYMDHMStringFormServerTimeInterval: messageBoardEntity.date], messageBoardEntity.message]
                                                                            andDate: [NSDate date]
                                                                            andType: BubbleTypeSomeoneElse
                                                                          andAuthor: messageBoardEntity.author]];
                    }
                }
            }
            [self.view.table_message reloadData];
        }
        else{}
    }];
}

#pragma mark - Notification
- (void) keyboardWillShow: (NSNotification *) notification {
    [self.view.constraintButton setConstant: [[[notification userInfo] objectForKey: UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height];
    [self.view layoutIfNeeded];
}

- (void) keyboardWillHide: (NSNotification *) notification {
    [self.view.constraintButton setConstant: 0];
    [self.view layoutIfNeeded];
}

#pragma mark - UIBubbleTableViewDataSource implementation
- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView {
    return [self.view.bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row {
    return self.view.bubbleData[row];
}

#pragma mark UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self closeKeyboard];
    return YES;
}

@end
