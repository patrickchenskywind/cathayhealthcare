//
//  ReferrenceView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ReferrenceView.h"
#import "TelephoneCell.h"

@interface ReferrenceView ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) NSArray *array_units;
@property (nonatomic,strong) NSArray *array_telNum;

@end

@implementation ReferrenceView

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.array_units = @[@"敦南健檢中心",@"內湖健檢中心",@"新竹健檢中心",@"國泰台南預防醫學中心"];
        self.array_telNum = @[@"(02)2739-0333",@"(02)8251-0258",@"(03)526-8555",@"(06)215-0599#202"];
    }
    return self;
}

-(void)layoutSubviews {
    [_button_goWeb.layer setCornerRadius:15];
}

#pragma mark UITableViewDelegate & Datasource

-(CGFloat)tableView: (UITableView *)tableView heightForRowAtIndexPath: (NSIndexPath *)indexPath {
    return 50;
}

-(NSInteger)tableView: (UITableView *)tableView numberOfRowsInSection: (NSInteger)section {
    return self.array_units.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath: (NSIndexPath *)indexPath {
    TelephoneCell *cell = [tableView dequeueReusableCellWithIdentifier: @"TelephoneCell"];
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"TelephoneCell" owner: self options: nil] objectAtIndex: 0];
        [cell setUnit: [self.array_units objectAtIndex: indexPath.row]
                phone: [self.array_telNum objectAtIndex: indexPath.row]];
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    }
    
    return cell;
}

@end
