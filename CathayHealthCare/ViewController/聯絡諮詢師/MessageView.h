//
//  MessageView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "UIBubbleTableView.h"

@interface MessageView : View

@property (nonatomic,strong) IBOutlet UIButton      *button_send;
@property (nonatomic,strong) IBOutlet UITextField   *text_sendMsg;
@property (nonatomic,strong) IBOutlet UIButton      *button_camera;
@property (nonatomic,strong) IBOutlet UIBubbleTableView *table_message;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButton;

@property (nonatomic,strong) NSMutableArray *bubbleData;

@end
