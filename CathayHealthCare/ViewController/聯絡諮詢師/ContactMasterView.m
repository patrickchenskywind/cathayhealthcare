//
//  ContactMasterView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ContactMasterView.h"

enum CONTACTMASTERVIEW_CONFIG {
    BUTTON_TOPBORDER   = 40,
    BUTTON_LEFTBORDER  = 30,
    BUTTON_RIGHTBORDER = 30
};


@implementation ContactMasterView

- (IBAction) buttonPress:(UIButton *)button {
    NSString *identify;
    
    if (button == self.button_referrence){
        identify = @"referrence";
    }
    else if (button == self.button_message){
        identify = @"message";
    }
    else if (button == self.button_group){
        identify = @"group";
    }
    [self.delegate goWhereWithIdentify: identify];
}

- (void) layoutSubviews {
    [self.button_group.layer setCornerRadius: 5];
    [self.button_message.layer setCornerRadius: 5];
    [self.button_referrence.layer setCornerRadius: 10];
}

@end
