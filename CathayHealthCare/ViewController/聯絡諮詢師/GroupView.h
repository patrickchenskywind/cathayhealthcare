//
//  GroupView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "GroupMessageCell.h"

@protocol GroupViewDelegate <NSObject>
-(void)goGroupDetail;
@end

@interface GroupView : View <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UITableView *table_message;
@property (nonatomic,strong) NSArray *array_message;
@property (nonatomic,weak) IBOutlet id<GroupViewDelegate> delegate;

@end
