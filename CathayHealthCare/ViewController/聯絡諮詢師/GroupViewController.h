//
//  GroupViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "GroupView.h"
#import "GroupDetailViewController.h"

@interface GroupViewController : ViewController<GroupViewDelegate>

@end
