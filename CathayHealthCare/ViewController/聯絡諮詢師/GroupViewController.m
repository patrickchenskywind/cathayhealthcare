//
//  GroupViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GroupViewController.h"

@interface GroupViewController ()

@end

@implementation GroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"社群互動"];
}
-(void)goGroupDetail {
    GroupDetailViewController *gdvc = [[GroupDetailViewController alloc] initWithNibName: @"GroupDetailViewController" bundle:nil];
    [self.navigationController pushViewController: gdvc animated: YES];
}

@end
