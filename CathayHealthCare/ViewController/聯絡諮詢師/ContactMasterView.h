//
//  ContactMasterView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol ContactMasterViewDelegate <NSObject>
@required
    -(void)goWhereWithIdentify:(NSString *)identify;
@end

@interface ContactMasterView : View

@property (nonatomic,strong) IBOutlet UIButton *button_referrence;
@property (nonatomic,strong) IBOutlet UIButton *button_message;
@property (nonatomic,strong) IBOutlet UIButton *button_group;
@property (nonatomic,assign) IBOutlet id<ContactMasterViewDelegate> delegate;

@end
