//
//  ContactMasterViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ContactMasterViewController.h"
#import "LoginEntity.h"
#import "PremissionAlertView.h"
#import "ReferrenceViewController.h"
#import "MessageViewController.h"
#import "GroupViewController.h"
#import "ContactMasterView.h"

@interface ContactMasterViewController () <IPremissionAlertView, ContactMasterViewDelegate>
@end

@implementation ContactMasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"聯絡健管師"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titlebar_green"] forBarMetrics:UIBarMetricsDefault];
}

-(void)goWhereWithIdentify:(NSString *)identify{
    if ([identify isEqualToString:@"referrence"]) {
        ReferrenceViewController *rvc = [[ReferrenceViewController alloc] initWithNibName:@"ReferrenceViewController" bundle:nil];
        [self.navigationController pushViewController:rvc animated: YES];
    }
    else if ( [[LoginEntity get].premission isEqualToString: @"2" ] || [[LoginEntity get].premission isEqualToString: @"3"]) {
        if([identify isEqualToString:@"message"]){
            MessageViewController *mvc = [[MessageViewController alloc] initWithNibName:@"MessageViewController" bundle:nil];
            [self.navigationController pushViewController:mvc animated: YES];
        }
        else if ([identify isEqualToString:@"group"]){
            GroupViewController *gvc = [[GroupViewController alloc] initWithNibName:@"GroupViewController" bundle:nil];
            [self.navigationController pushViewController:gvc animated: YES];
        }
    }
    else {
        PremissionAlertView *premissionAlertView = [[[NSBundle mainBundle] loadNibNamed:@"PremissionAlertView" owner:self options:nil] objectAtIndex:0];
            [premissionAlertView setFrame:CGRectMake(0,
                                             0,
                                             CGRectGetWidth(self.view.bounds) - CGRectGetWidth(self.view.bounds) / 9,
                                             CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.view.bounds) / 1.4)];
            
            [premissionAlertView setCenter:CGPointMake(CGRectGetWidth(self.view.bounds)/2,
                                               CGRectGetHeight(self.view.bounds)/2)];
        
            [premissionAlertView setIDelegate:self];
            [self.view addSubview: premissionAlertView];
    }
}


#pragma mark - IPremissionAlertView
- (void)buttonConfirmPress {
    [self.view.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass: [PremissionAlertView class]]) {
            [obj removeFromSuperview];
        }
    }];
}
@end
