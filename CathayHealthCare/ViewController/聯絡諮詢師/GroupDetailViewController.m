//
//  GroupDetailViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "GroupDetailViewController.h"

@interface GroupDetailViewController ()

@end

@implementation GroupDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"社群互動"];
    [self changeRefreshButtonStyle];
}

-(void)changeRefreshButtonStyle {
    _button_refresh = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button_refresh setFrame:CGRectMake(0,
                                         0,
                                         self.navigationController.navigationBar.frame.size.height,
                                         self.navigationController.navigationBar.frame.size.height)];
    [_button_refresh setImage:[UIImage imageNamed:@"btn_refresh"] forState:UIControlStateNormal];
    [_button_refresh addTarget:self action:@selector(refreshButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *settingItem = [[UIBarButtonItem alloc] initWithCustomView:_button_refresh];
    [self.navigationItem setRightBarButtonItem:settingItem];
}

-(void)refreshButtonPress:(UIButton *)button {
    
}

@end
