//
//  NewRegistViewController.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "NewRegistViewController.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "BloodTypePickerViewController.h"
#import "Toos.h"
#import "RegisterAPI.h"

@interface NewRegistViewController ()<IDatePickerViewController, IBloodTypePickerViewController, ISwitchButton>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) BloodTypePickerViewController *bloodTypePickerViewController;
@property (strong, nonatomic) NSDate * birthdayTime;
@property (assign, nonatomic) NSInteger bloodType;

@end

@implementation NewRegistViewController

@dynamic view;

#pragma mark - LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"註冊"];
    [self.view.swithButtonCountry setIDelegate: self];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed: @"titlebar_green"] forBarMetrics: UIBarMetricsDefault];
}

#pragma mark - IBAction
- (IBAction)buttonSubmitPress:(id)sender {
    if ( [self checkAll] ) {
        RegisterAPI *api = [[RegisterAPI alloc] init];
        [api setEmail: self.view.textFieldEmail.text];
        [api setPassword: self.view.textFieldPasswordConfirm.text];
        [api setName: self.view.textFieldName.text];
        [api setBirthday: [Toos getThousandSecondStringFromDate: self.birthdayTime]];
        [api setBloodType: [NSString stringWithFormat:@"%@",@(self.bloodType)]];
        [api setGender: self.view.swithButtonGender.isOpen ? @"1" : @"0"];
        [api setAddress: self.view.textFieldAddress.text];
        [api setNationality: self.view.swithButtonCountry.isOpen ? @"1" : @"0"];
        [api setId: self.view.textFieldIdentity.text];
        
        [api post:^(HttpResponse *response) {
            BaseEntity *baseEntity = response.result.firstObject;
            if ( [Toos checkMsgCodeWihtMsg: baseEntity.msg] ) {
                [self.navigationController popViewControllerAnimated: YES];
            }
        }];
    }
}

- (IBAction)buttonBirthdayPress:(UIButton *)sender {
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self popoverControllerWithController: self.datePickerViewController width: 218 sender: sender];
}

- (IBAction)buttonBloodTypePress:(UIButton *)sender {
    if ( ! self.bloodTypePickerViewController){
        [self setBloodTypePickerViewController: [[BloodTypePickerViewController alloc] initWithNibName: @"BloodTypePickerViewController" bundle: nil]];
        [self.bloodTypePickerViewController setIDelegate: self];
    }
    [self popoverControllerWithController: self.bloodTypePickerViewController width: 162 sender: sender];
}

#pragma mark - IDatePickerViewController
- (void) dateDidSelect:(NSDate *) date{
    [self setBirthdayTime: date];
    [self.view.textFieldBirthday setText: [Toos getYMDFormDate: date]];
}

#pragma mark - IBloodTypePickerViewController
- (void) bloodTypeDidSelect:(NSInteger) row title: (NSString *) title{
    [self setBloodType: row];
    [self.view.textFieldBloodType setText: title];
}

#pragma mark - ISwitchButton
- (void) iSwitchButtonDidSwith: (BOOL) isOpen {
    if ( !  isOpen) {
        [self.view.labelIdentity setText: @"身份證字號"];
        if (self.view.textFieldIdentity.attributedPlaceholder.length > 0) {
            [self.view.textFieldIdentity setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入身份證字號"
                                                                                                   attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        }
    }
    else {
        [self.view.labelIdentity setText: @"居留證/\n護照號碼"];
        if (self.view.textFieldIdentity.attributedPlaceholder.length > 0) {
            [self.view.textFieldIdentity setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入居留證/護照號碼"
                                                                                                   attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        }
    }
}

#pragma mark - Unit
- (void) popoverControllerWithController: (id) controller width: (NSInteger) width  sender:(UIButton *)sender {
    [self closeKeyboard];
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: controller]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, width)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (BOOL) checkAll {
    [self checkEmail];
    [self checkPasswordText];
    [self checkPasswordConfirmText];
    [self checkName];
    [self checkBirthday];
    [self checkBloodType];
    [self checkAddress];
    [self checkPhone];
    [self checkIdentity];
    
    return ( [self checkEmail] && [self checkPassword] && [self checkName] && [self checkBirthday] && [self checkBloodType] && [self checkAddress] && [self checkPhone] && [self checkIdentity] && [self checkAgree] );
}

- (BOOL) checkEmail {
    if ( ! [self checkText: self.view.textFieldEmail.text]) {
        [self.view.textFieldEmail setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入email"
                                                                                            attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL)checkPassword {
    if ( [self checkPasswordText] && [self checkPasswordConfirmText] )
        return [self.view.textFieldPassword.text isEqualToString: self.view.textFieldPasswordConfirm.text];
    else
        return NO;
}

- (BOOL) checkPasswordText {
    if ( ! [self checkText: self.view.textFieldPassword.text] ) {
        [self.view.textFieldPassword setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入密碼"
                                                                                               attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL) checkPasswordConfirmText {
    if ( ! [self checkText: self.view.textFieldPasswordConfirm.text]) {
        [self.view.textFieldPasswordConfirm setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請再次輸入密碼"
                                                                                                      attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL) checkName {
    if ( ! [self checkText: self.view.textFieldName.text]) {
        [self.view.textFieldName setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入姓名"
                                                                                           attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL) checkBirthday {
    if ( ! [self checkText: self.view.textFieldBirthday.text]) {
        [self.view.textFieldBirthday setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請選擇生日"
                                                                                               attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL) checkBloodType {
    if ( ! [self checkText: self.view.textFieldBloodType.text]) {
        [self.view.textFieldBloodType setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請選擇血型"
                                                                                                attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL) checkAddress {
    if ( ! [self checkText: self.view.textFieldAddress.text]) {
        [self.view.textFieldAddress setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入地址"
                                                                                              attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL) checkPhone {
    if ( ! [self checkText: self.view.textFieldPhone.text]) {
        [self.view.textFieldPhone setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入手機號碼"
                                                                                            attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL) checkIdentity{
    if ( ! [self checkText: self.view.textFieldIdentity.text]) {
        if ( ! self.view.swithButtonCountry.isOpen) {
            [self.view.textFieldIdentity setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入身份證字號"
                                                                                                   attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        }
        else {
            [self.view.textFieldIdentity setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入居留證/護照號碼"
                                                                                                   attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
        }
        
        return NO;
    }
    else {
        return YES;
    }
}

- (BOOL) checkAgree {
    return [self.view.buttonArgee isSelected];
}

- (BOOL) checkText: (NSString *) text {
    if (text && text.length > 0 )
        return YES;
    else
        return NO;
}

@end
