//
//  NewRegistView.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "SwitchButton.h"

@interface NewRegistView : View

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPasswordConfirm;
@property (weak, nonatomic) IBOutlet UITextField *textFieldName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldBirthday;
@property (weak, nonatomic) IBOutlet UITextField *textFieldBloodType;
@property (weak, nonatomic) IBOutlet SwitchButton *swithButtonGender;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAddress;
@property (weak, nonatomic) IBOutlet UITextField *textFieldPhone;
@property (weak, nonatomic) IBOutlet SwitchButton *swithButtonCountry;
@property (weak, nonatomic) IBOutlet UITextField *textFieldIdentity;
@property (weak, nonatomic) IBOutlet UIButton *buttonArgee;
@property (weak, nonatomic) IBOutlet UITextView *textViewPolicy;

@property (weak, nonatomic) IBOutlet UILabel *labelIdentity;

@end
