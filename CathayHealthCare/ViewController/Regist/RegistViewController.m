//
//  RegistViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RegistViewController.h"
#import "UIColor+Hex.h"

@interface RegistViewController ()
enum REGIST_INDEX
{
    NUM_EMAIL           = 0,
    NUM_PASSWORD        = 1,
    NUM_COMFIRM_PW      = 2,
    NUM_NAME            = 3,
    NUM_BIRTH           = 4,
    NUM_BLOODTYPE       = 5,
    NUM_GENDER          = 6,
    NUM_ADDRESS         = 7,
    NUM_PHONE           = 8,
    NUM_COUNTRY         = 9,
    NUM_IDENTIFY        = 10,
    NUM_AGREE_PRIVATE   = 11,
    NUM_PRIVATE_DESC    = 12,
    
    BUTTON_HEIGHT               = 40,
    CELL_HEIGHT                 = 55,
    CELL_WITHDESC_HEIGHT        = 65,
    CELL_PRIVATE_DESC_HEIGHT    = 100
};
@property (nonatomic,strong) NSArray *array_title;
@property (nonatomic,assign) float screenWidth;
@property (nonatomic,assign) float screenHeight;

@end

@implementation RegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _array_title = @[@"email",@"密碼",@"確認密碼",@"姓名",@"生日",
                     @"血型",@"性別",@"地址",@"手機號碼",@"國籍",@"身分證字號",@"",@""];
    
    _screenWidth  = [UIScreen mainScreen].bounds.size.width;
    _screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    [self newObject];
    [self setLayout];
}

-(void)newObject {
    if (!_table_main) {
        _table_main = [[UITableView alloc] init];
        _table_main.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_table_main setDelegate:self];
        [_table_main setDataSource:self];
        [_table_main reloadData];
        [self.view addSubview:_table_main];
    }
    if (!_button_regist) {
        _button_regist = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_button_regist setTitle:@"確定註冊" forState:UIControlStateNormal];
        [_button_regist setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button_regist setBackgroundColor:[UIColor colorWithHex:0x86BC0F]];
        [self.view addSubview:_button_regist];
    }
    
    if (!_textField_email) {
        _textField_email = [self newTextField];
    }
    if (!_textField_pw) {
        _textField_pw = [self newTextField];
    }
    if (!_textField_confirm) {
        _textField_confirm = [self newTextField];
    }
    if (!_textField_name) {
        _textField_name = [self newTextField];
    }
    if (!_textField_birth) {
        _textField_birth = [[PullDownTextField alloc] init];
        [_textField_birth initialWithpullDownContent:@[@"1990/9/9",@"1989/9/6"] borderColor:[UIColor colorWithHex:0xCCB2A0]];
        [_textField_birth setDelegate:self];
    }
    if (!_textField_blood) {
        _textField_blood = [[PullDownTextField alloc] init];
        [_textField_blood initialWithpullDownContent:@[@"A",@"B",@"O",@"AB"] borderColor:[UIColor colorWithHex:0xCCB2A0]];
    }
    if (!_textField_gender) {
        _textField_gender = [[SwitchButton alloc] init];
    }
    if (!_textField_address) {
        _textField_address = [self newTextField];
    }
    if (!_textField_phone) {
        _textField_phone = [self newTextField];
    }
    if (!_textField_country) {
        _textField_country = [[SwitchButton alloc] init];
    }
    if (!_textField_id) {
        _textField_id = [self newTextField];
    }
    
    if (!_button_agree) {
        _button_agree = [RadioButton buttonWithType:UIButtonTypeCustom];
    }
    if (!_text_private) {
        _text_private = [[UITextView alloc] init];
    }
    
}

-(UITextField *)newTextField
{
    UITextField *textField = [[UITextField alloc] init];
    [textField.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [textField.layer setBorderWidth:1];
    [textField.layer setCornerRadius:4];
    [textField setDelegate:self];
    return textField;
}


-(void)setLayout
{
    float tableHight = _screenHeight - BUTTON_HEIGHT;
    [_table_main setFrame:CGRectMake(0,
                                     0,
                                     _screenWidth,
                                     tableHight)];
    
    
    float buttonY = _table_main.frame.size.height + _table_main.frame.origin.y;
    [_button_regist setFrame:CGRectMake(0,
                                        buttonY,
                                        _screenWidth,
                                        BUTTON_HEIGHT)];
}

#pragma mark PullDownTextFieldDelegate
-(void)showPullDownOption {
//    if (!_textField_birth.maskView) {
//        _textField_birth.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
//        
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeTableView)];
//        [_textField_birth.maskView addGestureRecognizer:tap];
//        [_textField_birth.maskView setBackgroundColor:[UIColor clearColor]];
//        
//    }
//    
//    if (!_textField_birth.table_content) {
//        _textField_birth.table_content = [[UITableView alloc] init];
//        
//        
//        _textField_birth.table_content.delegate = _textField_birth;
//        _textField_birth.table_content.dataSource = _textField_birth;
//        [_textField_birth.table_content reloadData];
//    }
}

-(void)removeTableView
{
    [_textField_birth removeTableView];
}
#pragma mark TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_array_title count];// 同意個資與個資政策內容
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == NUM_PASSWORD || indexPath.row == NUM_PHONE)
        return CELL_WITHDESC_HEIGHT;
    else if (indexPath.row == NUM_PRIVATE_DESC)
        return CELL_PRIVATE_DESC_HEIGHT;
    else
        return CELL_HEIGHT;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell ;//= [tableView dequeueReusableCellWithIdentifier:@"registCell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:@"registCell"];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, _screenWidth/3-20, CELL_HEIGHT)];
        
        if (indexPath.row <= NUM_IDENTIFY) {
            [label setText:[_array_title objectAtIndex:indexPath.row]];
        }
        //[cell setBackgroundColor:[UIColor colorWithRed:arc4random()%255/255.0 green:arc4random()%255/255.0 blue:arc4random()%255/255.0 alpha:1.0]];
        [cell addSubview:label];
        [self setTextFieldWithCell:cell andIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
    
    return cell;
}

-(void)setTextFieldWithCell:(UITableViewCell *)cell andIndexPath:(NSIndexPath *)indexPath {
    float textFieldX = _screenWidth /3;
    float textFieldY = 15;
    float textFieldWidth = _screenWidth/3*2-20;
    float textFieldHeight = 30;
    
    if (indexPath.row == NUM_EMAIL)
    {
        [_textField_email setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [cell addSubview:_textField_email];
    }
    if (indexPath.row == NUM_PASSWORD)
    {
        [_textField_pw setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_textField_pw.frame), CGRectGetMaxY(_textField_pw.frame), CGRectGetWidth(_textField_pw.frame), 15)];
        [label setFont:[UIFont systemFontOfSize: 10]];
        [label setText:@"(密碼建議設定至少六碼)"];
        
        [cell addSubview:_textField_pw];
        [cell addSubview:label];
    }
    if (indexPath.row == NUM_COMFIRM_PW) {
        [_textField_confirm setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [cell addSubview:_textField_confirm];
    }
    if (indexPath.row == NUM_NAME) {
        [_textField_name setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [cell addSubview:_textField_name];
    }
    if (indexPath.row == NUM_BIRTH) {
        [_textField_birth setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [cell addSubview:_textField_birth];
    }
    if (indexPath.row == NUM_BLOODTYPE)
    {
        [_textField_blood setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [cell addSubview:_textField_blood];
    }
    if (indexPath.row == NUM_GENDER)
    {
        [_textField_gender setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [_textField_gender setOnName:@"女" offName:@"男" color:[UIColor colorWithHex:0xCCB2A0] nameColor:[UIColor colorWithHex:0x917F72] isOpen:NO];
        [cell addSubview:_textField_gender];
    }
    if (indexPath.row == NUM_ADDRESS)
    {
        [_textField_address setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [cell addSubview:_textField_address];
    }
    if (indexPath.row == NUM_PHONE)
    {
        [_textField_phone setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [cell addSubview:_textField_phone];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_textField_phone.frame), CGRectGetMaxY(_textField_phone.frame), CGRectGetWidth(_textField_phone.frame), 15)];
        [label setFont:[UIFont systemFontOfSize:10]];
        [label setText:@"(手機格式:0123456789)"];
        
        [cell addSubview:_textField_phone];
        [cell addSubview:label];
    }
    if (indexPath.row == NUM_COUNTRY)
    {
        [_textField_country setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [_textField_country setOnName:@"外籍" offName:@"本國" color:[UIColor colorWithHex:0xCCB2A0] nameColor:[UIColor colorWithHex:0x917F72] isOpen:NO];
        [cell addSubview:_textField_country];
    }
    if (indexPath.row == NUM_IDENTIFY) {
        [_textField_id setFrame:CGRectMake(textFieldX, textFieldY, textFieldWidth, textFieldHeight)];
        [cell addSubview:_textField_id];
    }
    if (indexPath.row == NUM_AGREE_PRIVATE) {
        [_button_agree setFrame:CGRectMake(20, 0, 30, 30)];
        [_button_agree setYesStyle:[UIImage imageNamed:@"checkbox_register_checked"]
                        andNoStyle:[UIImage imageNamed:@"checkbox_register_uncheck"] isSelect:NO];
        [_button_agree setCenter:CGPointMake(_button_agree.center.x,
                                             CELL_HEIGHT/2)];
        UILabel *label = [[UILabel alloc] init];
        [label setFrame:CGRectMake(CGRectGetMaxX(_button_agree.frame) +20,
                                   0,
                                   200, 40)];
        [label setCenter:CGPointMake(label.center.x, _button_agree.center.y)];
        [label setText:@"我同意個資政策"];
        [cell addSubview:_button_agree];
        [cell addSubview:label];
    }
    if (indexPath.row == NUM_PRIVATE_DESC) {
        [_text_private setFrame:CGRectMake(10, 20, _table_main.frame.size.width -40, CELL_PRIVATE_DESC_HEIGHT - 20)];
        [_text_private setText:@"aaaaaaa\nbbbbbbbbbbbbbbbbbccccccccccccddddddbbbbbbbbbbbbbbbbbccccccccccccddddddbbbbbbbbbbbbbbbbbccccccccccccddddddbbbbbbbbbbbbbbbbbccccccccccccddddddbbbbbbbbbbbbbbbbbccccccccccccdddddd"];
        [_text_private.layer setBorderColor:[[UIColor colorWithHex:0xCCB2A0] CGColor]];
        [_text_private.layer setBorderWidth:1];
        [_text_private setEditable:NO];
        [_text_private setScrollEnabled:NO];
        [_text_private.layer setCornerRadius:4];
        [cell addSubview:_text_private];
    }
}

#pragma UITextField
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}
@end
