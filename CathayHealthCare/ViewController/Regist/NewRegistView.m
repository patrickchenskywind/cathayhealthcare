//
//  NewRegistView.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "NewRegistView.h"
#import "UIColor+Hex.h"

@implementation NewRegistView


- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    [self.swithButtonGender setOnName: @"女"
                              offName: @"男"
                                color: [UIColor colorWithHex: 0xCCB2A0]
                            nameColor: [UIColor colorWithHex: 0x917F72]
                               isOpen: NO];
    [self.swithButtonCountry setOnName: @"外籍"
                               offName: @"本國"
                                 color: [UIColor colorWithHex: 0xCCB2A0]
                             nameColor: [UIColor colorWithHex: 0x917F72]
                                isOpen: NO];
    
    [self.textViewPolicy.layer setBorderColor: [[UIColor colorWithHex: 0xCCB2A0] CGColor]];
    [self.textViewPolicy.layer setBorderWidth: 1];
    [self.textViewPolicy.layer setCornerRadius: 4];
}

- (IBAction)buttonArgeePress:(UIButton *)sender {
    [sender setSelected: ! sender.selected];
}

@end
