//
//  NewRegistViewController.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "NewRegistView.h"

@interface NewRegistViewController : ViewController

@property (retain, nonatomic)  NewRegistView *view;

@end
