//
//  RegistViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullDownTextField.h"
#import "SwitchButton.h"
#import "RadioButton.h"

@interface RegistViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,PullDownTextFieldDelegate>


@property (nonatomic,strong) UITableView  *table_main;

@property (nonatomic,strong) UITextField *textField_email;
@property (nonatomic,strong) UITextField *textField_pw;
@property (nonatomic,strong) UITextField *textField_confirm;
@property (nonatomic,strong) UITextField *textField_name;
@property (nonatomic,strong) PullDownTextField *textField_birth;

@property (nonatomic,strong) PullDownTextField *textField_blood;
@property (nonatomic,strong) SwitchButton *textField_gender;
@property (nonatomic,strong) UITextField *textField_address;
@property (nonatomic,strong) UITextField *textField_phone;
@property (nonatomic,strong) SwitchButton *textField_country;
@property (nonatomic,strong) UITextField *textField_id;
@property (nonatomic,strong) RadioButton *button_agree;
@property (nonatomic,strong) UITextView  *text_private;

@property (nonatomic,strong) UIButton    *button_regist;

@end
