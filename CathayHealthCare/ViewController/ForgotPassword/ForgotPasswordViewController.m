//
//  ForgotPasswordViewController.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "ForgotPassAPI.h"
#import "Toos.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"忘記密碼"];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed: @"titlebar_green"] forBarMetrics: UIBarMetricsDefault];
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
    if (self.view.textFieldEmail.text && self.view.textFieldEmail.text.length > 0) {
        ForgotPassAPI *api = [[ForgotPassAPI alloc] init];
        [api setMail: self.view.textFieldEmail.text];
        [api post:^(HttpResponse *response) {
            BaseEntity *baseEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: baseEntity.msg]) {
                [self.navigationController popViewControllerAnimated: YES];
            }
        }];
    }
    else {
        [self.view.textFieldEmail setAttributedPlaceholder: [[NSAttributedString alloc] initWithString: @"請輸入email"
                                                                                            attributes: @{NSForegroundColorAttributeName: [UIColor redColor]}]];
    }
}

@end
