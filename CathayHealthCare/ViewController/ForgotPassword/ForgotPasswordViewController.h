//
//  ForgotPasswordViewController.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "ForgotPasswordView.h"

@interface ForgotPasswordViewController : ViewController

@property (nonatomic, strong) ForgotPasswordView *view;

@end
