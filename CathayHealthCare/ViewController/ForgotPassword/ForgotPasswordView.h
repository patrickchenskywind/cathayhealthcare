//
//  ForgotPasswordView.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@interface ForgotPasswordView : View

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;

@end
