//
//  LaunchTarget3ViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "LaunchTarget3View.h"

@interface LaunchTarget3ViewController : ViewController

@property (nonatomic, retain) LaunchTarget3View * view;

@end
