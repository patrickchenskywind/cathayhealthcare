//
//  OperateViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "OperateViewController.h"

@interface OperateViewController ()

@end

@implementation OperateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"操作說明"];
    [self.navigationItem setHidesBackButton: YES];
}

-(void)goWhere {
    [self.navigationController pushViewController: [[LaunchTarget1ViewController alloc] initWithNibName: @"LaunchTarget1ViewController" bundle: nil] animated: YES];
}

@end
