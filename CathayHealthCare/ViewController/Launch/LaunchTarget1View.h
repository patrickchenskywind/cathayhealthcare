//
//  LaunchTargetView.h
//  CathayHealthCare
//
//  Created by Patrick on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "RadioButton.h"

@protocol LaunchTarget1ViewDelegate <NSObject>

-(void)goWhereWithWorkType: (NSString *) workType;

@end

@interface LaunchTarget1View : View

@property (nonatomic,strong) IBOutlet RadioButton *button_lowWork;
@property (nonatomic,strong) IBOutlet RadioButton *button_midWork;
@property (nonatomic,strong) IBOutlet RadioButton *button_hardWork;
@property (nonatomic,strong) IBOutlet UIButton    *button_next;

@property (nonatomic,weak) IBOutlet id<LaunchTarget1ViewDelegate> delegate;

@property (nonatomic,strong) NSString *workType;

@end
