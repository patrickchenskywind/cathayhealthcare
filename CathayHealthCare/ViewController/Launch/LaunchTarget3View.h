//
//  LaunchTarget3View.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "RadioButton.h"

@interface LaunchTarget3View : View 

@property (nonatomic,strong) IBOutlet RadioButton *button_keep;
@property (nonatomic,strong) IBOutlet RadioButton *button_increase;
@property (nonatomic,strong) IBOutlet RadioButton *button_decrease;

@property (nonatomic,strong) IBOutlet UITextField *text_target;
@property (nonatomic,strong) IBOutlet UITextField *text_targetAdd;
@property (nonatomic,strong) IBOutlet UITextField *text_targetSub;

@property (nonatomic,strong) IBOutlet UIButton *button_finish;
@property (nonatomic,strong) IBOutlet UIButton *button_last;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

@end
