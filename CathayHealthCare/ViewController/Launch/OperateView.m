//
//  OperateView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "OperateView.h"

@implementation OperateView

-(IBAction)buttonPress:(id)sender{
    [_delegate goWhere];
}


-(void)layoutSubviews {
    [self newObject];
    [self setLayout];
}


-(void)newObject {
    if (!_gest_swipeRight || !_gest_swipeLeft) {
        [self setGest_swipeLeft: [[UISwipeGestureRecognizer alloc] initWithTarget: self action: @selector(changePage:)]];
        [self setGest_swipeRight: [[UISwipeGestureRecognizer alloc] initWithTarget: self action: @selector(changePage:)]];
        
        [_gest_swipeLeft setDirection: UISwipeGestureRecognizerDirectionLeft];
        [_gest_swipeRight setDirection: UISwipeGestureRecognizerDirectionRight];
        
        [_scroll_operate addGestureRecognizer:_gest_swipeRight];
        [_scroll_operate addGestureRecognizer:_gest_swipeLeft];
        
        UIImage *image1= [UIImage imageNamed:@"temp1"];
        UIImage *image2= [UIImage imageNamed:@"temp2"];
        UIImage *image3= [UIImage imageNamed:@"temp3"];
        
        UIImageView *imv1 = [[UIImageView alloc] initWithImage:image1];
        UIImageView *imv2 = [[UIImageView alloc] initWithImage:image2];
        UIImageView *imv3 = [[UIImageView alloc] initWithImage:image3];
        
        [_scroll_operate addSubview:imv1];
        [_scroll_operate addSubview:imv2];
        [_scroll_operate addSubview:imv3];
        
        _array_operateImage = @[imv1,imv2,imv3];
    }
}

-(void)setLayout {
    [_scroll_operate setContentSize:CGSizeMake(CGRectGetWidth(_scroll_operate.frame)* [_array_operateImage count],
                                               CGRectGetHeight(_scroll_operate.frame))];
    
    for (int i = 0 ; i < _array_operateImage.count ; i++)     {
        UIImageView *imageView = [_array_operateImage objectAtIndex:i];
        [imageView setContentMode:UIViewContentModeScaleToFill];
        [imageView setFrame:CGRectMake(i * CGRectGetWidth(_scroll_operate.frame),
                                       0,
                                       CGRectGetWidth(_scroll_operate.frame),
                                       CGRectGetHeight(_scroll_operate.frame))];
    }
}

-(void)changePage:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft){
        if (_scroll_operate.contentOffset.x < CGRectGetWidth(_scroll_operate.frame) * ([_array_operateImage count]-1) )
        {
            [self setUserInteractionEnabled: NO];
            [UIView animateWithDuration:0.2
                             animations:^{
                                 float offsetX = _scroll_operate.contentOffset.x + _scroll_operate.frame.size.width;
                                 [_scroll_operate setContentOffset: CGPointMake(offsetX,
                                                                                _scroll_operate.contentOffset.y)];
                                 [_pageControl setCurrentPage: _pageControl.currentPage +1];}
                             completion: ^(BOOL finished) {
                                 [self setUserInteractionEnabled: YES];
                             }];
            
        }
    }
    else if (swipe.direction == UISwipeGestureRecognizerDirectionRight){
        if (_scroll_operate.contentOffset.x <= 0) {
            return;
        }
        else {
            [self setUserInteractionEnabled: NO];
            
            [UIView animateWithDuration: 0.2
                             animations: ^{
                                 float offsetX = _scroll_operate.contentOffset.x - _scroll_operate.frame.size.width;
                                 [_scroll_operate setContentOffset: CGPointMake(offsetX,
                                                                                _scroll_operate.contentOffset.y)];
                                 [_pageControl setCurrentPage: _pageControl.currentPage -1];
                                 
                             }
                             completion:^(BOOL finished) {
                                 [self setUserInteractionEnabled: YES];
                             }];
        }
    }
}

@end
