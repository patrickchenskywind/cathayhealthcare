//
//  LaunchUserInfo.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "SwitchButton.h"
#import "LaunchRemindView.h"

@protocol LaunchTarge2tViewDelegate <NSObject>
@required
    - (void) goWhere;
    - (void) goBack;
    - (void) buttonBitthdayPress: (UIButton *)sender;
    - (BOOL) buttonNextPress;

@end

@interface LaunchTarge2tView : View <LaunchRemindViewDelegate,UITextFieldDelegate>

@property (nonatomic,strong) IBOutlet SwitchButton      *button_gender;
@property (nonatomic,strong) IBOutlet UIView            *textField_birth;
@property (nonatomic,strong) IBOutlet UITextField       *textField_tall;
@property (nonatomic,strong) IBOutlet UITextField       *textField_weight;
@property (nonatomic,strong) IBOutlet UITextField       *textField_heart;
@property (nonatomic,strong) IBOutlet UIButton          *button_next;
@property (nonatomic,strong) IBOutlet UIButton          *button_back;
@property (nonatomic,weak) IBOutlet UILabel            *labelBithDay;
@property (nonatomic,weak) IBOutlet UILabel            *labelBMI;

@property (nonatomic,strong) UIView *maskView;
@property (nonatomic,strong) LaunchRemindView *view_remind;
@property (nonatomic,assign) BOOL remindWasShow;
@property (nonatomic,weak) IBOutlet id<LaunchTarge2tViewDelegate> delegate;
@property (nonatomic,strong) NSString *workType;
@property (nonatomic,strong) NSString *birthdayTime;

@end
