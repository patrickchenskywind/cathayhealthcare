//
//  OperateView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol OperateViewDelegate <NSObject>

-(void)goWhere;

@end

@interface OperateView : View

@property (nonatomic,strong) IBOutlet UIScrollView *scroll_operate;
@property (nonatomic,strong) UISwipeGestureRecognizer *gest_swipeRight;
@property (nonatomic,strong) UISwipeGestureRecognizer *gest_swipeLeft;
@property (nonatomic,strong) NSArray *array_operateImage;
@property (nonatomic,strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic,strong) IBOutlet UIButton      *button_next;
@property (nonatomic,strong) IBOutlet UIButton      *button_skip;

@property (nonatomic,weak)IBOutlet id<OperateViewDelegate> delegate;


@end
