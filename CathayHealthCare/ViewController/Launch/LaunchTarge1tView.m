//
//  LaunchTargetView.m
//  CathayHealthCare
//
//  Created by Patrick on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LaunchTarget1View.h"
#import "LoginEntity.h"
#import "DefindConfig.h"

@implementation LaunchTarget1View

- (void) awakeFromNib {
    [super awakeFromNib];
    [self setupButtonStyle: _button_hardWork];
    [self setupButtonStyle: _button_lowWork];
    [self setupButtonStyle: _button_midWork];
    
    if ([LoginEntity get]) {
        LoginEntity *loginEntity = [LoginEntity get];
        if (loginEntity.workType && loginEntity.workType.length > 0) {
            if ([loginEntity.workType isEqualToString: @"0"])
                [self.button_lowWork setIsSelected: YES];
            else if ([loginEntity.workType isEqualToString: @"1"])
                [self.button_midWork setIsSelected: YES];
            else if ([loginEntity.workType isEqualToString: @"2"])
                [self.button_hardWork setIsSelected: YES];
            
            [self setWorkType: loginEntity.workType];
        }
    }
}

-(IBAction)buttonPress:(UIButton *)button {
    if (button == _button_lowWork) {
        [_button_lowWork setIsSelected: YES];
        [_button_hardWork setIsSelected: NO];
        [_button_midWork setIsSelected: NO];
    }
    else if(button == _button_midWork){
        [_button_lowWork setIsSelected: NO];
        [_button_midWork setIsSelected: YES];
        [_button_hardWork setIsSelected: NO];
    }
    else if (button == _button_hardWork){
        [_button_lowWork setIsSelected: NO];
        [_button_midWork setIsSelected: NO];
        [_button_hardWork setIsSelected: YES];
    }
    else if(button == _button_next){
        if (_button_lowWork.isSelected)
            [self setWorkType: @"0"];
        else if (_button_midWork.isSelected)
            [self setWorkType: @"1"];
        else if (_button_hardWork.isSelected)
            [self setWorkType: @"2"];
        else
            [self setWorkType: @""];
        
        if ( ! [[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
            LoginEntity *loginEntity = [[LoginEntity alloc] init];
            [loginEntity setWorkType: self.workType];
            [loginEntity saveToLocal];
        }
        
        [self.delegate goWhereWithWorkType: self.workType];
    }
}

- (void) setupButtonStyle: (RadioButton *) button {
    [button setYesStyle: [UIImage imageNamed: @"checkbox_target_checked"] andNoStyle: [UIImage imageNamed: @"checkbox_target_uncheck"] isSelect: NO];
}

@end
