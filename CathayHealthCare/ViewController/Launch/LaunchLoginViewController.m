//
//  LoginViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LaunchLoginViewController.h"
#import "DefindConfig.h"

@interface LaunchLoginViewController ()

@end

@implementation LaunchLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"會員登入"];
    [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed: @"titlebar_green.png"] forBarMetrics: UIBarMetricsDefault];
}

-(void)goWhere {
    OperateViewController *operateVC = [[OperateViewController alloc] initWithNibName: @"OperateViewController" bundle: nil];
    [self.navigationController pushViewController: operateVC animated: YES];
}

-(void)goToMain {
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: YES] forKey: HAS_BEEN_LAUNCH];
    [self.navigationController dismissViewControllerAnimated: NO completion: nil];
}

@end
