//
//  LaunchLoginView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"


@protocol LaunchLoginViewDelegate <NSObject>
-(void)goWhere;
-(void)goToMain;
@end

@interface LaunchLoginView : View <UITextFieldDelegate>

@property (nonatomic,strong) IBOutlet UIView *view_loginWindow;
@property (nonatomic,strong) IBOutlet UIButton *button_login;
@property (nonatomic,strong) IBOutlet UIButton *button_skip;

@property (nonatomic,strong) UITextField *textField_account;
@property (nonatomic,strong) UITextField *textField_password;
@property (nonatomic,strong) UIView      *view_borderLine;

@property (nonatomic,weak) IBOutlet id<LaunchLoginViewDelegate> delegate;

@end
