//
//  LaunchTarget3ViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LaunchTarget3ViewController.h"
#import "DefindConfig.h"
#import "UIColor+Hex.h"
#import "LoginEntity.h"
#import "SetBodyManageAPI.h"
#import "Toos.h"

@interface LaunchTarget3ViewController ()<UITextFieldDelegate>
@end

@implementation LaunchTarget3ViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(textFieldTextDidChangeNotification:)
                                                 name: UITextFieldTextDidChangeNotification
                                               object: nil];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setupDefaultInfo];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}


- (void) textFieldTextDidChangeNotification:(NSNotification *)notification {
    if ([notification.object isEqual: self.view.text_target]) {
        [self setupTargetButton];
    }
}

- (void) setupTargetButton {
    if (self.view.text_target.text.length > 0 && [LoginEntity get]) {
        LoginEntity *loginEntity = [LoginEntity get];
        if (loginEntity.weight && loginEntity.weight.length > 0) {
            [self setupLabelTargetWithWeight: [loginEntity.weight doubleValue] target: [self.view.text_target.text doubleValue]];
        }
    }
}

- (void) setupLabelTargetWithWeight: (double) weight target: (double) target {
    if (weight > target) {
        [self.view.button_increase setIsSelected: NO];
        [self.view.button_decrease setIsSelected: YES];
        [self.view.button_keep setIsSelected: NO];
    }
    else if (weight < target){
        [self.view.button_increase setIsSelected: YES];
        [self.view.button_decrease setIsSelected: NO];
        [self.view.button_keep setIsSelected: NO];
    }
    else{
        [self.view.button_increase setIsSelected: NO];
        [self.view.button_decrease setIsSelected: NO];
        [self.view.button_keep setIsSelected: YES];
    }
}

- (void) setupDefaultInfo {
    if ([LoginEntity get]) {
        LoginEntity *loginEntity = [LoginEntity get];
        
        NSString *weight = @"";
        NSString *minWeightString = @"";
        NSString *maxWeightString = @"";
        if ( loginEntity.weight && loginEntity.weight.length > 0 ) {
            weight = loginEntity.weight;
            if (loginEntity.height && loginEntity.height.length > 0 ) {
                CGFloat minWeight = (([loginEntity.height doubleValue] / 100) * ([loginEntity.height doubleValue] / 100)) * 18.5;
                minWeightString = [NSString stringWithFormat: @"%0.1f", minWeight];
                
                CGFloat maxWeight = (([loginEntity.height doubleValue] / 100) * ([loginEntity.height doubleValue] / 100)) * 24;
                maxWeightString = [NSString stringWithFormat: @"%0.1f", maxWeight];
            }
            
            if (loginEntity.weightGoal && loginEntity.weightGoal.length > 0)
                [self.view.text_target setText: loginEntity.weightGoal];
            else
                [self.view.text_target setText: loginEntity.weight];
        }
        
        [self.view.labelTitle setText: [NSString stringWithFormat: @"您現在的體重是 %@ 公斤\n理想體重範圍是 %@ ~ %@ 公斤",weight,minWeightString,maxWeightString]];
        
        if (loginEntity.weightGoal && loginEntity.weightGoal.length > 0)
            [self.view.text_target setText: loginEntity.weightGoal];
        
        if (loginEntity.weightTarget && loginEntity.weightTarget.length > 0)
            [self setupWeightTarget: loginEntity];
        
        [self setupTargetButton];
    }
}

- (void) setupWeightTarget : (LoginEntity *)loginEntity {
    if ([loginEntity.weightTarget isEqualToString: @"0.0"] || [loginEntity.weightTarget isEqualToString: @"0"]) {
        [self.view.button_keep setIsSelected: YES];
    }
    else {
        if ([loginEntity.weightTarget rangeOfString: @"-"].length > 0) {
            [self.view.button_decrease setIsSelected: YES];
            [self.view.text_targetSub setText: [loginEntity.weightTarget stringByReplacingOccurrencesOfString: @"-" withString: @""]];
        }
        else {
            [self.view.button_increase setIsSelected: YES];
            [self.view.text_targetAdd setText: loginEntity.weightTarget];
        }
    }
}

-(IBAction) goBack: (id)sender {
    [self.navigationController popViewControllerAnimated: YES];
}

-(IBAction) goToMain: (id)sender  {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetBodyManageAPI *setBodyManageAPI = [[SetBodyManageAPI alloc] init];
        [setBodyManageAPI setWeightGoal: self.view.text_target.text];
        [setBodyManageAPI setWeightTarget: [self getWeightTargerText]];
        [setBodyManageAPI post:^(HttpResponse *response) {
            BaseEntity *baseEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: baseEntity.msg]) {
                [self saveBodySettingToLocal];
            }
        }];
    }
    else {
        [self saveBodySettingToLocal];
    }
}

- (NSString *) getWeightTargerText {
    if (self.view.button_keep.isSelected)
        return @"0";
    else if (self.view.button_increase.isSelected)
        return self.view.text_targetAdd.text;
    else if (self.view.button_decrease.isSelected)
        return [NSString stringWithFormat:@"-%@",self.view.text_targetSub.text];
    else
        return @"0";
}

- (void) saveBodySettingToLocal {
    if ([LoginEntity get])
        [self setupLoginEntity: [LoginEntity get]];
    else
        [self setupLoginEntity: [[LoginEntity alloc] init]];
}

- (void) setupLoginEntity: (LoginEntity *)loginEntity {
    [loginEntity setWeightGoal: self.view.text_target.text];
    [loginEntity setWeightTarget: [self getWeightTargerText]];
    [loginEntity saveToLocal];
    
    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool: YES]
                                              forKey: HAS_BEEN_LAUNCH];
    [self.navigationController dismissViewControllerAnimated: NO
                                                  completion: nil];
}

- (void) setupUI {
    [self setTitle: @"管理目標"];
    [self.navigationItem setHidesBackButton: YES];
    
    UIImage *yesStyle = [UIImage imageNamed: @"checkbox_target_checked"];
    UIImage *noStyle  = [UIImage imageNamed: @"checkbox_target_uncheck"];
    
    [self.view.button_keep setYesStyle: yesStyle andNoStyle: noStyle isSelect: NO];
    [self.view.button_decrease setYesStyle: yesStyle andNoStyle: noStyle isSelect: NO];
    [self.view.button_increase setYesStyle: yesStyle andNoStyle: noStyle isSelect: NO];
    
    [self.view.text_target.layer setBorderColor: [[UIColor colorWithHex:0xCCB2A0] CGColor]];
    [self.view.text_target.layer setBorderWidth: 1];
    [self.view.text_target.layer setCornerRadius: 5];
    
    [self.view.text_targetAdd.layer setBorderColor: [[UIColor colorWithHex:0x00A29A] CGColor]];
    [self.view.text_targetAdd.layer setBorderWidth: 1];
    [self.view.text_targetAdd.layer setCornerRadius: 5];
    
    [self.view.text_targetSub.layer setBorderColor: [[UIColor colorWithHex:0x00A29A] CGColor]];
    [self.view.text_targetSub.layer setBorderWidth: 1];
    [self.view.text_targetSub.layer setCornerRadius: 5];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
