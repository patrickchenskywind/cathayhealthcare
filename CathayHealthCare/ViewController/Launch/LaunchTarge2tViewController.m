//
//  LaunchUserInfoViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LaunchTarge2tViewController.h"

#import "LaunchTarget3ViewController.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "Toos.h"
#import "LoginEntity.h"
#import "UIColor+Hex.h"

@interface LaunchTarge2tViewController ()<LaunchTarge2tViewDelegate, IDatePickerViewController , UITextFieldDelegate>
@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) NSDate * birthdayTime;
@end

@implementation LaunchTarge2tViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(textFieldTextDidChangeNotification:) name: UITextFieldTextDidChangeNotification object: nil];
    ;
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self setupDefaultInfo];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void) setupDefaultInfo {
    if ([LoginEntity get]) {
        LoginEntity *loginEntity = [LoginEntity get];
        if (loginEntity.gender && loginEntity.gender.length > 0) {
            if ([loginEntity.gender isEqualToString: @"0"])
                [self.view.button_gender setIsOpen: YES];
        }
        if (loginEntity.birthday && loginEntity.birthday.length > 0 )
            [self.view.labelBithDay setText: [Toos getYMDStringFormServerTimeInterval: loginEntity.birthday]];
        
        if (loginEntity.height && loginEntity.height.length > 0 )
            [self.view.textField_tall setText: loginEntity.height];
        
        if (loginEntity.weight && loginEntity.weight.length > 0)
            [self.view.textField_weight setText: loginEntity.weight];
        
        [self setupBMI];
        
        if (loginEntity.heartBeat && loginEntity.heartBeat.length > 0)
            [self.view.textField_heart setText: loginEntity.heartBeat];
    }
}

- (void) setWorkType:(NSString *)workType {
    _workType = workType;
    [self.view setWorkType: self.workType];
}

- (void) textFieldTextDidChangeNotification:(NSNotification *)notification {
    if ([notification.object isEqual: self.view.textField_tall] || [notification.object isEqual: self.view.textField_weight] )
        [self setupBMI];
}

- (void) setupBMI {
    if (self.view.textField_weight.text.length > 0 && self.view.textField_tall.text.length > 0) {
        CGFloat height = [self.view.textField_tall.text floatValue] / 100;
        CGFloat weight = [self.view.textField_weight.text floatValue];
        CGFloat bmi = weight / (height * height);
        [self.view.labelBMI setText: [NSString stringWithFormat: @"%.2f",bmi]];
    }
    else {
        [self.view.labelBMI setText: @""];
    }
}

- (BOOL) buttonNextPress {
    if (( self.workType && self.workType.length > 0 ) &&
        ( self.view.labelBithDay.text && self.view.labelBithDay.text.length > 0) &&
        ( self.view.textField_tall.text && self.view.textField_tall.text.length > 0) &&
        ( self.view.textField_weight.text && self.view.textField_weight.text.length > 0) &&
        ( self.view.textField_heart && self.view.textField_heart.text.length > 0) ) {
        return YES;
    }
    else {
        return NO;
    }
}

- (void) goWhere {
    LaunchTarget3ViewController *target3_VC = [[LaunchTarget3ViewController alloc] initWithNibName: @"LaunchTarget3ViewController" bundle: nil];
    [self.navigationController pushViewController: target3_VC animated: YES];
}

- (void) goBack {
    [self.navigationController popViewControllerAnimated: YES];
}

- (void) buttonBitthdayPress: (UIButton *)sender {
    [self closeKeyboard];
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    
    
    
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *) date{
    [self setBirthdayTime: date];
    [self.view setBirthdayTime: [Toos getThousandSecondStringFromDate: date]];
    [self.view.labelBithDay setText: [Toos getYMDFormDate: date]];
}

- (void) setupUI {
    [self.navigationItem setHidesBackButton:YES];
    [self setTitle:@"基本資料"];
    [self.view.button_gender setOnName: @"女"
                      offName: @"男"
                        color: [UIColor colorWithHex:0x00A29A]
                    nameColor: [UIColor whiteColor]
                       isOpen: NO];
    
    self.view.textField_tall.layer.borderColor = [[UIColor colorWithHex: 0x00A29A] CGColor];
    self.view.textField_tall.layer.borderWidth = 1;
    self.view.textField_tall.layer.cornerRadius = 5;
    
    self.view.textField_heart.layer.borderColor = [[UIColor colorWithHex: 0x00A29A] CGColor];
    self.view.textField_heart.layer.borderWidth = 1;
    self.view.textField_heart.layer.cornerRadius = 5;
    
    self.view.textField_weight.layer.borderColor = [[UIColor colorWithHex: 0x00A29A] CGColor];
    self.view.textField_weight.layer.borderWidth = 1;
    self.view.textField_weight.layer.cornerRadius = 5;
    
    self.view.textField_birth.layer.borderColor = [[UIColor colorWithHex: 0x00A29A] CGColor];
    self.view.textField_birth.layer.borderWidth = 1;
    self.view.textField_birth.layer.cornerRadius = 5;
}

@end
