//
//  LaunchViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "MainViewController.h"
#import "LaunchLoginViewController.h"

@interface LaunchViewController : ViewController
@property (nonatomic,strong) NSTimer *timer;
@end
