//
//  LaunchUserInfoViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "LaunchTarge2tView.h"

@interface LaunchTarge2tViewController : ViewController

@property (strong, nonatomic) NSString * workType;

@property (retain, nonatomic) LaunchTarge2tView *view;

@end
