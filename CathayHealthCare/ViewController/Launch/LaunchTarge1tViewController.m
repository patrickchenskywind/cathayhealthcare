//
//  LaunchTargetViewController.m
//  CathayHealthCare
//
//  Created by Patrick on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LaunchTarge1tViewController.h"
#import "LaunchTarge2tViewController.h"

@implementation LaunchTarget1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton: YES];
    [self setTitle: @"管理目標"];
}

-(void)goWhereWithWorkType: (NSString *) workType {
    LaunchTarge2tViewController *launchTarge2tViewController = [[LaunchTarge2tViewController alloc] initWithNibName: @"LaunchTarge2tViewController" bundle: nil];
    [launchTarge2tViewController setWorkType: workType];
    [self.navigationController pushViewController: launchTarge2tViewController animated: YES];
}

@end
