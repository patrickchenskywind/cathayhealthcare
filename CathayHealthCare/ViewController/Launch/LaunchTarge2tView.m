//
//  LaunchUserInfo.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LaunchTarge2tView.h"
#import "UIColor+Hex.h"
#import "DefindConfig.h"
#import "LoginEntity.h"
#import "Toos.h"
#import "SetBaseSettingAPI.h"

@implementation LaunchTarge2tView

- (IBAction)buttonBirthdayPress:(id)sender {
    [self.delegate buttonBitthdayPress: sender];
}

-(IBAction)buttonPress:(id)sender {
    if (sender == _button_back) {
        [_delegate goBack];
    }
    else if (sender == _button_next){
        if ( ! [self.delegate buttonNextPress]) {
            if (!_maskView) {
                _maskView = [[UIView alloc] initWithFrame:self.bounds];
                [_maskView setBackgroundColor:[UIColor blackColor]];
                [_maskView setAlpha:0.5];
                [self addSubview:_maskView];
            }
            if (!_view_remind) {
                _view_remind = [[[NSBundle mainBundle] loadNibNamed: @"LaunchRemindView" owner: self options: nil] objectAtIndex:0];
                [_view_remind setFrame :CGRectMake(0,
                                                   0,
                                                   CGRectGetWidth(self.bounds)-60,
                                                   CGRectGetHeight(self.bounds)-200)];
                [_view_remind setCenter: CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2)];
                [_view_remind setDelegate: self];
                [self addSubview: _view_remind];
            }
        }
        else {
            [self executDataHandle];
        }
    }
}

- (void) executDataHandle {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetBaseSettingAPI *api = [[SetBaseSettingAPI alloc] init];
        [api setGender: self.button_gender.isOpen ? @"1" : @"0"];
        [api setBirthday: self.birthdayTime];
        [api setHeight: self.textField_tall.text];
        [api setWeight: self.textField_weight.text];
        [api setWorkType: self.workType];
        [api setHeartbeat: [LoginEntity get].heartBeat];
        [api post:^(HttpResponse *response) {
            BaseEntity *baseEntity = response.result.firstObject;
            if ( [Toos checkMsgCodeWihtMsg: baseEntity.msg] ) {
                [self saveBaseSettingToLocal];
            }
        }];
    }
    else {
        [self saveBaseSettingToLocal];
    }
}

- (void) saveBaseSettingToLocal {
    if ([LoginEntity get])
        [self setupLoginEntity: [LoginEntity get]];
    else
        [self setupLoginEntity: [[LoginEntity alloc] init]];
}

- (void) setupLoginEntity: (LoginEntity *)loginEntity {
    [loginEntity setGender: self.button_gender.isOpen ? @"1" : @"0"];
    [loginEntity setBirthday: self.birthdayTime];
    [loginEntity setHeight: self.textField_tall.text];
    [loginEntity setWeight: self.textField_weight.text];
    [loginEntity setWorkType: self.workType];
    [loginEntity setHeartBeat: self.textField_heart.text];
    [loginEntity saveToLocal];
    [self.delegate goWhere];
}

-(void)disMissLaunchRemindView {
    if (_view_remind) {
        [_view_remind removeFromSuperview];
        _view_remind = nil;
    }
    if (_maskView) {
        [_maskView removeFromSuperview];
        _maskView = nil;
    }
    [self executDataHandle];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
