//
//  LaunchViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LaunchViewController.h"
#import "DefindConfig.h"

@implementation LaunchViewController

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _timer = [NSTimer scheduledTimerWithTimeInterval: 1
                                              target: self
                                            selector: @selector(goWhere:)
                                            userInfo: nil
                                             repeats: NO];
}

-(void)goWhere:(NSTimer *)timer{
    BOOL hasBeenLaunch = [[[NSUserDefaults standardUserDefaults] objectForKey: HAS_BEEN_LAUNCH] boolValue];
    if ( ! hasBeenLaunch) {
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController: [[LaunchLoginViewController alloc] initWithNibName: @"LaunchLoginViewController"
                                                                                                                                               bundle: nil]];
        [self presentViewController: navi animated: NO completion: nil];
    }
    else {
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController: [[MainViewController alloc] initWithNibName: @"MainViewController" bundle: nil]];
        [navi setModalTransitionStyle: UIModalTransitionStyleCrossDissolve];
        [self presentViewController: navi animated: YES completion: nil];
    }
    _timer = nil;
}

@end
