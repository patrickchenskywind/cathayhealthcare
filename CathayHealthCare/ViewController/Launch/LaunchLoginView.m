//
//  LaunchLoginView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LaunchLoginView.h"
#import "UIColor+Hex.h"
#import "LoginAPI.h"
#import "Toos.h"

@implementation LaunchLoginView

- (void) awakeFromNib {
    [super awakeFromNib];
    [self newObject];
    [self setLayout];
}

- (IBAction)button_login_press:(id)sender {
    //    if (self.textField_account.text.length > 0 && self.textField_password.text.length > 0) {
    //    LoginAPI * api = [[LoginAPI alloc] init];
    //        [api setAccount: self.textField_account.text];
    //        [api setPassword: self.textField_password.text];
    //        [api setDeviceType: @"0"];
    //        [api setPushToken: [[NSUserDefaults standardUserDefaults] objectForKey: DEVICE_TOKEN]];
    
    //    [api post:^(HttpResponse *response) {
    //        LoginEntity *loginEntity = response.result.firstObject;
    //        if ([loginEntity.msg isEqualToString: SUCCESSCODE]) {
    //            [[NSUserDefaults standardUserDefaults] setObject: loginEntity.accessToken forKey: ACCESSTOKEN];
    //            [[NSUserDefaults standardUserDefaults] synchronize];
    //            [loginEntity saveToLocal];
    //            [self.delegate goToMain];
    //        }
    //    }];
    //    }
    
#warning 使用假資料
    LoginAPI * api = [[LoginAPI alloc] init];
    [api setAccount: @"apple60210@yahoo.com.tw"];
    [api setPassword: @"000000"];
    [api setDeviceType: @"1"];
    [api setPushToken: @"b62afd4d85634e699a3f1b73770ffc02"];
    [api post:^(HttpResponse *response) {
        LoginEntity *loginEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg: loginEntity.msg]) {
            [[NSUserDefaults standardUserDefaults] setObject: loginEntity.accessToken forKey: ACCESSTOKEN];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [loginEntity saveToLocal];
            [self.delegate goWhere];
        }
    }];
}

- (IBAction)button_skip_press:(id)sender {
    [_delegate goWhere];
}
- (IBAction)button_forgot_password:(id)sender {
    NSLog(@"button_forgot_password");
}

-(void)newObject{
    if (!_textField_account) {
        _textField_account = [[UITextField alloc] init];
        [_textField_account setTextAlignment: NSTextAlignmentCenter];
        [_textField_account setTextColor: [UIColor colorWithHex:0x7FD0CC]];
        [_textField_account setBorderStyle: UITextBorderStyleNone];
        _textField_account.attributedPlaceholder = [[NSAttributedString alloc] initWithString: @"請輸入帳號或ID"
                                                                                   attributes: @{NSForegroundColorAttributeName: [UIColor colorWithHex: 0x7FD0CC]}];
        [_textField_account setDelegate: self];
        [_view_loginWindow addSubview: _textField_account];
    }
    if (!_textField_password) {
        _textField_password = [[UITextField alloc] init];
        [_textField_password setTextAlignment: NSTextAlignmentCenter];
        _textField_password.attributedPlaceholder = [[NSAttributedString alloc] initWithString: @"請輸入密碼"
                                                                                    attributes: @{NSForegroundColorAttributeName: [UIColor colorWithHex: 0x7FD0CC]}];
        [_textField_password setTextColor: [UIColor colorWithHex:0x7FD0CC]];
        [_textField_password setBorderStyle: UITextBorderStyleNone];
        [_textField_password setDelegate: self];
        [_view_loginWindow addSubview: _textField_password];
    }
    if (!_view_borderLine) {
        _view_borderLine = [[UIView alloc] init];
        [_view_borderLine setBackgroundColor: [UIColor colorWithHex: 0x00A29A]];
        [_view_loginWindow addSubview: _view_borderLine];
    }
}

-(void)setLayout{
    _view_loginWindow.layer.borderColor = [[UIColor colorWithHex: 0x00A29A] CGColor];
    _view_loginWindow.layer.borderWidth = 1;
    _view_loginWindow.layer.cornerRadius = 14;
    
    [_textField_account setFrame:CGRectMake(0,
                                            0,
                                            CGRectGetWidth(_view_loginWindow.frame),
                                            CGRectGetHeight(_view_loginWindow.frame)/2)];
    
    
    [_textField_password setFrame:CGRectMake(0,
                                             CGRectGetMaxY(_textField_account.frame),
                                             CGRectGetWidth(_textField_account.frame),
                                             CGRectGetHeight(_textField_account.frame))];
    
    [_view_borderLine setFrame:CGRectMake(0,
                                          0,
                                          CGRectGetWidth(_view_loginWindow.frame) -20,
                                          1)];
    [_view_borderLine setCenter:CGPointMake(CGRectGetWidth(_view_loginWindow.frame)/2,
                                            CGRectGetHeight(_view_loginWindow.frame)/2)];
    
}

@end
