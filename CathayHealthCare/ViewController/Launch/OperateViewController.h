//
//  OperateViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "OperateView.h"
#import "LaunchTarge1tViewController.h"

@interface OperateViewController : ViewController<OperateViewDelegate>

@end
