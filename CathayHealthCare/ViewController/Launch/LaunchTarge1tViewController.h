//
//  LaunchTargetViewController.h
//  CathayHealthCare
//
//  Created by Patrick on 2015/7/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "LaunchTarget1View.h"

@interface LaunchTarget1ViewController : ViewController<LaunchTarget1ViewDelegate>
@end
