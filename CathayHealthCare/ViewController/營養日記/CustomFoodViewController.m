//
//  CustomFoodViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "CustomFoodViewController.h"
#import "UIColor+Hex.h"
#import "CustomFoodCell.h"
#import "SetNewFoodAPI.h"
#import "Toos.h"
#import "FoodListEntity.h"


enum Config
{
    CELLHEIGHT = 44,
    
    SIXFOODTAG_TOAST = 0,
    SIXFOODTAG_FISH  = 1,
    SIXFOODTAG_MILK  = 2,
    SIXFOODTAG_VEGE  = 3,
    SIXFOODTAG_FRUIT = 4,
    SIXFOODTAG_PEANUT= 5,
    
    TAG_FAT          = 1000,
    TAG_CARB         = 2000
};

@interface CustomFoodViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    BOOL isFatOpen;
    BOOL isCarbOpen;
    
    NSArray *array_sixFoodTitle;
    NSArray *array_nutrtionTitle;
    NSArray *array_sixFoodUnit;
    NSArray *array_nutrtionUnit;
    NSArray *array_fatTitle;
    NSArray *array_fatUnit;
    NSArray *array_carbTitle;
    NSArray *array_carbUnit;
    
    UITableView   *table_Main;
    UITableView   *table_Fat;
    UITableView   *table_Carb;
    UIScrollView  *scroll_Main;
    
    UITextField *text_name;
    UITextField *text_explain;
    UITextField *text_cal;
    UITextField *text_unit;
    
    UIImageView *imv_camera;
    UIButton    *button_send;
    
    UITextField *currentTextField;
    
}
@end

@implementation CustomFoodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"自訂食物"];
    [self.view setBackgroundColor:[UIColor colorWithHex:0xFFF1CF]];
    
    [self initialParameter];
    [self initialScrollView];
    [self initialStandardForm];
    [self initialTables];
    [self initialSendButton];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self setupLayout];
}

#pragma mark initialize

-(void)initialParameter
{
    array_sixFoodTitle  = @[@"全穀根莖類",@"豆魚肉蛋類",@"低脂乳品類",@"蔬菜類",@"水果類",@"油脂及堅果種子類"];
    array_sixFoodUnit   = @[@"碗",@"份",@"杯",@"碟",@"份",@"茶匙",];
    array_nutrtionTitle = @[@"蛋白質",@"脂肪",@"碳水化合物",@"鈉",@"鉀",@"鈣",@"鐵"];
    array_nutrtionUnit  = @[@"公克",@"公克",@"公克",@"毫克",@"毫克",@"毫克",@"毫克"];
    array_fatTitle      = @[@"單元不飽和",@"多元不飽和",@"飽和",@"反式",@"膽固醇"];
    array_fatUnit       = @[@"公克"     ,@"公克"     ,@"公克",@"公克",@"毫克"];
    array_carbTitle     = @[@"糖",@"膳食纖維"];
    array_carbUnit      = @[@"公克",@"公克"];
    isFatOpen  = NO;
    isCarbOpen = NO;
}

-(void)initialScrollView
{
    scroll_Main = [[UIScrollView alloc] init];
    [self.view addSubview:scroll_Main];
}

-(void)initialTables
{
    
    if (!table_Main) {
        table_Main = [[UITableView alloc] initWithFrame:self.view.bounds];
        table_Main.separatorStyle = UITableViewCellSeparatorStyleNone;
        table_Main.delegate = self;
        table_Main.dataSource = self;
        table_Main.scrollEnabled = NO;
        [scroll_Main addSubview:table_Main];
    }
    
    if (!table_Fat) {
        table_Fat = [[UITableView alloc] init];
        table_Fat.separatorStyle = UITableViewCellSeparatorStyleNone;
        table_Fat.delegate = self;
        table_Fat.dataSource = self;
        table_Fat.scrollEnabled = NO;
    }
    
    if (!table_Carb) {
        table_Carb = [[UITableView alloc] init];
        table_Carb.separatorStyle = UITableViewCellSeparatorStyleNone;
        table_Carb.delegate = self;
        table_Carb.dataSource = self;
        table_Carb.scrollEnabled = NO;
    }
    
}


-(void)initialStandardForm
{
    
    text_name          = [[UITextField alloc] init];
    text_name.delegate = self;
    [text_name       setPlaceholder:@"品名 ex.香蕉"];
    [text_name       setBackgroundColor:[UIColor whiteColor]];
    [text_name.layer setCornerRadius:5];
    [text_name.layer setBorderColor:[UIColor colorWithHex:0x917F72].CGColor];
    [text_name.layer setBorderWidth:1];
    [text_name       setTextAlignment:NSTextAlignmentCenter];
    [scroll_Main     addSubview:text_name];
    [self addButtonToKeyBoardPadWithTextField:text_name];
    
    text_explain          = [[UITextField alloc] init];
    text_explain.delegate = self;
    [text_explain       setPlaceholder:@"說明 ex.美而美"];
    [text_explain       setBackgroundColor:[UIColor whiteColor]];
    [text_explain.layer setCornerRadius:5];
    [text_explain.layer setBorderColor:[UIColor colorWithHex:0x917F72].CGColor];
    [text_explain.layer setBorderWidth:1];
    [text_explain       setTextAlignment:NSTextAlignmentCenter];
    [scroll_Main        addSubview:text_explain];
    [self addButtonToKeyBoardPadWithTextField:text_explain];
    
    text_cal          = [[UITextField alloc] init];
    text_cal.delegate = self;
    [text_cal       setPlaceholder:@"卡路里"];
    [text_cal       setBackgroundColor:[UIColor whiteColor]];
    [text_cal.layer setCornerRadius:5];
    [text_cal.layer setBorderColor:[UIColor colorWithHex:0x917F72].CGColor];
    [text_cal.layer setBorderWidth:1];
    [text_cal       setTextAlignment:NSTextAlignmentCenter];
    [scroll_Main    addSubview:text_cal];
    [self addButtonToKeyBoardPadWithTextField:text_cal];
    
    text_unit          = [[UITextField alloc] init];
    text_unit.delegate = self;
    [text_unit       setPlaceholder:@"份量"];
    [text_unit       setBackgroundColor:[UIColor whiteColor]];
    [text_unit.layer setCornerRadius:5];
    [text_unit.layer setBorderColor:[UIColor colorWithHex:0x917F72].CGColor];
    [text_unit.layer setBorderWidth:1];
    [text_unit       setTextAlignment:NSTextAlignmentCenter];
    [scroll_Main     addSubview:text_unit];
    [self addButtonToKeyBoardPadWithTextField:text_unit];
    
    imv_camera = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_camera_health_normal"]];
    [imv_camera  setBackgroundColor:[UIColor clearColor]];
    
    [scroll_Main addSubview:imv_camera];
}



-(void)initialSendButton
{
    button_send = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button_send setTitle:@"送出" forState:UIControlStateNormal];
    [button_send setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button_send setBackgroundColor:[UIColor colorWithHex:0xED8700]];
    [button_send addTarget:self action:@selector(sendButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button_send];
}

#pragma  mark Layout Setting
-(void)setupLayout{
    
    [self setScrollViewLayout];
    [self setStandardFormLayout];
    [self setTableLayout];
    [self setBottomLayout];
    
    [scroll_Main setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(table_Main.frame))];
}

-(void)setScrollViewLayout{
    
    float navigationHeight = self.navigationController.navigationBar.bounds.size.height;
    float statusBarHeight  = [UIApplication sharedApplication].statusBarFrame.size.height;
    float standarY         = navigationHeight + statusBarHeight;
    
    [scroll_Main setFrame:CGRectMake(0, standarY, self.view.frame.size.width, self.view.frame.size.height-standarY -CELLHEIGHT)];
}

-(void)setStandardFormLayout{
    
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    float textWidth  = screenWidth * 0.6;
    float textHeight = 30;
    float textX      = screenWidth * 0.2;
    float textBorder = 20;
    float imvWidth = screenWidth * 0.25;
    
    [text_name    setFrame:CGRectMake(textX,textBorder,textWidth,textHeight)];
    [text_explain setFrame:CGRectMake(text_name.frame.origin.x,CGRectGetMaxY(text_name.frame)+textBorder,textWidth,textHeight)];
    [text_cal     setFrame:CGRectMake(text_explain.frame.origin.x,CGRectGetMaxY(text_explain.frame)+textBorder,textWidth,textHeight)];
    [text_unit    setFrame:CGRectMake(text_cal.frame.origin.x,CGRectGetMaxY(text_cal.frame)+textBorder,textWidth,textHeight)];
    [imv_camera   setFrame:CGRectMake(screenWidth - (textBorder + imvWidth),CGRectGetMaxY(text_unit.frame)+textBorder,imvWidth, imvWidth)];
}

-(void)setTableLayout{
    
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    int   count       = (int)(array_sixFoodTitle.count + array_nutrtionTitle.count + array_carbTitle.count + array_fatTitle.count);
    
    [table_Main setFrame:CGRectMake(0, CGRectGetMaxY(imv_camera.frame)+20, screenWidth, CELLHEIGHT * count)];
}

-(void)setBottomLayout
{
    float screenWidth = [UIScreen mainScreen].bounds.size.width;
    [button_send setFrame:CGRectMake(0, CGRectGetMaxY(scroll_Main.frame), screenWidth, CELLHEIGHT+5)];
}


#pragma mark TableView Section Setting

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == table_Main)
        return 2;
    else
        return 1;
}


#pragma mark TableView Row Setting

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_Main && indexPath.section == 1) {
        if (indexPath.row == 1) {
            if (isFatOpen) {
                return CELLHEIGHT * 6;
            }
        }
        else if(indexPath.row == 2){
            if (isCarbOpen) {
                return CELLHEIGHT * 3;
            }
        }
    }
    return CELLHEIGHT;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_Main) {
        if (section == 0)
            return 6;
        else
            return 7;
    }
    else if(tableView == table_Fat){
        return 5;
    }
    else{
        return 2;
    }
}

#pragma mark TableView Cell making

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (tableView == table_Main) {
        
        if (indexPath.section == 0) {
            cell = [self sixFoodCellWithIndexPath:indexPath andTableView:tableView];
        }
        else{
            cell = [self nutritionCellWithIndexPath:indexPath andTableView:tableView];
        }
    }
    else if (tableView == table_Fat){
        
        cell = [self fatCellWithIndexPath:indexPath andTableView:tableView];
    }
    else{
        cell = [self carbCellWithIndexPath:indexPath andTableView:tableView];
    }
    
    ((CustomFoodCell *)cell).textField.delegate = self;
    [self addButtonToKeyBoardPadWithTextField:((CustomFoodCell *)cell).textField];
    
    return cell;
}

-(UITableViewCell *)nutritionCellWithIndexPath:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView
{
    
    
    CustomFoodCell *cell = [[CustomFoodCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:@"nutritionCell"];
    NSString *title = [NSString stringWithFormat:@"   %@",array_nutrtionTitle[indexPath.row]];
    NSString *unit  = [NSString stringWithFormat:@"   %@",array_nutrtionUnit[indexPath.row]];
    cell.label_title.text = title;
    cell.label_unit.text  = unit;
    
    if (indexPath.row == 1) {
        
        float height = isFatOpen ? CELLHEIGHT * 5 : 0;
        
        cell.isArrowHidden = NO;
        cell.isOpen = isFatOpen;
        cell.textField.enabled = NO;
        cell.backgroundColor = [UIColor colorWithHex:0xCCB2A0];
        [cell addSubview:table_Fat];
        
        [table_Fat setFrame:CGRectMake(0, CELLHEIGHT, [UIScreen mainScreen].bounds.size.width , height)];
    }
    else if (indexPath.row == 2){
        
        float height = isCarbOpen ? CELLHEIGHT * 2 : 0;
        
        cell.isArrowHidden = NO;
        cell.isOpen = isCarbOpen;
        cell.textField.enabled = NO;
        cell.backgroundColor = [UIColor colorWithHex:0xCCB2A0];
        [cell addSubview:table_Carb];
        
        [table_Carb setFrame:CGRectMake(0, CELLHEIGHT, [UIScreen mainScreen].bounds.size.width, height)];
    }
    
    return cell;
}

-(UITableViewCell *)sixFoodCellWithIndexPath:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView
{
    
    CustomFoodCell *cell = [[CustomFoodCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"sixFoodCell"];
    
    NSString *title = [NSString stringWithFormat:@"   %@",array_sixFoodTitle[indexPath.row]];
    NSString *unit  = [NSString stringWithFormat:@"   %@",array_sixFoodUnit[indexPath.row]];
    cell.label_title.text = title;
    cell.label_unit.text  = unit;
    
    return cell;
}

-(UITableViewCell *)fatCellWithIndexPath:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView{
    
    
    
    CustomFoodCell *cell = [[CustomFoodCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:@"fatCell"];
    NSString *title = [NSString stringWithFormat:@"      %@",array_fatTitle[indexPath.row]];
    NSString *unit  = [NSString stringWithFormat:@"   %@",array_fatUnit[indexPath.row]] ;
    cell.label_title.text = title;
    cell.label_unit.text  = unit;
    cell.textField.tag = TAG_FAT;
    
    return cell;
}

-(UITableViewCell *)carbCellWithIndexPath:(NSIndexPath *)indexPath andTableView:(UITableView *)tableView{
    
    CustomFoodCell *cell = [[CustomFoodCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:@"carbCell"];
    
    NSString *title = [NSString stringWithFormat:@"      %@",array_carbTitle[indexPath.row]];
    NSString *unit  = [NSString stringWithFormat:@"  %@",array_carbUnit[indexPath.row]];
    cell.label_title.text = title;
    cell.label_unit.text  = unit;
    cell.textField.tag = TAG_CARB;
    
    return cell;
}




#pragma mark TableView Header
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (tableView == table_Main)
        return CELLHEIGHT;
    else
        return 0;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    if (tableView == table_Main) {
        
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_customfood"]];
        UILabel     *label   = [[UILabel alloc] init];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTextColor:[UIColor blackColor]];
        
        if (section == 0)
            [label setText:@"六大食物分類"];
        else
            [label setText:@"營養素分析"];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
        [label   setFrame:view.bounds];
        [imgView setFrame:view.bounds];
        [view    addSubview:imgView];
        [view    addSubview:label];
        return view;
    }
    else{
        return nil;
    }
    
}

#pragma mark TableView Select Event

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomFoodCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (tableView == table_Main && indexPath.section == 1) {
        
        if (indexPath.row == 1) {
            isFatOpen = !isFatOpen;
            cell.isOpen = isFatOpen;
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
        }
        else if (indexPath.row == 2){
            isCarbOpen = !isCarbOpen;
            cell.isOpen = isCarbOpen;
            [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
        }
        
    }
}

#pragma mark UITextField Delegate

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.tag == TAG_FAT) {
        
        int sum = 0;
        for (int i = 0; i < array_fatTitle.count ; i++) {
            
            NSIndexPath    *indextPath = [NSIndexPath indexPathForRow:i inSection:0];
            CustomFoodCell *cell       = [table_Fat cellForRowAtIndexPath:indextPath];
            
            sum += [cell.textField.text intValue];
        }
        
        if (sum != 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
            CustomFoodCell *cell = [table_Main cellForRowAtIndexPath:indexPath];
            cell.textField.text = [NSString stringWithFormat:@"%d",sum];
        }
    }
    else if(textField.tag == TAG_CARB){
        
        int sum = 0;
        
        for (int i = 0 ; i< array_carbTitle.count; i++) {
            
            NSIndexPath    *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            CustomFoodCell *cell      = [table_Carb cellForRowAtIndexPath:indexPath];
            
            sum += [cell.textField.text intValue];
        }
        
        if (sum != 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:1];
            CustomFoodCell *cell = [table_Main cellForRowAtIndexPath:indexPath];
            cell.textField.text = [NSString stringWithFormat:@"%d",sum];
        }
        
        
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextField = textField;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark add Button To KeyBoard Pad

-(void)addButtonToKeyBoardPadWithTextField:(UITextField *)textField{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad:)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStylePlain target:self action:@selector(doneWithNumberPad:)],
                           nil];
    [numberToolbar sizeToFit];
    textField.inputAccessoryView = numberToolbar;
}

#pragma mark KeyBoard Pad Button Event
-(void)cancelNumberPad:(UIBarButtonItem *)sender{
    [currentTextField resignFirstResponder];
    currentTextField.text = @"";
}

-(void)doneWithNumberPad:(UIBarButtonItem *)sender{
    [currentTextField resignFirstResponder];

}

#pragma mark Button Event



-(void)sendButtonPress:(UIButton *)button
{
    
    NSString *emptyMessage = @"";
    
    if ([text_name.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        emptyMessage = [emptyMessage stringByAppendingString:@"名稱"];
    }
    if ([text_explain.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        emptyMessage = [emptyMessage stringByAppendingString:@"、品牌"];
    }
    if ([text_cal.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        emptyMessage = [emptyMessage stringByAppendingString:@"、卡路里"];
    }
    if ([text_unit.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        emptyMessage = [emptyMessage stringByAppendingString:@"、單位"];
    }
    
    if (emptyMessage.length >=1) {
        
        emptyMessage = [emptyMessage stringByAppendingString:@"，不可空白"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:emptyMessage delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        [self postNewFoodToAPI];
    }
    
}

-(void)postNewFoodToAPI
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    for (int i = 0; i< array_sixFoodTitle.count; i++) {
        
        NSIndexPath    *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        CustomFoodCell *cell      = [table_Main cellForRowAtIndexPath:indexPath];
        
        switch (i) {
            case 0:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"starchSixType"];// 全穀根莖
                break;
            case 1:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"proteinSixType"];// 肉蛋豆魚
                break;
            case 2:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"dairySixType"];// 低脂乳品
                break;
            case 3:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"vegetableSixType"];// 蔬菜
                break;
            case 4:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"fruitSixType"];// 水果
                break;
            case 5:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"oilsSixType"];// 油脂及堅果
                break;
                
            default:
                break;
        }
        
    }
    
    for (int i = 0; i< array_nutrtionTitle.count; i++) {
        NSIndexPath    *indexPath = [NSIndexPath indexPathForRow:i inSection:1];
        CustomFoodCell *cell      = [table_Main cellForRowAtIndexPath:indexPath];
        
        switch (i) {
            case 0:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"protein"];// 蛋白質
                break;
            case 1:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"fat"];// 脂肪
                break;
            case 2:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"carbohydrates"];// 碳水化合物
                break;
            case 3:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"sodium"];// 鈉
                break;
            case 4:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"potassium"];// 鉀
                break;
            case 5:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"calcium"];// 鈣
                break;
            case 6:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"iron"];// 鐵
                break;
                
            default:
                break;
        }
    }
    
    for (int i = 0; i < array_fatTitle.count; i++) {
        NSIndexPath    *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        CustomFoodCell *cell      = [table_Fat cellForRowAtIndexPath:indexPath];
        switch (i) {
            case 0:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"monounsaturated"];// 單元不飽和
                break;
            case 1:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"polyunsaturated"];// 多元不飽和
                break;
            case 2:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"saturation"];// 飽和
                break;
            case 3:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"trans"];// 反式
                break;
            case 4:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"cholesterol"];// 膽固醇
                break;
            default:
                break;
        }
    }
    
    for (int i = 0; i< array_carbTitle.count; i++) {
        NSIndexPath    *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        CustomFoodCell *cell      = [table_Carb cellForRowAtIndexPath:indexPath];
        
        switch (i) {
            case 0:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"sugar"];// 糖
                break;
            case 1:
                [dic setObject:cell.textField.text?cell.textField.text:@"" forKey:@"fiber"];// 纖維
                break;
            default:
                break;
        }
    }
    
    [dic setObject:text_name.text    forKey:@"name"];
    [dic setObject:text_explain.text forKey:@"brand"];
    [dic setObject:text_cal.text     forKey:@"cal"];
    [dic setObject:text_unit.text    forKey:@"unit"];
    NSString *imageString = @"";
    
    if (imv_camera.image) {
        NSData *imageData = UIImagePNGRepresentation(imv_camera.image);
        imageString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    

    

    SetNewFoodAPI *api = [[SetNewFoodAPI alloc] init];
    
    [api setAccessToken:[[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN]];
    [api setName:            [dic objectForKey:@"name"]];
    [api setBrand:           [dic objectForKey:@"brand"]];
    [api setCal:             [dic objectForKey:@"cal"]];
    [api setUnit:            [dic objectForKey:@"unit"]];
    [api setFoodGraph:       imageString];
    [api setStarchSixType:   [dic objectForKey:@"starchSixType"]];
    [api setProteinSixType:  [dic objectForKey:@"proteinSixType"]];
    [api setDairySixType:    [dic objectForKey:@"dairySixType"]];
    [api setVegetableSixType:[dic objectForKey:@"vegetableSixType"]];
    [api setFruitSixType:    [dic objectForKey:@"fruitSixType"]];
    [api setOilsSixType:     [dic objectForKey:@"oilsSixType"]];
    [api setProtein:         [dic objectForKey:@"protein"]];
    [api setFat:             [dic objectForKey:@"fat"]];
    [api setCarbohydrates:   [dic objectForKey:@"carbohydrates"]];
    [api setSodium:          [dic objectForKey:@"sodium"]];
    [api setPotassium:       [dic objectForKey:@"potassium"]];
    [api setCalcium:         [dic objectForKey:@"calcium"]];
    [api setIron:            [dic objectForKey:@"iron"]];
    [api setMonounsaturated: [dic objectForKey:@"monounsaturated"]];
    [api setPolyunsaturated: [dic objectForKey:@"polyunsaturated"]];
    [api setSaturation:      [dic objectForKey:@"saturation"]];
    [api setTrans:           [dic objectForKey:@"trans"]];
    [api setCholesterol:     [dic objectForKey:@"cholesterol"]];
    [api setSugar:           [dic objectForKey:@"sugar"]];
    [api setFiber:           [dic objectForKey:@"fiber"]];
    
    [api post:^(HttpResponse *response) {
        
        SetNewFoodEntity *setNewFoodEntity = response.result.firstObject;
        NSLog(@"%@",setNewFoodEntity.msg);
        if([Toos checkMsgCodeWihtMsg:setNewFoodEntity.msg])
        {
            NSManagedObjectContext *context  = [[AppDelegate sharedAppDelegate] managedObjectContext];
            NSManagedObject        *foodData = nil;
            
            foodData = [NSEntityDescription insertNewObjectForEntityForName:@"FoodListEntity" inManagedObjectContext:context];
            
            [foodData setValue:setNewFoodEntity.fid        forKey:@"fid"];
            [foodData setValue:@"7"                        forKey:@"providerType"];
            [foodData setValue:[dic objectForKey:@"name"]  forKey:@"name"];
            [foodData setValue:[dic objectForKey:@"brand"] forKey:@"brand"];
            [foodData setValue:[dic objectForKey:@"unit"]  forKey:@"unit"];
            [foodData setValue:[dic objectForKey:@"cal"]   forKey:@"cal"];
            
            NSError *error = nil;
            [context save:&error];
            
            if (error) {
                NSLog(@"add new food to data base fail");
            }
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"add food fail"
                                                           delegate:nil
                                                  cancelButtonTitle:@"ok"
                                                  otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    

}

@end
