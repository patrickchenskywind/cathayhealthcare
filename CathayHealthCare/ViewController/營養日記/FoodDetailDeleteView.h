//
//  FoodDetailDeleteView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class SetDineListEntity;
@protocol IFoodDetailDeleteView <NSObject>

@required
    - (void) dismissFoodDetailDeleteView;
    - (void) buttonDeleteDidPress: (SetDineListEntity *) setDineListEntity totalCal:(NSString *)totalCal;

@end

@interface FoodDetailDeleteView : View
@property (weak, nonatomic) IBOutlet UITableView *tabelView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelQuantity;
@property (weak, nonatomic) IBOutlet UILabel *labelCal;

@property (nonatomic,strong) NSArray *array_sixCate;
@property (nonatomic,strong) NSArray *array_cateTitle;
@property (nonatomic,strong) NSArray *array_nutrition;
@property (nonatomic,strong) NSArray *array_nutritionTitle;
@property (nonatomic,strong) UIView *view_mask;
@property (nonatomic,weak)id<IFoodDetailDeleteView> iDelegate;

@property (nonatomic, strong) SetDineListEntity *setDineListEntity;

@end
