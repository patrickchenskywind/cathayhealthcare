//
//  NutritionalAnalysisView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "NutritionalAnalysisView.h"
#import "UIColor+Hex.h"
#import "NutritionCell.h"


@interface NutritionalAnalysisView()<UITableViewDataSource,UITableViewDelegate,NutritionalViewDelegate>

@property (nonatomic,strong) NSArray *array_nutrition;
@property (nonatomic,strong) UITableView *table_nutrition;
@property (nonatomic,strong) UIView      *view_maskRight;
@property (nonatomic,strong) UIView      *view_maskLeft;



@end

@implementation NutritionalAnalysisView




-(void)awakeFromNib
{
    _view_pieChart = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width*0.70, [UIScreen mainScreen].bounds.size.width*0.70)];
    
    
    _array_nutrition = @[@{@"color":[UIColor colorWithHex:0xFFC030],
                           @"title":@"蛋白質",
                           @"weight":[NSNumber numberWithFloat:300.0],
                           @"percent":[NSNumber numberWithFloat:30.0],
                           @"showButton":@NO},
                         @{@"color":[UIColor colorWithHex:0xF99786],
                           @"title":@"脂肪",
                           @"weight":[NSNumber numberWithFloat:300.0],
                           @"percent":[NSNumber numberWithFloat:30.0],
                           @"showButton":@YES},
                         @{@"color":[UIColor colorWithHex:0x33C0D6],
                           @"title":@"碳水化合物",
                           @"weight":[NSNumber numberWithFloat:400.0],
                           @"percent":[NSNumber numberWithFloat:40.0],
                           @"showButton":@YES}];
    

    
    _pieChart = [[XYPieChart alloc] initWithFrame:_view_pieChart.bounds];
    _pieChart.userInteractionEnabled = NO;
    [_pieChart setDelegate:self];
    [_pieChart setDataSource:self];
    [_pieChart reloadData];
    [_view_pieChart addSubview:_pieChart];
    
    [_mainView addSubview:_view_pieChart];
    
    _view_maskLeft = [[UIView alloc] init];
    _view_maskRight = [[UIView alloc] init];
    [_mainView addSubview:_view_maskRight];
    [_mainView addSubview:_view_maskLeft];
    
    
    _table_nutrition = [[UITableView alloc] init];
    [_table_nutrition setDelegate: self];
    [_table_nutrition setDataSource:self];
    [_table_nutrition reloadData];
    
    [_mainView addSubview:_table_nutrition];
    
}




-(void)layoutSubviews
{
    [self setFrame:self.superview.bounds];
    
    [_view_pieChart setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2,
                                          _view_pieChart.frame.size.height/2 + 10)];
    
    float leftCircleWidth = CGRectGetWidth(_view_pieChart.frame)*0.7;
    float rightCircleWidth = CGRectGetWidth(_view_pieChart.frame)*0.9;
    
    float leftCircleOffset = CGRectGetWidth(_view_pieChart.frame) *9/20;
    float rightCircleOffset = CGRectGetWidth(_view_pieChart.frame) *9/20;
    
    
    
    
    [_view_pieChart setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width/2,
                                          _view_pieChart.frame.size.height/2 + 10)];

    [_view_maskLeft setFrame:CGRectMake(0, 0, leftCircleWidth, leftCircleWidth)];
    [_view_maskLeft setCenter:CGPointMake(_view_pieChart.center.x - leftCircleOffset,
                                          _view_pieChart.center.y - leftCircleOffset)];
    [_view_maskLeft.layer setCornerRadius:leftCircleWidth/2];
    [_view_maskLeft setBackgroundColor:[UIColor whiteColor]];
    [_view_maskLeft setAlpha:0.5];
    
    [_view_maskRight setFrame:CGRectMake(0, 0, rightCircleWidth, rightCircleWidth)];
    [_view_maskRight setCenter:CGPointMake(_view_pieChart.center.x  + rightCircleOffset,
                                           _view_pieChart.center.y  + rightCircleOffset)];
    [_view_maskRight.layer setCornerRadius:rightCircleWidth/2];
    [_view_maskRight setBackgroundColor:[UIColor whiteColor]];
    [_view_maskRight setAlpha:0.5];
    
    
    
    [_table_nutrition setFrame:CGRectMake(10,
                                          CGRectGetMaxY(_view_pieChart.frame)+10,
                                          [UIScreen mainScreen].bounds.size.width -20,
                                          50*3)];
    [_mainView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,
                                         CGRectGetMaxY(_table_nutrition.frame))];
    
}

#pragma NutritionViewDelegate
-(void)showDetailNutrition
{
    
}

#pragma mark TableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_array_nutrition count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NutritionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NutritionCell"];
    
    if (!cell) {
        UIColor *color = [[_array_nutrition objectAtIndex:indexPath.row] objectForKey:@"color"];
        NSString *nutrition = [[_array_nutrition objectAtIndex:indexPath.row] objectForKey:@"title"];
        NSString *weight = [NSString stringWithFormat:@"%d公克",(int)[[[_array_nutrition objectAtIndex:indexPath.row] objectForKey:@"weight"] floatValue]];
        NSString *percent = [NSString stringWithFormat:@"%d％",(int)[[[_array_nutrition objectAtIndex:indexPath.row] objectForKey:@"percent"] floatValue]];
        BOOL showButton = [[[_array_nutrition objectAtIndex:indexPath.row] objectForKey:@"showButton"] boolValue];
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NutritionCell" owner:self options:nil] objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setDelegate:self];
        [cell setColor:color
             nutrition:nutrition
                weight:weight
               percent:percent
            showButton:showButton];
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

#pragma mark XYPieChartDataSource
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart{
    return _array_nutrition.count;
}
- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index{
    NSNumber *value = [[_array_nutrition objectAtIndex:index] objectForKey:@"percent"];
    return [value floatValue];
}
- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index{
    UIColor *color = [[_array_nutrition objectAtIndex:index] objectForKey:@"color"];
    return color;
    
}
- (NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index{
    return @"test";
}

#pragma mark XYPieChartDelegate
- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index{
    
}
- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index{
    
}
- (void)pieChart:(XYPieChart *)pieChart willDeselectSliceAtIndex:(NSUInteger)index{
    
}
- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index{
    
}

@end
