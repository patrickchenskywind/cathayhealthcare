//
//  FoodDetailAddView.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol IFoodDetailAddView <NSObject>
@required
    - (void) dismissFoodDetailAddView;
    - (void) buttonChoosePhotoPress;
    - (void) buttonSubmitPressWithFid:(NSString *)fid quan:(NSString *)quan foodGraph:(NSString *)foodGraph cal:(NSString *)cal;

@end

@interface FoodDetailAddView : View

@property (weak, nonatomic) IBOutlet UITableView *tabelView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldQuan;
@property (weak, nonatomic) IBOutlet UILabel *labelCal;
@property (weak, nonatomic) IBOutlet UIView *viewForImage;
@property (weak, nonatomic) IBOutlet UILabel *labelChangePrompt;

@property (nonatomic,strong) NSArray *array_sixCate;
@property (nonatomic,strong) NSArray *array_cateTitle;
@property (nonatomic,strong) NSArray *array_nutrition;
@property (nonatomic,strong) NSArray *array_nutritionTitle;
@property (nonatomic,strong) UIView *view_mask;
@property (nonatomic,weak)id<IFoodDetailAddView> iDelegate;


@property (nonatomic, strong) NSString *fid;

- (void) setupUI;
@end

