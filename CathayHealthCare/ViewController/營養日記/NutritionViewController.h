//
//  NutritionViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "DineRecordView.h"
#import "FoodDetailDeleteView.h"
#import "DailyFoodView.h"
#import "NutritionalAnalysisView.h"

@interface NutritionViewController : ViewController

@property (nonatomic,strong) IBOutlet UIView   *mainView;
@property (nonatomic,strong) IBOutlet UIButton *button_record;
@property (nonatomic,strong) IBOutlet UIButton *button_daily;
@property (nonatomic,strong) IBOutlet UIButton *button_analysis;

@property (nonatomic,strong) DineRecordView          *dineRecordView;
@property (nonatomic,strong) FoodDetailDeleteView    *foodDetailDeleteView;
@property (nonatomic,strong) DailyFoodView           *dailyFoodView;
@property (nonatomic,strong) NutritionalAnalysisView *nutritionalAnalysisView;

@end
