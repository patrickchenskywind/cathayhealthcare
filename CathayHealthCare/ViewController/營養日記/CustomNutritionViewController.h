//
//  CustomNutritionViewController.h
//  CathayHealthcare
//
//  Created by Skywind on 2015/7/20.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"

@interface CustomNutritionViewController : ViewController

@property (nonatomic,strong) IBOutlet UITextField *text_name;
@property (nonatomic,strong) IBOutlet UITextField *text_brand;
@property (nonatomic,strong) IBOutlet UITextField *text_dose;
@property (nonatomic,strong) IBOutlet UITextField *text_time;
@property (nonatomic,strong) IBOutlet UITextField *text_unit;

@end
