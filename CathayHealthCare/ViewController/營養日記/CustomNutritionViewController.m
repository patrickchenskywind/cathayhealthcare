//
//  CustomNutritionViewController.m
//  CathayHealthcare
//
//  Created by Skywind on 2015/7/20.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "CustomNutritionViewController.h"
#import "SetNourishmentAPI.h"
#import "SetNourishmentEntity.h"
#import "AppDelegate.h"
#import "Toos.h"
#import "NourishmentListEntity.h"

@interface CustomNutritionViewController ()<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>

@property(nonatomic,strong) UIPickerView *picker_dose;
@property(nonatomic,strong) UIPickerView *picker_time;
@property(nonatomic,strong) UIView       *isolationView;
@property(nonatomic,strong) UIWindow     *window;


@end

@implementation CustomNutritionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setTitle:@"自訂營養品"];
    _text_name.delegate = self;
    _text_dose.delegate = self;
    _text_brand.delegate = self;
    _text_time.delegate = self;
    _text_unit.delegate = self;
}

#pragma mark getter

-(UIPickerView *)picker_dose{
    if (!_picker_dose) {
        _picker_dose = [[UIPickerView alloc] init];
        _picker_dose.delegate = self;
        _picker_dose.dataSource = self;
        _picker_dose.center = CGPointMake([UIScreen mainScreen].bounds.size.width * 0.5,
                                          [UIScreen mainScreen].bounds.size.height * 0.5);
        _picker_dose.backgroundColor = [UIColor whiteColor];
    }
    return _picker_dose;
}

-(UIPickerView *)picker_time{
    if (!_picker_time) {
        _picker_time = [[UIPickerView alloc] init];
        _picker_time.delegate = self;
        _picker_time.dataSource = self;
        _picker_time.center = CGPointMake([UIScreen mainScreen].bounds.size.width * 0.5,
                                          [UIScreen mainScreen].bounds.size.height * 0.5);
        _picker_time.backgroundColor = [UIColor whiteColor];
    }
    return _picker_time;
}

-(UIView *)isolationView{
    if (!_isolationView) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPicker:)];
        _isolationView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _isolationView.backgroundColor = [UIColor grayColor];
        _isolationView.alpha = 0.5;
        [_isolationView addGestureRecognizer:tap];
    }
    return _isolationView;
}

-(UIWindow *)window{
    if (!_window) {
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        _window = delegate.window;
    }
    return _window;
}

#pragma mark dismiss Picker
-(void)dismissPicker:(id)sender{
    
    [_picker_time removeFromSuperview];
    [_picker_dose removeFromSuperview];
    [_isolationView removeFromSuperview];
}

#pragma mark Button Event

-(IBAction)doseButtonPress:(id)sender{
    
    [_picker_dose removeFromSuperview];
    [_picker_time removeFromSuperview];
    [_isolationView removeFromSuperview];
    
    [self.window addSubview:self.isolationView];
    [self.window addSubview:self.picker_dose];
}

-(IBAction)timeButtonPress:(id)sender{
    [_picker_dose removeFromSuperview];
    [_picker_time removeFromSuperview];
    [_isolationView removeFromSuperview];
    
    [self.window addSubview:self.isolationView];
    [self.window addSubview:self.picker_time];
}

-(IBAction)sendButtonPress:(id)sender{
    
    if (_text_name.text.length  == 0 ||
        _text_brand.text.length == 0 ||
        _text_dose.text.length  == 0 ||
        _text_time.text.length  == 0 ||
        _text_unit.text.length  == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"請輸入詳細資訊"
                                                       delegate:nil
                                              cancelButtonTitle:@"ok"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        SetNourishmentAPI *api = [[SetNourishmentAPI alloc] init];
        [api setName:_text_name.text];
        [api setBrand:_text_brand.text];
        [api setDose:_text_dose.text];
        
        [api post:^(HttpResponse *response) {
            SetNourishmentEntity *entity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg:entity.msg]) {
                
                NSManagedObjectContext *context  = [[AppDelegate sharedAppDelegate] managedObjectContext];
                NSManagedObject        *foodData = nil;
                foodData = [NSEntityDescription insertNewObjectForEntityForName:@"NourishmentListEntity" inManagedObjectContext:context];
                
                [foodData setValue:entity.nid       forKey:@"nid"];
                [foodData setValue:@"5"             forKey:@"providerType"];
                [foodData setValue:_text_name.text  forKey:@"name"];
                [foodData setValue:_text_brand.text forKey:@"brand"];
                [foodData setValue:_text_dose.text       forKey:@"dose"];
                
                NSError *error ;
                [context save:&error];
                
                NSLog(@"SetNourishmentAPI success");
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                NSLog(@"SetNourishmentAPI fail");
            }
            
        }];
    }
}

#pragma mark UIPickerViewDelegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == _picker_dose)
        return 6;
    else
        return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *string ;
    
    if (pickerView == _picker_dose){
        string = [NSString stringWithFormat:@"DOSETYPE %zd",row];
    }
    else{
        string = [NSString stringWithFormat:@"NUTRITIONTIME %zd",row];
    }
    return  NSLocalizedString(string, nil);
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSString *string;
    if (pickerView == _picker_dose){
        string = [NSString stringWithFormat:@"DOSETYPE %zd",row];
        _text_dose.text = NSLocalizedString(string, nil);
    }
    else{
        string = [NSString stringWithFormat:@"NUTRITIONTIME %zd",row];
        _text_time.text = NSLocalizedString(string, nil);
    }
    [pickerView removeFromSuperview];
    [_isolationView removeFromSuperview];
}

#pragma mark UITextFielde Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
