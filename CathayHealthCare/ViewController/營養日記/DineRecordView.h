//
//  DineRecordView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol DineRecordViewDelegate <NSObject>
@required
    -(void) showFoodDetail;
    -(void) presentAddFoodView;

@end

@interface DineRecordView : View

@property (weak  ,nonatomic) id<DineRecordViewDelegate> delegate;
@property (weak  ,nonatomic) IBOutlet UILabel     *labelDate;
@property (strong,nonatomic) IBOutlet UITableView *table;

@property (nonatomic,strong) IBOutlet UILabel     *label_suggestCal;
@property (nonatomic,strong) IBOutlet UILabel     *label_cal;
@property (nonatomic,strong) IBOutlet UILabel     *label_resultCal;

@end
