//
//  AddFoodViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddFoodViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "DineTypePickerViewController.h"
#import "FoodDetailDeleteView.h"
#import "FoodDetailAddView.h"
#import "CustomFoodViewController.h"
#import "CustomNutritionViewController.h"
#import "Toos.h"
#import "NourishmentListEntity.h"
#import "FoodListEntity.h"
#import "PhotoFromIPC.h"
#import "UIImageCrop.h"
#import "SetDineAPI.h"
#import "SetDineListEntity.h"
#import "SetNourishmentRecordAPI.h"

@interface AddFoodViewController () <IFoodDetailDeleteView, IFoodDetailAddView, IAddFoodDetailsView, IAddNutritionDetailsView, IDatePickerViewController,IDineTypePickerViewController, PhotoFromIPCDelegate, UIImageCropDelegate>

@property (strong, nonatomic) FoodDetailDeleteView *foodDetailDeleteView;
@property (strong, nonatomic) FoodDetailAddView    *foodDetailAddView;

@property (strong, nonatomic) WYPopoverController           *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController      *datePickerViewController;
@property (strong, nonatomic) DineTypePickerViewController  *dineTypePickerViewController;
@property (strong, nonatomic) NSDate    *dateDidSelect;
@property (assign, nonatomic) NSInteger dineTypeDidSelect;
@property (strong, nonatomic) NSString  *searchSting;

@property (strong, nonatomic) PhotoFromIPC *photoFromIPC;

@property (strong, nonatomic) SetDineAPI *setDineAPIForBreakfast;
@property (strong, nonatomic) SetDineAPI *setDineAPIForLunch;
@property (strong, nonatomic) SetDineAPI *setDineAPIForDinner;
@property (strong, nonatomic) SetDineAPI *setDineAPIForDessert;
@property (strong, nonatomic) SetDineAPI *setDineAPIForWater;
@property (strong, nonatomic) SetNourishmentRecordAPI *setDineAPIForNutrition;

@property (strong, nonatomic) NSString *stringCalForBreakfast;
@property (strong, nonatomic) NSString *stringCalForLunch;
@property (strong, nonatomic) NSString *stringCalForDinner;
@property (strong, nonatomic) NSString *stringCalForDessert;
@property (strong, nonatomic) NSString *stringCalForWater;

@end

@implementation AddFoodViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self dateDidSelect: [NSDate date]];
    [self dineTypeDidSelect: 0 title: NSLocalizedString([NSString stringWithFormat: @"DINETYPE 0"], nil)];
    [self setPhotoFromIPC: [[PhotoFromIPC alloc] initWithDelegate: self]];
    
    [self setSetDineAPIForBreakfast: [[SetDineAPI alloc] init]];
    [self setSetDineAPIForLunch:     [[SetDineAPI alloc] init]];
    [self setSetDineAPIForDinner:    [[SetDineAPI alloc] init]];
    [self setSetDineAPIForDessert:   [[SetDineAPI alloc] init]];
    [self setSetDineAPIForWater:     [[SetDineAPI alloc] init]];
    [self setSetDineAPIForNutrition: [[SetNourishmentRecordAPI alloc] init]];

    [self setStringCalForBreakfast: @"0"];
    [self setStringCalForLunch:     @"0"];
    [self setStringCalForDinner:    @"0"];
    [self setStringCalForDessert:   @"0"];
    
    [self setupUI];
    [self setupParameters];
    [self setupSubviews];
    [self setupObserve];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(textFieldTextDidChange)
                                                 name: UITextFieldTextDidChangeNotification
                                               object: nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver: self name: UITextFieldTextDidChangeNotification object: nil];
}
- (void) textFieldTextDidChange {
    
    if (self.view.text_foodName.text.length <= 0) {
        
        if (self.dineTypeDidSelect <= 3) {
            
            [self.view.foodView setShowSearch: NO];
            [self.view.foodView.foodTableView1 reloadData];
        }
        else if (self.dineTypeDidSelect == 5) {
            
//            [self.view.nutritionView setShowSearch: NO];
//            [self.view.nutritionView.nutritionTableView1 reloadData];
        }
    }
}

#pragma mark - IBAction

- (IBAction)buttonDatePress:(id)sender {
    if ( ! self.datePickerViewController) {
        
        DatePickerViewController *datePicker =[[DatePickerViewController alloc] initWithNibName:@"DatePickerViewController"
                                                                                         bundle:nil];
        [self setDatePickerViewController: datePicker];
        [self.datePickerViewController.datePicker setMaximumDate: [NSDate date]];
        [self.datePickerViewController setIDelegate: self];
    }
    
    [self popoverControllerWithController: self.datePickerViewController width: 218 sender: sender];
}

- (IBAction)buttonDineTypePress:(id)sender {
    
    if ( ! self.dineTypePickerViewController) {
        
        DineTypePickerViewController *dinePicker = [[DineTypePickerViewController alloc] initWithNibName:@"DineTypePickerViewController"
                                                                                                  bundle:nil];
        [self setDineTypePickerViewController: dinePicker];
        [self.dineTypePickerViewController setIDelegate: self];
        
    }
    
    [self popoverControllerWithController: self.dineTypePickerViewController width: 162 sender: sender];
}

- (IBAction)buttonSearchPress:(id)sender {
    
    if (self.view.text_foodName.text.length > 0) {
        
        if ( self.dineTypeDidSelect == 5 ) {
            NSArray *arraySearchNutrition = [NourishmentListEntity seachWithString: self.view.text_foodName.text];
            self.view.nutritionView.array_nutrition = arraySearchNutrition;
        }
        else if ( self.dineTypeDidSelect <= 3 ){
            
            NSArray *arraySearchFood = [FoodListEntity seachWithString: self.view.text_foodName.text];
            [self.view.foodView setShowSearch: YES];
            [self.view.foodView setArrayForSearch: arraySearchFood];
        }
    }
}

- (IBAction)buttonScanPress:(id)sender {
}

-(IBAction)sendButtonPress:(id)sender{
    
    AddFoodDetailsView      *foodView       = self.view.foodView;
    AddWaterDetailsView     *waterView      = self.view.waterView;
    AddNutritionDetailsView *nutritionView  = self.view.nutritionView;
    
    NSArray         *list          = foodView.arratForSet;
    NSArray         *waterList     = waterView.array_water;
    NSMutableArray  *nutritionList = nutritionView.array_nutritionSelected;
    
    NSMutableArray *foodList    = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < list.count ; i++ ) {

        SetDineListEntity *setDineListEntity = list[i];
        NSDictionary      *dic               = @{@"fid"      :setDineListEntity.fid,
                                                 @"quan"     :setDineListEntity.quan,
                                                 @"foodGraph":setDineListEntity.foodGraph};
        [foodList addObject:dic];
    }
    
    if (self.dineTypeDidSelect == 0) {
        // 早餐
        if (foodList.count == 0) {
            return;
        }

        [self sendDineInfoForBreakfastWithFoodArray:foodList];
        
    }
    else if(self.dineTypeDidSelect == 1){
        // 中餐
        if (foodList.count == 0) {
            return;
        }

        [self sendDineInfoForLunchWithFoodArray:foodList];
    }
    else if(self.dineTypeDidSelect == 2){
        // 晚餐
        if (foodList.count == 0) {
            return;
        }

        [self sendDineInfoForDinnerWithFoodArray:foodList];
    }
    else if(self.dineTypeDidSelect == 3){
        // 點心
        if (foodList.count == 0) {
            return;
        }

        [self sendDineInfoForWaterWithWaterArray:foodList];
    }
    else if(self.dineTypeDidSelect == 4){
        // water
        if (waterList.count == 0) {
            return;
        }

        [self sendDineInfoForWaterWithWaterArray:waterList];
    }
    else if(self.dineTypeDidSelect == 5){
        // nutrition
        if (nutritionList.count == 0) {
            return;
        }

        [self sendDineInfoForNutritionWithNutritionArray:nutritionList];
    }
    
}

#pragma mark Send Dine Info

-(void)sendDineInfoForBreakfastWithFoodArray:(NSArray *)array
{
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN];
    
    
    
    [self.setDineAPIForBreakfast setAccessToken:accessToken];
    [self.setDineAPIForBreakfast setDid:@""];
    [self.setDineAPIForBreakfast setDate:[Toos getThousandSecondStringFromDate:[NSDate date]]];
    [self.setDineAPIForBreakfast setDineType:@"0"];
    [self.setDineAPIForBreakfast setArrayDineList:array];
    [self.setDineAPIForBreakfast post:^(HttpResponse *response) {
        
        BaseEntity *baseEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg:baseEntity.msg]) {
            NSLog(@"send dine food success");
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSLog(@"%@",baseEntity.msg);
        }
    }];
}

-(void)sendDineInfoForLunchWithFoodArray:(NSArray *)array
{
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN];
    
    [self.setDineAPIForLunch setAccessToken:accessToken];
    [self.setDineAPIForLunch setDid:@""];
    [self.setDineAPIForLunch setDate:[Toos getThousandSecondStringFromDate:[NSDate date]]];
    [self.setDineAPIForLunch setDineType:@"1"];
    [self.setDineAPIForLunch setArrayDineList:array];
    
    [self.setDineAPIForLunch post:^(HttpResponse *response) {
        
        BaseEntity *baseEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg:baseEntity.msg]) {
            NSLog(@"send dine food success");
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSLog(@"%@",baseEntity.msg);
        }
    }];
}

-(void)sendDineInfoForDinnerWithFoodArray:(NSArray *)array{
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN];
    
    [self.setDineAPIForDinner setAccessToken:accessToken];
    [self.setDineAPIForDinner setDid:@""];
    [self.setDineAPIForDinner setDate:[Toos getThousandSecondStringFromDate:[NSDate date]]];
    [self.setDineAPIForDinner setDineType:@"2"];
    [self.setDineAPIForDinner setArrayDineList:array];
    
    [self.setDineAPIForDinner post:^(HttpResponse *response) {
        
        BaseEntity *baseEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg:baseEntity.msg]) {
            NSLog(@"send dine food success");
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSLog(@"%@",baseEntity.msg);
        }
    }];
}

-(void)sendDineInfoForDessertWithFoodArray:(NSArray *)array{
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN];
    
    [self.setDineAPIForDessert setAccessToken:accessToken];
    [self.setDineAPIForDessert setDid:@""];
    [self.setDineAPIForDessert setDate:[Toos getThousandSecondStringFromDate:[NSDate date]]];
    [self.setDineAPIForDessert setDineType:@"3"];
    [self.setDineAPIForDessert setArrayDineList:array];
    
    [self.setDineAPIForDessert post:^(HttpResponse *response) {
        
        BaseEntity *baseEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg:baseEntity.msg]) {
            NSLog(@"send dine food success");
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSLog(@"%@",baseEntity.msg);
        }
    }];
}

-(void)sendDineInfoForWaterWithWaterArray:(NSArray *)array{
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN];
    
    
    [self.setDineAPIForWater setAccessToken:accessToken];
    [self.setDineAPIForWater setDid:@""];
    [self.setDineAPIForWater setDate:[Toos getThousandSecondStringFromDate:[NSDate date]]];
    [self.setDineAPIForWater setDineType:@"4"];
    [self.setDineAPIForWater setArrayDineList:array];
    
    [self.setDineAPIForWater post:^(HttpResponse *response) {
        
        BaseEntity *baseEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg:baseEntity.msg]) {
            NSLog(@"send dine food success");
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSLog(@"%@",baseEntity.msg);
        }
    }];
}

-(void)sendDineInfoForNutritionWithNutritionArray:(NSMutableArray *)array{
    
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN];
    
    for (NSMutableDictionary *dic in array) {
        [dic removeObjectForKey:@"name"];
    }
    
    
    [self.setDineAPIForNutrition setAccessToken:accessToken];
    [self.setDineAPIForNutrition setDid:@""];
    [self.setDineAPIForNutrition setDate:[Toos getThousandSecondStringFromDate:[NSDate date]]];
    [self.setDineAPIForNutrition setArrayNourishmentList:array];
    
    [self.setDineAPIForNutrition post:^(HttpResponse *response) {
        
        BaseEntity *baseEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg:baseEntity.msg]) {
            NSLog(@"send dine food success");
            [self.navigationController popViewControllerAnimated:YES];
        }
        else{
            NSLog(@"%@",baseEntity.msg);
        }
    }];
}

#pragma mark - Unit
- (void) setupUI {
    [self setTitle: @"輸入食物"];
    [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed: @"titlebar_orange"] forBarMetrics: UIBarMetricsDefault];
}

- (void) setupParameters {
    [self.view.foodView setIDelegate: self];
    [self.view.nutritionView setIDelegate: self];
}

- (void) setupObserve {
    [RACObserve(self.view.textField_dineCate, text) subscribeNext:^(id textField_dineCateText) {
        [self.view.foodView setHidden: YES];
        [self.view.waterView setHidden: YES];
        [self.view.nutritionView setHidden: YES];
        
        if ([textField_dineCateText isEqualToString: NSLocalizedString(@"DINETYPE 0", nil)] ||
            [textField_dineCateText isEqualToString: NSLocalizedString(@"DINETYPE 1", nil)] ||
            [textField_dineCateText isEqualToString: NSLocalizedString(@"DINETYPE 2", nil)] ||
            [textField_dineCateText isEqualToString: NSLocalizedString(@"DINETYPE 3", nil)]) {
            [self.view.foodView setHidden: NO];
            [self.view.button_barcode setHidden: NO];
            [self.view.button_search setHidden: NO];
        }
        else if ([textField_dineCateText isEqualToString: NSLocalizedString(@"DINETYPE 4", nil)]) {

            [self.view.waterView setHidden: NO];
            [self.view.button_barcode setHidden: YES];
            [self.view.button_search setHidden: YES];

            
        }
        else if ([textField_dineCateText isEqualToString: NSLocalizedString(@"DINETYPE 5", nil)]) {
            [self.view.nutritionView setHidden: NO];
            [self.view.button_barcode setHidden: YES];
            [self.view.button_search setHidden: NO];
        }
    }];
}

- (void) setupSubviews {
    [self setupSubview: self.view.foodView parentView: self.view.contentView];
    [self setupSubview: self.view.waterView parentView: self.view.contentView];
    [self setupSubview: self.view.nutritionView parentView: self.view.contentView];
}

- (void) setupSubview: (UIView *) subview parentView: (UIView *) parentView {
    [subview setHidden: YES];
    [parentView addSubview: subview];
    [subview setTranslatesAutoresizingMaskIntoConstraints: NO];
    
    [parentView addConstraint: [NSLayoutConstraint constraintWithItem: subview
                                                            attribute: NSLayoutAttributeTop
                                                            relatedBy: NSLayoutRelationEqual
                                                               toItem: parentView
                                                            attribute: NSLayoutAttributeTop
                                                           multiplier: 1
                                                             constant: 0]];
    [parentView addConstraint: [NSLayoutConstraint constraintWithItem: subview
                                                            attribute: NSLayoutAttributeLeft
                                                            relatedBy: NSLayoutRelationEqual
                                                               toItem: parentView
                                                            attribute: NSLayoutAttributeLeft
                                                           multiplier: 1
                                                             constant: 0]];
    [parentView addConstraint: [NSLayoutConstraint constraintWithItem: subview
                                                            attribute: NSLayoutAttributeRight
                                                            relatedBy: NSLayoutRelationEqual
                                                               toItem: parentView
                                                            attribute: NSLayoutAttributeRight
                                                           multiplier: 1
                                                             constant: 0]];
    [parentView addConstraint: [NSLayoutConstraint constraintWithItem: subview
                                                            attribute: NSLayoutAttributeBottom
                                                            relatedBy: NSLayoutRelationEqual
                                                               toItem: parentView
                                                            attribute: NSLayoutAttributeBottom
                                                           multiplier: 1
                                                             constant: 0]];
}

- (void) popoverControllerWithController: (id) controller width: (NSInteger) width  sender:(UIButton *)sender {
    [self closeKeyboard];
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: controller]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, width)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

#pragma mark - IFoodDetailAddView
-(void)dismissFoodDetailAddView {
    [self.foodDetailAddView.view_mask removeFromSuperview];
    [self.foodDetailAddView removeFromSuperview];
    [self setFoodDetailAddView: nil];
}

- (void) buttonChoosePhotoPress {
    [self.photoFromIPC show: nil];
}

- (void) buttonSubmitPressWithFid:(NSString *)fid quan:(NSString *)quan foodGraph:(NSString *)foodGraph cal:(NSString *)cal {
    SetDineListEntity *setDineListEntity = [[SetDineListEntity alloc] init];
    [setDineListEntity setFid: fid];
    [setDineListEntity setQuan: quan];
    [setDineListEntity setFoodGraph: foodGraph];
    switch (self.dineTypeDidSelect) {
        case 0:
            [self setStringCalForBreakfast: [NSString stringWithFormat: @"%0.01f", [self.stringCalForBreakfast doubleValue] + [cal doubleValue]]];
            [self setupSetDineAPI: self.setDineAPIForBreakfast setDineListEntity: setDineListEntity totalCal: self.stringCalForBreakfast];
            break;
        case 1:
            [self setStringCalForLunch: [NSString stringWithFormat: @"%0.01f", [self.stringCalForLunch doubleValue] + [cal doubleValue]]];
            [self setupSetDineAPI: self.setDineAPIForLunch setDineListEntity: setDineListEntity totalCal: self.stringCalForLunch];
            break;
        case 2:
            [self setStringCalForDinner: [NSString stringWithFormat: @"%0.01f", [self.stringCalForDinner doubleValue] + [cal doubleValue]]];
            [self setupSetDineAPI: self.setDineAPIForDinner setDineListEntity: setDineListEntity totalCal: self.stringCalForDinner];
            break;
        case 3:
            [self setStringCalForDessert: [NSString stringWithFormat: @"%0.01f", [self.stringCalForDessert doubleValue] + [cal doubleValue]]];
            [self setupSetDineAPI: self.setDineAPIForDessert setDineListEntity: setDineListEntity totalCal: self.stringCalForDessert];
            break;
        default:
            break;
    }
}

- (void) setupSetDineAPI: (SetDineAPI*)setDineAPI
       setDineListEntity: (SetDineListEntity *)setDineListEntity
                totalCal: (NSString *)totalCal {
    NSMutableArray *mutableArray = [NSMutableArray array];
    if (setDineAPI.arrayDineList.count > 0) {
        mutableArray = [setDineAPI.arrayDineList mutableCopy];
    }
    else {
        [setDineAPI setArrayDineList: [NSArray array]];
    }
    [mutableArray addObject: setDineListEntity];
    [setDineAPI setArrayDineList: [mutableArray copy]];
    [self.view.foodView setArratForSet: setDineAPI.arrayDineList];
    [self.view.foodView setTotalCal: totalCal];
    [self dismissFoodDetailAddView];
}

#pragma mark - IFoodDetailDeleteView
- (void) buttonDeleteDidPress: (SetDineListEntity *) setDineListEntity totalCal:(NSString *)totalCal {
    switch (self.dineTypeDidSelect) {
        case 0:
            [self setStringCalForBreakfast: [NSString stringWithFormat: @"%0.01f", [self.stringCalForBreakfast doubleValue] - [totalCal doubleValue]]];
            [self setupDeleteForArray: self.setDineAPIForBreakfast setDineListEntity: setDineListEntity totalCal: self.stringCalForBreakfast];
            break;
        case 1:
            [self setStringCalForLunch: [NSString stringWithFormat: @"%0.01f", [self.stringCalForLunch doubleValue] - [totalCal doubleValue]]];
            [self setupDeleteForArray: self.setDineAPIForLunch setDineListEntity: setDineListEntity totalCal: self.stringCalForLunch];
            break;
        case 2:
            [self setStringCalForDinner: [NSString stringWithFormat: @"%0.01f", [self.stringCalForDinner doubleValue] - [totalCal doubleValue]]];
             [self setupDeleteForArray: self.setDineAPIForDinner setDineListEntity: setDineListEntity totalCal: self.stringCalForDinner];
            break;
        case 3:
            [self setStringCalForDessert: [NSString stringWithFormat: @"%0.01f", [self.stringCalForDessert doubleValue] - [totalCal doubleValue]]];
             [self setupDeleteForArray: self.setDineAPIForDessert setDineListEntity: setDineListEntity totalCal: self.stringCalForDessert];
            break;
        default:
            break;
    }
}

- (void) setupDeleteForArray: (SetDineAPI *)setDineAPI setDineListEntity: (SetDineListEntity *) setDineListEntity totalCal:(NSString *)totalCal {
    NSMutableArray *mutableArray = [setDineAPI.arrayDineList mutableCopy];
    [mutableArray removeObject: setDineListEntity];
    [setDineAPI setArrayDineList: mutableArray.count > 0 ? [mutableArray copy] : [NSArray array]];
    [self.view.foodView setArratForSet: setDineAPI.arrayDineList];
    [self.view.foodView setTotalCal: totalCal];
    [self dismissFoodDetailDeleteView];
}

-(void)dismissFoodDetailDeleteView {
    [self.foodDetailDeleteView.view_mask removeFromSuperview];
    [self.foodDetailDeleteView removeFromSuperview];
    [self setFoodDetailDeleteView: nil];
}

#pragma mark - IAddFoodDetailsView
-(void) buttonCustomFoodPress {
    CustomFoodViewController *customFoodViewController = [[CustomFoodViewController alloc] initWithNibName: @"CustomFoodViewController"
                                                                                     bundle: nil];
    [self.navigationController pushViewController: customFoodViewController animated: YES];
}

- (void) foodTableView1CellDidPress:(FoodListEntity *) foodListEntity {
    
    [self setFoodDetailAddView: [[[NSBundle mainBundle] loadNibNamed: @"FoodDetailAddView" owner: self options: nil] objectAtIndex: 0]];
    [self.foodDetailAddView setFrame: CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width * 0.9, [UIScreen mainScreen].bounds.size.height * 0.7)];
    [self.foodDetailAddView setCenter: CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2)];
    [self.foodDetailAddView setFid: foodListEntity.fid];
    [self.foodDetailAddView setIDelegate: self];
    [self.view addSubview: self.foodDetailAddView];
}

- (void) foodTableView2CellDidPress:(SetDineListEntity *)setDineListEntity {
    [self setFoodDetailDeleteView: [[[NSBundle mainBundle] loadNibNamed: @"FoodDetailDeleteView" owner: self options: nil] objectAtIndex: 0]];
    [self.foodDetailDeleteView setFrame: CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width * 0.9, [UIScreen mainScreen].bounds.size.height * 0.7)];
    [self.foodDetailDeleteView setCenter: CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2)];
    [self.foodDetailDeleteView setSetDineListEntity: setDineListEntity];
    [self.foodDetailDeleteView setIDelegate: self];
    [self.view addSubview: self.foodDetailDeleteView];
}

#pragma mark - IAddNutritionDetailsView
-(void) buttonCustomNutritionPress {
    CustomNutritionViewController *customNutritionViewController = [[CustomNutritionViewController alloc] initWithNibName: @"CustomNutritionViewController"
                                                                                                        bundle: nil];
    [self.navigationController pushViewController: customNutritionViewController animated: YES];
}

#pragma mark - IDatePickerViewController
- (void) dateDidSelect:(NSDate *) date{
    [self setDateDidSelect: date];
    [self.view.text_date setText: [Toos getYMDFormDate: date]];
}

#pragma mark - IDineTypePickerViewController
- (void) dineTypeDidSelect: (NSInteger) row title: (NSString *) title {
    [self setDineTypeDidSelect: row];
    [self.view.textField_dineCate setText: title];
    [self.view.text_foodName setText: @""];
    
    if ([title isEqualToString: NSLocalizedString(@"DINETYPE 4", nil)]) {
        [self.view.text_foodName setPlaceholder: @"請輸入飲水量"];
        [self.view.text_foodName setKeyboardType: UIKeyboardTypeNumberPad];
        [self.view.text_foodName becomeFirstResponder];
    }
    else if ([title isEqualToString: NSLocalizedString(@"DINETYPE 5", nil)]) {
        [self.view.text_foodName setPlaceholder: @"請輸入營養品名稱"];
        [self.view.text_foodName setKeyboardType: UIKeyboardTypeDefault];
    }
    else {
        [self.view.text_foodName setPlaceholder: @"請輸入食物名稱"];
        [self.view.text_foodName setKeyboardType: UIKeyboardTypeDefault];
        switch (row) {
            case 0:
                [self.view.foodView setTotalCal: self.stringCalForBreakfast];
                [self.view.foodView setArratForSet: self.setDineAPIForBreakfast.arrayDineList];
                break;
            case 1:
                [self.view.foodView setTotalCal: self.stringCalForLunch];
                [self.view.foodView setArratForSet: self.setDineAPIForLunch.arrayDineList];
                break;
            case 2:
                [self.view.foodView setTotalCal: self.stringCalForDinner];
                [self.view.foodView setArratForSet: self.setDineAPIForDinner.arrayDineList];
                break;
            case 3:
                [self.view.foodView setTotalCal: self.stringCalForDessert];
                [self.view.foodView setArratForSet: self.setDineAPIForDessert.arrayDineList];
                break;
            default:
                break;
        }
    }
}

#pragma mark - PhotoFromIPCDelegate
- (void) getPhoto: (UIImage *)image {
    UIImageCrop *imageCrop = [[UIImageCrop alloc] init];
    [imageCrop setDelegate: self];
    [imageCrop setRatioOfWidthAndHeight: 200 / 200];
    [imageCrop setImage: image];
    [imageCrop showWithAnimation: NO];
}

#pragma mark - UIImageCropDelegate
- (void) cropImage: (UIImage *)cropImage forOriginalImage: (UIImage *) originalImage {
    if (self.foodDetailAddView) {
        [self.foodDetailAddView.imageView setImage: cropImage];
        [self.foodDetailAddView.labelChangePrompt setHidden: NO];
    }
    
    else if (self.foodDetailDeleteView)
        [self.foodDetailDeleteView.imageView setImage: cropImage];
}



@end
