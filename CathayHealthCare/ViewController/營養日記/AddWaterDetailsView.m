//
//  AddWaterDetailsView.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddWaterDetailsView.h"
#import "AddWaterCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation AddWaterDetailsView





-(NSMutableArray *)array_water
{
    if (!_array_water) {
        _array_water = [[NSMutableArray alloc] init];
    }
    return _array_water;
}


#pragma mark tableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 51;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array_water.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AddWaterCell *cell      = [tableView dequeueReusableCellWithIdentifier:@"AddWaterCell"];
    NSDictionary *waterInfo = _array_water[indexPath.row];
    if (!cell){
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AddWaterCell" owner:self options:nil] objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    [cell setCellWithName: [NSString stringWithFormat:@"%d. 水",(int)indexPath.row +1]
                       ml: [waterInfo objectForKey:@"quan"]];
    [cell.button_delete setTag:indexPath.row];
    [cell.button_delete addTarget:self action:@selector(deleteWater:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(void)deleteWater:(UIButton *)button
{
    NSInteger buttonTag = button.tag;
    [_array_water removeObjectAtIndex:buttonTag];
    [_table reloadData];
}

@end
