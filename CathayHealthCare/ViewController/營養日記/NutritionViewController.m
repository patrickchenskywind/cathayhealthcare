//
//  NutritionViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "NutritionViewController.h"
#import "AddFoodViewController.h"
#import "UIColor+Hex.h"
#import "LoginEntity.h"

#define NAVIHEIGHT self.navigationController.navigationBar.frame.size.height

@interface NutritionViewController ()<IFoodDetailDeleteView,DineRecordViewDelegate>
@end

@implementation NutritionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self newObject];
    [self buttonRecordPress: _button_record];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setTitle: @"營養日記"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titlebar_orange"] forBarMetrics:UIBarMetricsDefault];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
        
    LoginEntity *loginEntity = [LoginEntity get];
    
    if (loginEntity.workType.length     == 0 ||
        loginEntity.gender.length       == 0 ||
        loginEntity.birthday.length     == 0 ||
        loginEntity.height.length       == 0 ||
        loginEntity.weight.length       == 0 ||
        loginEntity.weightGoal.length   == 0 ||
        loginEntity.weightTarget.length == 0 ||
        loginEntity.heartBeat.length    == 0 ){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"資料填寫不完整"
                                                       delegate:nil
                                              cancelButtonTitle:@"ok"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)buttonRecordPress:(UIButton *)sender {
    if ( ! sender.selected) {
        [self.mainView bringSubviewToFront:self.dineRecordView];
        [sender setSelected: ! sender.selected];
        [self.button_daily setSelected: NO];
        [self.button_analysis setSelected: NO];
    }
}

- (IBAction)buttonDailyPress:(UIButton *)sender {
    if ( ! sender.selected) {
        [self.mainView bringSubviewToFront: self.dailyFoodView];
        [sender setSelected: ! sender.selected];
        [self.button_record setSelected: NO];
        [self.button_analysis setSelected: NO];
    }
}

- (IBAction)buttonAnalysisPress:(UIButton *)sender {
    if ( ! sender.selected) {
        [self.mainView bringSubviewToFront: self.nutritionalAnalysisView];
        [sender setSelected: ! sender.selected];
        [self.button_record setSelected: NO];
        [self.button_daily setSelected: NO];
    }
}

-(void)newObject {
    if (!_dineRecordView) {
        _dineRecordView = [[[NSBundle mainBundle] loadNibNamed: @"DineRecordView" owner: self options: nil] objectAtIndex: 0];
        [_dineRecordView setDelegate:self];
        [_mainView addSubview:_dineRecordView];
    }
    
    if (!_dailyFoodView) {
        _dailyFoodView = [[[NSBundle mainBundle] loadNibNamed: @"DailyFoodView" owner: self options: nil] objectAtIndex: 0];
        [_mainView addSubview:_dailyFoodView];
    }
    
    if (!_nutritionalAnalysisView) {
        _nutritionalAnalysisView = [[[NSBundle mainBundle] loadNibNamed: @"NutritionalAnalysisView" owner: self options: nil] objectAtIndex: 0];
        [_mainView addSubview:_nutritionalAnalysisView];
    }
}

#pragma mark DineRecordViewDelegate
//-(void)showFoodDetail {
//    if (!_foodDetailView) {
//        _foodDetailView = [[[NSBundle mainBundle] loadNibNamed:@"FoodDetailView" owner:self options:nil] objectAtIndex:0];
//        [_foodDetailView setFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds)-50,
//                                             CGRectGetHeight(self.view.bounds)-70 - NAVIHEIGHT)];
//        [_foodDetailView setCenter:CGPointMake(CGRectGetWidth(self.view.bounds)/2,
//                                               CGRectGetHeight(self.view.bounds)/2 +NAVIHEIGHT)];
//        [_foodDetailView setIDelegate: self];
//        [self.view addSubview: _foodDetailView];
//    }
//}

-(void)presentAddFoodView {
    [self.navigationController pushViewController: [[AddFoodViewController alloc] initWithNibName:@"AddFoodViewController" bundle: nil] animated: YES];
}

#pragma mark FoodDetailViewDelegate
//-(void)dismissFoodDetailView {
//    if (_foodDetailView.view_mask) {
//        [_foodDetailView.view_mask removeFromSuperview];
//        _foodDetailView.view_mask = nil;
//    }
//    if (_foodDetailView) {
//        [_foodDetailView removeFromSuperview];
//        _foodDetailView = nil;
//    }
//}

@end
