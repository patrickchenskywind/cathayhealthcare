//
//  AddNutritionDetailsView.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddNutritionDetailsView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AddFoodSearchCell.h"
#import "AddFoodCell.h"
#import "NourishmentListEntity.h"
#import "NutritionDoseSetView.h"
#import "AppDelegate.h"

@class AddFoodView;
@interface AddNutritionDetailsView()<NutritionDoseSetViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>{
    UIView               *isolationView;
    NutritionDoseSetView *setDoseView;
    UIPickerView         *picker_time;
    UIPickerView         *picker_dose;
}
@end

@implementation AddNutritionDetailsView

- (void) awakeFromNib {
    [super awakeFromNib];
    RAC(self.nutritionTableView1HeightConstraint, constant) = [RACObserve(self.nutritionTableView1, contentSize) map:^id(id value) {
        return @([value CGSizeValue].height);
    }];
    RAC(self.nutritionTableView2HeightConstraint, constant) = [RACObserve(self.nutritionTableView2, contentSize) map:^id(id value) {
        return @([value CGSizeValue].height);
    }];
    _array_nutrition = @[];
    _array_nutritionSelected = [NSMutableArray array];
}

-(void)setArray_nutrition:(NSArray *)array_nutrition{
    _array_nutrition = array_nutrition;
    for (NourishmentListEntity *entity in _array_nutrition) {
        NSLog(@"%@",entity.name);
    }
    [_nutritionTableView1 reloadData];
}

-(void)setArray_nutritionSelected:(NSMutableArray *)array_nutritionSelected
{
    _array_nutritionSelected = array_nutritionSelected;
    
    [_nutritionTableView2 reloadData];
}

#pragma mark - IBAction
- (IBAction)buttonCustomNutritionPress:(id)sender {
    [self.iDelegate buttonCustomNutritionPress];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _nutritionTableView1) {
        return _array_nutrition.count;
    }
    else{
        return _array_nutritionSelected.count;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 51;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.nutritionTableView1)
    {
        AddFoodSearchCell *cell;
        NourishmentListEntity *nourishment = _array_nutrition[indexPath.row];
        NSString  *string      = [NSString stringWithFormat: @"SHORT_PROVIDERTYPE %@",nourishment.providerType? nourishment.providerType:@""];
        NSString *unitName = NSLocalizedString(string, nil);
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"AddFoodSearchCell" owner:self options:nil] objectAtIndex:0];
        }
        [cell setCellWithUnit:unitName foodName:nourishment.name index:indexPath.row object:nourishment isFavor:NO isFood:NO];
        
        return cell;
    }
    else if(tableView == self.nutritionTableView2){
        AddFoodCell *cell ;
        NSDictionary *dic = [_array_nutritionSelected objectAtIndex:indexPath.row];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"AddFoodCell" owner:self options:nil] objectAtIndex:0];
        }
        [cell setCellWithFood:[NSString stringWithFormat:@"%d.%@",(int)indexPath.row +1,[dic objectForKey:@"name"]]
                        count:[dic objectForKey:@"frequency"]
                          cal:[dic objectForKey:@"time"]];
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == _nutritionTableView1) {
        
        
        AppDelegate            *appDelegate = [UIApplication sharedApplication].delegate;
        AddFoodSearchCell      *cell        = [tableView cellForRowAtIndexPath:indexPath];

        
        if (!isolationView) {
            isolationView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
            isolationView.backgroundColor = [UIColor grayColor];
            isolationView.alpha = 0.5;
            [appDelegate.window addSubview:isolationView];
            [appDelegate.window addSubview:isolationView];
        }
        
        if (!setDoseView) {
            setDoseView = [[[NSBundle mainBundle] loadNibNamed:@"NutritionDoseSetView" owner:self options:nil] objectAtIndex:0];
            [setDoseView setFrame:CGRectMake(0, 0, 300 , 200)];
            [setDoseView setCenter:CGPointMake([UIScreen mainScreen].bounds.size.width * 0.5,
                                               [UIScreen mainScreen].bounds.size.height * 0.5)];
            [setDoseView setDelegate:self];
            [setDoseView setNourishment:cell.object];
            [appDelegate.window addSubview:setDoseView];
            
        }
    }
}



#pragma mark NutritionDoseSetViewDelegate

-(void)okButtonPressWithNutritionDoseSetView:(NutritionDoseSetView *)view
{
    if (view.text_dose.text.length >0 && view.text_time.text.length>0) {
        
        NourishmentListEntity *nourishment = view.nourishment;
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:
                                @{@"nid"        :nourishment.nid,
                                  @"frequency"  :view.text_dose.text,
                                  @"time"       :view.text_time.text,
                                  @"name"       :nourishment.name}];
        
        
        [_array_nutritionSelected addObject:dic];
        [_nutritionTableView2 reloadData];
        [self clearView];
        
    }
    else{
        UIAlertView *alet = [[UIAlertView alloc] initWithTitle:nil message:@"請輸入完整資訊" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alet show];
    }
}

-(void)cancelButtonPressWithNutritionDoseSetView:(NutritionDoseSetView *)view
{
    [self clearView];
}

-(void)clearView{
    [isolationView removeFromSuperview];
    [setDoseView removeFromSuperview];
    [picker_dose removeFromSuperview];
    [picker_time removeFromSuperview];
    
    isolationView = nil;
    setDoseView = nil;
    picker_time = nil;
    picker_dose = nil;
}

-(void)showDoseListWithNutritionDoseSetView:(NutritionDoseSetView *)view
{
    [picker_time removeFromSuperview];
    [picker_dose removeFromSuperview];
    
    picker_dose = [[UIPickerView alloc] init];
    picker_dose.delegate = self;
    picker_dose.dataSource = self;
    picker_dose.center = setDoseView.center;
    picker_dose.backgroundColor = [UIColor whiteColor];
    
    AppDelegate   *appDelegate = [UIApplication sharedApplication].delegate;
    [[appDelegate window] addSubview:picker_dose];
    
}

-(void)showTimeListWithNutritionDoseSetView:(NutritionDoseSetView *)view
{
    [picker_time removeFromSuperview];
    [picker_dose removeFromSuperview];
    
    picker_time = [[UIPickerView alloc] init];
    picker_time.delegate = self;
    picker_time.dataSource = self;
    picker_time.center = setDoseView.center;
    picker_time.backgroundColor = [UIColor whiteColor];
    
    AppDelegate   *appDelegate = [UIApplication sharedApplication].delegate;
    [[appDelegate window] addSubview:picker_time];
}

#pragma mark UIPickerViewDelegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == picker_dose)
        return 6;
    else
        return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *string ;
    
    if (pickerView == picker_dose){
        string = [NSString stringWithFormat:@"DOSETYPE %zd",row];
    }
    else{
        string = [NSString stringWithFormat:@"NUTRITIONTIME %zd",row];
    }
    return  NSLocalizedString(string, nil);
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSString *string;
    if (pickerView == picker_dose){
        string = [NSString stringWithFormat:@"DOSETYPE %zd",row];
        setDoseView.text_dose.text = NSLocalizedString(string,nil);
    }
    else{
        string = [NSString stringWithFormat:@"NUTRITIONTIME %zd",row];
        setDoseView.text_time.text = NSLocalizedString(string, nil);
    }
    [pickerView removeFromSuperview];
}
@end
