//
//  AddFoodView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "AddFoodDetailsView.h"
#import "AddWaterDetailsView.h"
#import "AddNutritionDetailsView.h"

@interface AddFoodView : View

@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet AddFoodDetailsView *foodView;
@property (nonatomic, weak) IBOutlet AddWaterDetailsView *waterView;
@property (nonatomic, weak) IBOutlet AddNutritionDetailsView *nutritionView;
@property (nonatomic, weak) IBOutlet UITextField *text_date;
@property (nonatomic, weak) IBOutlet UITextField *text_foodName;
@property (nonatomic, weak) IBOutlet UITextField *textField_dineCate;
@property (nonatomic, weak) IBOutlet UIButton *button_barcode;
@property (nonatomic, weak) IBOutlet UIButton *button_search;
@property (nonatomic, weak) IBOutlet UIView *headerView;

@end
