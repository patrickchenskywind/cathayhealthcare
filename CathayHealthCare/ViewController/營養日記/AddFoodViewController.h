//
//  AddFoodViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "AddFoodView.h"

@interface AddFoodViewController : ViewController

@property(nonatomic, retain) AddFoodView *view;

@end
