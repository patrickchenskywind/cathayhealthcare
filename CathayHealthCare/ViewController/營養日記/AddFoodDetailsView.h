//
//  FoodView.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@class FoodListEntity, SetDineListEntity;

@protocol IAddFoodDetailsView <NSObject>

@required
    - (void) buttonCustomFoodPress;
    - (void) foodTableView1CellDidPress:(FoodListEntity *) foodListEntity;
    - (void) foodTableView2CellDidPress:(SetDineListEntity *)setDineListEntity;

@end

@interface AddFoodDetailsView : View

@property (nonatomic, weak) id <IAddFoodDetailsView> iDelegate;

@property (nonatomic, weak) IBOutlet UITableView *foodTableView1;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *foodTableView1HeightConstraint;
@property (nonatomic, weak) IBOutlet UITableView *foodTableView2;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *foodTableView2HeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalCal;

@property (nonatomic, assign) BOOL showSearch;
@property (nonatomic, strong) NSArray *arrayForSearch;
@property (nonatomic, strong) NSArray *arrayForFavorites;

@property (nonatomic, strong) NSString *totalCal;
@property (nonatomic, strong) NSArray *arratForSet;

@end
