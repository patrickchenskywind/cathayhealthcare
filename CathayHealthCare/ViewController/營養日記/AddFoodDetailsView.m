//
//  FoodView.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddFoodDetailsView.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AddFoodSearchCell.h"
#import "AddFoodCell.h"
#import "FoodListEntity.h"
#import "DefindConfig.h"
#import "SetDineListEntity.h"

@interface AddFoodDetailsView () <UITableViewDataSource, UITableViewDelegate, IAddFoodSearchCell>

@end

@implementation AddFoodDetailsView

- (void) awakeFromNib {
    [super awakeFromNib];
    [self setArrayForSearch: [NSArray array]];
    [self setArrayForFavorites: [NSArray array]];
    [self setArratForSet: [NSArray array]];
    RAC(self.foodTableView1HeightConstraint, constant) = [RACObserve(self.foodTableView1, contentSize) map:^id(id value) {
        return @([value CGSizeValue].height);
    }];
    RAC(self.foodTableView2HeightConstraint, constant) = [RACObserve(self.foodTableView2, contentSize) map:^id(id value) {
        return @([value CGSizeValue].height);
    }];
    [self setupArrayForFavorites];
}

#pragma mark - Setter
- (void) setArrayForSearch:(NSArray *)arrayForSearch {
    _arrayForSearch = arrayForSearch;
    [self.foodTableView1 reloadData];
}

- (void) setArratForSet:(NSArray *)arratForSet {
    _arratForSet = arratForSet;
    [self.foodTableView2 reloadData];
}

- (void) setTotalCal:(NSString *)totalCal {
    _totalCal = totalCal;
    [self.labelTotalCal setText: [NSString stringWithFormat: @"總熱量 %@ 大卡",_totalCal ? _totalCal : @"0"]];
}

#pragma mark - IBAction
- (IBAction)buttonCustomFoodPress:(id)sender {
    [self.iDelegate buttonCustomFoodPress];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 51;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual: self.foodTableView1])
        return self.showSearch ? self.arrayForSearch.count : self.arrayForFavorites.count;
    else
        return self.arratForSet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.foodTableView1) {
        
        AddFoodSearchCell *cell = [tableView dequeueReusableCellWithIdentifier: @"AddFoodSearchCell"];
        
        if ( ! cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed: @"AddFoodSearchCell" owner: self options: nil] objectAtIndex: 0];
            cell.iDelegate = self;
        }
        if ( ! self.showSearch) {
            FoodListEntity *foodListEntity = self.arrayForFavorites[indexPath.row];
            NSString *string = [NSString stringWithFormat: @"SHORT_PROVIDERTYPE %@",foodListEntity.providerType];
            [cell setCellWithUnit: NSLocalizedString(string, nil)
                         foodName: foodListEntity.name
                            index: indexPath.row
                           object: foodListEntity
                          isFavor: YES
                           isFood: YES];
        }
        else if (self.showSearch) {
            FoodListEntity *foodListEntity = self.arrayForSearch[indexPath.row];
            BOOL isFavor = NO;
            for (FoodListEntity *obj in self.arrayForFavorites) {
                if ([((FoodListEntity*)obj).fid isEqualToString: foodListEntity.fid])
                    isFavor = YES;
            }
            NSString *string = [NSString stringWithFormat: @"SHORT_PROVIDERTYPE %@",foodListEntity.providerType];
            [cell setCellWithUnit: NSLocalizedString(string, nil)
                         foodName: foodListEntity.name
                            index: indexPath.row
                           object: foodListEntity
                          isFavor: isFavor
                           isFood: YES];
        }
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    else if (tableView == self.foodTableView2) {
        AddFoodCell *cell = [tableView dequeueReusableCellWithIdentifier: @"AddFoodCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed: @"AddFoodCell" owner: self options: nil] objectAtIndex: 0];
        }
        SetDineListEntity *setDineListEntity = self.arratForSet[indexPath.row];
        FoodListEntity    *foodListEntity    = [FoodListEntity seachWithFid: setDineListEntity.fid];
        
        [cell setCellWithFood: [NSString stringWithFormat: @"%ld.%@", (NSInteger)indexPath.row +1, foodListEntity.name]
                        count: [NSString stringWithFormat: @"%@ 份",setDineListEntity.quan]
                          cal: [NSString stringWithFormat: @"%0.01f大卡", [foodListEntity.cal doubleValue] * [setDineListEntity.quan doubleValue]]];
        
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual: self.foodTableView1]) {
        [self.iDelegate  foodTableView1CellDidPress: self.showSearch ? self.arrayForSearch[indexPath.row] : self.arrayForFavorites[indexPath.row]];
    }
    else if ([tableView isEqual: self.foodTableView2]) {
        [self.iDelegate foodTableView2CellDidPress: self.arratForSet[indexPath.row]];
    }
}

#pragma mark - IAddFoodSearchCell
-(void)favorButtonPressIndex:(NSInteger)index
                      object:(id)object
                     isFavor:(BOOL)isFavor
                      isFood:(BOOL)isFood {
    if (isFood) {
        NSMutableArray *mutableArray = [NSMutableArray array];
        if ([[NSUserDefaults standardUserDefaults] objectForKey: FOOD_FAVORITES_LIST])
            mutableArray = [[[NSUserDefaults standardUserDefaults] objectForKey: FOOD_FAVORITES_LIST] mutableCopy];
        
        if ( ! isFavor)
            [mutableArray addObject: ((FoodListEntity*)object).fid];
        else
            [mutableArray removeObject: ((FoodListEntity*)object).fid];
        NSArray *array = [mutableArray copy];
        [[NSUserDefaults standardUserDefaults] setObject: array forKey: FOOD_FAVORITES_LIST];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self setupArrayForFavorites];
        if (self.showSearch) {
            [self.foodTableView1 reloadRowsAtIndexPaths: [NSArray arrayWithObject: [NSIndexPath indexPathForRow: index inSection: 0]]
                                       withRowAnimation: UITableViewRowAnimationNone];
        }
        else {
            [self.foodTableView1 reloadData];
        }
    }
}


- (void) setupArrayForFavorites {
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey: FOOD_FAVORITES_LIST];
    if (array.count > 0) {
        [self setArrayForFavorites: [FoodListEntity seachWithFidArray: array]];
    }
    else{
        [self setArrayForFavorites: nil];
    }
}


@end
