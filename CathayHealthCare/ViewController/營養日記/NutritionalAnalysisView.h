//
//  NutritionalAnalysisView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "XYPieChart.h"

@interface NutritionalAnalysisView : View <XYPieChartDataSource,XYPieChartDelegate>



@property (nonatomic,strong) IBOutlet UIScrollView *mainView;
@property (nonatomic,strong) IBOutlet UIView       *topView;
@property (nonatomic,strong) XYPieChart *pieChart;
@property (nonatomic,strong) UIView    *view_pieChart;


@end
