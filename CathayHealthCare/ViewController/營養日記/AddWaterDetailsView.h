//
//  AddWaterDetailsView.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@interface AddWaterDetailsView : View

@property (nonatomic,strong) NSMutableArray       *array_water;
@property (nonatomic,strong) IBOutlet UITableView *table;
@property (nonatomic,strong) IBOutlet UILabel     *label_total;
@end
