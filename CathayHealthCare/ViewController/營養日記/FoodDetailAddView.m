//
//  FoodDetailAddView.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "FoodDetailAddView.h"
#import "FoodDetailDeleteView.h"
#import "GetDineDetailAPI.h"
#import "FoodListEntity.h"
#import "Toos.h"

@interface FoodDetailAddView ()<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) DineDetailEntity *dineDetailEntity;

@end


@implementation FoodDetailAddView

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _array_cateTitle = @[@"全穀根莖類",
                             @"豆魚肉蛋類",
                             @"低脂乳品類",
                             @"蔬菜類",
                             @"水果類",
                             @"油脂及堅果種子類"];
        
        _array_nutritionTitle = @[@"蛋白質",
                                  @"脂肪",
                                  @"單元不飽和",
                                  @"多元不飽和",
                                  @"飽和",
                                  @"反式",
                                  @"膽固醇",
                                  @"碳水化合物",
                                  @"糖",
                                  @"膳食纖維",
                                  @"鈉",
                                  @"鉀",
                                  @"鈣",
                                  @"鐵",];

        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(textFieldTextDidChange) name: UITextFieldTextDidChangeNotification object: nil];
    }
    return self;
}

-(void)awakeFromNib
{
    self.textFieldQuan.text = [NSString stringWithFormat:@"%d",1];
    self.textFieldQuan.keyboardType = UIKeyboardTypeNumberPad;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self setupUI];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void) textFieldTextDidChange {
    if (self.textFieldQuan.text.length > 0)
        [self setupUI];
}

- (void) setIDelegate:(id<IFoodDetailAddView>)iDelegate {
    _iDelegate = iDelegate;
    if ( ! _view_mask) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(buttonPress:)];
        _view_mask = [[UIView alloc] initWithFrame: ((UIViewController *)iDelegate).view.bounds];
        [_view_mask setBackgroundColor: [UIColor blackColor]];
        [_view_mask setAlpha: 0.5];
        [_view_mask addGestureRecognizer: tap];
        [((UIViewController *)iDelegate).view addSubview: _view_mask];
    }
    GetDineDetailAPI *api = [[GetDineDetailAPI alloc] init];
    [api setFid: self.fid];
    [api post:^(HttpResponse *response) {
        DineDetailEntity *dineDetailEntity = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg: dineDetailEntity.msg]) {
            [self setDineDetailEntity: dineDetailEntity];
            [self setupUI];
        }
    }];
}

- (void) setupUI {
    [self.labelName setText: self.dineDetailEntity.name];
    [self.labelCal setText: [NSString stringWithFormat: @"%0.1f大卡", [self.dineDetailEntity.cal doubleValue] * [self.textFieldQuan.text doubleValue]]];
    [self.tabelView reloadData];
}

#pragma mark - IBAction
- (IBAction)buttonChoosePhotoPress:(id)sender {
    [self.iDelegate buttonChoosePhotoPress];
}

- (IBAction)buttonSubmitPress:(id)sender {
    [self.iDelegate buttonSubmitPressWithFid: self.fid
                                        quan: self.textFieldQuan.text
                                   foodGraph: [Toos getBase64StringFromImage: self.imageView.image]
                                         cal: [NSString stringWithFormat: @"%0.1f", [self.dineDetailEntity.cal doubleValue] * [self.textFieldQuan.text doubleValue]]];
}

-(IBAction)buttonPress:(id)sender {
    [self.iDelegate dismissFoodDetailAddView];
}

#pragma mark UITableViewDelegate and Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [_array_cateTitle count];
    }
    else{
        return [_array_nutritionTitle count];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleValue1 reuseIdentifier: @"foodDetailCell"];
    switch (indexPath.section) {
        case 0: {
            double calculate;
            switch (indexPath.row) {
                case 0:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.starchSixType doubleValue];
                    [cell.detailTextLabel setText: [NSString stringWithFormat: @"%.01f碗",calculate]];
                    break;
                case 1:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.proteinSixType doubleValue];
                    [cell.detailTextLabel setText: [NSString stringWithFormat: @"%.01f份",calculate]];
                    break;
                case 2:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.dairySixType doubleValue];
                    [cell.detailTextLabel setText: [NSString stringWithFormat: @"%.01f杯",calculate]];
                    break;
                case 3:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.vegetableSixType doubleValue];
                    [cell.detailTextLabel setText: [NSString stringWithFormat: @"%.01f碟",calculate]];
                    break;
                case 4:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.fruitSixType doubleValue];
                    [cell.detailTextLabel setText: [NSString stringWithFormat: @"%.01f份",calculate]];
                    break;
                case 5:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.oilsSixType doubleValue];
                    [cell.detailTextLabel setText: [NSString stringWithFormat: @"%.01f茶匙",calculate]];
                    break;
                default:
                    break;
            }
            [cell.textLabel setText: [self.array_cateTitle objectAtIndex: indexPath.row]];
            
        }
            break;
        case 1:{
            double calculate;
            switch (indexPath.row) {
                case 0:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.protein doubleValue];
                    break;
                case 1:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.fat doubleValue];
                    break;
                case 2:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.monounsaturated doubleValue];
                    break;
                case 3:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.polyunsaturated doubleValue];
                    break;
                case 4:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.saturation doubleValue];
                    break;
                case 5:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.trans doubleValue];
                    break;
                case 6:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.cholesterol doubleValue];
                    break;
                case 7:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.carbohydrates doubleValue];
                    break;
                case 8:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.sugar doubleValue];
                    break;
                case 9:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.fiber doubleValue];
                    break;
                case 10:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.sodium doubleValue];
                    break;
                case 11:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.potassium doubleValue];
                    break;
                case 12:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.calcium doubleValue];
                    break;
                case 13:
                    calculate = [self.textFieldQuan.text doubleValue] * [self.dineDetailEntity.iron doubleValue];
                    break;
                default:
                    break;
            }
            [cell.textLabel setText: [self.array_nutritionTitle objectAtIndex: indexPath.row]];
            [cell.detailTextLabel setText: [NSString stringWithFormat: @"%.01f毫克",calculate]];
        }
            
            break;
        default:
            break;
    }
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    
    UIImageView *headerBg = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"bg_fooddata"]];
    [headerBg setFrame:view.bounds];
    [view addSubview:headerBg];
    
    UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];
    
    if (section == 0) {
        [label setText: @"六大食物分類"];
    }
    else{
        [label setText: @"營養素分析"];
    }
    [view addSubview:label];
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.window endEditing: YES];
}



@end
