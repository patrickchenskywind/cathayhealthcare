//
//  AddFoodView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddFoodView.h"
#import "UIColor+Hex.h"

@interface AddFoodView()<UITextFieldDelegate>

@end

@implementation AddFoodView
-(void)layoutSubviews {
    [super layoutSubviews];
    [_headerView.layer setShadowColor: [[UIColor blackColor] CGColor]];
    [_headerView.layer setShadowOffset: CGSizeMake(3.0f, 3.0f)];
    [_headerView.layer setShadowOpacity: 0.5f];
    [_headerView.layer setShadowRadius: 10.0f];

    [_button_barcode.layer setCornerRadius: 5];
    [_button_search.layer setCornerRadius: 5];
    
    [_text_foodName setDelegate:self];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.window endEditing: YES];
}

#pragma mark UITextFieldDelegate

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (!self.waterView.hidden) {
        
        int waterIntake = [self.text_foodName.text intValue];
        if (waterIntake == 0) {
            return YES;
        }
        else{
            NSString     *waterQuan = [NSString stringWithFormat:@"%d",waterIntake];
            NSDictionary *dic = @{@"fid"       : @"",
                                  @"quan"      : waterQuan,
                                  @"foodGraph" : @""};
            [self.waterView.array_water addObject:dic];
            [self.waterView.table reloadData];
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

@end
