//
//  DineRecordView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "DineRecordView.h"
#import "DineRecordCell.h"
#import "Toos.h"
#import "GetDineRecordAPI.h"
#import "DineRecordEntity.h"
#import "DineRecordParentEntity.h"

@interface DineRecordView ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSDate  *dateForShow;
@property (nonatomic, strong) NSArray *array_dineRecord;

@end

@implementation DineRecordView

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setDateForShow: [NSDate date]];
    }
    return self;
}

-(void)awakeFromNib{
    NSString *suggestCal = [[NSUserDefaults standardUserDefaults] objectForKey:SUGGEST_CAL];
    _label_suggestCal.text = suggestCal;
    
    _array_dineRecord = @[];
    
    GetDineRecordAPI *api = [[GetDineRecordAPI alloc] init];
    [api setAccessToken:[[NSUserDefaults standardUserDefaults] objectForKey:ACCESSTOKEN]];
    [api setDate:[Toos getThousandSecondStringFromDate:[NSDate date]]];
    [api post:^(HttpResponse *response) {
        DineRecordParentEntity *object = response.result.firstObject;
        if ([Toos checkMsgCodeWihtMsg:object.msg]) {
            _array_dineRecord = object.arrayDine ? object.arrayDine : [NSArray array];
            
            int sumCal = 0;
            int suggestCal = [_label_suggestCal.text intValue];
            for (DineRecordEntity *entity in _array_dineRecord) {
                sumCal += [entity.cal intValue];
            }
            
            int resultCal = suggestCal - sumCal;
            
            _label_cal.text       = [NSString stringWithFormat:@"%d",sumCal];
            _label_resultCal.text = [NSString stringWithFormat:@"%d",resultCal];
            
            
            [_table reloadData];
            NSLog(@"GetDineRecordAPI: success");
        }
        else{
            _array_dineRecord = [NSArray array];
            [_table reloadData];
            NSLog(@"GetDineRecordAPI: fail");
        }
    }];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self setFrame: self.superview.bounds];
    [self setupLabelDate];
}

#pragma mark - IBAction
-(IBAction)buttonPress:(UIButton *)button {
    [_delegate presentAddFoodView];
}

- (IBAction)butttonNextDayPress:(id)sender {
    if ( ! [Toos isCurrentDay: self.dateForShow]) {
        [self setDateForShow: [Toos addOneDay: self.dateForShow]];
        [self setupLabelDate];
    }
}

- (IBAction)buttonBeforeDayPress:(id)sender {
    [self setDateForShow: [Toos subOneDay: self.dateForShow]];
    [self setupLabelDate];
}

#pragma mark UITableViewDelegate & DataSource

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DineRecordCell *cell = [tableView dequeueReusableCellWithIdentifier: @"DineRecordCell"];
    
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"DineRecordCell" owner: self options: nil] objectAtIndex: 0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    BOOL     isTitle = indexPath.row == 0;
    NSString *foodName;
    NSString *quan;
    NSString *cal;
    NSString *dineType;
    
    
    if (isTitle) {
        dineType = @"餐別";
        foodName = @"食物名稱";
        quan     = @"份量";
        cal      = @"卡路里";
    }
    else{
        
        DineRecordEntity *entity = [_array_dineRecord objectAtIndex:indexPath.row-1];
        
        foodName = entity.name;
        quan     = entity.quan;
        cal      = entity.cal;
        
        switch ([entity.dineType intValue]) {
            case 0: dineType = @"早餐";   break;
            case 1: dineType = @"中餐";   break;
            case 2: dineType = @"晚餐";   break;
            case 3: dineType = @"點心";   break;
            case 4: dineType = @"水";     break;
            case 5: dineType = @"營養品";  break;
            default: dineType = @"";      break;
        }

    }
    
    [cell setDineCate: dineType
             foodName: foodName
               number: quan
                  cal: cal
              isTitle: isTitle];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_array_dineRecord count]+1;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_delegate showFoodDetail];
}

#pragma mark Label Date Setting
- (void) setupLabelDate {
    [self.labelDate setText: [Toos getYMDFormDate: self.dateForShow]];
}

@end