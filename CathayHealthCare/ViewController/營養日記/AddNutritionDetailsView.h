//
//  AddNutritionDetailsView.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/21.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol IAddNutritionDetailsView <NSObject>

@required
    -(void) buttonCustomNutritionPress;

@end

@interface AddNutritionDetailsView : View

@property (nonatomic, weak) id <IAddNutritionDetailsView> iDelegate;

@property (nonatomic, weak) IBOutlet UITableView *nutritionTableView1;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *nutritionTableView1HeightConstraint;
@property (nonatomic, weak) IBOutlet UITableView *nutritionTableView2;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *nutritionTableView2HeightConstraint;

@property (nonatomic,strong) NSArray *array_nutrition;
@property (nonatomic,strong) NSMutableArray *array_nutritionSelected;


@end
