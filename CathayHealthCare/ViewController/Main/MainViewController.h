//
//  MainViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/12.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "MainView.h"


@interface MainViewController : ViewController

@property (nonatomic, retain) MainView *view;

@end
