//
//  MainView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/18.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "MainView.h"

@interface MainView ()
@property (nonatomic,strong) UIView      *view_buttonBG;
@property (nonatomic,strong) UIImageView *view_buttonBG2;
@property (nonatomic,strong) UIView      *view_dish;
@property (nonatomic,strong) UILabel     *label_costTitle;
@property (nonatomic,strong) UILabel     *label_costCal;
@end
@implementation MainView

-(IBAction)buttonPress:(UIButton *)button {
    int buttonTag ;
    if (button == _bt_suggest)
        buttonTag = BUTTONTAG_SUGGEST;
    else if (button == _bt_healthRecord)
        buttonTag = BUTTONTAG_HEALTHRECORD;
    else if (button == _bt_nutrition)
        buttonTag = BUTTONTAG_NUTRITION;
    else if (button == _bt_sport)
        buttonTag = BUTTONTAG_SPORT;
    else if (button == _bt_healthProgress)
        buttonTag = BUTTONTAG_HEALTHPROGRESS;
    else if (button == _bt_contactTeacher)
        buttonTag = BUTTONTAG_CONTACTTEACHER;
    else if (button == _bt_calendar)
        buttonTag = BUTTONTAG_CALENDAR;
    else if (button == _bt_rank)
        buttonTag = BUTTONTAG_RANK;
    else if (button == _bt_setting)
        buttonTag = BUTTONTAG_SETTING;
    [_delegate goWhereWithTag: buttonTag];
}

-(void)layoutSubviews {
    [self newObject];
    [self setLayout];
}


-(void)newObject
{
    //---------------------top
    if (!_view_dish) {
        _view_dish = [[UIView alloc] init];
        _view_dish.backgroundColor = [UIColor whiteColor];
        [self addSubview:_view_dish];
    }
    if (!_label_costTitle) {
        _label_costTitle = [[UILabel alloc] init];
        [_label_costTitle setTextAlignment:NSTextAlignmentCenter];
        [_label_costTitle setFont:[UIFont systemFontOfSize:14]];
        [_label_costTitle setText:@"今日消耗卡路里"];
        [_view_dish addSubview:_label_costTitle];
    }
    
    if (!_imv_dishIcon) {
        _imv_dishIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage_todayconsume"]];
        [_view_dish addSubview:_imv_dishIcon];
    }
    
    if (!_label_costCal) {
        _label_costCal = [[UILabel alloc] init];
        [_label_costCal setTextAlignment:NSTextAlignmentCenter];
        [_label_costCal setFont:[UIFont systemFontOfSize:17]];
        [_label_costCal setText:@" 大卡"];
        [_view_dish addSubview:_label_costCal];
    }
    
    if (!_label_dairyCal) {
        _label_dairyCal = [[UILabel alloc] init];
        [_label_dairyCal setTextAlignment:NSTextAlignmentCenter];
        [_label_dairyCal setText:@"每日建議卡路里\n\n大卡"];
        [_label_dairyCal setNumberOfLines:3];
        [_label_dairyCal setTextColor:[UIColor whiteColor]];
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            [_label_dairyCal setFont:[UIFont systemFontOfSize:12.0]];
        }
        else{
            [_label_dairyCal setFont:[UIFont systemFontOfSize:15.0]];
        }
        [_view_titleBG addSubview:_label_dairyCal];
    }
    if (!_imv_dairyCal) {
        _imv_dairyCal = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage_dailycorolies"]];
        [_view_titleBG addSubview:_imv_dairyCal];
    }
    
    if (!_label_targetDay) {
        _label_targetDay = [[UILabel alloc] init];
        [_label_targetDay setTextAlignment:NSTextAlignmentCenter];
        [_label_targetDay setText:@"剩餘目標天數\n\n天大卡"];
        [_label_targetDay setNumberOfLines:3];
        [_label_targetDay setTextColor:[UIColor whiteColor]];
        if ([UIScreen mainScreen].bounds.size.width == 320) {
            [_label_targetDay setFont:[UIFont systemFontOfSize:12.0]];
        }
        else{
            [_label_targetDay setFont:[UIFont systemFontOfSize:15.0]];
        }
        [_view_titleBG addSubview:_label_targetDay];
    }
    if (!_imv_targetDay) {
        _imv_targetDay = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage_target"]];
        [_view_titleBG addSubview:_imv_targetDay];
    }
    
    
    // -------------------------center
    if (!_view_buttonBG) {
        _view_buttonBG = [[UIView alloc] init];
        _view_buttonBG.backgroundColor = [UIColor clearColor];
        [_view_bg addSubview:_view_buttonBG];
    }
    if (!_view_buttonBG2) {
        _view_buttonBG2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"homepage_dottedline"]];
        _view_buttonBG2.backgroundColor = [UIColor clearColor];
        [_view_bg addSubview:_view_buttonBG2];
    }
    
    
    // 中間的五個按鈕初始化
    if (!_bt_suggest) {
        _bt_suggest = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bt_suggest setBackgroundImage:[UIImage imageNamed:@"homepage_dailyadvice"] forState:UIControlStateNormal];
        [_bt_suggest addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
        [_view_bg addSubview:_bt_suggest];

    }
    if ( !_label_suggest) {
        _label_suggest = [[UILabel alloc] init];
        if ([UIScreen mainScreen].bounds.size.width ==320) {
            [_label_suggest setFont:[UIFont systemFontOfSize:12.0]];
        }
        [_label_suggest setNumberOfLines:2];
        [_label_suggest setText:@"每日管理\n建議"];
        [_label_suggest setTextColor:[UIColor whiteColor]];
        [_label_suggest setTextAlignment:NSTextAlignmentCenter];
        [_bt_suggest addSubview:_label_suggest];
    }
    if (!_bt_nutrition) {
        _bt_nutrition = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_bt_nutrition setBackgroundImage:[UIImage imageNamed:@"homepage_fooddiary"] forState:UIControlStateNormal];
        [_bt_nutrition addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
        [_view_bg addSubview:_bt_nutrition];
    }
    if (!_label_nutrition) {
        _label_nutrition = [[UILabel alloc] init];
        if ([UIScreen mainScreen].bounds.size.width ==320) {
            [_label_nutrition setFont:[UIFont systemFontOfSize:12.0]];
        }
        [_label_nutrition setText:@"營養日記"];
        [_label_nutrition setTextColor:[UIColor whiteColor]];
        [_label_nutrition setTextAlignment:NSTextAlignmentCenter];
        [_bt_nutrition addSubview:_label_nutrition];
    }
    if (!_bt_sport) {
        _bt_sport = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_bt_sport setBackgroundImage:[UIImage imageNamed:@"homepage_sportdiary"] forState:UIControlStateNormal];
        [_bt_sport addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
        [_view_bg addSubview:_bt_sport];
    }
    if (!_label_sport) {
        _label_sport = [[UILabel alloc] init];
        if ([UIScreen mainScreen].bounds.size.width ==320) {
            [_label_sport setFont:[UIFont systemFontOfSize:12.0]];
        }
        [_label_sport setText:@"運動日記"];
        [_label_sport setTextColor:[UIColor whiteColor]];
        [_label_sport setTextAlignment:NSTextAlignmentCenter];
        [_bt_sport addSubview:_label_sport];
    }
    if (!_bt_healthProgress) {
        _bt_healthProgress = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_bt_healthProgress setBackgroundImage:[UIImage imageNamed:@"homepage_healthprogress"] forState:UIControlStateNormal];
        [_bt_healthProgress addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
        [_view_bg addSubview:_bt_healthProgress];
    }
    
    if (!_label_healthProgress) {
        _label_healthProgress = [[UILabel alloc] init];
        if ([UIScreen mainScreen].bounds.size.width ==320) {
            [_label_healthProgress setFont:[UIFont systemFontOfSize:12.0]];
        }
        [_label_healthProgress setText:@"健康進展"];
        [_label_healthProgress setTextColor:[UIColor whiteColor]];
        [_label_healthProgress setTextAlignment:NSTextAlignmentCenter];
        [_bt_healthProgress addSubview:_label_healthProgress];

    }
    if (!_bt_healthRecord) {
        _bt_healthRecord = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_bt_healthRecord setBackgroundImage:[UIImage imageNamed:@"homepage_healthrecord_b"] forState:UIControlStateNormal];
        [_bt_healthRecord addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
        [_view_bg addSubview:_bt_healthRecord];
    }
    if (!_label_healthRecord) {
        _label_healthRecord = [[UILabel alloc] init];
        if ([UIScreen mainScreen].bounds.size.width ==320) {
            [_label_healthRecord setFont:[UIFont systemFontOfSize:12.0]];
        }
        [_label_healthRecord setText:@"健康數值\n紀錄"];
        [_label_healthRecord setNumberOfLines:2];
        [_label_healthRecord setTextColor:[UIColor whiteColor]];
        [_label_healthRecord setTextAlignment:NSTextAlignmentCenter];
        [_bt_healthRecord addSubview:_label_healthRecord];
        
    }
}

-(void)setLayout {
    float width = CGRectGetWidth(_view_bg.frame);
    float height = CGRectGetHeight(_view_bg.frame);
    float centerX = width /2.0;
    float centerY = height / 2.0;
    
    if (width> height) {
        width = height;
    }
    else{
        height = width;
    }
    // --------------top
    [_view_dish setFrame:CGRectMake(0,
                                    0,
                                    CGRectGetHeight(_view_titleBG.frame)+30,
                                    CGRectGetHeight(_view_titleBG.frame)+30)];
    [_view_dish setCenter:CGPointMake(CGRectGetMidX(_view_titleBG.frame),
                                      CGRectGetMidY(_view_titleBG.frame))];
    
    [_view_dish.layer setCornerRadius:CGRectGetWidth(_view_dish.frame)/2];
    [_view_dish.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_view_dish.layer setShadowOffset:CGSizeMake(3.0f, 3.0f)];
    [_view_dish.layer setShadowOpacity:0.5f];
    [_view_dish.layer setShadowRadius:10.0f];
    
    [_label_costTitle setFrame:CGRectMake(0, 0, CGRectGetWidth(_view_dish.frame),
                                          CGRectGetHeight(_view_dish.frame)/10)];
    [_label_costTitle setCenter:CGPointMake(CGRectGetWidth(_view_dish.frame)/2,
                                            CGRectGetHeight(_view_dish.frame)/2)];
    
    [_label_costCal setFrame:CGRectMake(0, CGRectGetMaxY(_label_costTitle.frame)+10,
                                        CGRectGetWidth(_view_dish.frame),
                                        CGRectGetHeight(_view_dish.frame)/10*2)];
    [_label_costCal setCenter:CGPointMake(CGRectGetWidth(_view_dish.frame)/2,
                                          _label_costCal.center.y)];
    [_imv_dishIcon setFrame:CGRectMake(0, 0, 30, 30)];
    [_imv_dishIcon setCenter:CGPointMake(CGRectGetWidth(_view_dish.frame)/2, CGRectGetHeight(_view_dish.frame)/4)];
    
    [_label_dairyCal setFrame:CGRectMake(0, _view_titleBG.frame.size.height/3, (_view_titleBG.frame.size.width - _view_dish.frame.size.width)/2, _view_titleBG.frame.size.height/3*2)];
    
    [_imv_dairyCal setCenter:CGPointMake(_label_dairyCal.center.x, _view_titleBG.frame.size.height/3/2)];
    
    [_label_targetDay setFrame:CGRectMake(_label_dairyCal.frame.size.width + _view_dish.frame.size.width, _label_dairyCal.frame.origin.y, _label_dairyCal.frame.size.width, _label_dairyCal.frame.size.height)];
    
    [_imv_targetDay setCenter:CGPointMake(_label_targetDay.center.x, _imv_dairyCal.center.y)];
    
    
    
    // --------------center
    [_view_buttonBG setFrame:CGRectMake(0, 0, width, height)];
    [_view_buttonBG setCenter:CGPointMake(centerX, centerY)];
    [_view_buttonBG2 setFrame:CGRectMake(0, 0, width/4.0 * 3.0, height/4.0 *3.0)];
    [_view_buttonBG2 setCenter:CGPointMake(centerX, centerY)];

    
    float buttonWidth = (CGRectGetWidth(_view_buttonBG2.frame)/7.0 *2.0)*1.2;
    float buttonHeight =(CGRectGetWidth(_view_buttonBG2.frame)/7.0 *2.0)*1.2;
    
    [_bt_suggest setFrame:CGRectMake(CGRectGetMinX(_view_buttonBG2.frame),
                                     CGRectGetMinY(_view_buttonBG2.frame),
                                     buttonWidth,
                                     buttonHeight)];

    [_label_suggest setFrame:CGRectMake(0,
                                        CGRectGetMidY(_bt_suggest.bounds),
                                        CGRectGetWidth(_bt_suggest.bounds),
                                        CGRectGetHeight(_bt_suggest.bounds)/2)];

    
    [_bt_nutrition setFrame:CGRectMake(CGRectGetMaxX(_view_buttonBG2.frame) - CGRectGetWidth(_bt_suggest.frame),
                                       CGRectGetMinY(_view_buttonBG2.frame),
                                       buttonWidth,
                                       buttonHeight)];
    [_label_nutrition setFrame:CGRectMake(0,
                                          CGRectGetMidY(_bt_nutrition.bounds),
                                          CGRectGetWidth(_bt_nutrition.bounds),
                                          CGRectGetHeight(_bt_nutrition.bounds)/4)];
    
    [_bt_sport setFrame:CGRectMake(CGRectGetMinX(_view_buttonBG2.frame),
                                   CGRectGetMaxY(_view_buttonBG2.frame) - CGRectGetHeight(_bt_suggest.frame),
                                   buttonWidth,
                                   buttonHeight)];
    [_label_sport setFrame:CGRectMake(0, CGRectGetMidY(_bt_sport.bounds),
                                      CGRectGetWidth(_bt_sport.bounds),
                                      CGRectGetHeight(_bt_sport.bounds)/4)];
    
    [_bt_healthProgress setFrame:CGRectMake(CGRectGetMinX(_bt_nutrition.frame),
                                            CGRectGetMinY(_bt_sport.frame),
                                            buttonWidth ,
                                            buttonHeight)];
    [_label_healthProgress setFrame:CGRectMake(0, CGRectGetMidY(_bt_healthProgress.bounds),
                                               CGRectGetWidth(_bt_healthProgress.bounds),
                                               CGRectGetHeight(_bt_healthProgress.bounds)/4)];
    
    
    [_bt_healthRecord setFrame:CGRectMake(CGRectGetMinX(_view_buttonBG2.frame),
                                          CGRectGetMinY(_view_buttonBG2.frame),
                                          CGRectGetWidth(_view_buttonBG2.frame)/2.0,
                                          CGRectGetHeight(_view_buttonBG2.frame)/2.0)];
    [_label_healthRecord setFrame:CGRectMake(0, CGRectGetMidY(_bt_healthRecord.bounds),
                                             CGRectGetWidth(_bt_healthRecord.bounds),
                                             CGRectGetHeight(_bt_healthRecord.bounds)/2)];
    
    
    [_bt_healthRecord setCenter:CGPointMake(centerX, centerY)];
    
    [_view_buttonBG2 setFrame:CGRectMake(CGRectGetMinX(_view_buttonBG2.frame),
                                         CGRectGetMinY(_view_buttonBG2.frame),
                                         CGRectGetWidth(_view_buttonBG2.frame)*0.9,
                                         CGRectGetHeight(_view_buttonBG2.frame)*0.9)];
    [_view_buttonBG2 setCenter:CGPointMake(centerX, centerY)];
    
}

@end
