//
//  MainView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/18.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppConfig.h"

@protocol MainViewDelegate <NSObject>

-(void)goWhereWithTag:(int)buttonTag;

@end


@interface MainView : UIView

enum DIRECTION_NUM
{
    //BUTTON_NUM
    BUTTONTAG_SUGGEST        = 0,
    BUTTONTAG_HEALTHRECORD   = 1,
    BUTTONTAG_NUTRITION      = 2,
    BUTTONTAG_SPORT          = 3,
    BUTTONTAG_HEALTHPROGRESS = 4,
    BUTTONTAG_CONTACTTEACHER = 5,
    BUTTONTAG_CALENDAR       = 6,
    BUTTONTAG_RANK           = 7,
    BUTTONTAG_SETTING        = 8,
    

};

#define NAVIGATION_BAR_HEIGHT ((UIViewController *)_delegate).navigationController.navigationBar.frame.size.height
#define STATUS_BAR_HEIGHT     [UIApplication sharedApplication].statusBarFrame.size.height


@property (nonatomic,strong)  UIButton *bt_suggest;
@property (nonatomic,strong)  UIButton *bt_healthRecord;
@property (nonatomic,strong)  UIButton *bt_nutrition;
@property (nonatomic,strong)  UIButton *bt_sport;
@property (nonatomic,strong)  UIButton *bt_healthProgress;
@property (nonatomic,strong)  UILabel  *label_suggest;
@property (nonatomic,strong)  UILabel  *label_healthRecord;
@property (nonatomic,strong)  UILabel  *label_nutrition;
@property (nonatomic,strong)  UILabel  *label_sport;
@property (nonatomic,strong)  UILabel  *label_healthProgress;

@property (nonatomic,strong) IBOutlet UIButton *bt_contactTeacher;
@property (nonatomic,strong) IBOutlet UIButton *bt_calendar;
@property (nonatomic,strong) IBOutlet UIButton *bt_rank;
@property (nonatomic,strong) IBOutlet UIButton *bt_setting;
@property (nonatomic,strong) IBOutlet UIView   *view_titleBG;
@property (nonatomic,strong) IBOutlet UIView   *view_bg;

@property (nonatomic,strong) UILabel *label_dairyCal;
@property (nonatomic,strong) UILabel *label_targetDay;
@property (nonatomic,strong) UIImageView *imv_dairyCal;
@property (nonatomic,strong) UIImageView *imv_targetDay;
@property (nonatomic,strong) UIImageView *imv_dishIcon;




@property (nonatomic,weak) IBOutlet id<MainViewDelegate> delegate;

@end
