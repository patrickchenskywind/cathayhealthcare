//
//  MainViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/12.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "MainViewController.h"
#import "MainView.h"
#import "NutritionViewController.h"
#import "SportViewController.h"
#import "HealthProgressViewController.h"
#import "ContactMasterViewController.h"
#import "CalendarViewController.h"
#import "RankViewController.h"
#import "HealthRecordViewController.h"
#import "RankViewController.h"
#import "SettingViewController.h"
#import "SuggestViewController.h"
#import "LoginEntity.h"
#import "Toos.h"

@interface MainViewController ()<UIScrollViewDelegate ,MainViewDelegate>
@property (nonatomic,strong) UIButton *button_notification;
@end

@implementation MainViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];

}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed:@"titlebar_green"] forBarMetrics:UIBarMetricsDefault];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self setupSuggestLabel];
}

-(void)button_notification:(UIButton *)button {}
-(void)setNotificationCenterButton {}

- (void) setupUI {
    [self setTitle: @"國泰健康管理"];
    [self.navigationController.navigationBar setBackgroundImage: [UIImage imageNamed:@"titlebar_green"] forBarMetrics:UIBarMetricsDefault];
    _button_notification = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button_notification setFrame:CGRectMake(0,
                                              0,
                                              self.navigationController.navigationBar.frame.size.height,
                                              self.navigationController.navigationBar.frame.size.height)];
    [_button_notification setImage: [UIImage imageNamed:@"homepage_notificationcenter"] forState: UIControlStateNormal];
    [_button_notification addTarget: self action: @selector(button_notification:) forControlEvents: UIControlEventTouchUpInside];
    UIBarButtonItem *settingItem = [[UIBarButtonItem alloc] initWithCustomView:_button_notification];
    [self.navigationItem setRightBarButtonItem:  settingItem];
}

#pragma mark MainViewDelegate
-(void)goWhereWithTag:(int)buttonTag {
    UIViewController *vc = [[UIViewController alloc] init];
    switch (buttonTag) {
        case BUTTONTAG_SUGGEST:
            if ([LoginEntity get]) {
                LoginEntity *loginEntity = [LoginEntity get];
                if ((loginEntity.gender && loginEntity.gender.length > 0) &&
                    (loginEntity.weight && loginEntity.weight.length > 0) &&
                    (loginEntity.height && loginEntity.height.length > 0) &&
                    (loginEntity.birthday && loginEntity.birthday.length > 0) &&
                    (loginEntity.workType && loginEntity.workType.length > 0 ) &&
                    (loginEntity.weightTarget && loginEntity.weightTarget.length > 0 ) &&
                    (loginEntity.weightGoal && loginEntity.weightGoal.length > 0 ) ) {
                    
                    vc = [[SuggestViewController alloc] initWithNibName: @"SuggestViewController" bundle: nil];
                }
                else {
                    vc = [[SettingViewController alloc] initWithNibName: @"SettingViewController" bundle: nil];
                }
            }
            break;
        case BUTTONTAG_HEALTHRECORD:
            vc = [[HealthRecordViewController alloc] initWithNibName: @"HealthRecordViewController" bundle: nil];
            break;
        case BUTTONTAG_NUTRITION:
            vc = [[NutritionViewController alloc] initWithNibName: @"NutritionViewController" bundle: nil];
            break;
        case BUTTONTAG_SPORT:
            vc = [[SportViewController alloc] initWithNibName: @"SportViewController" bundle: nil];
            break;
        case BUTTONTAG_HEALTHPROGRESS:
            vc = [[HealthProgressViewController alloc] initWithNibName: @"HealthProgressViewController" bundle: nil];
            break;
        case BUTTONTAG_CONTACTTEACHER:
            vc = [[ContactMasterViewController alloc] initWithNibName: @"ContactMasterViewController" bundle: nil];
            break;
        case BUTTONTAG_CALENDAR:
            vc = [[CalendarViewController alloc] initWithNibName: @"CalendarViewController" bundle:nil];
            break;
        case BUTTONTAG_RANK:
            vc = [[RankViewController alloc] initWithNibName:@"RankViewController" bundle:nil];
            break;
        case BUTTONTAG_SETTING:
            vc = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
            break;
        default:
            break;
    }
    [self.navigationController pushViewController: vc animated: YES];
}

- (void) setupSuggestLabel {
    if ([LoginEntity get]) {
        LoginEntity *loginEntity = [LoginEntity get];
        double baseMetabolize = 0.0;
        float  suggestCal;
        //基礎代謝率
        if ((loginEntity.gender && loginEntity.gender.length > 0) &&
            (loginEntity.weight && loginEntity.weight.length > 0) &&
            (loginEntity.height && loginEntity.height.length > 0) &&
            (loginEntity.birthday && loginEntity.birthday.length > 0) ) {
            double weight = [loginEntity.weight doubleValue];
            double height = [loginEntity.height doubleValue];
            double age = [Toos getUserAgeWithTimeInterVal: loginEntity.birthday];
            if ([loginEntity.gender isEqualToString: @"0"])
                baseMetabolize = (66.47 + 13.75 * weight + 5 * height - 6.76 * age);
            //BMR(男)=(66.47 + 13.75 x 體重 + 5 x 身高(公分) - 6.76 x 歲數)
            else if ([loginEntity.gender isEqualToString: @"1"])
                baseMetabolize = (665.1 + 9.56 * weight + 1.85 * height - 4.76 * age);
            //BMR(女)=(665.1 + 9.56 x 體重 + 1.85x身高(公分) - 4.76 x 歲數)
            
            //每日建議攝取卡路里
            if (loginEntity.workType && loginEntity.workType.length > 0) {
                double workTypIndex = [self getWorktypeIndex: loginEntity.workType];
                if ([loginEntity.weightTarget isEqualToString: @"0.0"] || [loginEntity.weightTarget isEqualToString: @"0"]) {
                    suggestCal = baseMetabolize * workTypIndex + (baseMetabolize * workTypIndex * 0.1);
                    [self.view.label_dairyCal setText: [NSString stringWithFormat: @"每日建議卡路里\n\n%.1f大卡",suggestCal]];
                    //維持體重=【(基礎代謝率x活動指數)+(基礎代謝率x活動指數x0.1)】(四捨五入進位至十位數)  (活動指數:輕度工作=1.3 ，中度工作=1.5 ，重度工作=1.7)
                }
                else if ([loginEntity.weightTarget rangeOfString: @"-"].length > 0) {
                    double target = [[loginEntity.weightTarget stringByReplacingOccurrencesOfString: @"-" withString: @""] doubleValue];
                    suggestCal = (baseMetabolize * workTypIndex) + (baseMetabolize * workTypIndex * 0.1) - (7700 * target / 7);
                    [self.view.label_dairyCal setText: [NSString stringWithFormat: @"每日建議卡路里\n\n%.1f大卡", suggestCal]];
                    //減重=【(基礎代謝率x活動指數)+(基礎代謝率x活動指數x0.1)】─【(7700x每週享瘦目標)/7】(四捨五入進位至十位數)  (活動指數:輕度工作=1.3 ，中度工作=1.5 ，重度工作=1.7)
                }
                else {
                    double target = [[loginEntity.weightTarget stringByReplacingOccurrencesOfString: @"-" withString: @""] doubleValue];
                    suggestCal = (baseMetabolize * workTypIndex) + (baseMetabolize * workTypIndex * 0.1) + (7700 * target / 7);
                    [self.view.label_dairyCal setText: [NSString stringWithFormat: @"每日建議卡路里\n\n%.1f大卡", suggestCal]];
                    //增重=【(基礎代謝率x活動指數)+(基礎代謝率x活動指數x0.1)】+【(7700x每週享胖目標)/7】(四捨五入進位至十位數)  (活動指數:輕度工作=1.3 ，中度工作=1.5 ，重度工作=1.7)
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.1f",suggestCal] forKey:SUGGEST_CAL];
    }
    
}

- (double) getWorktypeIndex : (NSString *)workType {
    if ([workType isEqualToString: @"0"])
        return 1.3;
    else if ([workType isEqualToString: @"1"])
        return 1.5;
    else if ([workType isEqualToString: @"2"])
        return 1.7;
    else
        return 0;
}

@end
