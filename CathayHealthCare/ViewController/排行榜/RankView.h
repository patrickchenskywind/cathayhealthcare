//
//  RankView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalRankView.h"
#import "SportRankView.h"

@interface RankView : UIView

@property (nonatomic,strong)IBOutlet UIButton *button_cal;
@property (nonatomic,strong)IBOutlet UIButton *button_sport;

@property (nonatomic,strong)SportRankView   *sportView;
@property (nonatomic,strong)CalRankView     *calView;

@end
