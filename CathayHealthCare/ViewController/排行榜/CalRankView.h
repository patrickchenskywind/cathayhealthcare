//
//  CalRankView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalRankCell.h"

@interface CalRankView : UIView<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UITableView *table_rank;
@property (nonatomic,strong) IBOutlet UILabel     *label_dateSecion;
@property (nonatomic,strong) IBOutlet UIButton    *button_forward;
@property (nonatomic,strong) IBOutlet UIButton    *button_back;
@property (nonatomic,strong) IBOutlet UILabel     *label_rank;
@property (nonatomic,strong) IBOutlet UIButton    *button_share;

@property (nonatomic,strong) NSArray *array_ranking;

@end
