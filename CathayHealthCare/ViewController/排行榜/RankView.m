//
//  RankView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RankView.h"
#import "UIColor+Hex.h"

@implementation RankView

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    [self buttonPress:_button_cal];
}


-(IBAction)buttonPress:(UIButton *)sender{
    if (sender == _button_cal) {
        [self bringSubviewToFront:_calView];
        [_button_cal setBackgroundImage:[UIImage imageNamed:@"bg_tab_green"] forState:UIControlStateNormal];
        [_button_cal setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button_sport setBackgroundImage:nil forState:UIControlStateNormal];
        [_button_sport setTitleColor:[UIColor colorWithHex:0x917F72] forState:UIControlStateNormal];
    }
    else if (sender == _button_sport){
        [self bringSubviewToFront:_sportView];
        [_button_sport setBackgroundImage:[UIImage imageNamed:@"bg_tab_green"] forState:UIControlStateNormal];
        [_button_sport setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button_cal setBackgroundImage:nil forState:UIControlStateNormal];
        [_button_cal setTitleColor:[UIColor colorWithHex:0x917F72] forState:UIControlStateNormal];

    }
}


-(void)layoutSubviews{
    
    if (!_sportView) {
        _sportView = [[[NSBundle mainBundle] loadNibNamed:@"SportRankView" owner:self options:nil] objectAtIndex:0];
        [_sportView setFrame:CGRectMake(0,
                                        CGRectGetMaxY(_button_sport.frame),
                                        self.bounds.size.width,
                                        self.bounds.size.height - CGRectGetMaxY(_button_sport.frame))];
        [self buttonPress:_button_sport];
        [self addSubview:_sportView];
    }
    
    if (!_calView){
        _calView = [[[NSBundle mainBundle] loadNibNamed:@"CalRankView" owner:self options:nil] objectAtIndex:0];
        [_calView setFrame:CGRectMake(0,
                                      CGRectGetMaxY(_button_cal.frame),
                                      self.bounds.size.width,
                                      self.bounds.size.height - CGRectGetMaxY(_button_cal.frame))];
        [self buttonPress:_button_cal];
        [self addSubview:_calView];
    }

    

    
        
}
@end
