//
//  SportRankView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/2.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SportRankView.h"

@interface SportRankView()
@property (nonatomic,assign) int maxValue;
@end

@implementation SportRankView
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _array_ranking = @[@{@"name":@"王XX",@"cal":@2000},
                           @{@"name":@"風XX",@"cal":@1100},
                           @{@"name":@"王XX",@"cal":@1300},
                           @{@"name":@"風XX",@"cal":@1100},
                           @{@"name":@"王XX",@"cal":@1210},
                           @{@"name":@"風XX",@"cal":@1330},
                           @{@"name":@"王XX",@"cal":@1440},
                           @{@"name":@"風XX",@"cal":@900},
                           @{@"name":@"王XX",@"cal":@800},
                           @{@"name":@"風XX",@"cal":@700}];
        NSSortDescriptor *sortDesc = [[NSSortDescriptor alloc] initWithKey:@"cal" ascending:NO];
        _array_ranking = [_array_ranking sortedArrayUsingDescriptors:@[sortDesc]];
        _maxValue = [[(NSDictionary *)[_array_ranking objectAtIndex:0] objectForKey:@"cal"] intValue];
        
    }
    return self;
}


#pragma mark UITableViewDelegate & Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_array_ranking count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CalRankCell *cell = [tableView dequeueReusableCellWithIdentifier:@"calRankCell"];
    
    
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CalRankCell" owner:self options:nil] objectAtIndex:0];
        
        NSDictionary *info_dic = [_array_ranking objectAtIndex:indexPath.row];
        NSString *name = [info_dic objectForKey:@"name"];
        NSNumber *value = [info_dic objectForKey:@"cal"];
        
        [cell setRankCellWithRank:(int)indexPath.row +1
                             name:name
                            value:[value intValue]
                      andMaxValue:_maxValue];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
@end
