//
//  SuggestViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SuggestViewController.h"
#import "SuggestCell.h"
#import "LoginEntity.h"
#import "Toos.h"
#import "SuggestRemindView.h"

@interface SuggestViewController () <SuggestRemindViewDelegate>

@property (strong, nonatomic) NSMutableArray *mutableArrayForDaySuggest;
@property (strong, nonatomic) NSMutableArray *mutableArrayForSixFood;
@property (strong, nonatomic) NSMutableArray *mutableArrayForThreeNutrition;
@property (strong, nonatomic) SuggestRemindView *viewRemind;
@property (strong, nonatomic) UIView *maskView;
@end

@implementation SuggestViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"每日管理建議"];
    [self setMutableArrayForDaySuggest: [NSMutableArray array]];
    [self setMutableArrayForSixFood: [NSMutableArray array]];
    [self setMutableArrayForThreeNutrition: [NSMutableArray array]];
    [self setupInfo];
}

- (IBAction)buttonSixFoodPress:(PullDownButton *)sender {
    [sender setIsPullDown: ! sender.isPullDown];
    if (sender.isPullDown)
        [self executViewAnimate: self.view.constraintTableSixFoodHeight constant: 300];
    else
        [self executViewAnimate: self.view.constraintTableSixFoodHeight constant: 0];
}

- (IBAction)buttonNutritionPress:(PullDownButton *)sender {
    [sender setIsPullDown: ! sender.isPullDown];
    if (sender.isPullDown)
        [self executViewAnimate: self.view.constraintTableThreeNutritionHeight constant: 150];
    else
        [self executViewAnimate: self.view.constraintTableThreeNutritionHeight constant: 0];
}
- (IBAction)buttonIKnowPress:(id)sender {
    [self.navigationController popToRootViewControllerAnimated: YES];
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (void) executViewAnimate:(NSLayoutConstraint *)target constant: (NSInteger) constant {
    [UIView animateWithDuration: 0.3 animations:^{
        [target setConstant: constant];
        [self.view layoutIfNeeded];
    }];
}

- (void) setupInfo {
    if ([LoginEntity get]) {
        LoginEntity *loginEntity = [LoginEntity get];
        //基礎代謝率
        double weight = [loginEntity.weight doubleValue];
        double height = [loginEntity.height doubleValue];
        double age = [Toos getUserAgeWithTimeInterVal: loginEntity.birthday];
        if ([loginEntity.gender isEqualToString: @"0"])
            [self.mutableArrayForDaySuggest addObject: [NSString stringWithFormat: @"%.1f", (66.47 + 13.75 * weight + 5 * height - 6.76 * age)]];
        //BMR(男)=(66.47 + 13.75 x 體重 + 5 x 身高(公分) - 6.76 x 歲數)
        else if ([loginEntity.gender isEqualToString: @"1"])
            [self.mutableArrayForDaySuggest addObject:[NSString stringWithFormat: @"%.1f", (665.1 + 9.56 * weight + 1.85 * height - 4.76 * age)]];
        //BMR(女)=(665.1 + 9.56 x 體重 + 1.85x身高(公分) - 4.76 x 歲數)
        
        //每日建議攝取卡路里
        double workTypIndex = [self getWorktypeIndex: loginEntity.workType];
        double baseMetabolize = [self.mutableArrayForDaySuggest[0] doubleValue];
        if ([loginEntity.weightTarget isEqualToString: @"0.0"] || [loginEntity.weightTarget isEqualToString: @"0"]) {
            [self.mutableArrayForDaySuggest addObject: [NSString stringWithFormat: @"%.1f", baseMetabolize * workTypIndex + (baseMetabolize * workTypIndex * 0.1)]];
            //維持體重=【(基礎代謝率 x 活動指數) + (基礎代謝率x活動指數 x 0.1)】(四捨五入進位至十位數)  (活動指數: 輕度工作 = 1.3 ，中度工作 = 1.5 ，重度工作 = 1.7)
        }
        else if ([loginEntity.weightTarget rangeOfString: @"-"].length > 0) {
            double target = [[loginEntity.weightTarget stringByReplacingOccurrencesOfString: @"-" withString: @""] doubleValue];
            [self.mutableArrayForDaySuggest addObject: [NSString stringWithFormat: @"%.1f", (baseMetabolize * workTypIndex) + (baseMetabolize * workTypIndex * 0.1) - (7700 * target / 7)]];
            //減重=【(基礎代謝率 x 活動指數) + (基礎代謝率x活動指數 x 0.1)】─【(7700x每週享瘦目標) / 7】(四捨五入進位至十位數)  (活動指數: 輕度工作 = 1.3 ，中度工作 = 1.5 ，重度工作 = 1.7)
        }
        else {
            double target = [[loginEntity.weightTarget stringByReplacingOccurrencesOfString: @"-" withString: @""] doubleValue];
            [self.mutableArrayForDaySuggest addObject: [NSString stringWithFormat: @"%.1f", (baseMetabolize * workTypIndex) + (baseMetabolize * workTypIndex * 0.1) + (7700 * target / 7)]];
            //增重=【(基礎代謝率 x 活動指數) + (基礎代謝率 x 活動指數 x 0.1)】+【(7700x每週享胖目標) / 7】(四捨五入進位至十位數)  (活動指數: 輕度工作 = 1.3 ，中度工作 = 1.5 ，重度工作 = 1.7)
        }
        
#warning 數值檢查提示  需檢查是否顯示正常
        if (self.mutableArrayForDaySuggest[1] && ((NSString *)self.mutableArrayForDaySuggest[1]).length > 0) {
            NSInteger tempInteger = [self.mutableArrayForDaySuggest[1] integerValue];
            if (loginEntity.gender && loginEntity.gender.length >0) {
                if ([loginEntity.gender isEqualToString: @"0"]) {
                    if (tempInteger < 1500) {
                        [self showRemindView];
                    }
                }
                else if ([loginEntity.gender isEqualToString: @"1"]){
                    if (tempInteger < 1200) {
                        [self showRemindView];
                    }
                }
            }
        }
        
        //每日建議飲水量
        [self.mutableArrayForDaySuggest addObject: [NSString stringWithFormat:@"%.1f", [loginEntity.weight doubleValue] * 30]];
        //體重 x 30
        
        //每周建議運動量
        [self.mutableArrayForDaySuggest addObject: @"至少150分鐘"];
        
        //六大類食物建議份數
        [self setupMutableArrayForSixFoodWithSuggestCalories: self.mutableArrayForDaySuggest[1]];
        //參閱『每日建議卡路里六大類食物建議份數』excel
        
        //三大營養素建議量
        double suggestCalories = [((NSString *)self.mutableArrayForDaySuggest[1]) doubleValue];
        [self.mutableArrayForThreeNutrition addObject: [NSString stringWithFormat: @"%.1f~%.1f",(suggestCalories * 0.15) / 4 ,(suggestCalories * 0.25) / 4]];
        //(1)蛋白質=(每日建議卡路里 x 0.15) / 4 ~ (每日建議卡路里 x 0.25) / 4
        [self.mutableArrayForThreeNutrition addObject: [NSString stringWithFormat: @"%.1f", suggestCalories * 0.3 / 9]];
        //(2)脂肪 =< (每日建議卡路里 x 0.3) / 9
        [self.mutableArrayForThreeNutrition addObject: [NSString stringWithFormat: @"%.1f~%.1f",(suggestCalories * 0.5) / 4 ,(suggestCalories * 0.55) / 4]];
        //(3)碳水化合物 =(每日建議卡路里 x 0.5) / 4 ~ (每日建議卡路里 x 0.55) / 4
    }
}

- (void) showRemindView {
    if (!self.maskView) {
        self.maskView = [[UIView alloc] initWithFrame:self.view.bounds];
        [self.maskView setBackgroundColor: [UIColor blackColor]];
        [self.maskView setAlpha:0.5];
        [self.view addSubview:self.maskView];
    }
    if (!self.viewRemind) {
        self.viewRemind = [[[NSBundle mainBundle] loadNibNamed: @"LaunchRemindView" owner: self options: nil] objectAtIndex: 0];
        [self.viewRemind setFrame :CGRectMake(0,
                                              0,
                                              CGRectGetWidth(self.view.bounds) - 60,
                                              CGRectGetHeight(self.view.bounds) - 400)];
        [self.viewRemind setCenter: CGPointMake(CGRectGetWidth(self.view.bounds) / 2, CGRectGetHeight(self.view.bounds) /2 )];
        [self.viewRemind setDelegate: self];
        [self.view addSubview: self.viewRemind];
    }
}

-(void)dismissSuggestRemindView {
    if (self.viewRemind) {
        [self.viewRemind removeFromSuperview];
        [self setViewRemind: nil];
    }
    if (self.maskView) {
        [self.maskView removeFromSuperview];
        [self setMaskView: nil];
    }
}

- (double) getWorktypeIndex : (NSString *)workType {
    if ([workType isEqualToString: @"0"])
        return 1.3;
    else if ([workType isEqualToString: @"1"])
        return 1.5;
    else if ([workType isEqualToString: @"2"])
        return 1.7;
    else
        return 0;
}

#pragma mark UITableViewDelegate & DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.view.table_suggest)
        return 6;
    else if (tableView == self.view.table_sixFood)
        return 6;
    else if (tableView == self.view.table_threeNutrition)
        return 3;
    else
        return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SuggestCell *cell = [[[NSBundle mainBundle] loadNibNamed: @"SuggestCell" owner: self options: nil] objectAtIndex: 0];
    [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    int cellTag;
    if (tableView == self.view.table_suggest) {
        cellTag = (int)indexPath.row;
        if (indexPath.row < self.mutableArrayForDaySuggest.count)
            [cell setCellWithTag: cellTag andContent: self.mutableArrayForDaySuggest[indexPath.row]];
        else
            [cell setCellWithTag: cellTag andContent: @"0"];
    }
    
    else if (tableView == self.view.table_sixFood) {
        cellTag = 10 + ((int)indexPath.row);
        [cell setCellWithTag: cellTag andContent: self.mutableArrayForSixFood[indexPath.row]];
    }
    
    else if (tableView == self.view.table_threeNutrition) {
        cellTag = 20 + (int)indexPath.row;
        [cell setCellWithTag: cellTag andContent: self.mutableArrayForThreeNutrition[indexPath.row]];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGRectZero.size.height;
}

- (void) setupMutableArrayForSixFoodWithSuggestCalories:(NSString *)suggestCalories {
    NSInteger calories = [suggestCalories integerValue];
    switch (calories) {
        case 950 ... 1049:
            [self.mutableArrayForSixFood addObject: @"1.75 碗"];
            [self.mutableArrayForSixFood addObject: @"2.5 份"];
            [self.mutableArrayForSixFood addObject: @"1 杯"];
            [self.mutableArrayForSixFood addObject: @"3 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"2 茶匙"];
            break;
        case  1050 ... 1149:
            [self.mutableArrayForSixFood addObject: @"1.75 碗"];
            [self.mutableArrayForSixFood addObject: @"3 份"];
            [self.mutableArrayForSixFood addObject: @"1 杯"];
            [self.mutableArrayForSixFood addObject: @"3 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"2.5 茶匙"];
            break;
        case  1150 ... 1249:
            [self.mutableArrayForSixFood addObject: @"2 碗"];
            [self.mutableArrayForSixFood addObject: @"3 份"];
            [self.mutableArrayForSixFood addObject: @"1 杯"];
            [self.mutableArrayForSixFood addObject: @"3 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"3.5 茶匙"];
            break;
        case  1250 ... 1349:
            [self.mutableArrayForSixFood addObject: @"2 碗"];
            [self.mutableArrayForSixFood addObject: @"3.5 份"];
            [self.mutableArrayForSixFood addObject: @"1 杯"];
            [self.mutableArrayForSixFood addObject: @"3 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"4.5 茶匙"];
            break;
        case  1350 ... 1449:
            [self.mutableArrayForSixFood addObject: @"2.25 碗"];
            [self.mutableArrayForSixFood addObject: @"3.5 份"];
            [self.mutableArrayForSixFood addObject: @"1 杯"];
            [self.mutableArrayForSixFood addObject: @"3 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"5 茶匙"];
            break;
        case  1450 ... 1549:
            [self.mutableArrayForSixFood addObject: @"2.5 碗"];
            [self.mutableArrayForSixFood addObject: @"4 份"];
            [self.mutableArrayForSixFood addObject: @"1 杯"];
            [self.mutableArrayForSixFood addObject: @"3 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"5 茶匙"];
            break;
        case  1550 ... 1649:
            [self.mutableArrayForSixFood addObject: @"2.75 碗"];
            [self.mutableArrayForSixFood addObject: @"4.5 份"];
            [self.mutableArrayForSixFood addObject: @"1 杯"];
            [self.mutableArrayForSixFood addObject: @"3.5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"5 茶匙"];
            break;
        case  1650 ... 1749:
            [self.mutableArrayForSixFood addObject: @"2.75 碗"];
            [self.mutableArrayForSixFood addObject: @"4.5 份"];
            [self.mutableArrayForSixFood addObject: @"1.5 杯"];
            [self.mutableArrayForSixFood addObject: @"3.5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"5.5 茶匙"];
            break;
        case  1750 ... 1849:
            [self.mutableArrayForSixFood addObject: @"3 碗"];
            [self.mutableArrayForSixFood addObject: @"4.5 份"];
            [self.mutableArrayForSixFood addObject: @"1.5 杯"];
            [self.mutableArrayForSixFood addObject: @"3.5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  1850 ... 1949:
            [self.mutableArrayForSixFood addObject: @"3.25 碗"];
            [self.mutableArrayForSixFood addObject: @"5 份"];
            [self.mutableArrayForSixFood addObject: @"1.5 杯"];
            [self.mutableArrayForSixFood addObject: @"4 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  1950 ... 2049:
            [self.mutableArrayForSixFood addObject: @"3.25 碗"];
            [self.mutableArrayForSixFood addObject: @"5.5 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"4 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  2050 ... 2149:
            [self.mutableArrayForSixFood addObject: @"3.5 碗"];
            [self.mutableArrayForSixFood addObject: @"6 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"4.5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  2150 ... 2249:
            [self.mutableArrayForSixFood addObject: @"3.75 碗"];
            [self.mutableArrayForSixFood addObject: @"6 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  2250 ... 2349:
            [self.mutableArrayForSixFood addObject: @"4 碗"];
            [self.mutableArrayForSixFood addObject: @"6.5 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  2350 ... 2449:
            [self.mutableArrayForSixFood addObject: @"4.25 碗"];
            [self.mutableArrayForSixFood addObject: @"7 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  2450 ... 2549:
            [self.mutableArrayForSixFood addObject: @"4.5 碗"];
            [self.mutableArrayForSixFood addObject: @"7.5 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  2550 ... 2649:
            [self.mutableArrayForSixFood addObject: @"4.75 碗"];
            [self.mutableArrayForSixFood addObject: @"8 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"5 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6 茶匙"];
            break;
        case  2650 ... 2749:
            [self.mutableArrayForSixFood addObject: @"4.75 碗"];
            [self.mutableArrayForSixFood addObject: @"8.5 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"6 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6.5 茶匙"];
            break;
        case  2750 ... 2849:
            [self.mutableArrayForSixFood addObject: @"5 碗"];
            [self.mutableArrayForSixFood addObject: @"9 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"6 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"6.5 茶匙"];
            break;
        case  2850 ... 2949:
            [self.mutableArrayForSixFood addObject: @"5.25 碗"];
            [self.mutableArrayForSixFood addObject: @"9 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"6 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"7 茶匙"];
            break;
        case  2950 ... 3049:
            [self.mutableArrayForSixFood addObject: @"5.5 碗"];
            [self.mutableArrayForSixFood addObject: @"9 份"];
            [self.mutableArrayForSixFood addObject: @"2 杯"];
            [self.mutableArrayForSixFood addObject: @"7 碗"];
            [self.mutableArrayForSixFood addObject: @"2 份"];
            [self.mutableArrayForSixFood addObject: @"8 茶匙"];
            break;
        default:
            [self.mutableArrayForSixFood addObject: @""];
            [self.mutableArrayForSixFood addObject: @""];
            [self.mutableArrayForSixFood addObject: @""];
            [self.mutableArrayForSixFood addObject: @""];
            [self.mutableArrayForSixFood addObject: @""];
            [self.mutableArrayForSixFood addObject: @""];
            break;
    }
}

@end
