//
//  SuggestView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "PullDownButton.h"

@interface SuggestView : View 

@property (weak, nonatomic) IBOutlet UITableView        *table_suggest;
@property (weak, nonatomic) IBOutlet UITableView        *table_sixFood;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTableSixFoodHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTableThreeNutritionHeight;
@property (weak, nonatomic) IBOutlet UITableView        *table_threeNutrition;
@property (weak, nonatomic) IBOutlet PullDownButton     *button_food;
@property (weak, nonatomic) IBOutlet PullDownButton     *button_nutrition;
@property (weak, nonatomic) IBOutlet UIButton           *button_ok;

@end
