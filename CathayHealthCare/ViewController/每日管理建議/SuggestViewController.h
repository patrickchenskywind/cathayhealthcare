//
//  SuggestViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/22.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "SuggestView.h"

@interface SuggestViewController : ViewController
@property (nonatomic, retain) SuggestView *view;
@end
