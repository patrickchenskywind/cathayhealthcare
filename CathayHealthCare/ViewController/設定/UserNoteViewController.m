//
//  UserNoteViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/16.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "UserNoteViewController.h"

@interface UserNoteViewController ()

@end

@implementation UserNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"使用者說明"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
