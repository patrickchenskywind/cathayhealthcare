//
//  UserInfoViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "SwitchButton.h"

@interface UserInfoViewController : ViewController

@property (nonatomic,strong) IBOutlet SwitchButton *button_gender;
@property (weak, nonatomic)  IBOutlet UIView *text_birth;
@property (weak, nonatomic)  IBOutlet UILabel *labelBirth;
@property (weak, nonatomic)  IBOutlet UIView *textWorkType;
@property (weak, nonatomic)  IBOutlet UILabel *labelWorkType;
@property (nonatomic,strong) IBOutlet UITextField *text_height;
@property (nonatomic,strong) IBOutlet UITextField *text_weight;
@property (weak, nonatomic)  IBOutlet UILabel *labelBMI;

@end
