//
//  NotitficationSettingViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/16.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "NotitficationSettingViewController.h"

@interface NotitficationSettingViewController ()

@property (strong, nonatomic) IBOutlet UISwitch *switchNoti;
@property (strong, nonatomic) IBOutlet UISwitch *switchNews;
@property (strong, nonatomic) IBOutlet UISwitch *switchStoreData;
@property (strong, nonatomic) IBOutlet UISwitch *switchCalendar;

@end

@implementation NotitficationSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"通知設定"];
}

#pragma mark - Action
- (IBAction)switchNoti:(UISwitch *)sender {
    BOOL isOpen = sender.isSelected;
    if ( sender == _switchNoti ) {
        if ( isOpen)
            NSLog(@"開啟通知");
        else
            NSLog(@"通知全部關閉");
    }
    
    if ( sender == _switchNews) {
        if ( isOpen)
            NSLog(@"接受新聞通知");
        else
            NSLog(@"不接受新聞通知");
    }

    if ( sender == _switchCalendar) {
        if ( isOpen)
            NSLog(@"接受日曆通知");
        else
            NSLog(@"不接受日曆");
    }
    
    if ( sender == _switchStoreData) {
        if ( isOpen)
            NSLog(@"接受每日提醒");
        else
            NSLog(@"不接受每日提醒");
    }
}

@end
