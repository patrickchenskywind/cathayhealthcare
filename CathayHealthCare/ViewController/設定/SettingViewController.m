//
//  SettingViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SettingViewController.h"
#import "NotitficationSettingViewController.h"
#import "UserNoteViewController.h"
#import "UserInfoViewController.h"
#import "TargetViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"設定"];
}

- (IBAction)buttonUserDataPress:(id)sender {
    [self.navigationController pushViewController: [[UserInfoViewController alloc] initWithNibName: @"UserInfoViewController" bundle: nil] animated: YES];
}

- (IBAction)buttonTargertPress:(id)sender {
    [self.navigationController pushViewController: [[TargetViewController alloc] initWithNibName: @"TargetViewController" bundle:nil] animated: YES];
}

- (IBAction)buttonNotiSettingPress:(id)sender {
    [self.navigationController pushViewController: [[UserNoteViewController alloc] initWithNibName: @"UserNoteViewController" bundle: nil] animated: YES];
}

- (IBAction)buttonExplainPress:(id)sender {
    [self.navigationController pushViewController: [[NotitficationSettingViewController alloc] initWithNibName: @"NotitficationSettingViewController" bundle:nil] animated: YES];
}

@end
