//
//  TargetViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "RadioButton.h"

@interface TargetViewController : ViewController

@property (nonatomic,strong) IBOutlet UITextField *text_targetWeight;
@property (nonatomic,strong) IBOutlet UITextField *text_targetFrom;
@property (nonatomic,strong) IBOutlet UITextField *text_targetTo;
@property (nonatomic,strong) IBOutlet UITextField *text_targetAdd;
@property (nonatomic,strong) IBOutlet UITextField *text_heartbeat;

@property (nonatomic,strong) IBOutlet UILabel *label1;
@property (nonatomic,strong) IBOutlet UILabel *label2;
@property (nonatomic,strong) IBOutlet UILabel *label3;
@property (nonatomic,strong) IBOutlet UILabel *label4;
@property (nonatomic,strong) IBOutlet UILabel *label5;
@property (nonatomic,strong) IBOutlet UILabel *label6;
@property (nonatomic,strong) IBOutlet UILabel *label7;
@property (nonatomic,strong) IBOutlet UILabel *label8;
@property (nonatomic,strong) IBOutlet UILabel *label9;

@property (nonatomic,strong) IBOutlet UIView *view_targetSetting;

@property (nonatomic,strong) IBOutlet RadioButton *button_keep;
@property (nonatomic,strong) IBOutlet RadioButton *button_add;

@end
