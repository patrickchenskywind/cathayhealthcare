//
//  TargetViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "TargetViewController.h"
#import "UIColor+Hex.h"
#import "LoginEntity.h"
#import "SetBodyManageAPI.h"
#import "SetBaseSettingAPI.h"
#import "Toos.h"

@interface TargetViewController ()

@end

@implementation TargetViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupDefaultValues];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(textFieldTextDidChangeNotification:)
                                                 name: UITextFieldTextDidChangeNotification
                                               object: nil];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

#pragma mark - Action
- (IBAction)sendRequest:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetBodyManageAPI *setBodyManageAPI = [[SetBodyManageAPI alloc] init];
        [setBodyManageAPI setWeightGoal: self.text_targetWeight.text];
        if (self.button_keep.isSelected)
            [setBodyManageAPI setWeightTarget: @"0"];
        else if (self.button_add.isSelected) {
            if ([self.label4.text isEqualToString: @"每週減重"])
                [setBodyManageAPI setWeightTarget: [NSString stringWithFormat:@"-%@",self.text_targetAdd.text]];
            else
                [setBodyManageAPI setWeightTarget: self.text_targetAdd.text];
        }
        else
            [setBodyManageAPI setWeightTarget: @"0"];
        
        [setBodyManageAPI post:^(HttpResponse *response) {
            BaseEntity *baseEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: baseEntity.msg]) {
                [self saveBodyManageAPIToLocal];
                SetBaseSettingAPI *setBaseSettingAPI = [[SetBaseSettingAPI alloc] init];
                LoginEntity *loginEntity = [LoginEntity get];
                [setBaseSettingAPI setWeight: loginEntity.workType];
                [setBaseSettingAPI setGender: loginEntity.gender];
                [setBaseSettingAPI setHeight: loginEntity.height];
                [setBaseSettingAPI setWeight: loginEntity.weight];
                [setBaseSettingAPI setBirthday: loginEntity.birthday];
                [setBaseSettingAPI setHeartbeat: self.text_heartbeat.text];
                [setBaseSettingAPI post:^(HttpResponse *response) {
                    BaseEntity *baseEntity = response.result.firstObject;
                    if ([Toos checkMsgCodeWihtMsg: baseEntity.msg]) {
                        [self saveSetBaseSettingAPIToLocal];
                    }
                }];
            }
        }];
    }
    else {
        [self saveBodyManageAPIToLocal];
        [self saveSetBaseSettingAPIToLocal];
    }
}

- (void) saveBodyManageAPIToLocal {
    if ([LoginEntity get])
        [self setupLoginEntity: [LoginEntity get]];
    else
        [self setupLoginEntity: [[LoginEntity alloc] init]];
}

- (void) saveSetBaseSettingAPIToLocal {
    if ([LoginEntity get])
        [self setupLoginEntityWithHerat: [LoginEntity get]];
    else
        [self setupLoginEntityWithHerat: [[LoginEntity alloc] init]];
}

- (void) setupLoginEntity: (LoginEntity *)loginEntity {
    [loginEntity setWeightGoal: self.text_targetWeight.text];
    if (self.button_keep.isSelected) {
        [loginEntity setWeightTarget: @"0"];
    }
    else {
        if ([self.label4.text isEqualToString: @"每週減重"])
            [loginEntity setWeightTarget: [NSString stringWithFormat:@"-%@",self.text_targetAdd.text]];
        else
            [loginEntity setWeightTarget: self.text_targetAdd.text];
    }
    [loginEntity saveToLocal];
}

- (void) setupLoginEntityWithHerat: (LoginEntity *)loginEntity {
    [loginEntity setHeartBeat: self.text_heartbeat.text];
    [loginEntity saveToLocal];
    [self.navigationController popViewControllerAnimated: YES];
}

- (void) textFieldTextDidChangeNotification:(NSNotification *)notification {
    if ([notification.object isEqual: self.text_targetWeight]) {
        if (self.text_targetWeight.text.length > 0 && [LoginEntity get]) {
            LoginEntity *loginEntity = [LoginEntity get];
            if (loginEntity.weight && loginEntity.weight.length > 0) {
                [self setupLabelTargetWithWeight: [loginEntity.weight doubleValue] target: [self.text_targetWeight.text doubleValue]];
            }
        }
    }
}

- (void) setupDefaultValues {
    if ([LoginEntity get]) {
        LoginEntity *loginEntity = [LoginEntity get];
        if (loginEntity.weightTarget && loginEntity.weightTarget.length > 0)
            [self.text_targetWeight setText: loginEntity.weightGoal];
        
        if (loginEntity.height && loginEntity.height.length > 0) {
            CGFloat minWeight = (([loginEntity.height doubleValue] / 100) * ([loginEntity.height doubleValue] / 100)) * 18.5;
            [self.text_targetFrom setText: [NSString stringWithFormat: @"%0.1f", minWeight]];
            CGFloat maxWeight = (([loginEntity.height doubleValue] / 100) * ([loginEntity.height doubleValue] / 100)) * 24;
            [self.text_targetTo setText: [NSString stringWithFormat: @"%0.1f", maxWeight]];
        }
        
        if (loginEntity.heartBeat && loginEntity.heartBeat.length > 0)
            [self.text_heartbeat setText: loginEntity.heartBeat];
        
        [self setupWeightTarget: loginEntity];
    }
}

- (void) setupWeightTarget : (LoginEntity *)loginEntity {
    if (loginEntity.weightTarget && loginEntity.weightTarget.length > 0) {
        if ([loginEntity.weightTarget isEqualToString: @"0.0"] || [loginEntity.weightTarget isEqualToString: @"0"]) {
            [self.button_keep setIsSelected: YES];
        }
        else {
            [self.button_add setIsSelected: YES];
            [self.text_targetAdd setText: [loginEntity.weightTarget stringByReplacingOccurrencesOfString: @"-" withString: @""]];
        }
        
        if ((loginEntity.weight && loginEntity.weight.length > 0)) {
            [self setupLabelTargetWithWeight: [loginEntity.weight doubleValue] target: [loginEntity.weightGoal doubleValue]];
        }
    }
}

- (void) setupLabelTargetWithWeight: (double) weight target: (double) target {
    if (weight > target)
        [self.label4 setText: @"每週減重"];
    else if (weight < target)
        [self.label4 setText: @"每週增重"];
    else
        [self buttonKeepPress: nil];
}

- (IBAction)buttonKeepPress:(id)sender {
    if (self.button_add.isSelected) {
        [self.button_add setIsSelected: NO];
    }
}

- (IBAction)buttonTargetPress:(id)sender {
    if (self.button_keep.isSelected) {
        [self.button_keep setIsSelected: NO];
    }
}

- (void) setupUI {
    [self setTitle:@"管理目標"];
    
    [_button_add setYesStyle: [UIImage imageNamed:@"checkbox_target_checked"]
                  andNoStyle: [UIImage imageNamed:@"checkbox_target_uncheck"]
                    isSelect: NO];
    [_button_keep setYesStyle: [UIImage imageNamed: @"checkbox_target_checked"]
                   andNoStyle: [UIImage imageNamed:@"checkbox_target_uncheck"]
                     isSelect: NO];
    
    if ([UIScreen mainScreen].bounds.size.width == 320) {
        float fontSize = 13.0;
        [_label1 setFont: [UIFont systemFontOfSize: fontSize]];
        [_label2 setFont: [UIFont systemFontOfSize: fontSize]];
        [_label3 setFont: [UIFont systemFontOfSize: fontSize]];
        [_label4 setFont: [UIFont systemFontOfSize: fontSize]];
        [_label5 setFont: [UIFont systemFontOfSize: fontSize]];
        [_label6 setFont: [UIFont systemFontOfSize: fontSize]];
        [_label7 setFont: [UIFont systemFontOfSize: fontSize]];
        [_label8 setFont: [UIFont systemFontOfSize: fontSize]];
        [_label9 setFont: [UIFont systemFontOfSize: fontSize]];
    }
    
    UIColor *borderColor = [UIColor colorWithHex: 0x00A29A];
    [_text_targetWeight.layer setBorderColor: borderColor.CGColor];
    [_text_targetFrom.layer setBorderColor: borderColor.CGColor];
    [_text_targetTo.layer setBorderColor: borderColor.CGColor];
    [_text_targetAdd.layer setBorderColor: borderColor.CGColor];
    [_text_heartbeat.layer setBorderColor: borderColor.CGColor];
    [_view_targetSetting.layer setBorderColor: borderColor.CGColor];
    
    [_text_targetWeight.layer setBorderWidth: 1];
    [_text_targetFrom.layer setBorderWidth: 1];
    [_text_targetTo.layer setBorderWidth: 1];
    [_text_targetAdd.layer setBorderWidth: 1];
    [_text_heartbeat.layer setBorderWidth: 1];
    [_view_targetSetting.layer setBorderWidth: 1];
    
    [_text_targetWeight.layer setCornerRadius: 5];
    [_text_targetFrom.layer setCornerRadius: 5];
    [_text_targetTo.layer setCornerRadius: 5];
    [_text_targetAdd.layer setCornerRadius: 5];
    [_text_heartbeat.layer setCornerRadius: 5];
    [_view_targetSetting.layer setCornerRadius: 10];
}


@end
