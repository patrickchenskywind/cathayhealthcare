//
//  SettingViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"

@interface SettingViewController : ViewController

@property (nonatomic,strong) IBOutlet UIButton *button_userData;
@property (nonatomic,strong) IBOutlet UIButton *button_targert;
@property (nonatomic,strong) IBOutlet UIButton *button_explain;
@property (nonatomic,strong) IBOutlet UIButton *button_notiSetting;

@end
