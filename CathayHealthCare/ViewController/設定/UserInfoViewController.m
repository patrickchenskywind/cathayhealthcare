//
//  UserInfoViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "UserInfoViewController.h"
#import <WYPopoverController/WYPopoverController.h>
#import "DatePickerViewController.h"
#import "WorkPickerViewController.h"
#import "UIColor+Hex.h"
#import "LoginEntity.h"
#import "Toos.h"
#import "DefindConfig.h"
#import "SetBaseSettingAPI.h"


@interface UserInfoViewController ()<IDatePickerViewController, IWorkPickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) DatePickerViewController *datePickerViewController;
@property (strong, nonatomic) WorkPickerViewController *workPickerViewController;
@property (strong, nonatomic) NSString * birthdayTime;
@property (strong, nonatomic) NSString * workType;

@end

@implementation UserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(textFieldTextDidChangeNotification:)
                                                 name: UITextFieldTextDidChangeNotification
                                               object: nil];
    ;
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}
- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setupUserInfo];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void) textFieldTextDidChangeNotification:(NSNotification *)notification {
    if ([notification.object isEqual: self.text_height] || [notification.object isEqual: self.text_weight] )
        [self setupBMI];
}

#pragma mark - Action
- (IBAction)sendRequest:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetBaseSettingAPI *api = [[SetBaseSettingAPI alloc] init];
        [api setGender: self.button_gender.isOpen ? @"1" : @"0"];
        [api setBirthday: self.birthdayTime];
        [api setHeight: self.text_height.text];
        [api setWeight: self.text_weight.text];
        [api setWorkType: self.workType];
        [api setHeartbeat: [LoginEntity get].heartBeat];
        [api post:^(HttpResponse *response) {
            BaseEntity *baseEntity = response.result.firstObject;
            if ( [Toos checkMsgCodeWihtMsg: baseEntity.msg] ) {
                [self saveBaseSettingToLocal];
            }
        }];
    }
    else {
        [self saveBaseSettingToLocal];
    }
}

- (void) saveBaseSettingToLocal {
    if ([LoginEntity get])
        [self setupLoginEntity: [LoginEntity get]];
    else
        [self setupLoginEntity: [[LoginEntity alloc] init]];
}


- (void) setupLoginEntity: (LoginEntity *)loginEntity {
    [loginEntity setGender: self.button_gender.isOpen ? @"1" : @"0"];
    [loginEntity setBirthday: self.birthdayTime];
    [loginEntity setHeight: self.text_height.text];
    [loginEntity setWeight: self.text_weight.text];
    [loginEntity setWorkType: self.workType];
    [loginEntity saveToLocal];
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction) buttonBitthdayPress: (UIButton *)sender {
    if ( ! self.datePickerViewController) {
        [self setDatePickerViewController: [[DatePickerViewController alloc] initWithNibName: @"DatePickerViewController" bundle: nil]];
        [self.datePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.datePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 218)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}
- (IBAction)buttonWorkTypePress:(UIButton *)sender {
    if ( ! self.workPickerViewController) {
        [self setWorkPickerViewController: [[WorkPickerViewController alloc] initWithNibName: @"WorkPickerViewController" bundle: nil]];
        [self.workPickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.workPickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 162)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (void) dateDidSelect:(NSDate *)date {
    [self.labelBirth setText: [Toos getYMDFormDate: date]];
    [self setBirthdayTime: [Toos getThousandSecondStringFromDate: date]];
}

- (void) workTypeDidSelect:(NSInteger)row title:(NSString *)title {
    [self.labelWorkType setText: title];
    [self setWorkType: [NSString stringWithFormat: @"%zd", row]];
}

- (void) setupUserInfo {
    if ([LoginEntity get]) {
        LoginEntity *loginEntity = [LoginEntity get];
        if (loginEntity.gender && loginEntity.gender.length >0 )
            [self.button_gender setIsOpen: [loginEntity.gender isEqualToString: @"1"]];
        
        if (loginEntity.birthday && loginEntity.birthday.length > 0) {
            [self.labelBirth setText: [Toos getYMDStringFormServerTimeInterval: loginEntity.birthday]];
            [self setBirthdayTime: loginEntity.birthday];
        }
        
        if (loginEntity.height && loginEntity.height.length > 0)
            [self.text_height setText: loginEntity.height];
        
        if (loginEntity.weight && loginEntity.weight.length > 0)
            [self.text_weight setText: loginEntity.weight];
        
        [self setupBMI];
        [self setupWorkType: loginEntity];
    }
}

- (void) setupBMI {
    if (self.text_weight.text.length > 0 && self.text_height.text.length > 0) {
        CGFloat height = [self.text_height.text floatValue] / 100;
        CGFloat weight = [self.text_weight.text floatValue];
        CGFloat bmi = weight / (height * height);
        [self.labelBMI setText: [NSString stringWithFormat: @"%.2f",bmi]];
    }
    else {
        [self.labelBMI setText: @""];
    }
}

- (void) setupWorkType:(LoginEntity *)loginEntity {
    if (loginEntity.workType && loginEntity.workType.length > 0){
        NSString *workType = [NSString stringWithFormat:@"WORKTYPE %@",loginEntity.workType];
        [self.labelWorkType setText: NSLocalizedString(workType, nil)];
        [self setWorkType: loginEntity.workType];
    }
}

- (void) setupUI {
    [self setTitle:@"管理目標"];
    [_button_gender setOnName: @"女" offName: @"男" color: [UIColor colorWithHex: 0x00A29A] nameColor: [UIColor whiteColor] isOpen: NO];
    [_text_height.layer setBorderColor:[[UIColor colorWithHex:0x00A29A] CGColor]];
    [_text_height.layer setBorderWidth:1];
    [_text_height.layer setCornerRadius:5];
    [_text_weight.layer setBorderColor:[[UIColor colorWithHex:0x00A29A] CGColor]];
    [_text_weight.layer setBorderWidth:1];
    [_text_weight.layer setCornerRadius:5];
    [_text_birth.layer setBorderColor:[[UIColor colorWithHex:0x00A29A] CGColor]];
    [_text_birth.layer setBorderWidth:1];
    [_text_birth.layer setCornerRadius:5];
    [_text_weight.layer setCornerRadius:5];
    [_textWorkType.layer setBorderColor:[[UIColor colorWithHex:0x00A29A] CGColor]];
    [_textWorkType.layer setBorderWidth:1];
    [_textWorkType.layer setCornerRadius:5];
}

@end
