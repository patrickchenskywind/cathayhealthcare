//
//  HealthProgressView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HealthProgressView.h"
#import "UIColor+Hex.h"

@implementation HealthProgressView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        if (!_chartView) {
            _chartView = [[[NSBundle mainBundle] loadNibNamed:@"HealthChartView" owner:self options:nil] objectAtIndex:0];
            [self addSubview:_chartView];
        }
        
        if (!_dailyView) {
            _dailyView = [[[NSBundle mainBundle] loadNibNamed:@"DailyRecordView" owner:self options:nil] objectAtIndex:0];
            [self addSubview:_dailyView];
        }

    }
    return self;
}

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    [self subPageButtonPress:_button_daily];
}



-(IBAction)subPageButtonPress:(UIButton *)button{
    
    if (button == _button_daily) {
        [self bringSubviewToFront:_dailyView];
        [_button_daily setBackgroundImage:[UIImage imageNamed:@"bg_tab_blue"] forState:UIControlStateNormal];
        [_button_daily setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button_chart setBackgroundImage:Nil forState:UIControlStateNormal];
        [_button_chart setTitleColor:[UIColor colorWithHex:0x917F72] forState:UIControlStateNormal];
        
    }
    else if (button == _button_chart){
        [self bringSubviewToFront:_chartView];
        [_button_chart setBackgroundImage:[UIImage imageNamed:@"bg_tab_blue"] forState:UIControlStateNormal];
        [_button_chart setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_button_daily setBackgroundImage:Nil forState:UIControlStateNormal];
        [_button_daily setTitleColor:[UIColor colorWithHex:0x917F72] forState:UIControlStateNormal];
    }
}


-(void)layoutSubviews
{
    
    CGRect viewRect = self.bounds;
    [_dailyView setFrame:CGRectMake(0,
                                    CGRectGetMaxY(_button_daily.frame),
                                    CGRectGetWidth(viewRect),
                                    viewRect.size.height - CGRectGetMaxY(_button_daily.frame))];
    [_chartView setFrame:_dailyView.frame];
    
}

@end
