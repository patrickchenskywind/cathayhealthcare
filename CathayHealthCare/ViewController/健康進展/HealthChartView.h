//
//  HealthChartView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChart.h"
#import "PullDownTextField.h"

@interface HealthChartView : UIView

@property (nonatomic,strong) IBOutlet PullDownTextField *text_item;
@property (nonatomic,strong) IBOutlet UIButton    *button_share;
@property (nonatomic,strong) IBOutlet UIView      *view_content;
@property (nonatomic,strong) IBOutlet UIButton    *button_sevenDays;
@property (nonatomic,strong) IBOutlet UIButton    *button_month;
@property (nonatomic,strong) IBOutlet UIButton    *button_threeMonths;
@property (nonatomic,strong) IBOutlet UIButton    *button_halfYear;
@property (nonatomic,strong) IBOutlet UIButton    *button_year;

//@property (nonatomic,strong) PNChart *lineChart;



@end
