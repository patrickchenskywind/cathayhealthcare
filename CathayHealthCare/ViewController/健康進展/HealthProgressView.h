//
//  HealthProgressView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DailyRecordView.h"
#import "HealthChartView.h"

@protocol HealthProgressViewDelegate <NSObject>
@end

@interface HealthProgressView : UIView
@property (nonatomic,strong) IBOutlet UIButton *button_daily;
@property (nonatomic,strong) IBOutlet UIButton *button_chart;

@property (nonatomic,strong) DailyRecordView *dailyView;
@property (nonatomic,strong) HealthChartView *chartView;

@property (nonatomic,weak) IBOutlet id<HealthProgressViewDelegate> delegate;

-(IBAction)subPageButtonPress:(UIButton *)button;

@end
