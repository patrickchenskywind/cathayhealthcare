//
//  DailyRecordView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "DailyRecordView.h"
#import "UIColor+Hex.h"

@implementation DailyRecordView


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _array_detailTitle = @[@"體重",
                               @"體脂",
                               @"腰圍",
                               @"血壓",
                               @"脈搏",
                               @"飯前血糖",
                               @"飯後血糖",
                               @"糖化血紅素",
                               @"三酸甘油脂",
                               @"高密度脂蛋白",
                               @"睡眠",
                               @"已完成目標天數"];
        
        _array_value = @[@"80公斤",
                         @"25%",
                         @"90公分",
                         @"120/80 mm/Hg",
                         @"80次/分鐘",
                         @"20mg/dL",
                         @"20mg/dL",
                         @"10%",
                         @"150mg/dL",
                         @"80mg/dL",
                         @"8小時5分鐘",
                         @"60天"];
    }
    NSLog(@"%@",self.table_detail);
    return self;
}




#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _array_detailTitle.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dailyRecordCell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"dailyRecordCell"];
        
        NSString *title = [_array_detailTitle objectAtIndex:indexPath.row];
        NSString *value = [_array_value objectAtIndex:indexPath.row];
        [cell.textLabel setText: [NSString stringWithFormat:@"   %@",title]];
        [cell.textLabel setTextAlignment:NSTextAlignmentLeft];
        [cell.textLabel setTextColor:[UIColor colorWithHex:0x917F72]];
        [cell.detailTextLabel setText:[NSString stringWithFormat:@"   %@",value]];
        [cell.detailTextLabel setTextAlignment:NSTextAlignmentLeft];
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

#pragma mark
-(void)layoutSubviews
{
    
}

@end
