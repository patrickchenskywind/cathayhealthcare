//
//  HealthProgressViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HealthProgressViewController.h"

@interface HealthProgressViewController ()

@end

@implementation HealthProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"健康進展"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titlebar_blue"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
}







@end
