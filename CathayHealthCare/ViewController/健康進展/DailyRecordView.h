//
//  DailyRecordView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DailyRecordView : UIView<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UIButton *button_back;
@property (nonatomic,strong) IBOutlet UIButton *button_forward;
@property (nonatomic,strong) IBOutlet UITextField *text_date;
@property (nonatomic,strong) IBOutlet UITableView *table_detail;

@property (nonatomic,strong) NSArray *array_detailTitle;
@property (nonatomic,strong) NSArray *array_value;

@end
