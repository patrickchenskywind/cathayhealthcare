//
//  HealthChartView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HealthChartView.h"
#import "UIColor+Hex.h"

enum PROGRESSVIEW_CONFIG
{
    BUTTON_SHARE_RIGHTBORDER    = 20,
    BUTTON_SHARE_TOPBORDER      = 20,
    BUTTON_SHARE_BOTTOMBORDER   = 20,
    TEXT_ITEM_LEFTBORDER        = 20,
    BUTTON_TIME_SIDEBORDER      = 10,
    BUTTON_TIME_TOPBORDER       = 20,
    BUTTON_TIME_COUNT           = 5
};

#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)

@interface HealthChartView()
@property (nonatomic,strong) NSArray *array_buttons;
@end

@implementation HealthChartView

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {

    }
    return self;
}
-(void)awakeFromNib
{
    _array_buttons = @[_button_halfYear,
                       _button_month,
                       _button_sevenDays,
                       _button_threeMonths,
                       _button_year];
    [self buttonPress:_button_sevenDays];
}
-(void)willMoveToSuperview:(UIView *)newSuperview
{
    [_text_item initialWithpullDownContent:@[@"腰圍",@"體重"] borderColor:[UIColor colorWithHex:0x917F72]];
    [_text_item.text setText:@"體重"];
}

-(IBAction)buttonPress:(UIButton *)sender
{
    for (UIButton *button in _array_buttons) {
        if (button == sender) {
            if (sender == _button_sevenDays) {
                [_button_sevenDays setBackgroundImage:[UIImage imageNamed:@"btn_time_blue_l_pressed"] forState:UIControlStateNormal];
            }
            else if (sender == _button_year){
                [_button_year setBackgroundImage:[UIImage imageNamed:@"btn_time_blue_r_pressed"] forState:UIControlStateNormal];
            }
            else{
                [button setBackgroundImage:[UIImage imageNamed:@"btn_time_blue_m_pressed"] forState:UIControlStateNormal];
            }
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else{
            if (button == _button_sevenDays) {
                [_button_sevenDays setBackgroundImage:[UIImage imageNamed:@"btn_time_blue_l_normal"] forState:UIControlStateNormal];
            }
            else if (button == _button_year){
                [_button_year setBackgroundImage:[UIImage imageNamed:@"btn_time_blue_r_normal"] forState:UIControlStateNormal];
            }
            else{
                [button setBackgroundImage:[UIImage imageNamed:@"btn_time_blue_m_normal"] forState:UIControlStateNormal];
            }
            [button setTitleColor:[UIColor colorWithHex:0x00A29A] forState:UIControlStateNormal];
        }
    }
}

-(void)layoutSubviews {
    
//    
//    if (_view_content && !_lineChart) {
//        _lineChart = [[PNChart alloc] initWithFrame:_view_content.bounds];
//        [_lineChart setBackgroundColor:[UIColor clearColor]];
//        [_lineChart setXLabels:@[@"5/24",@"5/25",@"5/26",@"5/27",@"5/28",@"5/29",@"5/30"]];
//        [_lineChart setYValues:@[@"1",@"3",@"1",@"4",@"7",@"8",@"5"]];
//        [_lineChart strokeChart];
//        [_view_content addSubview:_lineChart];
//    }

}



@end
