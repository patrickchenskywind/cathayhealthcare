//
//  HealthProgressViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HealthProgressModel.h"
#import "HealthProgressView.h"

@interface HealthProgressViewController : UIViewController

@property (nonatomic,strong) HealthProgressModel *model_progress;

@end
