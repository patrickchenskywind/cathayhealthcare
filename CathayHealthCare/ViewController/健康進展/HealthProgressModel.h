//
//  HealthProgressModel.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HealthProgressModel : NSObject

@property (nonatomic,strong) NSArray *array_itemTitle;
@property (nonatomic,strong) NSArray *array_itemValue;
+(instancetype)sharedInstance;
@end
