//
//  HealthProgressModel.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HealthProgressModel.h"

@implementation HealthProgressModel

+(instancetype)sharedInstance
{
    static HealthProgressModel *sharedInstance ;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id)init{
    self = [super init];
    if (self) {
        //initial
        _array_itemTitle = @[@"體重",
                             @"體脂",
                             @"腰圍",
                             @"血壓",
                             @"脈搏",
                             @"飯前血糖",
                             @"飯後血糖",
                             @"糖化血紅素",
                             @"三酸甘油脂",
                             @"高密度脂蛋白",
                             @"睡眠",
                             @"已完成目標天數"];
        
        _array_itemValue = @[@"80公斤",
                             @"25%",
                             @"90公分",
                             @"120/80 mm/Hg",
                             @"80次/分鐘",
                             @"20mg/dL",
                             @"20mg/dL",
                             @"10%",
                             @"150mg/dL",
                             @"80mg/dL",
                             @"8小時5分鐘",
                             @"60天"];
        
    }
    return self;
}

@end
