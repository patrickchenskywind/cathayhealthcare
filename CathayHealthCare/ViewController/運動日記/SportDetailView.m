//
//  SportDetailView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SportDetailView.h"
#import "SetWorkOutAPI.h"
#import "DefindConfig.h"
#import "Toos.h"

@implementation SportDetailView

-(void) layoutSubviews {
    [super layoutSubviews];
    NSString *sportType = [NSString stringWithFormat:@"SPORTTYPE %@",self.sportRecordEntity.sportType];
    [self.labelWorkType setText: NSLocalizedString(sportType, nil)];
    [self.labelDuration setText: [NSString stringWithFormat:@"%d分鐘", ([self.sportRecordEntity.sportTime integerValue] / 60)]];
    [self.labelCal setText: [NSString stringWithFormat: @"%@大卡",self.sportRecordEntity.cal]];
    [self.labelSteps setText: [NSString stringWithFormat: @"%@步",self.sportRecordEntity.sportStep]];
    [self.labelCombustionTime setText: [NSString stringWithFormat:@"%d分鐘", ([self.sportRecordEntity.burnTime integerValue] / 60)]];
    [self.labelHeartbeat setText: [NSString stringWithFormat:@"%@/分",self.sportRecordEntity.heartBeat]];
}

- (IBAction)deleteButtonPress:(id)sender {
    
    if ((self.sportRecordEntity.sid && self.sportRecordEntity.sid > 0) && [[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
        SetWorkOutAPI *api = [[SetWorkOutAPI alloc] init];
        [api setSid: self.sportRecordEntity.sid];
        [api post:^(HttpResponse *response) {
            BaseEntity *baseEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: baseEntity.msg]) {
                [self.sportRecordEntity removeBySid];
                [self.iDelegate dataHasBeenDelete];
                [self closeButtonPress: nil];
            }
        }];
    }
    else {
        [self.sportRecordEntity removeByDate];
        [self.iDelegate dataHasBeenDelete];
        [self closeButtonPress: nil];
    }
}

-(IBAction)closeButtonPress:(id)sender {
    if (_maskView) {
        [_maskView removeFromSuperview];
        _maskView = nil;
    }
    [self removeFromSuperview];
}

-(void)addDetailViewWithView:(UIView *)superView {
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:superView.bounds];
        [_maskView setBackgroundColor:[UIColor blackColor]];
        [_maskView setAlpha:0.5];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonPress:)];
        [_maskView addGestureRecognizer:tap];
    }
    [self setCenter:CGPointMake(CGRectGetMidX(superView.bounds),
                                CGRectGetMidY(superView.bounds))];
    [superView addSubview:_maskView];
    [superView addSubview:self];
}

@end
