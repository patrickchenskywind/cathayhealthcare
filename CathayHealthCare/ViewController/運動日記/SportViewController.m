//
//  SportViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SportViewController.h"
#import "AddRecordViewController.h"

@interface SportViewController ()
@end

@implementation SportViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"運動日記"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"titlebar_purple"] forBarMetrics:UIBarMetricsDefault];
    [self.view buttonRecordPress: self.view.button_record];
}

#pragma mark SportViewDelegate
-(void)addSportRecord {
    [self.navigationController pushViewController: [[AddRecordViewController alloc] initWithNibName: @"AddRecordViewController" bundle:nil] animated: YES];
}

@end
