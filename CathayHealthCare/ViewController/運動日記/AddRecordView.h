//
//  AddRecordView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@interface AddRecordView : View

// Line 1
@property (nonatomic,strong) IBOutlet UITextField *text_category;
@property (nonatomic,strong) IBOutlet UILabel     *label_heartSection;
@property (nonatomic,strong) IBOutlet UITextField *text_time;
@property (nonatomic,strong) IBOutlet UITextField *text_cal;
@property (nonatomic,strong) IBOutlet UITextField *text_stepNum;
@property (nonatomic,strong) IBOutlet UITextField *text_calSection;
@property (nonatomic,strong) IBOutlet UITextField *text_heart;

@property (nonatomic,strong) IBOutlet UIButton    *button_send;

@end
