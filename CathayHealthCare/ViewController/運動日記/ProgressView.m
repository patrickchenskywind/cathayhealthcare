//
//  ProgressView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ProgressView.h"
#import "UIColor+Hex.h"


#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)

@interface ProgressView()
@property (nonatomic,strong) NSArray *array_buttons;
@end

@implementation ProgressView

-(void)awakeFromNib {
    [super awakeFromNib];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    
    PNLineChart * lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0, 30, self.view_content.bounds.size.width  , self.view_content.bounds.size.height- 40)];
    [lineChart setXLabels:@[@"SEP 1",@"SEP 2",@"SEP 3",@"SEP 4",@"SEP 5"]];
    
    // Line Chart No.1
    [lineChart setShowCoordinateAxis: YES];
    NSArray * data01Array = @[@60.1, @160.1, @126.4, @500, @186.2];
    PNLineChartData *data01 = [PNLineChartData new];
    [data01 setColor: PNFreshGreen];
    [data01 setItemCount: lineChart.xLabels.count];
    [data01 setInflexionPointStyle: PNLineChartPointStyleCircle];
    [data01 setGetData: ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    }];
    [lineChart setChartData: @[data01]];
    [lineChart setBackgroundColor: [UIColor clearColor]];
    [lineChart strokeChart];
    
    [self.view_content addSubview: lineChart];
}


@end
