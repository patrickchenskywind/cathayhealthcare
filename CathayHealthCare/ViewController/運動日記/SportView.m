//
//  SportView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SportView.h"

@interface SportView ()<ISportDetailView>

@end

@implementation SportView

- (IBAction)buttonRecordPress:(UIButton *)sender {
    if ( ! sender.selected) {
        if ( ! self.recordView) {
            self.recordView = [[[NSBundle mainBundle] loadNibNamed:@"RecordView" owner:self options:nil] objectAtIndex:0];
            [self.recordView setFrame:CGRectMake(0,
                                                 CGRectGetMaxY(self.button_record.frame),
                                                 CGRectGetWidth(self.frame),
                                                 CGRectGetHeight(self.frame)-CGRectGetMaxY(self.button_record.frame))];
            [self.recordView setDelegate:self];
            [self addSubview:self.recordView];
        }
        [self bringSubviewToFront:self.recordView];
        [sender setSelected: ! sender.selected];
        [self.button_progress setSelected: NO];
    }
}

- (IBAction)buttonProgressPress:(UIButton *)sender {
    if ( ! sender.selected) {
        if (!self.progressView) {
            self.progressView = [[[NSBundle mainBundle] loadNibNamed:@"ProgressView" owner:self options:nil] objectAtIndex:0];
            [self.progressView setBackgroundColor: [UIColor whiteColor]];
            [self.progressView setFrame:CGRectMake(0,
                                                   CGRectGetMaxY(self.button_progress.frame),
                                                   CGRectGetWidth(self.frame),
                                                   CGRectGetHeight(self.frame)-CGRectGetMaxY(self.button_progress.frame))];
            [self addSubview: self.progressView];
        }
        [self bringSubviewToFront:self.progressView];
        [sender setSelected: ! sender.selected];
        [self.button_record setSelected: NO];
    }
}

#pragma mark - RecordViewDelegate
-(void)showSportDetailWithSportRecordEntity: (SportRecordEntity *)sportRecordEntity {
    SportDetailView *sportDetailView = [[[NSBundle mainBundle] loadNibNamed: @"SportDetailView" owner: self options: nil] objectAtIndex: 0];
    [sportDetailView setSportRecordEntity: sportRecordEntity];
    [sportDetailView setIDelegate: self];
    [sportDetailView addDetailViewWithView: self];
}

-(void)addRecordButtonPress {
    [self.delegate addSportRecord];
}

#pragma mark - ISportDetailView
- (void) dataHasBeenDelete {
    [self.recordView setupTableData];
}

@end
