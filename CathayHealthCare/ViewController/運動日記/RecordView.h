//
//  RecordView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SportRecordCell.h"
#import "GetSportRecordAPI.h"
@protocol RecordViewDelegate <NSObject>

@required
    -(void)showSportDetailWithSportRecordEntity: (SportRecordEntity *)sportRecordEntity;
    -(void)addRecordButtonPress;

@end

@interface RecordView : UIView<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UIButton      *button_forward;
@property (nonatomic,strong) IBOutlet UIButton      *button_back;
@property (nonatomic,strong) IBOutlet UIButton      *button_garmin;
@property (nonatomic,strong) IBOutlet UIButton      *button_addRecord;
@property (nonatomic,strong) IBOutlet UITextField   *text_date;
@property (nonatomic,strong) IBOutlet UITableView   *table_record;
@property (nonatomic,strong) IBOutlet UILabel       *label_category;
@property (nonatomic,strong) IBOutlet UILabel       *label_time;
@property (nonatomic,strong) IBOutlet UILabel       *label_cal;
@property (nonatomic,strong) IBOutlet UITextView    *textView_desc;

@property (weak, nonatomic) IBOutlet UILabel *labelDate;

@property (nonatomic,strong) NSArray *array_sportRecord;

@property (nonatomic,weak)id<RecordViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintGarminViewHeight;

- (void) setupTableData;

@end
