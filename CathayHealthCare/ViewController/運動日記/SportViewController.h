//
//  SportViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "SportView.h"

@interface SportViewController : ViewController <SportViewDelegate>

@property (nonatomic, strong) SportView *view;

@end
