//
//  SportDetailView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "SportRecordEntity.h"

@protocol ISportDetailView <NSObject>
@required
    - (void) dataHasBeenDelete;

@end

@interface SportDetailView : View
@property (nonatomic, weak) id<ISportDetailView> iDelegate;
@property (nonatomic, strong) SportRecordEntity * sportRecordEntity;
@property (nonatomic,strong) UIView *maskView;
@property (weak, nonatomic) IBOutlet UILabel *labelWorkType;
@property (weak, nonatomic) IBOutlet UILabel *labelDuration;
@property (weak, nonatomic) IBOutlet UILabel *labelCal;
@property (weak, nonatomic) IBOutlet UILabel *labelSteps;
@property (weak, nonatomic) IBOutlet UILabel *labelCombustionTime;
@property (weak, nonatomic) IBOutlet UILabel *labelHeartbeat;

-(void)addDetailViewWithView:(UIView *)superView;

@end
