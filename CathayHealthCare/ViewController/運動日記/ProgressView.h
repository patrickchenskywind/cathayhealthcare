//
//  ProgressView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import <PNChart/PNChart.h>


@interface ProgressView : View

@property (nonatomic,strong)IBOutlet UIButton *button_share;
@property (nonatomic,strong)IBOutlet UIView   *view_content;

@property (weak, nonatomic) IBOutlet UIView *pnchart;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end
