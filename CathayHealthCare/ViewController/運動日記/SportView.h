//
//  SportView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "RecordView.h"
#import "ProgressView.h"
#import "SportDetailView.h"


@protocol SportViewDelegate <NSObject>
-(void)addSportRecord;
@end

@interface SportView : View <RecordViewDelegate>

@property (nonatomic,strong) IBOutlet UIButton *button_record;
@property (nonatomic,strong) IBOutlet UIButton *button_progress;

@property (nonatomic,strong) RecordView        *recordView;
@property (nonatomic,strong) ProgressView      *progressView;
@property (nonatomic,strong) SportDetailView   *sportDetailView;

@property (nonatomic,weak)   IBOutlet id<SportViewDelegate> delegate;

- (IBAction)buttonRecordPress:(UIButton *)sender;
- (IBAction)buttonProgressPress:(UIButton *)sender;

@end
