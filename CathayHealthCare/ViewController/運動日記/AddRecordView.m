//
//  AddRecordView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddRecordView.h"

@interface AddRecordView()<UITextFieldDelegate>
{
    UITextField *currentTextField;
}

@end

@implementation AddRecordView


-(void)awakeFromNib
{
    _text_cal.delegate        = self;
    _text_calSection.delegate = self;
    _text_category.delegate   = self;
    _text_heart.delegate      = self;
    _text_stepNum.delegate    = self;
    _text_time.delegate       = self;
    
    [self addButtonToKeyBoardPadWithTextField:_text_cal];
    [self addButtonToKeyBoardPadWithTextField:_text_calSection];
    [self addButtonToKeyBoardPadWithTextField:_text_category];
    [self addButtonToKeyBoardPadWithTextField:_text_heart];
    [self addButtonToKeyBoardPadWithTextField:_text_stepNum];
    [self addButtonToKeyBoardPadWithTextField:_text_time];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextField = textField;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark add Button To KeyBoard Pad

-(void)addButtonToKeyBoardPadWithTextField:(UITextField *)textField{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad:)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStylePlain target:self action:@selector(doneWithNumberPad:)],
                           nil];
    [numberToolbar sizeToFit];
    textField.inputAccessoryView = numberToolbar;
}

#pragma mark KeyBoard Pad Button Event
-(void)cancelNumberPad:(UIBarButtonItem *)sender{
    [currentTextField resignFirstResponder];
    currentTextField.text = @"";
}

-(void)doneWithNumberPad:(UIBarButtonItem *)sender{
    [currentTextField resignFirstResponder];
    
}
@end
