//
//  AddRecordViewController.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import "AddRecordView.h"

@protocol IAddRecordViewController <NSObject>
@required
    - (void) newRecordCreate;

@end

@interface AddRecordViewController : ViewController

@property (nonatomic, weak) id<IAddRecordViewController> iDelegate;
@property (nonatomic, strong) AddRecordView *view;

@end
