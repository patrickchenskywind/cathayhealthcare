//
//  RecordView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RecordView.h"
#import "UIColor+Hex.h"
#import "Toos.h"
#import "DefindConfig.h"

@interface RecordView ()

@property (nonatomic, strong) NSDate *dateForShow;

@end

@implementation RecordView

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setDateForShow: [NSDate date]];
//        if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
//            [self executGetSportRecordAPI];
//        }
//        else {
            [self setArray_sportRecord: [SportRecordEntity getRecordByNSDate: self.dateForShow]];
//        }
    }
    return self;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    if ( ! [[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN])
        [self.constraintGarminViewHeight setConstant: 0];
    [self layoutIfNeeded];
    [self setupLabelDate];
    [_button_garmin.layer setBorderColor: [[UIColor colorWithHex: 0x5546FCF] CGColor]];
    [_button_garmin.layer setBorderWidth: 1];
    [_button_garmin.layer setCornerRadius: 5];
}

- (void) executGetSportRecordAPI {
    GetSportRecordAPI *api = [[GetSportRecordAPI alloc] init];
    [api post:^(HttpResponse *response) {
        SportRecordParentEntity *sportRecordParentEntity = [[SportRecordParentEntity alloc] init];
        if ([Toos checkMsgCodeWihtMsg: sportRecordParentEntity.msg]) {
            [SportRecordEntity removeAll];
            [sportRecordParentEntity.arraySport enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [((SportRecordEntity *)obj) saveToLocal];
                [self setArray_sportRecord: [SportRecordEntity getRecordByNSDate: self.dateForShow]];
                [self setupTableData];
            }];
        }
    }];
}

- (void) setupTableData {
    [self setArray_sportRecord: [SportRecordEntity getRecordByNSDate: self.dateForShow]];
    [self.table_record reloadData];
}

- (void) setupLabelDate {
    [self.labelDate setText: [Toos getYMDFormDate: self.dateForShow]];
}

- (IBAction)buttonForwardPress:(id)sender {
    if ( ! [Toos isCurrentDay: self.dateForShow]) {
        [self setDateForShow: [Toos addOneDay: self.dateForShow]];
        [self setupLabelDate];
        [self setupTableData];
    }
}

- (IBAction)buttonBackPress:(id)sender {
    [self setDateForShow: [Toos subOneDay: self.dateForShow]];
    [self setupLabelDate];
    [self setupTableData];
}

-(IBAction)addRecordButtonPress:(id)sender {
    [_delegate addRecordButtonPress];
}

#pragma mark UITableViewDelegate & Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array_sportRecord.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SportRecordCell *cell = [tableView dequeueReusableCellWithIdentifier: @"recordCell"];
    if ( ! cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed: @"SportRecordCell" owner: self options: nil] objectAtIndex: 0];
        [cell setFrame:CGRectMake(CGRectGetMinX(cell.frame),
                                  CGRectGetMinY(cell.frame),
                                  CGRectGetWidth(self.bounds),
                                  CGRectGetHeight(cell.bounds))];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }
    SportRecordEntity *sportRecordEntity = self.array_sportRecord[indexPath.row];
    NSString *sportType = [NSString stringWithFormat: @"SPORTTYPE %@",sportRecordEntity.sportType];
    [cell.label_cate setText: NSLocalizedString(sportType, nil)];
    [cell.label_time setText: [NSString stringWithFormat:@"%d分鐘", ([sportRecordEntity.sportTime integerValue] / 60)]];
    [cell.label_cal  setText: sportRecordEntity.cal];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGRectZero.size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate showSportDetailWithSportRecordEntity: self.array_sportRecord[indexPath.row]];
}

@end
