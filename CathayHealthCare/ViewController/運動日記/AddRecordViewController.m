//
//  AddRecordViewController.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/29.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddRecordViewController.h"
#import <WYPopoverController/WYPopoverController.h>
#import "SportTypePickerViewController.h"
#import "DefindConfig.h"
#import "SetWorkOutAPI.h"
#import "Toos.h"
#import "SportRecordEntity.h"

@interface AddRecordViewController () <ISportTypePickerViewController>

@property (strong, nonatomic) WYPopoverController *wyPopoverController;
@property (strong, nonatomic) SportTypePickerViewController *sportTypePickerViewController;
@property (strong, nonatomic) NSString * sportType;
@end

@implementation AddRecordViewController

@dynamic view;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle: @"運動紀錄"];
}

- (IBAction)buttonSportTypePress:(UIButton *)sender {
    if ( ! self.sportTypePickerViewController) {
        [self setSportTypePickerViewController: [[SportTypePickerViewController alloc] initWithNibName: @"SportTypePickerViewController" bundle: nil]];
        [self.sportTypePickerViewController setIDelegate: self];
    }
    [self setWyPopoverController: [[WYPopoverController alloc] initWithContentViewController: self.sportTypePickerViewController]];
    [self.wyPopoverController setPopoverContentSize: CGSizeMake([UIScreen mainScreen].bounds.size.width, 216)];
    [self.wyPopoverController presentPopoverFromRect: sender.bounds inView: sender permittedArrowDirections: WYPopoverArrowDirectionAny animated: YES];
}

- (IBAction)buttonSubmitPress:(UIButton *)sender {
    if ([[NSUserDefaults standardUserDefaults] objectForKey: ACCESSTOKEN]) {
#warning 會員功能  需檢查是否正確
        SetWorkOutAPI *api = [[SetWorkOutAPI alloc] init];
        [api setSid: @""];
        [api setDate: [Toos getThousandSecondStringFromDate: [NSDate date]]];
        [api setSportType: self.sportType];
        [api setHeartBeat: self.view.text_heart.text];
        [api setSportTime: [NSString stringWithFormat: @"%.00f", [self.view.text_time.text floatValue] * 60]];
        [api setCal: self.view.text_cal.text];
        [api setBurnTime: [NSString stringWithFormat: @"%.00f", [self.view.text_calSection.text floatValue] * 60]];
        [api setSportStep: self.view.text_stepNum.text];
        [api post:^(HttpResponse *response) {
            BaseEntity *baseEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: baseEntity.msg]) {
                [self.iDelegate newRecordCreate];
                [self.navigationController popViewControllerAnimated: YES];
            }
        }];
    }
    else {
        SportRecordEntity *sportRecordEntity = [[SportRecordEntity alloc] init];
        [sportRecordEntity setSid: @""];
        
//        [sportRecordEntity setDate: [Toos getThousandSecondStringFromDate:  [[NSDate date] initWithTimeIntervalSince1970: [[NSDate date] timeIntervalSince1970] - 86400 * 2]]];
        [sportRecordEntity setDate: [Toos getThousandSecondStringFromDate:  [NSDate date]]];
        [sportRecordEntity setSportType: self.sportType];
        [sportRecordEntity setHeartBeat: self.view.text_heart.text];
        [sportRecordEntity setSportTime: [NSString stringWithFormat: @"%.00f", [self.view.text_time.text floatValue] * 60]];
        [sportRecordEntity setCal: self.view.text_cal.text];
        [sportRecordEntity setBurnTime: [NSString stringWithFormat: @"%.00f", [self.view.text_calSection.text floatValue] * 60]];
        [sportRecordEntity setSportStep: self.view.text_stepNum.text];
        [self.iDelegate newRecordCreate];
        [sportRecordEntity saveToLocal];
        [self.navigationController popViewControllerAnimated: YES];
    }
}

- (void) sportTypeDidSelect: (NSInteger) row title: (NSString *) title {
    [self.view.text_category setText: title];
    [self setSportType: [NSString stringWithFormat: @"%zd", row]];
}

@end
