//
//  ViewController.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "ViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "DefindConfig.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setAutomaticallyAdjustsScrollViewInsets: NO];
    [self.navigationItem setBackBarButtonItem: [[UIBarButtonItem alloc] initWithTitle: @""
                                                                                style: UIBarButtonItemStylePlain
                                                                               target: nil
                                                                               action: nil]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(showLoading) name: kNSNotificationShowMBProgressHUD object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(hideLoading) name: kNSNotificationCloseMBProgressHUD object: nil];
}


- (void)viewWillDisappear:(BOOL)animated{
    [self hideLoading];
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: kNSNotificationShowMBProgressHUD object: nil];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: kNSNotificationCloseMBProgressHUD object: nil];
}

- (void) showLoading {
    if ( ! [MBProgressHUD HUDForView: self.view.window] )
        [[MBProgressHUD showHUDAddedTo: self.view animated: YES] show: YES];
}

- (void) hideLoading {
    MBProgressHUD *hud = [MBProgressHUD HUDForView: self.view];
    if ( hud )
        [hud hide: YES];
}

- (void) closeKeyboard {
    [self.view.window endEditing: YES];
}

@end
