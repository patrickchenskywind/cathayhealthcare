//
//  View.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "DefindConfig.h"

@implementation View

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self closeKeyboard];
    return YES;
}

- (void) closeKeyboard {
    [self.window endEditing: YES];
}

@end
