//
//  DineTypePickerViewController.h
//  
//
//  Created by LiminLin on 2015/9/21.
//
//

#import <UIKit/UIKit.h>

@protocol  IDineTypePickerViewController <NSObject>

@required
- (void) dineTypeDidSelect: (NSInteger) row title: (NSString *) title;

@end

@interface DineTypePickerViewController : UIViewController

@property (weak, nonatomic) id <IDineTypePickerViewController> iDelegate;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end

