//
//  DatePickerViewController.m
//  
//
//  Created by LiminLin on 2015/9/4.
//
//

#import "DatePickerViewController.h"

@interface DatePickerViewController ()
@end

@implementation DatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)datePickerViewValueChanged:(UIDatePicker *)sender {
    [self.iDelegate dateDidSelect: sender.date];
}

@end
