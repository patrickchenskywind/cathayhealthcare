//
//  BloodTypePickerViewController.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  IBloodTypePickerViewController <NSObject>

@required
    - (void) bloodTypeDidSelect:(NSInteger) row title: (NSString *) title;

@end

@interface BloodTypePickerViewController : UIViewController

@property (weak, nonatomic) id <IBloodTypePickerViewController> iDelegate;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end
