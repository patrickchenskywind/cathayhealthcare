//
//  DatePickerViewController.h
//  
//
//  Created by LiminLin on 2015/9/4.
//
//

#import <UIKit/UIKit.h>

@protocol IDatePickerViewController <NSObject>

@required
  - (void) dateDidSelect: (NSDate *) date;

@end

@interface DatePickerViewController : UIViewController

@property (weak, nonatomic) id <IDatePickerViewController> iDelegate;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
