//
//  DineTypePickerViewController.m
//  
//
//  Created by LiminLin on 2015/9/21.
//
//

#import "DineTypePickerViewController.h"

@interface DineTypePickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>
@end

@implementation DineTypePickerViewController

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 6;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * dineString = [NSString stringWithFormat: @"DINETYPE %zd", row];
    return  NSLocalizedString(dineString, nil);
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSString * dineString = [NSString stringWithFormat: @"DINETYPE %zd",row];
    [self.iDelegate dineTypeDidSelect: row title: NSLocalizedString(dineString, nil)];
}

@end
