//
//  WorkPickerViewController.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "WorkPickerViewController.h"

@interface WorkPickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation WorkPickerViewController

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * workString = [NSString stringWithFormat:@"WORKTYPE %zd",row];
    return  NSLocalizedString(workString, nil);
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSString * workString = [NSString stringWithFormat:@"WORKTYPE %zd",row];
    [self.iDelegate workTypeDidSelect: row title: NSLocalizedString(workString, nil)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
