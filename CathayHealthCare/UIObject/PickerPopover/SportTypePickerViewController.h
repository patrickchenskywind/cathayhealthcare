//
//  SportTypePickerViewController.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  ISportTypePickerViewController <NSObject>

@required
- (void) sportTypeDidSelect: (NSInteger) row title: (NSString *) title;

@end

@interface SportTypePickerViewController : UIViewController

@property (weak, nonatomic) id <ISportTypePickerViewController> iDelegate;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end

