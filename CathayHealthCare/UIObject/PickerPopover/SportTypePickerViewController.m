//
//  SportTypePickerViewController.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SportTypePickerViewController.h"

@interface SportTypePickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation SportTypePickerViewController

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 64;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * sportString = [NSString stringWithFormat: @"SPORTTYPE %zd", row + 67];
    return  NSLocalizedString(sportString, nil);
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSString * workString = [NSString stringWithFormat: @"SPORTTYPE %zd",row + 67];
    [self.iDelegate sportTypeDidSelect: row + 67 title: NSLocalizedString(workString, nil)];
}

@end
