//
//  WorkPickerViewController.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  IWorkPickerViewController <NSObject>

@required
- (void) workTypeDidSelect:(NSInteger) row title: (NSString *) title;

@end

@interface WorkPickerViewController : UIViewController

@property (weak, nonatomic) id <IWorkPickerViewController> iDelegate;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@end
