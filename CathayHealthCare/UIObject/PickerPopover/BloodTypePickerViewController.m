//
//  BloodTypePickerViewController.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "BloodTypePickerViewController.h"

@interface BloodTypePickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation BloodTypePickerViewController

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 5;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * workString = [NSString stringWithFormat: @"BLOODTYPE %zd",row];
    return  NSLocalizedString(workString, nil);
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSString * workString = [NSString stringWithFormat: @"BLOODTYPE %zd",row];
    [self.iDelegate bloodTypeDidSelect: row title: NSLocalizedString(workString, nil)];
}

@end
