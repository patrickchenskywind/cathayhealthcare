//
//  NutritionCell.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/17.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NutritionalViewDelegate <NSObject>
-(void)showDetailNutrition;
@end


@interface NutritionCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UIView    *view_color;
@property (nonatomic,strong) IBOutlet UILabel   *label_title;
@property (nonatomic,strong) IBOutlet UILabel    *label_weight;
@property (nonatomic,strong) IBOutlet UILabel    *label_percent;
@property (nonatomic,strong) IBOutlet UIButton   *button_pullDown;


@property (nonatomic,weak)id<NutritionalViewDelegate> delegate;

-(void)setColor:(UIColor *)color nutrition:(NSString *)nutrition weight:(NSString *)weight percent:(NSString *)percent showButton:(BOOL)showButton;
@end
