//
//  AddWaterCell.m
//  CathayHealthcare
//
//  Created by Skywind on 2015/7/19.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddWaterCell.h"

@implementation AddWaterCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCellWithName:(NSString *)name ml:(NSString *)ml{
    [_label_ml setText: ml];
    [_label_name setText: name];
}
@end
