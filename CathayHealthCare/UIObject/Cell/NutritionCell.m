//
//  NutritionCell.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/17.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "NutritionCell.h"

@implementation NutritionCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setColor:(UIColor *)color nutrition:(NSString *)nutrition weight:(NSString *)weight percent:(NSString *)percent showButton:(BOOL)showButton
{
    [_view_color setBackgroundColor:color];
    [_label_title setText:nutrition];
    [_label_weight setText:weight];
    [_label_percent setText:percent];
    _button_pullDown.hidden = !showButton;
    
}


-(IBAction)buttonPress:(id)sender
{
    [_delegate showDetailNutrition];
}

@end
