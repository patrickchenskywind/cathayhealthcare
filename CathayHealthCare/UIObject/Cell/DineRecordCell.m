//
//  DineRecordCell.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "DineRecordCell.h"
#import "UIColor+Hex.h"

@implementation DineRecordCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setDineCate:(NSString *)dineCate foodName:(NSString *)foodName number:(NSString *)number cal:(NSString *)cal isTitle:(BOOL)isTitle {
    _label_cate.text = dineCate;
    _label_foodName.text = foodName;
    _label_number.text = number;
    _label_cal.text = cal;
    [self setIsTitle:isTitle];
}

-(void)setIsTitle:(BOOL)isTitle {
    _isTitle = isTitle;
    
    if (_isTitle) {
        UIColor *titleColor = [UIColor colorWithHex: 0xED8700];
        [_label_cal setTextColor:titleColor];
        [_label_cate setTextColor:titleColor];
        [_label_foodName setTextColor:titleColor];
        [_label_number setTextColor:titleColor];
        _view_underLine.hidden = YES;
    }
    else{
        UIColor *textColor = [UIColor colorWithHex:0x917F72];
        [_label_cal setTextColor:textColor];
        [_label_cate setTextColor:textColor];
        [_label_foodName setTextColor:textColor];
        [_label_number setTextColor:textColor];
        _view_underLine.hidden = NO;

    }
}

@end
