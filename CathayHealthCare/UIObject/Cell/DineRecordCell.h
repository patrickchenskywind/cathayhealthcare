//
//  DineRecordCell.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DineRecordCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *label_cate;
@property (nonatomic,strong) IBOutlet UILabel *label_foodName;
@property (nonatomic,strong) IBOutlet UILabel *label_number;
@property (nonatomic,strong) IBOutlet UILabel *label_cal;
@property (nonatomic,strong) IBOutlet UIView  *view_underLine;

@property (nonatomic,assign) BOOL isTitle;

-(void)setDineCate:(NSString *)dineCate foodName:(NSString *)foodName number:(NSString *)number cal:(NSString *)cal isTitle:(BOOL)isTitle;

@end
