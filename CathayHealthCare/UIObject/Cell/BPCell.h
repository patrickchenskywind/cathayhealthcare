//
//  BPCell.h
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import <UIKit/UIKit.h>

@interface BPCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelSbp;
@property (weak, nonatomic) IBOutlet UILabel *labelDbp;
@property (weak, nonatomic) IBOutlet UILabel *labelHeartBeat;

@end
