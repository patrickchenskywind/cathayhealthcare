//
//  AddFoodSearchCell.m
//  CathayHealthcare
//
//  Created by Skywind on 2015/7/19.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddFoodSearchCell.h"
#import "UIColor+Hex.h"



@implementation AddFoodSearchCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [_button_favor.layer setCornerRadius: 5];
    [_button_favor.layer setBorderWidth: 1];
    [_button_favor.layer setBorderColor: [UIColor colorWithHex:0xED8700].CGColor];
}

-(void)setCellWithUnit:(NSString *)unit
              foodName:(NSString *)foodName
                 index:(NSInteger)index
                object:(id)object
               isFavor:(BOOL)isFavor
                isFood:(BOOL)isFood {
    [_label_resultName setText: foodName];
    [_label_unit setText: unit];
    [self setIndex: index];
    [self setObject: object];
    [self setIsFavor: isFavor];
    [self setIsFood: isFood];
    [self.button_favor setTitle: self.isFavor ? @" 取消最愛 ":@" 加入最愛 " forState:UIControlStateNormal];
}

- (IBAction)buttonFavorPress:(id)sender {
    [self.iDelegate favorButtonPressIndex: self.index object: self.object isFavor: self. isFavor isFood: self.isFood];
}


@end
