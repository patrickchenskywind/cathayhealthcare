//
//  CalRankCell.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalRankCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *label_rank;
@property (nonatomic,strong) IBOutlet UILabel *label_name;
@property (nonatomic,strong) IBOutlet UIView  *view_track;
@property (nonatomic,strong) IBOutlet UILabel *label_value;
@property (nonatomic,strong) UIView  *view_progress;

-(void)setRankCellWithRank:(int)rank name:(NSString *)name value:(int)value andMaxValue:(int)maxValue;

@end
