//
//  SportRecordCell.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/1.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SportRecordCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *label_cate;
@property (nonatomic,strong) IBOutlet UILabel *label_time;
@property (nonatomic,strong) IBOutlet UILabel *label_cal;

@end
