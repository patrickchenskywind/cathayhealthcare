//
//  CustomFoodCell.m
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/1.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "CustomFoodCell.h"
#import "UIColor+Hex.h"

@implementation CustomFoodCell



-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        [self initialObject];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setIsArrowHidden:(BOOL)isArrowHidden
{
    _isArrowHidden = isArrowHidden;
    _icon_arrow.hidden = isArrowHidden;
}

-(void)setIsOpen:(BOOL)isOpen
{
    if (_isOpen != isOpen) {
        _icon_arrow.transform = CGAffineTransformMakeRotation(M_PI);
        _isOpen = isOpen;
    }
}


-(void)layoutSubviews
{
    
    CGRect titleRect = CGRectMake(0,0,CGRectGetWidth(self.frame) * 0.5,44);
    CGRect textRect  = CGRectMake(CGRectGetMaxX(titleRect),(44-30)/2,CGRectGetWidth(self.frame) * 0.5 * 0.6,30);
    CGRect unitRect  = CGRectMake(CGRectGetMaxX(textRect),0,CGRectGetWidth(self.frame) * 0.5 * 0.4,44);
    CGRect iconRect  = CGRectMake(self.frame.size.width - 30, (44 - 30) *0.5, 30, 30);
    
    [_label_title setFrame:titleRect];
    [_label_unit  setFrame:unitRect];
    [_textField   setFrame:textRect];
    [_icon_arrow  setFrame:iconRect];
    
}

-(void)initialObject
{
    if (!_label_title) {
        _label_title = [[UILabel alloc] init];
        _label_title.textColor = [UIColor blackColor];
        [self addSubview:_label_title];
    }
    
    if (!_label_unit) {
        _label_unit = [[UILabel alloc] init];
        _label_unit.textColor = [UIColor blackColor];
        [self addSubview:_label_unit];
    }
    
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.textColor = [UIColor blackColor];
        
        _textField.layer.borderColor = [UIColor colorWithHex:0xCCB2A0].CGColor;
        _textField.layer.borderWidth = 1;
        _textField.layer.cornerRadius = 7;
        _textField.keyboardType = UIKeyboardTypeNumberPad;
        _textField.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_textField];
    }
    
    if (!_icon_arrow) {
        _icon_arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_down_brown"]];
        [self addSubview:_icon_arrow];
        _icon_arrow.hidden = YES;
    }
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}
@end
