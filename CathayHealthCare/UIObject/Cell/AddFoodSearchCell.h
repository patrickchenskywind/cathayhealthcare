//
//  AddFoodSearchCell.h
//  CathayHealthcare
//
//  Created by Skywind on 2015/7/19.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IAddFoodSearchCell <NSObject>

@required
    -(void)favorButtonPressIndex:(NSInteger)index
                          object:(id)object
                         isFavor:(BOOL)isFavor
                          isFood:(BOOL)isFood;

@end

@interface AddFoodSearchCell : UITableViewCell

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) id object;
@property (assign, nonatomic) BOOL isFavor;
@property (assign, nonatomic) BOOL isFood;
@property (weak, nonatomic)   id<IAddFoodSearchCell> iDelegate;

@property (nonatomic,strong) IBOutlet UILabel *label_unit;
@property (nonatomic,strong) IBOutlet UILabel *label_resultName;
@property (nonatomic,strong) IBOutlet UIButton *button_favor;

-(void)setCellWithUnit:(NSString *)unit
              foodName:(NSString *)foodName
                 index:(NSInteger)index
                object:(id)object
               isFavor:(BOOL)isFavor
                isFood:(BOOL)isFood;


@end
