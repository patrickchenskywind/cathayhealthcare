//
//  TelephoneCell.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "TelephoneCell.h"

@implementation TelephoneCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setUnit:(NSString *)unit phone:(NSString *)phone{
    [_label_phone setText:phone];
    [_label_unit setText:unit];
}

@end
