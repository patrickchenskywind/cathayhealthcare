//
//  SuggestCell.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/13.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SuggestCell.h"

@implementation SuggestCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCellWithTag:(int)cellTag andContent:(NSString *)content {
    /*
     基礎代謝率       = 11,
     每日建議攝取卡路里 = 12,
     飲水量           = 13,
     每週運動量        = 14,
     剩餘目標天數      = 15,
     今日消耗卡路里    = 16,
     
     全穀根莖類       = 21,
     豆魚肉蛋類       = 22,
     低脂乳品類       = 23,
     蔬菜類          = 24,
     水果類          = 25,
     油脂及堅果種子類  = 26,
     
     蛋白質           = 31,
     脂肪            = 32,
     碳水化合物       = 33
     */
    
    NSString *imageName;
    NSString *titleName;
    
    switch (cellTag) {
        case 基礎代謝率:
            imageName = @"icon_bmr";
            titleName = @"基礎代謝率";
            break;
        case 每日建議攝取卡路里:
            imageName = @"icon_dailycarolies";
            titleName = @"每日建議攝取卡路里";
            break;
        case 飲水量:
            imageName = @"icon_water";
            titleName = @"飲水量";
            break;
        case 每週運動量:
            imageName = @"icon_weeklysport";
            titleName = @"每週運動量";
            break;
        case 剩餘目標天數:
            imageName = @"icon_targetday";
            titleName = @"剩餘目標天數";
            break;
        case 今日消耗卡路里:
            imageName = @"icon_todayconsume";
            titleName = @"今日消耗卡路里";
            break;
        case 全穀根莖類:
            imageName = @"icon_toast";
            titleName = @"全穀根莖類";
            break;
        case 豆魚肉蛋類:
            imageName = @"icon_fish";
            titleName = @"豆魚肉蛋類";
            break;
        case 低脂乳品類:
            imageName = @"icon_milk";
            titleName = @"低脂乳品類";
            break;
        case 蔬菜類:
            imageName = @"icon_vege";
            titleName = @"蔬菜類";
            break;
        case 水果類:
            imageName = @"icon_fruit";
            titleName = @"水果類";
            break;
        case 油脂及堅果種子類:
            imageName = @"icon_peanut";
            titleName = @"油脂及堅果種子類";
            break;
        case 蛋白質:
            imageName = @"icon_egg";
            titleName = @"蛋白質";
            break;
        case 脂肪:
            imageName = @"icon_fat";
            titleName = @"脂肪";
            break;
        case 碳水化合物:
            imageName = @"icon_wheat";
            titleName = @"碳水化合物";
            break;
        default:
            break;
    }
    _imv_icon.image = [UIImage imageNamed:imageName];
    _label_title.text = titleName;
    _label_content.text = content;
}
@end
