//
//  RecordGeneralCell.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import <UIKit/UIKit.h>

@interface RecordGeneralCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leableLeft;
@property (weak, nonatomic) IBOutlet UILabel *leableRight;

@end
