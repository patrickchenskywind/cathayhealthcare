//
//  TelephoneCell.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TelephoneCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *label_unit;
@property (nonatomic,strong) IBOutlet UILabel *label_phone;

-(void)setUnit:(NSString *)unit phone:(NSString *)phone;
@end
