//
//  GroupMessageCell.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/30.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupMessageCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel    *label_groupName;
@property (nonatomic,strong) IBOutlet UILabel    *label_time;
@property (nonatomic,strong) IBOutlet UITextView *text_message;

@end
