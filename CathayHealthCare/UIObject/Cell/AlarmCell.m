//
//  AlarmCell.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AlarmCell.h"

@implementation AlarmCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setEventDesc:(NSString *)event isMark:(BOOL)isMark isAlarm:(BOOL)isAlarm
{
    [_view_mark setHidden:isMark];
    [_imv_alarm setHidden:isAlarm];
    [_label_event setText:event];

}

-(void)layoutSubviews
{
    [_view_mark.layer setCornerRadius:_view_mark.frame.size.height/2];
}

@end
