//
//  BSCell.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import <UIKit/UIKit.h>

@interface BSCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelBefore;
@property (weak, nonatomic) IBOutlet UILabel *labelAfter;
@property (weak, nonatomic) IBOutlet UILabel *labelHemoglobin;

@end
