//
//  CustomFoodCell.h
//  CathayHealthcare
//
//  Created by PatrickMac on 2015/10/1.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFoodCell : UITableViewCell

@property (nonatomic,strong) UILabel *label_title;
@property (nonatomic,strong) UILabel *label_unit;
@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) UIImageView *icon_arrow;

@property (nonatomic,assign) BOOL isOpen;
@property (nonatomic,assign) BOOL isArrowHidden;

@end
