//
//  AddWaterCell.h
//  CathayHealthcare
//
//  Created by Skywind on 2015/7/19.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddWaterCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel  *label_name;
@property (nonatomic,strong) IBOutlet UILabel  *label_ml;
@property (nonatomic,strong) IBOutlet UIButton *button_delete;

-(void)setCellWithName:(NSString *)name ml:(NSString *)ml;

@end
