//
//  AddFoodCell.h
//  CathayHealthcare
//
//  Created by Skywind on 2015/7/19.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddFoodCell : UITableViewCell


@property (nonatomic,strong) IBOutlet UILabel *label_food;
@property (nonatomic,strong) IBOutlet UILabel *label_count;
@property (nonatomic,strong) IBOutlet UILabel *label_cal;

-(void)setCellWithFood:(NSString *)food count:(NSString *)count cal:(NSString *)cal;

@end
