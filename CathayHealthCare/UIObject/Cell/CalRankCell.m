//
//  CalRankCell.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "CalRankCell.h"
#import "UIColor+Hex.h"

@implementation CalRankCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setRankCellWithRank:(int)rank name:(NSString *)name value:(int)value andMaxValue:(int)maxValue
{
    [_label_rank  setText:[NSString stringWithFormat:@"%d",rank]];
    [_label_name  setText:name];
    [_label_value setText:[NSString stringWithFormat:@"%d",value]];
    [_label_value setTextColor:[UIColor colorWithHex:0x727271]];
    
    float ratio = (float)value / (float)maxValue;
    
    if (!_view_progress) {
        _view_progress = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_view_track.frame)*ratio,CGRectGetHeight(_view_track.frame))];
        [_view_progress setBackgroundColor:[UIColor colorWithHex:0x9CD4CE]];
        [_view_track addSubview:_view_progress];
        [_view_track bringSubviewToFront:_label_value];
    }
}

@end
