//
//  SuggestCell.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/13.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

enum CELLTAG
{
    基礎代謝率       = 00,
    每日建議攝取卡路里 = 01,
    飲水量           = 02,
    每週運動量        = 03,
    剩餘目標天數      = 04,
    今日消耗卡路里    = 05,
    
    全穀根莖類       = 10,
    豆魚肉蛋類       = 11,
    低脂乳品類       = 12,
    蔬菜類          = 13,
    水果類          = 14,
    油脂及堅果種子類  = 15,
    
    蛋白質           = 20,
    脂肪            = 21,
    碳水化合物       = 22
};

@interface SuggestCell : UITableViewCell

@property (nonatomic,strong) IBOutlet  UIImageView *imv_icon;
@property (nonatomic,strong) IBOutlet  UILabel     *label_title;
@property (nonatomic,strong) IBOutlet  UILabel     *label_content;
@property (weak, nonatomic) IBOutlet UILabel *label_subContent;

-(void)setCellWithTag:(int)cellTag andContent:(NSString *)content;

@end
