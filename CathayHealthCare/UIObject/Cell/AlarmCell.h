//
//  AlarmCell.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIView        *view_mark;
@property (nonatomic,strong) IBOutlet UILabel       *label_event;
@property (nonatomic,strong) IBOutlet UIImageView   *imv_alarm;
-(void)setEventDesc:(NSString *)event isMark:(BOOL)isMark isAlarm:(BOOL)isAlarm;
@end
