//
//  AddFoodCell.m
//  CathayHealthcare
//
//  Created by Skywind on 2015/7/19.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "AddFoodCell.h"

@implementation AddFoodCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellWithFood:(NSString *)food count:(NSString *)count cal:(NSString *)cal
{
    [_label_food setText: food];
    [_label_count setText: count];
    [_label_cal setText: cal];
}
@end
