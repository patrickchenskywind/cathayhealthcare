//
//  UIBaseScrollView.m
//
//  Created by LiminLin on 2015/4/7.
//  Copyright (c) 2015年 Brocas. All rights reserved.
//

#import "UIBaseScrollView.h"
#import "SwitchButton.h"
#import "PullDownTextField.h"


@interface UIBaseScrollView ()<UIGestureRecognizerDelegate>

@end

@implementation UIBaseScrollView

- (instancetype) initWithCoder: (NSCoder *) coder{
    self = [super initWithCoder: coder];
    if ( self ) {
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object: nil];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object: nil];
    
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(handleTap:)];
        [gesture setNumberOfTouchesRequired: 1];
        [self addGestureRecognizer: gesture];
        [gesture setDelegate: self];
        [self setKeyboardDismissMode: UIScrollViewKeyboardDismissModeInteractive];
    }
    return self;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

#pragma mark - Notification
- (void) keyboardWillShow: (NSNotification *) notification {
    [self.constraintBottom setConstant: [[[notification userInfo] objectForKey: UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height];
    [self.superview layoutIfNeeded];
}

- (void) keyboardWillHide: (NSNotification *) notification {
    [self.constraintBottom setConstant: 0];
    [self.superview layoutIfNeeded];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ( [touch.view isKindOfClass: [UIButton class]] || [touch.view isKindOfClass: [SwitchButton class]] ) {
        return NO;
    };
    return YES;
//    for(UIView *view in gestureRecognizer.view.subviews){
//        if ([view isKindOfClass:[UIButton class]]) {
//            return NO;
//        }
//    }
//    return YES;
}


#pragma mark - UIGestureRecognizer
- (void) handleTap: (UIGestureRecognizer *) gesture {
    if (self.iDelegate && [self.iDelegate respondsToSelector:@selector(baseScrollDidPress)]){
        [self.iDelegate baseScrollDidPress];
    }
    if ( self.constraintBottom && self.constraintBottom.constant > 0 )
        [self endEditing: YES];
}

@end
