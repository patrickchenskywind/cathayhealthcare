//
//  NutritionDoseSetView.m
//  CathayHealthcare
//
//  Created by Patrick on 2015/10/6.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import "NutritionDoseSetView.h"

@implementation NutritionDoseSetView

-(void)awakeFromNib{
    [_text_dose setEnabled:NO];
    [_text_time setEnabled:NO];
}


-(IBAction)doseButtonPress:(UIButton *)sender{
    [_delegate showDoseListWithNutritionDoseSetView:self];
}

-(IBAction)timeButtonPress:(UIButton *)sender{
    [_delegate showTimeListWithNutritionDoseSetView:self];
}

-(IBAction)okButtonPress:(UIButton *)sender{
    [_delegate okButtonPressWithNutritionDoseSetView:self];
}

-(IBAction)cancelButtonPress:(UIButton *)sender{
    [_delegate cancelButtonPressWithNutritionDoseSetView:self];
}
@end
