//
//  CircleImageView.h
//  CathayHealthcare
//
//  Created by Daniel on 2015/7/16.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface CircleImageView : UIView

@property (nonatomic) IBInspectable UIColor *circleColor;

@end
