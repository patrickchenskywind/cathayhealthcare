//
//  CircleImageView.m
//  CathayHealthcare
//
//  Created by Daniel on 2015/7/16.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "CircleImageView.h"

@implementation CircleImageView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat radius = CGRectGetWidth(rect) > CGRectGetHeight(rect) ? CGRectGetWidth(rect) : CGRectGetHeight(rect);
    radius /= 2;
    CGContextAddArc(context, CGRectGetMidX(rect), CGRectGetMidY(rect), radius, 0, M_PI * 2, 1);
    CGContextSetFillColorWithColor(context, self.circleColor.CGColor);
    CGContextFillPath(context);
}


@end
