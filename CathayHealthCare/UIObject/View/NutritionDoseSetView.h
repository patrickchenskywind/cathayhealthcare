//
//  NutritionDoseSetView.h
//  CathayHealthcare
//
//  Created by Patrick on 2015/10/6.
//  Copyright © 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NourishmentListEntity.h"

@class NutritionDoseSetView;
@protocol NutritionDoseSetViewDelegate <NSObject>

-(void)okButtonPressWithNutritionDoseSetView:(NutritionDoseSetView *)view;
-(void)cancelButtonPressWithNutritionDoseSetView:(NutritionDoseSetView *)view;
-(void)showDoseListWithNutritionDoseSetView:(NutritionDoseSetView *)view;
-(void)showTimeListWithNutritionDoseSetView:(NutritionDoseSetView *)view;

@end

@interface NutritionDoseSetView : UIView

@property (nonatomic,strong) IBOutlet UITextField *text_dose;
@property (nonatomic,strong) IBOutlet UITextField *text_time;
@property (nonatomic,strong) NourishmentListEntity *nourishment;
@property (nonatomic,weak  ) id<NutritionDoseSetViewDelegate> delegate;

@end
