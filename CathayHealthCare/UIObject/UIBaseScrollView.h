//
//  UIBaseScrollView.h
//
//  Created by LiminLin on 2015/4/7.
//  Copyright (c) 2015年 Brocas. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBaseScrollView <NSObject>
@optional
    - (void) baseScrollDidPress;

@end

@interface UIBaseScrollView : UIScrollView
@property (weak, nonatomic) IBOutlet id <IBaseScrollView> iDelegate;
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;

@end
