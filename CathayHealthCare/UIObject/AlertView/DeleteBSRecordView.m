//
//  DeleteBSRecordView.m
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "DeleteBSRecordView.h"

@implementation DeleteBSRecordView

-(void)setDelegate:(id<DeleteBSRecordViewDelegate>)delegate {
    _delegate = delegate;
    UIView *vc = (UIView *)delegate;
    
    if (!_maskView) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonPress:)];
        
        _maskView = [[UIView alloc] initWithFrame:vc.bounds];
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0.5;
        [_maskView addGestureRecognizer:tap];
        [vc addSubview:_maskView];
    }
}

-(IBAction)buttonPress:(UIButton *)button {
    [_delegate dismissDeleteBSRecordView];
}

- (IBAction)buttonDeletePress:(id)sender {
    [self.delegate deleteBSDateButtonPress: self.view];
}

@end
