//
//  PremissionAlertView.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "PremissionAlertView.h"

@interface PremissionAlertView ()

@property (nonatomic, strong) UIView *view_mask;

@end

@implementation PremissionAlertView

- (void) setIDelegate:(id<IPremissionAlertView>)iDelegate {
    _iDelegate = iDelegate ;
    if (!self.view_mask) {
        self.view_mask = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                              0,
                                                              [UIScreen mainScreen].bounds.size.width,
                                                              [UIScreen mainScreen].bounds.size.height)];
    }
    
    [self.view_mask setBackgroundColor:[UIColor blackColor]];
    [self.view_mask setAlpha:0.5];
    [self.view_mask addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonConfirmPress:)]];
    [((UIViewController *)_iDelegate).view addSubview: self.view_mask];

}

- (IBAction)buttonConfirmPress:(id)sender {
    [self.view_mask removeFromSuperview];
    [self.iDelegate buttonConfirmPress];
}

@end
