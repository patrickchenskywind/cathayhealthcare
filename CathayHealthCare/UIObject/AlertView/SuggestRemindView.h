//
//  SuggestRemindView.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol SuggestRemindViewDelegate <NSObject>

-(void)dismissSuggestRemindView;

@end

@interface SuggestRemindView : View
@property (nonatomic,weak)IBOutlet id<SuggestRemindViewDelegate> delegate;

@end
