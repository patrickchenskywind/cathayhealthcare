//
//  PremissionAlertView.h
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/7.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol IPremissionAlertView <NSObject>

@required
- (void) buttonConfirmPress;

@end

@interface PremissionAlertView : View

@property (weak, nonatomic) id<IPremissionAlertView> iDelegate;

@end
