//
//  DeleteBSRecordView.h
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import <UIKit/UIKit.h>

@protocol DeleteBSRecordViewDelegate <NSObject>
@required
- (void) dismissDeleteBSRecordView;
- (void) deleteBSDateButtonPress:(id) view;

@end

@interface DeleteBSRecordView : UIView

@property (nonatomic,strong) UIView *maskView;
@property (nonatomic,weak)id<DeleteBSRecordViewDelegate> delegate;
@property (nonatomic, strong) id view;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelBefore;
@property (weak, nonatomic) IBOutlet UILabel *labelAfter;
@property (weak, nonatomic) IBOutlet UILabel *labelHemoglobin;

@end

