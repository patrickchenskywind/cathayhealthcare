//
//  DeleteRecordView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/13.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DeleteRecordViewDelegate <NSObject>
@required
    - (void) dismissDeleteRecordView;
    - (void) deleteDateButtonPress:(id) view;
@end

@interface DeleteRecordView : UIView

@property (weak, nonatomic) IBOutlet UILabel *labelLeft;
@property (weak, nonatomic) IBOutlet UILabel *labelRight;

@property (nonatomic,strong) UIView *maskView;
@property (nonatomic,weak)id<DeleteRecordViewDelegate> delegate;

@property (nonatomic, strong) id view;

@end
