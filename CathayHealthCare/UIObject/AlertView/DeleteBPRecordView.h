//
//  DeleteBPRecordView.h
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import <UIKit/UIKit.h>


@protocol DeleteBPRecordViewDelegate <NSObject>
@required
    - (void) dismissDeleteBPRecordView;
    - (void) deleteBPDateButtonPress:(id) view;
@end

@interface DeleteBPRecordView : UIView

@property (nonatomic,strong) UIView *maskView;
@property (nonatomic,weak)id<DeleteBPRecordViewDelegate> delegate;
@property (nonatomic, strong) id view;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelSbp;
@property (weak, nonatomic) IBOutlet UILabel *labelDbp;
@property (weak, nonatomic) IBOutlet UILabel *labelHeartbeat;


@end
