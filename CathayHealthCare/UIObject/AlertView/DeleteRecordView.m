//
//  DeleteRecordView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/13.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "DeleteRecordView.h"

@implementation DeleteRecordView

-(void)setDelegate:(id<DeleteRecordViewDelegate>)delegate {
    _delegate = delegate;
    UIView *vc = (UIView *)delegate;
    
    if (!_maskView) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonPress:)];
        
        _maskView = [[UIView alloc] initWithFrame:vc.bounds];
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0.5;
        [_maskView addGestureRecognizer:tap];
        [vc addSubview:_maskView];
    }
}

-(IBAction)buttonPress:(UIButton *)button {
    [_delegate dismissDeleteRecordView];
}

- (IBAction)buttonDeletePress:(id)sender {
    [self.delegate deleteDateButtonPress: self.view];
}

@end
