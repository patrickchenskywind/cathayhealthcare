//
//  LoginView.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "LoginView.h"
#import "LoginAPI.h"
#import "Toos.h"

@implementation LoginView

-(void)setDelegate:(id<LoginViewDelegate>)delegate {
    _delegate = delegate;
    
    if (!self.view_mask) {
        [self setView_mask: [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                    0,
                                                                    [UIScreen mainScreen].bounds.size.width,
                                                                     [UIScreen mainScreen].bounds.size.height)]];
    }
    
    [self.view_mask setBackgroundColor: [UIColor blackColor]];
    [self.view_mask setAlpha: 0.5];
    [self.view_mask addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(cancelButtonPress:)]];
    [((UIViewController *)self.delegate).view addSubview: self.view_mask];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self.view_input.layer setCornerRadius: 5];
    [self.view_input.layer setBorderWidth: 1];
    [self.view_input.layer setBorderColor: [UIColor colorWithHex: 0x00A29A].CGColor];
    [_textField_id setTextAlignment: NSTextAlignmentCenter];
    [_textField_pw setTextAlignment: NSTextAlignmentCenter];
}

-(IBAction)registerButtonPress:(id)sender {
    [self.delegate showRegistView];
}
-(IBAction)loginButtonPress:(id)sender{
//    if ( (self.textField_id.text && self.textField_id.text.length > 0) && self.textField_pw.text && self.textField_pw.text.length > 0 ) {
//        LoginAPI *api = [[LoginAPI alloc] init];
//        [api setAccount: self.textField_id.text];
//        [api setPassword: self.textField_pw.text];
//        [api setDeviceType: @"0"];
//        [api setPushToken: [[NSUserDefaults standardUserDefaults] objectForKey: DEVICE_TOKEN]];
//        
//        [api post:^(HttpResponse *response) {
//            LoginEntity *loginEntity = response.result.firstObject;
//            if ([Toos checkMsgCodeWihtMsg: loginEntity.msg]) {
//                [[NSUserDefaults standardUserDefaults] setObject: loginEntity.accessToken forKey: ACCESSTOKEN];
//                [[NSUserDefaults standardUserDefaults] synchronize];
//                [loginEntity saveToLocal];
//#warning 需確認哪些本地端資料需要刪除
//                
//                [self.delegate dissMissLoginView];
//            }
//        }];
//    }
    
#warning 使用假資料
        LoginAPI *api = [[LoginAPI alloc] init];
        
        [api setAccount: @"skyding@skywind.com.tw"];
        [api setPassword: @"000000"];
        [api setDeviceType: @"1"];
        [api setPushToken: @"b62afd4d85634e699a3f1b73770ffc02"];
        
        [api post:^(HttpResponse *response) {
            LoginEntity *loginEntity = response.result.firstObject;
            if ([Toos checkMsgCodeWihtMsg: loginEntity.msg]) {
                [[NSUserDefaults standardUserDefaults] setObject: loginEntity.accessToken forKey: ACCESSTOKEN];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [loginEntity saveToLocal];
                [self.delegate dissMissLoginView];
            }
        }];
}

-(IBAction)forgetPasswordButtonPress:(id)sender{
    [self.delegate forgetPasswordButtonPress];
}

-(IBAction)cancelButtonPress:(id)sender{
    [self.delegate dissMissLoginView];
}

@end
