//
//  SuggestRemindView.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SuggestRemindView.h"

@implementation SuggestRemindView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(IBAction)ButtonPress:(UIButton *)button {
    [_delegate dismissSuggestRemindView];
}

@end
