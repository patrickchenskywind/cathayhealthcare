//
//  LoginView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/15.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"
#import "UIColor+Hex.h"

@protocol LoginViewDelegate <NSObject>
@required
    - (void) dissMissLoginView;
    - (void) showRegistView;
    - (void) forgetPasswordButtonPress;

@end

@interface LoginView : View

@property (nonatomic,strong)IBOutlet UITextField *textField_id;
@property (nonatomic,strong)IBOutlet UITextField *textField_pw;

@property (nonatomic,strong)IBOutlet UIButton    *button_regist;
@property (nonatomic,strong)IBOutlet UIButton    *button_login;
@property (nonatomic,strong)IBOutlet UIButton    *button_forget_pw;
@property (nonatomic,strong)IBOutlet UIButton    *button_cancel;

@property (nonatomic,strong)IBOutlet UIView      *view_input;
@property (nonatomic,strong)         UIView      *view_mask;


@property (nonatomic,weak)id<LoginViewDelegate> delegate;

@end
