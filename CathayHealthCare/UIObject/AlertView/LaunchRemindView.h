//
//  LaunchRemindView.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/8.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol LaunchRemindViewDelegate <NSObject>

-(void)disMissLaunchRemindView;

@end
@interface LaunchRemindView : View
@property (nonatomic,weak)IBOutlet id<LaunchRemindViewDelegate> delegate;

@end
