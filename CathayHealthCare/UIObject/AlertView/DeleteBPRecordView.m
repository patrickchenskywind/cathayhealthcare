//
//  DeleteBPRecordView.m
//  
//
//  Created by LiminLin on 2015/10/7.
//
//

#import "DeleteBPRecordView.h"

@implementation DeleteBPRecordView
-(void)setDelegate:(id<DeleteBPRecordViewDelegate>)delegate {
    _delegate = delegate;
    UIView *vc = (UIView *)delegate;
    
    if (!_maskView) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonPress:)];
        
        _maskView = [[UIView alloc] initWithFrame:vc.bounds];
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0.5;
        [_maskView addGestureRecognizer:tap];
        [vc addSubview:_maskView];
    }
}

-(IBAction)buttonPress:(UIButton *)button {
    [_delegate dismissDeleteBPRecordView];
}

- (IBAction)buttonDeletePress:(id)sender {
    [self.delegate deleteBPDateButtonPress: self.view];
}

@end






