//
//  SwitchButton.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "View.h"

@protocol ISwitchButton <NSObject>
@optional
    - (void) iSwitchButtonDidSwith: (BOOL) isOpen;

@end

@interface SwitchButton : View

@property (nonatomic, weak) id<ISwitchButton> iDelegate;

@property (nonatomic,strong) UIView   *switcher;
@property (nonatomic,strong) UILabel  *label_name;
@property (nonatomic,strong) NSString *on_name;
@property (nonatomic,strong) NSString *off_name;
@property (nonatomic,assign) BOOL isInitialSetting;
@property (nonatomic,assign) BOOL isOpen;

-(void)setOnName:(NSString *)onTitle
         offName:(NSString *)offTitle
           color:(UIColor *)color
       nameColor:(UIColor *)nameColor
          isOpen:(BOOL)isOpen;
@end
