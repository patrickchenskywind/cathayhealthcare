//
//  SwitchButton.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/3.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "SwitchButton.h"

@implementation SwitchButton


-(void)setOnName:(NSString *)onTitle
         offName:(NSString *)offTitle
           color:(UIColor *)color
       nameColor:(UIColor *)nameColor
          isOpen:(BOOL)isOpen {
    [self initial];
    
    _on_name  = onTitle;
    _off_name = offTitle;
    _isInitialSetting = YES;
    
    self.layer.borderColor      = [color CGColor];
    _switcher.layer.borderColor = [color CGColor];
    _switcher.backgroundColor   = color;
    _label_name.textColor       = nameColor;
    
    [self setIsOpen:isOpen];
}


-(void)initial
{
    self.layer.cornerRadius = 5;
    self.layer.borderColor = [[UIColor greenColor] CGColor];
    self.layer.borderWidth = 1;
    
    
    if (!_switcher) {
        _switcher = [[UIView alloc] init];
    }
    [_switcher setFrame:CGRectMake(0,
                                   0,
                                   self.bounds.size.width/2,
                                   self.bounds.size.height)];
    
    _switcher.layer.cornerRadius = 5;
    _switcher.layer.borderColor  = [[UIColor greenColor] CGColor];
    _switcher.layer.borderWidth  = 1;
    _switcher.backgroundColor    = [UIColor greenColor];
    
    
    if (!_label_name) {
        _label_name = [[UILabel alloc] init];
    }
    [_label_name setFrame:CGRectMake(0,
                                     0,
                                     _switcher.frame.size.width,
                                     _switcher.frame.size.height)];
    
    [_label_name setTextAlignment:NSTextAlignmentCenter];
    [_label_name setTextColor:[UIColor whiteColor]];
    [_label_name setFont:[UIFont systemFontOfSize:25]];
    [_switcher addSubview:_label_name];
    
    [self addSubview:_switcher];
}

-(void)setIsOpen:(BOOL)isOpen
{
    _isOpen = isOpen;
    
    
    float animateTime;
    if (_isInitialSetting) {
        animateTime = 0.0;
        _isInitialSetting = NO;
    }
    else{
        animateTime = 0.2;
    }
    
    
    if (_isOpen) {
        self.userInteractionEnabled = NO;
        
        [UIView animateWithDuration:animateTime
                         animations:^{
                             [_switcher setFrame:CGRectMake(CGRectGetMidX(self.bounds),
                                                            CGRectGetMinY(self.bounds),
                                                            CGRectGetWidth(_switcher.bounds),
                                                            CGRectGetHeight(_switcher.bounds))];
                         }
                         completion:^(BOOL finished) {
                             _isOpen = YES;
                             [self setUserInteractionEnabled:YES];
                             [_label_name setText:_on_name];
                         }];
    }
    else{
        self.userInteractionEnabled = NO;
        [UIView animateWithDuration:animateTime
                         animations:^{
                             [_switcher setFrame:CGRectMake(CGRectGetMinX(self.bounds),
                                                            CGRectGetMinY(self.bounds),
                                                            CGRectGetWidth(_switcher.bounds),
                                                            CGRectGetHeight(_switcher.bounds))];}
                         completion:^(BOOL finished) {
                             _isOpen = NO;
                             self.userInteractionEnabled = YES;
                             [_label_name setText:_off_name];
                            
                         }];
    }
    
}




-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_isOpen) {
        [self setIsOpen:NO];
    }
    else{
        [self setIsOpen:YES];
    }
    [self.iDelegate iSwitchButtonDidSwith: self.isOpen];
}

-(void)layoutSubviews {
    [_switcher setFrame:CGRectMake(_switcher.frame.origin.x,
                                   _switcher.frame.origin.y,
                                   self.bounds.size.width/2,
                                   self.bounds.size.height)];
    
    [_label_name setFrame:CGRectMake(0,
                                     0,
                                     _switcher.frame.size.width,
                                     _switcher.frame.size.height)];
}


@end
