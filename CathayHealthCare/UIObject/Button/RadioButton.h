//
//  RadioButton.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RadioButton : UIButton

@property (nonatomic,assign) BOOL isSelected;
@property (nonatomic,strong) UIImage *yesStyle;
@property (nonatomic,strong) UIImage *noStyle;

-(void)setYesStyle:(UIImage *)yesStyle
        andNoStyle:(UIImage *)noStyle
          isSelect:(BOOL)isSelect;

@end
