//
//  InBoxButton.h
//  CathayHealthCare
//
//  Created by Orange on 2015/6/16.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InBoxButton : UIButton

@property (nonatomic,strong) UIView *bg_count;
@property (nonatomic,strong) UILabel *label_count;
@end
