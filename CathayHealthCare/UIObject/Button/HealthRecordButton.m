//
//  HealthRecordButton.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "HealthRecordButton.h"
#import "UIColor+Hex.h"

@implementation HealthRecordButton

-(void)layoutSubviews {
    [super layoutSubviews];
    float buttonWidth = CGRectGetWidth(self.bounds);
    float buttonHeight = CGRectGetHeight(self.bounds);
    [self.imageView setFrame: CGRectMake(buttonWidth/10, 0, buttonHeight /2, buttonHeight/ 2)];
    [self.imageView setCenter: CGPointMake(self.imageView.center.x, CGRectGetHeight(self.bounds)/2)];
    [self.imageView setImage: self.currentImage ];
    [self.titleLabel setFrame:CGRectMake(CGRectGetMaxX(self.imageView.frame),
                                         0,
                                         CGRectGetWidth(self.bounds)-CGRectGetMaxX(self.imageView.frame),
                                         buttonHeight)];
    [self.titleLabel setLineBreakMode: NSLineBreakByWordWrapping];
    [self.titleLabel setNumberOfLines: 2];
    
    [self setTitleColor: [UIColor colorWithHex:0x917F72] forState: UIControlStateNormal];
    [self setTitleColor: [UIColor colorWithHex:0x00A29A] forState: UIControlStateSelected];

}


//-(void)setIsSelected:(BOOL)isSelected {
//    _isSelected = isSelected;
//    
//    if (_isSelected) {
//        [_imv_icon setImage:_image_selected];
//        [_label_title setTextColor:[UIColor colorWithHex:0x00A29A]];
//        [_imv_bg setImage:_image_bg];
//    }
//    else{
//        [_imv_icon setImage:_image_nomal];
//        [_label_title setTextColor:[UIColor colorWithHex:0x917F72]];
//        [_imv_bg setImage:nil];
//    }
//}


//-(void)setFrame:(CGRect)frame buttonNum:(int)buttonNUM selected:(BOOL)selected {
//    [super setFrame:frame];
//    [self setButtonNUM:buttonNUM];
//    
//    if (!_imv_bg) {
//        _imv_bg = [[UIImageView alloc] init];
//        [self addSubview:_imv_bg];
//    }
//    
//    if (!_imv_icon) {
//        _imv_icon = [[UIImageView alloc] init];
//        [self addSubview:_imv_icon];
//    }
//    
//    if (!_label_title) {
//        _label_title = [[UILabel alloc] init];
//        _label_title.lineBreakMode = NSLineBreakByWordWrapping;
//        _label_title.numberOfLines = 2;
//        _label_title.text = _buttonTitle;
//        [self addSubview:_label_title];
//    }
//    [self setIsSelected:selected];
//}

//-(void)setButtonNUM:(int)buttonNUM {
//    _buttonNUM = buttonNUM;
//    
//    
//    NSArray *imageName_normal = @[@"icon_weight_normal",
//                                  @"icon_bodyfat_normal",
//                                  @"icon_waist_normal",
//                                  @"icon_heartbeat_normal",
//                                  @"icon_sugar_normal",
//                                  @"icon_tg_normal",
//                                  @"icon_hdl_normal",
//                                  @"icon_sleep_normal",
//                                  @"icon_health_normal"];
//    
//    NSArray *imageName_selected = @[@"icon_weight_blue",
//                                    @"icon_bodyfat_blue",
//                                    @"icon_waist_blue",
//                                    @"icon_heartbeat_blue",
//                                    @"icon_sugar_blue",
//                                    @"icon_tg_blue",
//                                    @"icon_hdl_blue",
//                                    @"icon_sleep_blue",
//                                    @"icon_health_blue"];
//    
//    NSArray *titleName = @[@"體重",
//                           @"體脂",
//                           @"腰圍",
//                           @"血壓/脈搏",
//                           @"血糖",
//                           @"三酸甘油脂",
//                           @"高密度脂蛋白",
//                           @"睡眠",
//                           @"健康狀況"];
//    
//    NSString *normalName    = [imageName_normal objectAtIndex:_buttonNUM-1];
//    NSString *selectedName  = [imageName_selected objectAtIndex:_buttonNUM-1];
//    NSString *bgName        = @"bg_healthrecord";
//    
//    _image_nomal            = [UIImage imageNamed: normalName];
//    _image_selected         = [UIImage imageNamed: selectedName];
//    _image_bg               = [UIImage imageNamed: bgName];
//    _buttonTitle            = [titleName objectAtIndex: _buttonNUM-1];
//    
    
//}



//-(void)layoutSubviews {
//    float buttonWidth = CGRectGetWidth(self.bounds);
//    float buttonHeight = CGRectGetHeight(self.bounds);
//
//    [_imv_icon setFrame:CGRectMake(buttonWidth/10, 0, buttonHeight /2, buttonHeight/ 2)];
//    [_imv_icon setCenter:CGPointMake(_imv_icon.center.x, CGRectGetHeight(self.bounds)/2)];
//    [_imv_bg setFrame:self.bounds];
//    [_label_title setFrame:CGRectMake(CGRectGetMaxX(_imv_icon.frame),
//                                      0,
//                                      CGRectGetWidth(self.bounds)-CGRectGetMaxX(_imv_icon.frame),
//                                      buttonHeight)];
//}

@end
