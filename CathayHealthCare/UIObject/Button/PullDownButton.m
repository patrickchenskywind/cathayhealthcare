//
//  PullDownButton.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/13.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "PullDownButton.h"

@implementation PullDownButton
- (void) drawRect:(CGRect)rect {
    [super drawRect:rect];
    if (!_imv_arrow) {
        _imv_arrow = [[UIImageView alloc] init];
        [_imv_arrow setFrame: CGRectMake(CGRectGetWidth(rect) - CGRectGetHeight(rect), 0, CGRectGetHeight(rect), CGRectGetHeight(rect))];
        UIImage *image = [UIImage imageNamed: @"arrow_down_white"];
        [_imv_arrow setImage: image];
        [self addSubview:_imv_arrow];
    }
}

-(void)setIsPullDown: (BOOL) isPullDown {
    _isPullDown = isPullDown;
    if (_isPullDown)
        [_imv_arrow setImage: [UIImage imageNamed: @"arrow_up_white"]];
    else
        [_imv_arrow setImage: [UIImage imageNamed: @"arrow_down_white"]];
}

@end
