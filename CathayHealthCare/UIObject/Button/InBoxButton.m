//
//  InBoxButton.m
//  CathayHealthCare
//
//  Created by Orange on 2015/6/16.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "InBoxButton.h"

@implementation InBoxButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)layoutSubviews
{
    if (!_bg_count){
        _bg_count = [[UIView alloc] init];
        [_bg_count setBackgroundColor:[UIColor redColor]];
        [self addSubview:_bg_count];
    }
    
    if (!_label_count){
        _label_count = [[UILabel alloc] init];
        [_label_count setTextAlignment:NSTextAlignmentCenter];
        [_label_count setText:@"1"];
        [_bg_count addSubview:_label_count];
    }
    
    [_bg_count setFrame:CGRectMake(self.frame.size.width/2,
                                   0,
                                   self.frame.size.width/2,
                                   self.frame.size.height/2)];
    
    [_bg_count.layer setCornerRadius:self.frame.size.width/2];
    [_label_count setFrame:CGRectMake(0,
                                      0,
                                      _bg_count.frame.size.width,
                                      _bg_count.frame.size.height)];
    
    
    
}
@end
