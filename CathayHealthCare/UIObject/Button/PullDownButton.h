//
//  PullDownButton.h
//  CathayHealthCare
//
//  Created by Orange on 2015/7/13.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PullDownButton : UIButton

@property (nonatomic,assign) BOOL isPullDown;
@property (nonatomic,strong) UIImageView *imv_arrow;

@end
