//
//  RadioButton.m
//  CathayHealthCare
//
//  Created by Orange on 2015/7/9.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RadioButton.h"

@implementation RadioButton

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
        _isSelected = NO;
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addTarget:self action:@selector(buttonPress:) forControlEvents:UIControlEventTouchUpInside];
        _isSelected = NO;
    }
    return self;
}

-(void)setYesStyle:(UIImage *)yesStyle
        andNoStyle:(UIImage *)noStyle
          isSelect:(BOOL)isSelect {
    _yesStyle = yesStyle;
    _noStyle = noStyle;
    
    [self setIsSelected:isSelect];
}

-(void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    if (_isSelected)
        [self setBackgroundImage: _yesStyle forState: UIControlStateNormal];
    else
        [self setBackgroundImage: _noStyle forState: UIControlStateNormal];
}

-(void)buttonPress:(UIButton *)button {
        [self setIsSelected:! _isSelected];
}

//-(void)layoutSubviews
//{
//    self.layer.cornerRadius = CGRectGetHeight(self.frame)/2;
//    self.layer.borderColor = [[UIColor brownColor] CGColor];
//    self.layer.borderWidth = 1;
//}
//
@end
