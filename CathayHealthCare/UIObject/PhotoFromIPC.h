//
//  PhotoFromIPC.h
//  DVBasketballFinder
//
//  Created by Encore on 2014/11/11.
//  Copyright (c) 2014年 Encore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol PhotoFromIPCDelegate <NSObject>

@optional
    - (void)isCancel;

@required
    - (void)getPhoto:(UIImage *)image;

@end


@interface PhotoFromIPC : NSObject

@property (weak, nonatomic) IBOutlet id<PhotoFromIPCDelegate> ipcDelegate;
@property (weak, nonatomic) IBOutlet UIViewController *VC;
@property (strong, nonatomic) UIImagePickerController *imagePickerController;

- (instancetype) initWithDelegate: (id<PhotoFromIPCDelegate>) ipcDelegate;
- (instancetype) initWithDelegate: (id<PhotoFromIPCDelegate>) ipcDelegate viewController: (id)viewController;
- (IBAction) show:(id)sender;

@end
