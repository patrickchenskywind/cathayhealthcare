//
//  PullDownTextField.h
//  MyObject
//
//  Created by Patrick on 2015/7/5.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PullDownTextFieldDelegate <NSObject>

@optional
-(void)showPullDownOption;

@end

@interface PullDownTextField : UIView<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) UITextField *text;
@property (nonatomic,strong) UIButton    *button;
@property (nonatomic,strong) NSArray     *content;
@property (nonatomic,strong) UITableView *table_content;
@property (nonatomic,strong) UIView      *maskView;
@property (nonatomic,weak)   id<PullDownTextFieldDelegate> delegate;

-(void)initialWithpullDownContent:(NSArray *)content
                      borderColor:(UIColor *)color;

-(void)removeTableView;

@end
