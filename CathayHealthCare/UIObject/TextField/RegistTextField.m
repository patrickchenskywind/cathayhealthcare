//
//  RegistTextField.m
//  CathayHealthcare
//
//  Created by LiminLin on 2015/9/14.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "RegistTextField.h"
#include "UIColor+Hex.h"

@implementation RegistTextField

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self.layer setBorderColor: [[UIColor colorWithHex: 0xCCB2A0] CGColor]];
        [self.layer setBorderWidth: 1];
        [self.layer setCornerRadius: 4];
        [self setLeftView: [[UIView alloc] initWithFrame: CGRectMake( 0, 0, 8, self.bounds.size.height)]];
        [self setLeftViewMode: UITextFieldViewModeAlways];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
