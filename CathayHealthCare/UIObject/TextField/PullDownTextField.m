//
//  PullDownTextField.m
//  MyObject
//
//  Created by Patrick on 2015/7/5.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "PullDownTextField.h"

@implementation PullDownTextField


-(void)initialWithpullDownContent:(NSArray *)content
                      borderColor:(UIColor *)color
{
    float textX,textY,textWidth,textHeight,buttonX,buttonY,buttonWidth,buttonHeight;
    
    self.layer.borderWidth = 1;
    self.layer.borderColor = [color CGColor];
    self.layer.cornerRadius = 5;
    
    if (!self.text) {
        self.text = [[UITextField alloc] init];
        self.text.font = [UIFont systemFontOfSize:17];
        [self addSubview:self.text];
    }
    
    if (content)
    {
        _content = content;
        
        if (!_button) {
            _button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        }
        
        textX = 14;
        textY = 0;
        textWidth = CGRectGetWidth(self.bounds) - CGRectGetHeight(self.bounds)-28;
        textHeight = CGRectGetHeight(self.bounds);
        buttonX = CGRectGetWidth(self.bounds) - CGRectGetHeight(self.bounds);
        buttonY = 0;
        buttonWidth  = CGRectGetHeight(self.bounds);
        buttonHeight = CGRectGetHeight(self.bounds);
        
        [_button setBackgroundImage:[UIImage imageNamed:@"arrow_down_brown"] forState:UIControlStateNormal];
        [_button setFrame:CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight)];
        [self.text setFrame:CGRectMake(textX, textY, textWidth, textHeight)];
        
        [_button setEnabled:NO];
        [self.text setEnabled:NO];
        
        [self addSubview:_button];
        
    }
    else{
        textX = 14;
        textY = 0;
        textWidth = CGRectGetWidth(self.bounds)-28;
        textHeight = CGRectGetHeight(self.bounds);
        
        [self.text setFrame:CGRectMake(textX, textY, textWidth, textHeight)];
    }


}

-(void)layoutSubviews
{
    float textX,textY,textWidth,textHeight,buttonX,buttonY,buttonWidth,buttonHeight;
    
    textX = 14;
    textY = 0;
    textWidth = CGRectGetWidth(self.bounds) - CGRectGetHeight(self.bounds)-28;
    textHeight = CGRectGetHeight(self.bounds);
    buttonX = CGRectGetWidth(self.bounds) - CGRectGetHeight(self.bounds);
    buttonY = 0;
    buttonWidth  = CGRectGetHeight(self.bounds);
    buttonHeight = CGRectGetHeight(self.bounds);
    
    [_button setFrame:CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight)];
    [self.text setFrame:CGRectMake(textX, textY, textWidth, textHeight)];
}


-(void)removeTableView
{
    if (_maskView) {
        [_maskView removeFromSuperview];
        _maskView = nil;
    }
    if (_table_content) {
        [_table_content removeFromSuperview];
        _table_content = nil;
    }
}


#pragma mark Touch Event
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!self.text.enabled && _content && !_table_content) {
        
        if (_delegate) {
            [_delegate showPullDownOption];
            
        }
        else{
            if (!_maskView) {
                _maskView = [[UIView alloc] initWithFrame:self.superview.bounds];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeTableView)];
                [_maskView addGestureRecognizer:tap];
                [_maskView setBackgroundColor:[UIColor clearColor]];
                
            }
            
            if (!_table_content) {
                _table_content = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.frame),
                                                                               CGRectGetMaxY(self.frame),
                                                                               CGRectGetWidth(self.frame),
                                                                               CGRectGetHeight(self.frame)*_content.count)];
                _table_content.delegate = self;
                _table_content.dataSource = self;
                [_table_content reloadData];
            }
            
            [self.superview addSubview:_maskView];
            [self.superview bringSubviewToFront:self];
            [self.superview addSubview:_table_content];
        }
    }
    
}


#pragma mark TableViewDelegate & Datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _content.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CGRectGetHeight(self.frame);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pullDownCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pullDownCell"];
        [cell.textLabel setText:[_content objectAtIndex:indexPath.row]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    self.text.text = cell.textLabel.text;
    [self removeTableView];
}

@end
