//
//  CircleImageCrop.h
//  DVBasketball
//
//  Created by LiminLin on 2015/4/17.
//  Copyright (c) 2015年 Brocas. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UIImageCropDelegate <NSObject>

- (void)cropImage: (UIImage*)cropImage forOriginalImage: (UIImage*)originalImage;

@end

@interface UIImageCrop : UIViewController

//下面倆哪個後面設置，即是哪個有效
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSURL *imageURL;

@property(nonatomic,weak) id < UIImageCropDelegate > delegate;
@property(nonatomic,assign) CGFloat ratioOfWidthAndHeight; //截取比例，寬高比

@property (nonatomic, assign) BOOL isCircleOpen;

- (void)showWithAnimation:(BOOL)animation;

@end