//
//  PhotoFromIPC.m
//  DVBasketballFinder
//
//  Created by Encore on 2014/11/11.
//  Copyright (c) 2014年 Encore. All rights reserved.
//

#import "PhotoFromIPC.h"
#import <AVFoundation/AVFoundation.h>

#define MAX_PHOTO_PIXEL 720.0

@interface PhotoFromIPC ()<UIImagePickerControllerDelegate, UIAlertViewDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) UIAlertView *alertChooseType;
@property (strong, nonatomic) UIAlertView *alertPermission;
@end

@implementation PhotoFromIPC

- (instancetype) init {
    self = [super init];
    if ( self ) {
        [self initializationWithImagePickerController];
        [self initializationWithAlertPermission];
        [self initializationWithAlertChooseType];
    }
    return self;
}

- (instancetype) initWithDelegate: (id<PhotoFromIPCDelegate>) ipcDelegate {
    return [self initWithDelegate: ipcDelegate viewController: ipcDelegate];
}

- (instancetype) initWithDelegate: (id<PhotoFromIPCDelegate>) ipcDelegate viewController: (id)viewController {
    self = [self init];
    if ( self ) {
        [self setIpcDelegate: ipcDelegate];
        [self setVC: viewController];
    }
    return self;
}

- (void) initializationWithImagePickerController {
    [self setImagePickerController: [[UIImagePickerController alloc] init]];
    [self.imagePickerController setDelegate: self];
}

- (void) initializationWithAlertPermission {
    [self setAlertPermission: [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"The camera is not turned permissions", nil)
                                                         message: NSLocalizedString(@"Please go to the settings page open permissions", nil)
                                                        delegate: self
                                               cancelButtonTitle: NSLocalizedString(@"Cancel", nil)
                                               otherButtonTitles: NSLocalizedString(@"Open", nil), nil]];
}

- (void) initializationWithAlertChooseType {
    [self setAlertChooseType: [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Pick a photo", nil)
                                                         message: nil
                                                        delegate: self
                                               cancelButtonTitle: NSLocalizedString(@"Cancel", nil)
                                               otherButtonTitles: NSLocalizedString(@"Choose from album", nil), NSLocalizedString(@"Take Photo", nil), nil]];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ( [alertView isEqual: self.alertChooseType] ) {
        switch ( buttonIndex ) {
            case 0:
                [self chooseCancel];
                break;
            case 1:
                [self choosePhoto];
                break;
            case 2:
                [self chooseTakePhoto];
                break;
        }
    }
    else if ( [alertView isEqual: self.alertPermission] && buttonIndex == 1 ) {
        [self openSettings];
    }
}

- (void) chooseCancel {
    if ( [self.ipcDelegate respondsToSelector: @selector(isCancel)] )
        [self.ipcDelegate isCancel];
}

- (void) choosePhoto {
    [self displayImagePickerController: UIImagePickerControllerSourceTypePhotoLibrary];
}

- (void) chooseTakePhoto {
    if( [AVCaptureDevice authorizationStatusForMediaType: AVMediaTypeVideo] == AVAuthorizationStatusAuthorized )
        [self displayImagePickerController: UIImagePickerControllerSourceTypeCamera];
    else
        [self requestAccess];
}

- (void) requestAccess {
    [AVCaptureDevice requestAccessForMediaType: AVMediaTypeVideo
                             completionHandler: ^(BOOL granted) {
                                 if( granted )
                                     [self.VC presentViewController: self.imagePickerController animated: YES completion: nil];
                                 else
                                     [self.alertPermission performSelectorOnMainThread: @selector(show) withObject: nil waitUntilDone: NO];
                             }];
}

- (void)openSettings {
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
}

- (void) displayImagePickerController: (UIImagePickerControllerSourceType) sourceType {
    [self.imagePickerController setSourceType: sourceType];
    [self.VC presentViewController: self.imagePickerController animated: YES completion: nil];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImageOrientation imageOrientation = image.imageOrientation;
    if( imageOrientation != UIImageOrientationUp ) {
        // 原始圖片可以根據照相時的角度來顯示，但UIImage無法判定，於是出現獲取的圖片會向左轉９０度的現象。
        // 以下為調整圖片角度的部分
        UIGraphicsBeginImageContext(image.size);
        [image drawInRect: CGRectMake(0, 0, image.size.width, image.size.height)];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        // 調整圖片角度完畢
    }
    
    // 1920/1080
    if ( image.size.width > image.size.height ) {
        // 橫的照片
        if ( image.size.width > MAX_PHOTO_PIXEL ) {
            CGFloat newHeight = image.size.height / ( image.size.width / MAX_PHOTO_PIXEL );
            image = [self reSizeImage: image toSize: CGSizeMake(MAX_PHOTO_PIXEL, newHeight)];
        }
    } else {
        // 直的照片
        if ( image.size.height > MAX_PHOTO_PIXEL ) {
            CGFloat newWidth = image.size.width / (image.size.height / MAX_PHOTO_PIXEL);
            image = [self reSizeImage: image toSize: CGSizeMake(newWidth, MAX_PHOTO_PIXEL)];
        }
    }
    
    [self.ipcDelegate getPhoto: image];
    [picker dismissViewControllerAnimated: YES completion: nil];
}

- (UIImage *)reSizeImage:(UIImage *)image toSize: (CGSize)reSize {
    UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
    [image drawInRect: CGRectMake(0, 0, reSize.width, reSize.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return reSizeImage;
}

#pragma mark - IBAction
- (IBAction) show: (id)sender {
    NSParameterAssert(self.ipcDelegate);
    NSParameterAssert(self.VC);
    
    [self.alertChooseType show];
}

@end
