//
//  CustomPageControl.m
//  CathayHealthcare
//
//  Created by Orange on 2015/7/17.
//  Copyright (c) 2015年 Patrick. All rights reserved.
//

#import "CustomPageControl.h"

@interface CustomPageControl()

@property (nonatomic,strong) UIImage *activeImage;
@property (nonatomic,strong) UIImage *inactiveImage;
@end

@implementation CustomPageControl
@synthesize activeImage,inactiveImage;


-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    activeImage = [UIImage imageNamed:@"active_page_image.png"];
    inactiveImage = [UIImage imageNamed:@"inactive_page_image.png"];
    
    return self;
}

-(void) updateDots
{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIImageView* dot = [self.subviews objectAtIndex:i];
        if (i == self.currentPage) dot.image = activeImage;
        else dot.image = inactiveImage;
    }
}

-(void) setCurrentPage:(NSInteger)page
{
    [super setCurrentPage:page];
    [self updateDots];
}

@end
