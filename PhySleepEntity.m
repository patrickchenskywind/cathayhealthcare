//
//  PhySleepEntity.m
//  
//
//  Created by LiminLin on 2015/10/8.
//
//

#import "PhySleepEntity.h"

@implementation PhySleepEntity

+ (RKObjectMapping *)responseMapping {
    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass: [self class]];
    
    [objectMapping addAttributeMappingsFromDictionary: [self propertyDictionary]];
    
    return objectMapping;
}

@end
